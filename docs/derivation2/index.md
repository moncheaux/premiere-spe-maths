# Rappels
!!! abstract "Tangente en un point d'une courbe de fonction"
    Pour une fonction pas trop exotique, un zoom autour d'un point montre que la courbe se comporte comme une droite : la tangente en ce point.

    <iframe scrolling="no" title="Zoom sur un point d'une courbe avec curseur" src="https://www.geogebra.org/material/iframe/id/ygydatuk/width/1920/height/918/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="1100px" height="460px" style="border:0px;"> </iframe>


    ??? tip "Une fonction pas trop exotique ?"
        Il existe des fonctions continues (dont la courbe n'a pas de "trou") mais qui ne sont nulle part dérivables.

        ![Fonction de Weierstrass](https://upload.wikimedia.org/wikipedia/commons/thumb/6/60/WeierstrassFunction.svg/720px-WeierstrassFunction.svg.png)

        Ce genre de fonctions, considérées au départ comme des "monstres" par les mathématiciens, sont en fait courantes dans certains domaines (finance, étude des gaz, etc.).

        [Si vous voulez en savoir plus](https://fr.wikipedia.org/wiki/Fonction_continue_nulle_part_d%C3%A9rivable).

!!! abstract "Nombre dérivé"

    Le coefficient directeur d'une tangente au point d'abscisse \(a\), quand il existe, est le nombre dérivé \(f'(a)\).  
    Nous disons alors que \(f\) est dérivable en \(a\).

    ![](images/derivee_tangente.png){width=50%; : .center}

    Il est défini comme une limite du taux de variation :

    <div style="color:red">

    \[
    \boxed{f'(a)=\lim\limits_{h\to 0} \dfrac{\strut f(a+h)-f(a)}{h}}
    \]

    </div>

    Il est alors possible de le calculer pour des fonctions pas trop compliquées...

!!! bug "Deux cas à connaître"
    Les fonctions racine carrée et valeur absolue ne sont pas dérivables en 0.

    ![](images/fig_racine_carree.png){width=50%; : .center}

    ![](images/fig_val_absolue.png){width=50%; : .center}


!!! abstract "Équation d'une tangente"

    L'équation réduite de la tangente au point d'abscisse \(a\) est :

    <div style="color:red">

    \[
    \boxed{y= f(a)+f'(a)(x-a)}
    \]

    </div>


!!! abstract "Calculs de dérivées des fonctions de base"

    Le tableau des dérivées des fonctions de base est :
 
    $$\begin{array}{|c|c|c|}
        \hline
        \mbox{Si }f(x)=... & \mbox{alors } f'(x)=...& \mbox{quand } x \in ...\\ \hline
        k\mbox{ (constante)}&0& ℝ \\ \hline
        x&1& ℝ \\ \hline
        x^2&2\,x& ℝ \\ \hline
        x^3&3\,x^2& ℝ \\ \hline
        x^n\ (n \in ℕ)&nx^{n-1}& ℝ \\ \hline
        \dfrac{\strut1}{\strut x} &-\dfrac{1}{x^2} & ℝ^* \\ \hline
    %    \dfrac{1}{x^n} \ (n>0)&-\dfrac{n}{x^{n+1}} & ℝ^* \\ \hline
        \sqrt{x}&\dfrac{\strut 1}{\strut 2\sqrt{x}} & ]0;+\infty[ \\ \hline
    %    \cos x &-\sin x& ℝ \\ \hline
    %    \sin x &\cos x& ℝ \\ \hline
    %%    \tan x &1+\tan^2 x& ℝ \\ \hline
    %    \ln x &1/x&\intoo0\pinfini\\ \hline
    %    \e^x &\e^x& ℝ \\ \hline
    \end{array}$$


!!! abstract "Opérations sur ces fonctions de base"

    Nous avons vu comment dériver des fonctions polynomiales telles que celles définies par les expressions \(5\,x^3-4\,x^2+3\,x-1\) ou \(2\,x^2-6\,x-5\) ou \(3\,\sqrt{x}-\dfrac{5}{x}\), etc. à l'aide des deux propriétés :

    <div style="color:red">

    \[
    \boxed{(k.u)'=k.u'}\qquad\boxed{(u+v)'=u'+v'}
    \]

    </div>


!!! tip "Et maintenant ?"
    Nous verrons dans ce second chapitre sur la dérivation :

    * des formules permettant de dériver des fonctions dont l'expression se présente entre autres sous forme d'un produit ou d'un quotient ;
    * une application essentielle des dérivées : l'étude des variations des fonctions et leurs utilisations.
