# II. Étude des variations d'une fonction

!!! abstract "Propriété (rappel) : variation d'une fonction affine"
    Soit \(f\) une fonction affine, d'expression \(f(x)=a\,x+b\).

    * si \(a>0\) alors \(f\) est strictement croissante sur ℝ ;
    * si \(a<0\) alors \(f\) est strictement décroissante sur ℝ ;
    * si \(a=0\) alors \(f\) est constante sur ℝ.

    <iframe scrolling="no" title="La fonction affine et ses paramètres" src="https://www.geogebra.org/material/iframe/id/VJsUYxmx/width/1024/height/580/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="1024px" height="580px" style="border:0px;"> </iframe>

    Le signe du coefficient directeur donne la variation d'une fonction affine.

!!! bug "Attention"
    Seules les droites ont un coefficient directeur donc la propriété ci-dessus ne s'applique qu'aux fonctions affines.  


!!! tip "Remarques"
    * Une fonction dérivable et croissante aura une courbe dont les tangentes ont un coefficient directeur positif, et réciproquement.
    * Comme le coefficient directeur d'une tangente est le nombre dérivé, le signe de celui-ci indique donc les variations de fonctions (même non affines).

!!! abstract "Théorème II.1"
    Soit $f$ une fonction dérivable sur un intervalle $I$.

    * Si <span style="color:red">$f'=0$</span> sur $I$ alors $f$ est <span style="color:red">constante</span> sur $I$.
    * Si <span style="color:red">$f'>0$</span> sur $I$ sauf en certains points où $f'$ s'annule alors $f$ est <span style="color:red">strictement croissante</span> sur $I$
    * Si <span style="color:red">$f'<0$</span> sur $I$ sauf en certains points où $f'$ s'annule alors $f$ est <span style="color:red">strictement décroissante</span> sur $I$

!!! tip "Remarque"
    Ce théorème, parfois appelé théorème de Lagrange, est l'un des plus utilisés en mathématiques !


!!! question "Exercice"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    Observer les courbes des exercices 7 à 10 page 113 et faire les tableaux de signes de $f$ et de $f'$.  
    62 et 63 page 125



!!! note "Méthode pour étudier les variations d'une fonction $f$ dérivable"
    1. calculer la dérivée $f'$ de la fonction $f$ ;
    2. étudier le signe de $f'$ ;
    3. faire le tableau du signe de $f'$, celui-ci donnant le sens de variation de $f$.


!!! tip "Remarque"
    Pour l'étude du signe d'une expression (ici \(f'\)) il n'y a que trois cas :

    * soit elle est toujours positive (exemple : \(3\,x^2+1\))
    * soit elle est toujours négative (exemple : \(-x^2-4\))
    * soit le signe dépend des valeurs de \(x\) (exemple : \(x^2-1\) qui est négative entre \(-1\) et 1 et positive ailleurs).



!!! example "Exemple II.1"
    Soit $f$ la fonction définie sur ℝ par $f(x)=-3\,x+7$. Alors :  
    1. $f'(x)=-3$ ;  
    2. donc $f'(x)<0$ pour tout $x$ ;  
    3. $f$ est donc strictement décroissante sur ℝ.

    Remarque : la fonction étant affine il suffisait ici de regarder le signe du coefficient directeur...


!!! example "Exemple II.2"
    Soit $f$ la fonction définie sur [$-3$ ; 7] par $f(x)=2\,x^3+5\,x-6$. Alors :  
    1. $f'(x)=6\,x^2+5$ ;  
    2. le carré d'un réel est positif, ainsi que 6 et 5 donc $f'(x)\geq 5>0$ pour tout $x$ ;  
    3. $f$ est donc strictement croissante sur [$-3$ ; 7] :  
    ![](images/tab_var7.png){width=40%; : .center}

!!! question "Exercice"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    3, 6, 13 page 122  
    1, 2 page 116



!!! example "Exemple II.3"
    Étudier les variations de la fonction $f$ définie sur [$-5$ ; 4] par
    \(f(x)=\dfrac{\strut x^3}{\strut 27}+\dfrac{x^2}{9}-\dfrac{x}{3}\).

    1. $f'(x)=\dfrac{x^2}{9}+\dfrac{2\,x}{9}-\dfrac{1}{3}=\dfrac{x^{2}+2\,x-3}{9}$.
    2. 9 est positif donc $f'(x)$ a le même signe que $x^{2}+2\,x-3$.  
    Cette fonction du second degré a le signe de $a=1$ donc est positive sauf entre les racines (...) $x_1=1$ et $x_2=-3$.  
    3. D'où le tableau de variation suivant :  
    ![](images/tab_var8.png){width=60%; : .center}

    


!!! example "Exemple II.4"
    Étudier les variations de la fonction $f$ définie sur $ℝ\backslash\{-3\}$ par
    \(f(x)=\dfrac{x^2+3x+4}{x+3}\).

    1. $f'(x)=\dfrac{(2x+3)(x+3)-1.(x^{2}+3x+4)}{(x+3)^2}=\dfrac{x^{2}+6x+5}{(x+3)^2}$.
    2. $(x+3)^2$ est toujours positif donc $f'(x)$ a le même signe que $x^{2}+6\,x+5$.  
    Cette fonction du second degré a le signe de $a=1$ donc est positive sauf entre les racines (...) $x_1=-1$ et $x_2=-5$.  
    3. En remarquant que $-3$ est une valeur interdite, ceci donne le tableau suivant :

    ![](images/tab_var1.png){width=50%; : .center}

    Remarques : les « valeurs » manquantes de \(f\), à certaines extrémités des flèches sont appelées _limites_ de \(f\) en \(-\infty\) et en \(-\infty\) (hors-programme de première).

!!! tip "Astuces"
    * Pensez à tracer la courbe de la fonction pour vérifier que votre tableau est cohérent.  
    Pour l'exemple II.3 :  
    ![](images/courbe_fonc2.png){width=50%; : .center}  
    Pour l'exemple II.4 :  
    ![](images/courbe_fonc.png){width=50%; : .center}  
    Ici la croissance avant $-5$ et après $-1$ n'est pas évidente mais il est possible de déplacer un point sur la courbe de la gauche vers la droite et de regarder si $y$ augmente ou diminue.
    * Pensez aussi à vérifier la cohérence des valeurs et du sens de variation (une fonction ne peut pas croître de la valeur 5 à la valeur 2 par exemple !).

!!! question "Exercice"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    13 page 122, 47 et 48 page 125  
    Exercice 1 [de la fiche d'exercices](../exo_derivation2.pdf)





!!! danger "Attention"
    * Ne confondez pas tableau de signe de \(f\) et tableau de variation de \(f\).
    * Ne confondez pas le signe de \(f\) et celui de \(f'\)...

!!! example "Exemple II.4 (suite)"
    Dans l'exemple précédent, le tableau de variation de la fonction est :  
    ![](images/tab_var1.png){width=60%; : .center}  
    donc son tableau de signe est :
    ![](images/tab_signe1.png){width=40%; : .center}

!!! example "Exemple II.5"
    Étude des variations d'une fonction du second degré, par exemple 
    \(f(x)=-3\,x^2+18\,x+5\) sur ℝ.

    1. $f'(x)=-6\,x+18$.
    2. Le signe de $f'(x)$ dépend ici de $x$ :  
    \(f'(x)>0
    \Longleftrightarrow
    -6\,x+18>0
    \Longleftrightarrow
    -6\,x>-18
    \Longleftrightarrow
    x<3
    .\)
    3. Voici donc le tableau de signe de $f'$ et de variation de $f$ :  
    ![](images/tab_var2.png){width=50%; : .center}

!!! question "Exercice"
    [Transmath](https://biblio.manuel-numerique.com/) : 4 page 119



!!! abstract "Propriété : variations d'une fonction du second degré"
    Tableaux de variations de \(f\) définie par \(f(x)=a\,x^2+b\,x+c\) sur ℝ :

    Si \(a>0\) :

    ![](images/tab_var3.png){width=50%; : .center}

    Si \(a<0\) :

    ![](images/tab_var4.png){width=50%; : .center}

    !!! abstract "Démonstration"

        1. $f'(x)=a\,(2\,x)+b=2\,a\,x+b$.
        2. Le signe de $f'(x)$ dépend ici de $x$ :  
        \(f'(x)>0  \Longleftrightarrow 2\,a\,x+b>0
        \Longleftrightarrow
        2\,a\,x>-b
        \Longleftrightarrow
        x>-\dfrac{\strut b}{\strut 2\,a} \text{ si }a>0
        \text{ ou }
        x<-\dfrac{\strut b}{\strut 2\,a} \text{ si }a<0\).
        3. Donc si \(a>0\) alors \(f\) est strictement croissante sur \(\left] -\dfrac{\strut b}{\strut 2\,a};+\infty\right[\) et si \(a<0\) alors \(f\) l'est sur \(\left] -\infty;-\dfrac{\strut b}{\strut 2\,a}\right[\).


!!! tip "Pour retenir cela"
    Quand \(a\) est positif, la parabole est dirigée vers le haut (pensez à un sourire) et quand \(a\) est négatif, la parabole est dirigée vers le bas (pensez à une grimace).



!!! example "Exemple II.5 (autre façon)"
    Étudiez les variations de la fonction définie par 
    \(f(x)=-3\,x^2+18\,x+5\) sur ℝ.

    Réponse :  
    $-\dfrac{b}{2\,a}=-\dfrac{18}{-6}=3$ est l'abscisse du sommet de la parabole.

    L'ordonnée du sommet est \(f\left(-\dfrac{b}{2\,a}\right)=f(3)=-3\times 3^2+18\times 3+5 = 32\).

    Enfin, comme \(a=-3\) est négatif, la fonction est d'abord croissante puis décroissante :
    ![](images/tab_var2b.png){width=40%; : .center}


!!! question "Exercice"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    21 à 24 page 123  
    70, 72 page 127  
    11 page 122
