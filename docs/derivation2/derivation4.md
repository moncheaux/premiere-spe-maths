# III. Extremum d'une fonction
## Recherche d'extrema

!!! abstract "Définitions"
    Soit $f$ une fonction définie sur $I$ et $u$ un nombre appartenant à $I$.

    * Le nombre $f(u)$ est le <span style="color:red">maximum</span> de $f$ sur $I$ si, pour tout $x$ de $I$, on a $f(x)\le f(u)$.  
    ![](images/fig_max.png){width=30%; : .center}
    * Le nombre $f(u)$ est le <span style="color:red">minimum</span> de $f$ sur $I$ si, pour tout $x$ de $I$, on a $f(x)\ge f(u)$.  
    ![](images/fig_min.png){width=30%; : .center}
    * un extremum est un maximum ou un minimum ;
    * s'il n'est un extremum que sur un sous-intervalle de l'ensemble de définition, nous disons qu'il est local ; sinon il est global.
    
!!! exemple "Exemple III.1"
    
    Sur le graphique ci-dessous (courbe sur [$-5$ ; 4] de $f(x)=\dfrac{x^3}{27}+\dfrac{x^2}{9}-\dfrac{x}{3}$),
    le nombre \(f(-3)=1\) est un maximum local (c'est le maximum de \(f\) sur [$-5$ ; 0] par exemple) mais il n'est pas global (ici c'est \(f(4)\) le maximum).  
    ![](images/courbe_fonc2.png){width=70%; : .center}



!!! danger "Attention"
    Contrairement à ce que l'exemple précédent suggère, le maximum de $f$ n'est pas forcément atteint au maximum de $x$ (quand il y en a un) !


!!! abstract "Propriété III.1"
    Si $f$ est dérivable et admet un extremum (local ou global) en $u$ alors \(f'(u)=0\).

!!! bug "Et la réciproque ?"
    La réciproque de cette propriété est fausse : par exemple si \(f(x)=x^3\) alors \(f'(0)=3 \times0^2=0\) mais \(f\) n'a aucun extremum en 0.

    ![](images/fig_cube.png){width=50%; : .center}

!!! abstract "Propriété III.2"
    Soit $f$ une fonction dérivable sur un intervalle $I$ et $u\in I$.

    $f$ a un extremum (maximum ou minimum) local ou global en $u$ si et seulement si $f'$ s'annule en $u$ en changeant de signe.


!!! exemple "Exemple III.2"
    === "Questions"
        Soit \(f\) la fonction définie sur ℝ par \(f(x)=x^4+4\,x^3-16\,x+2\).

        Prouver que \(f'(x)=4(x+2)^2(x-1)\). En déduire l'existence ou non d'extrema pour \(f\).

    === "Réponses"

        1. D'une part, \(f'(x)=4\,x^3+12\,x^2-16\).  
        D'autre part,  
        \(\begin{align}
        4(x+2)^2(x-1) 
        &=
        4(x^2+4\,x+4)(x-1)
        \\
        &=
        4(x^3+4\,x^2+4\,x-x^2-4\,x-4)
        \\
        &=
        4\,x^3+12\,x^2-16=f'(x).
        \end{align}\)
        2. Comme $4(x+2)^2$ reste positif (mais s'annule en $-2$), $f'(x)$ a le signe de $x-1$ et nous savons que  $x-1 >0 \iff x>1$.
        3. D'où le tableau de variations de $f$ :  
        ![](images/tab_var5.png)
        4. \(f'\) s'annule en 1 en changeant de signe donc \(f(1)=-9\) est un extremum local (ici un minimum local).  
        Par contre, \(f'\) s'annule en \(-2\) sans changer de signe donc \(f(-2)=18\) n'est pas un extremum.

!!! tip "Remarque"
    En présence d'un extremum, puisque la dérivée s'annule, il y a une tangente horizontale.

!!! question "Exercices"
    Exercices 2 et 3 [de la fiche d'exercices](../exo_derivation2.pdf)




## Utilisation d'extrema pour prouver des inégalités

!!! exemple "Exemple III.3"
    === "Question"

        Prouver que, pour tout $x$ réel positif, $(1+x)^3\geqslant 1+3\,x$.

        <!--Prouver que, pour tout $x$ réel positif, $\sqrt{1+2\,x}\le 1+x$.-->

    === "Réponse"

        Soit \(f\) la fonction définie sur \([0;+\infty[\) par \(f(x)=(1+x)^3-(1+3\,x)\).
        Nous devons prouver que \(f\) reste positive, pour cela, nous allons ici prouver que \(f\) a un minimum positif.

        1. Pour tout \(x\) de \([0;+\infty[\), 
        \(f'(x)=1 \times 3(1+x)^2 - 3=3((1+x)^2-1)=3(2\,x+x^2)=3\,x(x+2)\).
        2. Pour tout \(x\) de \([0;+\infty[\), nous avons \(3>0\), \(x\geqslant0\) et \((x+2)\geqslant2>0\) donc \(f'(x)\geqslant0\).  
        3. La fonction \(f\) est donc croissante sur \([0;+\infty[\) donc elle a pour minimum \(f(0)=(1+0)^3-1-3\times0 =1-1 = 0\).  
        Comme elle a un minimum positif, \(f\) reste positive.

        Remarque : il y avait ici une démonstration alternative, ne nécessitant pas l'utilisation de la dérivée (en développant \((1+x)^3\)), mais ce n'est pas toujours le cas...


??? exemple "Autre exemple (plus difficile)"
    === "Question"

        Prouver que, pour tout $x$ réel positif, $\sqrt{1+2\,x}\le 1+x$.

    === "Réponse"

        Soit \(f\) la fonction définie sur \(]0;+\infty[\) par \(f(x)=1+x-\sqrt{1+2\,x}\).
        Nous devons prouver que \(f\) reste positive, pour cela, nous allons ici prouver que \(f\) a un minimum positif.

        1. Pour tout \(x\) de \(]0;+\infty[\), 
        \(f'(x)=1-\dfrac{2}{2\sqrt{1+2\,x}}=\dfrac{\sqrt{1+2\,x}-1}{\sqrt{1+2\,x}}\).
        2. Le dénominateur est toujours positif. Pour le numérateur remarquons que :  
        \(\begin{align}        
        x\in]0;+\infty[
        &\Longrightarrow&
        x\ge 0
        \Longrightarrow
        2\,x\ge 0
        \Longrightarrow
        1+2\,x\ge 1
        \\
        &\Longrightarrow^{(*)}&
        \sqrt{1+2\,x} \ge \sqrt{1}
        \Longrightarrow
        \sqrt{1+2\,x}-1 \ge 0.
        \end{align}\)  
        $^{(*)}$ car la fonction racine carrée est croissante.  
        Le numérateur est donc aussi positif donc \(f'(x)\ge0\) sur \(]0;+\infty[\).
        3. La fonction \(f\) est donc croissante sur \(]0;+\infty[\) donc elle a pour minimum \(f(0)=1+0-\sqrt{1+2 \times 0}=0\).  
        Comme elle a un minimum positif, \(f\) reste positive.

        Remarque : il y avait ici une démonstration alternative, ne nécessitant pas l'utilisation de la dérivée (en développant \((1+x)^2\)), mais ce n'est pas toujours le cas...


!!! question "Exercices"
    Exercice 4 [de la fiche d'exercices](../exo_derivation2.pdf)  
    [Transmath](https://biblio.manuel-numerique.com/) : 56, 57 page 125 (limites admises : 1 en \(\pm\infty\) pour le 56 et 2 en \(\pm\infty\) pour le 57)
    


