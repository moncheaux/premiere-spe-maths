# IV. Positions relatives de deux courbes de fonctions

!!! tip "Principe"
    * Soient \(f\) et \(g\) deux fonctions. La courbe de \(f\) est au dessus de celle de \(g\) quand \(f(x)>g(x)\), autrement dit quand \(f(x)-g(x)>0\) et elle est en dessous de celle de \(g\) quand \(f(x)-g(x)<0\).
    * Si j'appelle \(h\) la fonction définie (sur un certain ensemble) par \(h(x) = f(x)-g(x)\), l'étude des variations de \(h\) peut permettre de savoir à quel moment \(h(x)\) est positif ou négatif.

!!! example "Exemple"
    === "Question"
        Étudier les positions relatives de la courbe de la fonction \(f\) définie sur ℝ par \(f(x)=x^3\) et de sa tangente en son point d'abscisse 2.

    === "Réponse"
        L'équation de cette tangente est \(y=f'(2)(x-2)+f(2)=12\,x-16\).

        La courbe de \(f\) est au dessus de sa tangente quand \(f(x) > 12\,x-16\) donc quand \(f(x) - (12\,x-16) > 0\).

        Soit donc \(h\) la fonction définie sur ℝ par \(h(x)= x^3-(12\,x-16) = x^3-12\,x+16\).  
        Alors \(h'(x)=3\,x^2-12 = 3(x^2-4) = 3(x-2)(x+2)\).  
        D'où le tableau de variations de $h$ :  
        ![](images/tab_var6.png){width=50%; : .center}

        Il semblerait (calculatrice graphique...) que \(h(-4)=0\) et, en effet :
        \(h(-4)=(-4)^3-12(-4)+16 = -64 +48+16=0\).

        Donc, d'après le tableau de variations de \(h\) :

        * la tangente coupe la courbe quand $x=2$ (forcément...) et $x=-4$ ;
        * sur $]-\infty ; -4[$ : \(h(x)<0\) donc la courbe est en dessous de sa tangente ;
        * sur $]-4 ; 2 [ \cup ] 2 ; +\infty[$ : \(h(x)>0\) donc la courbe est au dessus de sa tangente.

        ![](images/fig_cube_et_tangente.png){width=50%; : .center}

!!! question "Exercices"
    Exercices 5 et 6 [de la fiche d'exercices](../exo_derivation2.pdf)  
    [Transmath](https://biblio.manuel-numerique.com/) :  
    75 page 128  
    96 page 134

!!! question "Exercices sur les problèmes d'optimisation"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    81, 82 page 130

!!! question "Exercices sur la logique"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    85 et 87 page 131
