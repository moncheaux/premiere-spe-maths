# Quelques exercices pour vous entraîner
Les valeurs utilisées dans les exercices du site Wims sont générées aléatoirement, vous pouvez donc refaire plusieurs fois chacun d'entre eux.

## Dérivée de \(uv\)
* [texte à trou avec des propositions](https://learningapps.org/24537705) (une erreur dans le texte : "comme \((uv)'=\)") ;
* [un exercice sur Wims](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/derivation1ere.fr&cmd=new&exo=deriverProduit) ;

## Dérivée de \(\dfrac{1}{v}\)
* [choisir la bonne dérivée parmi trois choix](https://learningapps.org/13428692) ;

## Dérivée de \(\dfrac{u}{v}\)
* [choisir la bonne dérivée](https://learningapps.org/13428692) ;
* [un exercice sur Wims](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H6/analysis/OEFderivee.fr&cmd=new&exo=div1) ;
* [un autre exercice sur Wims](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/derivation1ere.fr&cmd=new&exo=deriverQuotient) ;

## Dérivée de \(g(a\,x+b)\)
* [un exercice sur Wims](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/derivation1ere.fr&cmd=new&exo=deriverComp) ;

## Calculs de dérivées
* [choisir la bonne formule et l'appliquer](https://learningapps.org/18283194) ;
* [associer des fonctions et leur dérivées](https://learningapps.org/6667785) ;

## Signe d'une expression
* [associer un tableau de signe avec des courbes (Seconde)](https://learningapps.org/7680778) ;
* [associer un tableau de signe avec des expressions](https://learningapps.org/10111210) ;
* [collections d'exercices Wims sur les tableaux de signes](https://wims.univ-cotedazur.fr/wims/wims.cgi?wims_window=new&+session=0Z9278B614_internal&+lang=fr&+module=H5%2Falgebra%2FtableauxSigne.fr) ;

## Variations d'une fonction
### Par lecture
* [dire si des affirmations sont vraies ou fausses](https://learningapps.org/18001535) ;
* [lire le signe de f' avec un tableau de variation](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/OEFevalwimsder1.fr&cmd=new&exo=signeder1) ;
* [compléter un tableau de variation (signe de f' connu)](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/OEFevalwimsder1.fr&cmd=new&exo=signeder2) ;
* [compléter un tableau de variation (avec des consignes données)](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/OEFevalwimsder1.fr&cmd=new&exo=signeder3) ;
* [associer une courbe de fonction avec celle de sa dérivée](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/OEFevalwimsder1.fr&cmd=new&exo=signeder4) ;
* [dire si des affirmations sont vraies ou fausses à partir d'une courbe](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/OEFevalwimsder1.fr&cmd=new&exo=signeder5) ;


### Par le calcul
* [tableau de variation d'une fonction du second degré](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H6/analysis/oeftablvar.fr&cmd=new&exo=deg2) ;
* [tableau de variation d'une fonction du second degré avec la dérivée](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H6/analysis/oeftablvar.fr&cmd=new&exo=deg2) ;
* [trouver la fonction du second degré correspondant à un tableau](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/algebra/OEFevalwimssecdeg.fr&cmd=new&exo=vartrinome5) ;
* [étude guidée des variations d'une fonction polynomiale de degré 3](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/derivation1ere.fr&cmd=new&exo=varPolydeg3) ;


## Problèmes d'optimisation
* [boîte ayant un prix minimal](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/OEFevalwimsder1.fr&cmd=new&exo=extrema2) ;
* [une recette maximale](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/OEFevalwimsder1.fr&cmd=new&exo=extrema5) ;





