# Des vidéos d'explications

## Dérivée de \(uv\)
* [deux calculs + ensemble de dérivabilité + une alternative possible (développer)](https://www.youtube.com/watch?v=np3D75SI0xA) ;
* [un autre exemple](https://www.youtube.com/watch?v=wTk1uwEKDZA) ;

## Dérivée de \(\dfrac{u}{v}\)
* [deux calculs de dérivées](https://www.youtube.com/watch?v=F9SzRPW3tOQ) ;
* [trois quotients différents : quelle formule utiliser ?](https://www.youtube.com/watch?v=RYLiq_FAlU8) ;

## Dérivée de \(g(a\,x+b)\)
* [explication sur trois exemples](https://www.youtube.com/watch?v=q8pnahU3l-U) ;
* [explication sur deux exemples (mais le collègue inverse les lettres \(f\) et \(g\)...)](https://www.youtube.com/watch?v=Le_XtQGaMM4) ;
* [un autre exemple plus complet, avec recherche de l'ensemble de dérivabilité](https://www.youtube.com/watch?v=uxGpr5LbfQU) ;
* [19 exemples pour gagner en vitesse](https://www.youtube.com/watch?v=Z9s0WFKVgq4) ;


## Calculs de dérivées
* [choisir la bonne formule sur quatre exemples](https://www.youtube.com/watch?v=p2OZTwCsNH4) ;
* [idem](https://www.youtube.com/watch?v=-oiK8bf-Edo) ;
* [idem sur deux exemples + étude de l'ensemble de dérivabilité](https://www.youtube.com/watch?v=WIc0zPpp0Qw) ;
* [plusieurs exemples, en partant des dérivées simples](https://www.youtube.com/watch?v=6YZAn5gNsLI) ;


## Signe d'une expression
* [signe d'une fonction affine et d'un produit de fonctions affines (Seconde)](https://www.youtube.com/watch?v=GxCrSt1twE0) ;
* [signe d'un quotient de fonctions affines (Seconde)](https://www.youtube.com/watch?v=9K9cvpyl-sk) ;
* [rappels de seconde sur les tableaux de signe](https://www.youtube.com/watch?v=G-OuUleUE8w) ;
* signe d'une fonction du second degré : [rappel de cours](https://www.youtube.com/watch?v=DbXO4O7N0TQ) et [exemples](https://www.youtube.com/watch?v=ktLhRIBDjxQ) ;
* [signe d'un quotient de fonctions du second degré](https://www.youtube.com/watch?v=VqRc8ZHTyI0) ;


## Variations d'une fonction
### Par lecture graphique
* [lecture du signe de la dérivée](https://www.youtube.com/watch?v=OjbCLcl4tYg) ;
* [trouver les variations d'une fonction à partir de la courbe de sa dérivée](https://www.youtube.com/watch?v=bCL2T9LAcmY) ;
* [rappels sur les dérivées, étude des variations, équation d'une tangente](https://www.youtube.com/watch?v=8HkLdZZoE78) ;


### Par le calcul
* [étude de deux fonctions polynomiales](https://youtu.be/LwlPq4QyZoo) ;
* [trouver les extrema d'une fonction](https://www.youtube.com/watch?v=33HStJJi8Bs) ;
* [résolution d'un problème d'optimisation](https://www.youtube.com/watch?v=5SpQpeZTFOM) ;
* variation d'une fonction du second degré : [avec la propriété du cours](https://www.youtube.com/watch?v=6CQUqCH3SZ4&list=PLoedD8O3xWgYS3ZzStct3ER0x1v0I1YlX&index=1) et [avec la forme canonique](https://www.youtube.com/watch?v=h1oO_uXzbHY&list=PLoedD8O3xWgYS3ZzStct3ER0x1v0I1YlX&index=12) ;


## Position relative de deux courbes
* [avec deux polynômes (en dérivant la différence)](https://www.youtube.com/watch?v=B_BVcvT1IXg) ;
* [avec deux polynômes (sans dériver la différence)](https://www.youtube.com/watch?v=djfcm7as29c) ;
* [avec deux polynômes et une factorisation à la fin](https://www.youtube.com/watch?v=NZhPC6_FBDM) ;
* [fonction second degré et fonction affine (sans dériver la différence)](https://www.youtube.com/watch?v=NdIGOCGGahc) ;
* [courbe et une tangente (avec un discriminant nul)](https://www.youtube.com/watch?v=9KlHpyIWeeM) ;
* [courbe et une tangente (sans dériver la différence)](https://www.youtube.com/watch?v=uZwemoNVtVU)


## Montrer une inégalité en utilisant la dérivée
* [exemple 1](https://www.youtube.com/watch?v=SrIT9_W-npw) ;
* [exemple 2](https://www.youtube.com/watch?v=ON14GJOYogw) ;
* [exemple 3](https://www.youtube.com/watch?v=DPRe9odUdjA)




