# I. Calculs de dérivées
## I.1 Dérivée d'un produit

!!! abstract "Définition I.1"
    Soient $u$ une fonction définie sur $J$ et $v$ une fonction définie sur $K$.  
    Alors $u \times v$ est définie sur $I=J\cap K$ par $(u \times v)(x)=u(x) \times v(x)$.

!!! example "Exemple I.1"
    Si $u$ est définie sur $[0;+\infty[$ par $u(x)=\sqrt{x}$  
    et $v$ est définie sur $]-\infty;2]$ par $v(x)=\sqrt{2-x}$  
    alors $u \times v$ est définie sur $[0;2]$ par $(u \times v)(x)=\sqrt{x}\sqrt{2-x}$.



!!!tip "Remarques"
    * nous écrirons $uv$ au lieu de $u \times v$ ;
    * soit \(f\) la fonction définie sur ℝ par \(f(x)=2\,x\), sa dérivée est alors \(f^\prime(x)=2 \times 1 = 2\).  
    Remarquons maintenant que  
    \(f=u\,v\) où \(u(x)=2\) et \(v(x)=x\), nous avons alors \((u\,v)^\prime (x)=f^\prime(x)=2\)
    tandis que \((u^\prime\,v^\prime) (x)= 0 \times1 =0\).  
    Par conséquent : <span style="color:red">\((uv)^\prime \neq u^\prime v^\prime\)</span>.  
    Cet exemple illustre le fait que la dérivée d'un produit **n'est pas le produit des dérivées**.


!!! abstract "Théorème I.1" 

    Soient $u$ et $v$ deux fonctions dérivables sur un intervalle $I$.
    Alors $u \times v$ est dérivable sur $I$ et :

    <div style="color:red">

    \[
    \boxed{(u \times v)'=u' \times v + u \times v'}
    \]

    </div>

    !!! abstract "Démonstration remarquable"
        Calculons le taux de variation de la fonction \(f=u\times v\) entre \(a\) et \(a+h\) :

        \[
        \begin{align}
        \dfrac{f(a+h)-f(a)}{\strut h}
        &=
        \dfrac{(u v)(a+h)-(u v)(a)}{\strut h}
        \\
        &=
        \dfrac{u(a+h) v(a+h) -u(a)  v(a)}{h}
        \\
        &=
        \dfrac{u(a+h) v(a+h) -u(a)v(a+h) + u(a)v(a+h) -u(a) v(a)}{h}
        \\
        &= \dfrac{u(a+h)-u(a)}{h} \times v(a+h) + \dfrac{v(a+h) - v(a)}{h} \times u(a).
        \end{align}
        \]

        Comme \(u\) et \(v\) sont dérivables en \(a\),
        \(
        \lim\limits_{h\to0}
        \dfrac{\strut u(a+h)-u(a)}{h} = u'(a)
        \)
        et
        \(
        \lim\limits_{h\to0}
        \dfrac{\strut v(a+h)-v(a)}{\strut h} = v'(a)
        \).

        Par ailleurs, 
        \(
        \lim\limits_{h\to0}
        v(a+h) = v(a)
        \)
        (c'est une conséquence de la dérivabilité de $v$ en $a$).

        Donc 
        \(
        \lim\limits_{h\to0}
        \dfrac{f(a+h)-f(a)}{\strut h} = u'(a)v(a)+v'(a)u(a)
        \), ce qui établit la dérivabilité de $uv$ en $a$ ainsi que la formule cherchée.



!!! exemple "Exemple I.2"
    === "Question"
        Justifiez la dérivabilité des fonctions suivantes sur un certain intervalle et calculez leur fonction dérivée  :  
        \(f(x)=(3\,x-4)\,\sqrt{x}\)  
        \(g(x)= \dfrac{2\,x^5-x^2+3}{x}\).


    === "Réponses pour $f$"
        \(f\) est de la forme \(uv\) où \(u(x)=3\,x-4\) et \(v(x)=\sqrt{x}\) ; 
        \(u\) est dérivable sur ℝ et \(v\) est dérivable sur $]0;+\infty[$
        donc 
        \(f\)  est dérivable sur $]0;+\infty[$.

        Par ailleurs, 
        \(u'(x)=3\) et \(v'(x)=\dfrac{1}{2\sqrt{x}}\)
        donc  
        \(f'(x)
        =
        (u'v+uv')(x) 
        =
        3 \times \sqrt{x} + (3\,x-4) \times \dfrac{1}{2\sqrt{x}}
        =
        \dfrac{6\,x+3\,x-4}{2\sqrt{x}}
        =
        \dfrac{9\,x-4}{2\sqrt{x}}\).

    === "Réponses pour $g$"

        En remarquant que 
        \(\dfrac{2\,x^5-x^2+3}{x} = (2\,x^5-x^2+3) \times \dfrac{1}{\strut x}\),  
        \(g\) est de la forme \(uv\) où \(u(x)=2\,x^5-x^2+3\) et \(v(x)=\dfrac{\strut 1}{\strut x}\) ;

        \(u\) est dérivable sur ℝ et \(v\) est dérivable sur ℝ$^*$
        donc 
        \(g\)  est dérivable sur ℝ$^*$.

        Par ailleurs, 
        \(u'(x)=10\,x^4-2\,x\) et \(v'(x)=-\dfrac{1}{\strut x^2}\)
        donc  
        \(\begin{align}
        g'(x)
        &=
        (u'v+uv')(x) 
        =
        (10\,x^4-2\,x) \times \dfrac{1}{\strut x} + (2\,x^5-x^2+3) \times \left(-\dfrac{1}{x^2}\right)
        \\
        &=
        \dfrac{\strut 10\,x^5-2\,x^2-(2\,x^5-x^2+3)}{\strut x^2}
        =
        \dfrac{8\,x^5-x^2-3}{x^2}.
        \end{align}\)
        
        Remarque : \(g\) pouvait aussi être ici transformée en somme :  
        \(g(x)= \dfrac{\strut 2\,x^5-x^2+3}{x\strut }
        =
        \dfrac{2\,x^5}{x} - \dfrac{x^2}{x} + \dfrac{3}{x}
        =
        2\,x^4 - x + 3 \times\dfrac{1}{x}
        \)
        donc 
        \(g^\prime(x)
        =
        8\,x^3 - 1 - \dfrac{3}{x^2}
        \).



!!! danger "Une erreur fréquente"
    La dérivée de \(uv\) n'est pas \(u'v'\).
    
    Cette erreur est sûrement dans le top 5 des erreurs en mathématiques (avec la mauvaise gestion des négatifs avec les puissances,  l'oubli du \(2ab\) dans le développement de \((a+b)^2\), etc.).



!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    50 page 101  
    112, 113 page 105



## I.2 Dérivée d'un quotient

!!! abstract "Définition I.2"
    Soient $u$ une fonction définie sur $J$ et $v$ une fonction définie sur $K$.  
    Soit $L$ l'ensemble des $x$ de $K$ tels que $v(x)\neq0$.
    Alors $\dfrac{u}{v}$ est la fonction définie sur $J\cap L$ par 
    $\left(	\dfrac{u}{v} \right) (x) = \dfrac{u(x)}{v(x)}$.


!!! example "Exemple I.3"
    Si $u$ est définie sur $]-\infty;2]$
    par $u(x)=\sqrt{2-x}$ et 
    $v$ est définie sur ℝ
    par $v(x)=x^2-1$  
    alors la fonction 
    $\dfrac{u}{v}$ est définie sur $]-\infty;-1[\cup]-1;1[\cup]1;2]$
    par $\left(	\dfrac{u}{v} \right) (x) = \dfrac{\sqrt{2-x}}{x^2-1}$.


!!!tip "Remarque"
    La dérivée d'un quotient **n'est pas le quotient des dérivées**.

    Par exemple soit \(f\) la fonction définie sur ℝ$^*$ par \(f(x)=\dfrac{1}{x}\), sa dérivée est alors \(f^\prime(x)=-\dfrac{1}{x^2}\), pas 
    \(\dfrac{0}{1}=0\) !

    Par conséquent : <span style="color:red">\(\left(\dfrac{u}{v}\right)^\prime \neq \dfrac{u^\prime}{v^\prime}\)</span>.

!!! abstract "Théorème I.2" 

    Soient $u$ et $v$ deux fonctions dérivables sur un intervalle $I$ (sur lequel \(v\) ne s'annule pas).

    Alors $\dfrac{\strut u}{\strut v}$ et $\dfrac{1}{v}$ sont dérivables sur $I$ et :

    <div style="color:red">

    \[
    \boxed{\left(\dfrac{1}{v} \right)^\prime  = -\dfrac{v^\prime}{v^2}}
    \]

    \[
    \boxed{\left(\dfrac{\strut u}{\strut v} \right)^\prime  = \dfrac{u^\prime v - uv^\prime}{v^2}}
    \]

    </div>

!!! exemple "Exemple I.4"
    === "Question"
        Justifiez la dérivabilité des fonctions suivantes sur un certain intervalle et calculez leur fonction dérivée  :

        \(f(x)=\dfrac{\strut 1}{\strut 4\,x-7}\)   
        \(g(x)=\dfrac{\strut 3\,x-4}{\strut x^2+x+1}\)  
        \(h(x)= \dfrac{\strut 5}{\strut 2\,x+4}- \dfrac{5\,x-2}{x+1}\).

    === "Réponses pour $f$"
        \(f\) est de la forme \(\dfrac{1}{v}\) où \(v(x)=4\,x-7\) ; 

        \(v\) est dérivable sur ℝ et s'annule en 7/4 donc \(f\) est dérivable sur \(ℝ\backslash\{7/4\}\).

        Par ailleurs, 
        \(v'(x)=4\)
        donc 
        \(f'(x)=-\dfrac{v'}{v^2} (x) = 
        -\dfrac{4}{(4\,x-7)^2}
        \).

    === "Réponses pour $g$"

        \(g\) est de la forme \(\dfrac{\strut u}{\strut v}\) où \(u(x)=3\,x-4\) et \(v(x)=x^2+x+1\) ;

        \(u\) est dérivable sur ℝ et \(v\) est dérivable sur ℝ et ne s'annule pas sur ℝ (le discriminant $\Delta$ est négatif)
        donc \(g\) est dérivable sur ℝ.

        Par ailleurs, 
        \(u'(x)=3\) et \(v'(x)=2\,x+1\)
        donc  
        \(\begin{align}
        g'(x)
        &=
        \dfrac{u' v - u v'}{\strut v^2}(x) 
        =
        \dfrac{\strut 3 \times (x^2+x+1) - (3\,x-4) \times (2\,x+1)}{\strut (x^2+x+1)^2}
        \\
        &=
        \dfrac{\strut 3 \,x^2+3\,x+3 - (6\,x^2+3\,x-8\,x-4) }{\strut (x^2+x+1)^2}
        =
        \dfrac{\strut -3 \,x^2+8\,x+7}{\strut (x^2+x+1)^2}.
        \end{align}\)

    === "Réponses pour $h$"
        $h(x)=h_1(x)-h_2(x)$ où 
        $h_1(x)=\dfrac{5}{2\,x+4}$
        et
        $h_2(x)= \dfrac{5\,x-2}{x+1}$.

        En remarquant que 
        \(\dfrac{5}{2\,x+4} = 5 \times \dfrac{1}{2\,x+4}\),
        \(h_1\) est de la forme \(k \times\dfrac{\strut 1}{\strut v}\) où \(k=5\) et 
        \(v(x)=2\,x+4\) ;

        \(v\) est dérivable sur ℝ et s'annule en $-2$  
        donc 
        \(h_1\)  est dérivable sur \(ℝ\backslash\{-2\}\) et 
        \(h_1'(x) = 5 \times \left(-\dfrac{2}{(2\,x+4)^2}\right)
        =
        -\dfrac{10}{(2\,x+4)^2}
        \).

        \(h_2\) est de la forme \(\dfrac{\strut u}{\strut v}\) où \(u(x)=5\,x-2\) et \(v(x)=x+1\) ;

        \(u\) est dérivable sur ℝ et \(v\) est dérivable sur ℝ et s'annule en $-1$
        donc 
        \(h_2\) est dérivable sur \(ℝ\backslash\{-1\}\).

        Par ailleurs, 
        \(u'(x)=5\) et \(v'(x)=1\)
        donc 

        \(
        h_2'(x)
        =\dfrac{u' v - u v'}{\strut v^2}(x) 
        =\dfrac{\strut 5 \times (x+1) - (5\,x-2) \times 1}{\strut (x+1)^2}
        =\dfrac{\strut 7}{\strut (x+1)^2}
        \).

        Conclusion : 
        \(h\) est dérivable sur \(ℝ\backslash\{-2;-1\}\)
        et  
        \(
        h'(x) = h_1'(x) - h_2'(x)
        =-\dfrac{10}{(2\,x+4)^2} - \dfrac{\strut 7}{\strut (x+1)^2}.
        \).

!!! tip "Remarque"
    Il est rarement utile de développer le carré présent au dénominateur.


!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    116, 123, 129 page 105



## I.3 Dérivées de certaines fonctions composées

!!!tip "Remarque importante"
    Soit par exemple une fonction \(g\) définie sur ℝ$^+$ par $g(x)=\sqrt{x}$. 
    Alors nous pouvons remplacer \(x\) par n'importe quel nombre positif :  
    \(f(2)=\sqrt{2}\)  
    \(f(5)=\sqrt{5}\)  
    mais aussi par une expression littérale quelconque :  
    \(f(u)=\sqrt{u}\) quand \(u\ge0\)  
    \(f(x+1)=\sqrt{x+1}\) quand \(x\ge-1\)  
    \(f(2\,x)=\sqrt{2\,x}\) quand \(x\ge0\)  
    \(f(2\,x+1)=\sqrt{2\,x+1}\) quand \(x\ge-\dfrac{1}{2}\)  
    etc.
    
    (nous avons déjà pratiqué cela quand nous remplaçions $n$ par $n+1$ dans l'expression du terme général d'une suite $(u_n)$).


!!!tip "Remarque"
    Nous cherchons maintenant à dériver des fonctions qui sont un enchaînement d'une fonction affine suivie d'une autre fonction.

    Par exemple $f(x)=\sqrt{5\,x+1}$ dont l'expression se décompose ainsi :
    $x\mapsto 5\,x+1 \overset{\sqrt{~}}{\longmapsto} \sqrt{5\,x+1}$.

!!! abstract "Théorème I.3" 
    Soient $a$ et $b$ deux réels, soit $g$ une fonction dérivable sur un ensemble $J$.

    Soit $I$ l'ensemble des réels $x$ tels que $a\,x+b\in J$.

    Alors la fonction $f$ définie sur un certain ensemble par 
    $f(x)=g(a\,x+b)$ est dérivable sur $I$ et :

    <div style="color:red">

    \[
    \boxed{f'(x)=a\times g'(a\,x+b)}
    \]

    </div>


!!! exemple "Exemple I.5"
    === "Question"
        Justifiez la dérivabilité des fonctions suivantes sur un certain intervalle et calculez leur fonction dérivée  :

        \(f_1(x)=(-3\,x+5)^7\)    
        \(f_2(x)=\sqrt{1-4\,x}\).

    === "Réponses pour $f_1$"
        \(f_1(x)=g(a\,x+b)\) où \(g(x) = x^7\) et \(a=-3\),  \(b=5\) ; 

        \(g\) est dérivable sur ℝ donc \(f_1\) aussi.

        Par ailleurs, 
        \(g'(x)=7\,x^6\)
        donc 
        \(g'(-3\,x+5)=7\,(-3\,x+5)^6
        \)
        d'où 
        \(f_1'(x)=-3 \times 7\,(-3\,x+5)^6
        = 
        -21\,(-3\,x+5)^6
        \).

        
    === "Réponses pour $f_2$"
        \(f_2(x)=g(a\,x+b)\) où \(g(x) = \sqrt{x}\) et \(a=-4\),  \(b=1\) ; 

        \(g\) est dérivable sur $]0;+\infty[$ donc il faut que 
        \(
        1-4\,x>0
        \).

        Or : 
        \(
        1-4\,x>0
        \iff
        -4\,x>-1
        \iff
        x<\dfrac{1}{4}
        \)
        donc \(f_2\)  est dérivable sur $\left]-\infty;\dfrac{1}{4}\right[$.

        Par ailleurs, 
        \(g'(x)=\dfrac{1}{2\sqrt{x}}\)
        donc 
        \(g'(1-4\,x) = \dfrac{1}{2\sqrt{1-4\,x}}
        \)
        d'où  
        \(
        f_2'(x)
        =
        -4 \times \dfrac{\strut 1}{\strut 2\sqrt{1-4\,x}}
        = 
        \dfrac{\strut -2}{\strut \sqrt{1-4\,x}}
        \).


!!! danger "Attention"
    * N'oubliez pas le \(a\) dans la dérivée (qui est en fait la dérivée de \(a\,x+b\) !).  
    Par exemple, la dérivée de \((4\,x-1)^8\) est \(32(4\,x-1)^7\), pas \(8(4\,x-1)^7\).
    * Ne dérivez pas simplement \(a\,x+b\).  
    Par exemple, la dérivée de \((4\,x-1)^8\) n'est pas \(4^8\) ni \(8\times4^7\) !!

!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    146 à 149 page 106
