# ⁉️ QCM
!!! bug "Attention"
    Il peut y avoir plusieurs réponses correctes à chaque question.

#### Q1 - La dérivée de \(uv\) est :
{{ qcm(["$u'v+uv'$", "$u'v'$", "$uv+u'v'$", "$u'v-uv'$", "$\dfrac{u'v+uv'}{v^2}$", "$\dfrac{u'v-uv'}{v^2}$"], [1], shuffle = True) }}

#### Q2 - La dérivée de \(\dfrac{u}{v}\) est :
{{ qcm(["$u'v+uv'$", "$u'v'$", "$uv+u'v'$", "$u'v-uv'$", "$\dfrac{u'v+uv'}{v^2}$", "$\dfrac{u'v-uv'}{v^2}$"], [6], shuffle = True) }}

#### Q3 - La dérivée de \(g(a\,x+b)\) est :
{{ qcm(["$a\,g'(a\,x+b)$", "$g(a\,x+b)$", "$g(a)$", "$g'(a\,x+b)$", "$a\,g(a\,x+b)$", "$g'(a)$"], [1], shuffle = True) }}

#### Q4 - La dérivée de \(\dfrac{3}{x+2}\) est :
{{ qcm(["$-\dfrac{3}{(x+2)^2}$", "$-\dfrac{3}{x^2+4\,x+4}$", "$\dfrac{3}{(x+2)^2}$", "$\dfrac{3}{1}=3$", "$\dfrac{0}{1}=0$", "$\dfrac{3}{x^2+4}$"], [1,2], shuffle = True) }}

#### Q5 - La dérivée de \(\sin(x)\) est \(\cos(x)\). Donc la dérivée de \(x\sin(x)\) est :
{{ qcm(["$\cos(x)$", "$x\,\cos(x)$", "$\sin(x)+x\,\cos(x)$", "$\sin(1)$", "$\cos(1)$"], [3], shuffle = True) }}

#### Q6 - La dérivée de \((4\,x^2-2)^{20}\) est :
{{ qcm(["$80\,x(4\,x^2-2)^{19}$", "$(8\,x)^{19}$", "$(8\,x-2)^{20}$", "$20(4\,x^2-2)^{19}$"], [1], shuffle = True) }}

#### Q7 - La dérivée de \(x\sqrt{x}\) est :
{{ qcm(["$\dfrac{3}{2}\,\sqrt{x}$", "$\dfrac{1}{2\,\sqrt{x}}$", "$\sqrt{x}+\dfrac{x}{2\,\sqrt{x}}$", "$3\,x$"], [1,3], shuffle = True) }}

#### Q8 - La dérivée de \(\dfrac{x+1}{x+2}\) est :
{{ qcm(["$\dfrac{1}{(x+2)^2}$", "$-\dfrac{1}{(x+2)^2}$", "1", "$-1$", "$\dfrac{x+1}{(x+2)^2}$", "$\dfrac{(x+1)^2}{(x+2)^2}$"], [1], shuffle = True) }}

#### Q9 - Cochez les expressions qui ont un signe constant :
{{ qcm(["\(3\,x^2+4\)", "\(-2-\sqrt{x}\)", "\(6\,x^4+5\,x^2\)", "\(x^2+x+2\)", "\(3\,x+4\)", "\(-5-4\,x\)", "\(x^2-5\)", "\(1-4\,x^2\)"], [1,2,3,4], shuffle = True) }}

#### Q10 - La fonction \(f\) définie sur \(ℝ\backslash\{-2\}\) par \(f(x)=\dfrac{x+1}{x+2}\) est :
{{ qcm(["strictement croissante sur \(]-\infty;-2[\cup ]-2;+\infty[\)", "strictement croissante sur ℝ \ {-2}", "strictement décroissante sur ℝ", "strictement croissante sur ℝ", "strictement décroissante sur \(]-\infty;-2[\cup ]-2;+\infty[\)", "strictement décroissante sur ℝ \ {-2}"], [1], shuffle = False) }}

#### Q11 - La fonction \(f\) définie sur ℝ admet un maximum en \(-4\). Alors :
{{ qcm(["$f'(-4)=0$", "$f(-4)\geq f(3)$", "$f(-4)=0$", "$f'(-4)>0$", "$f(-4)>0$"], [1,2], shuffle = True) }}

#### Q12 - Sur le graphique ci-dessous :
{{ qcm(["$f'(-1)>0$", "$f'(1)<0$", "$f'(0)=0$", "$f'(2)>0$", "$f'(-1)<0$", "$f'(1)>0$", "$f'(0)>0$", "$f'(0)<0$", "$f'(2)<0$", "$f'$ s'annule une seule fois"], [1,2,3,4], shuffle = True) }}

![](../../derivation1/images/pente_tangente.png){width=50%; : .center}


## Autres QCMs
* [Petit QCM sur le calcul de dérivées](https://xymaths.fr/Lycee/Common/QCM/Exercices-Calcul-Derivees-bis.php)
* [Un autre sur le même thème](https://www.solumaths.com/fr/jeux-maths-en-ligne/jouer/quiz-calcul-derivee-fonction)
* [Trouver des erreurs dans des tableaux de variation](http://www.qcmdemath.net/math/QCM65.htm)
* [Petite série d'exercices en ligne](https://www.cmath.fr/1ere/etude_de_fonction/exercices.php)
* [Quelques questions un peu plus difficiles](https://www.maths-cours.fr/quiz/1re-derivee-produit-quotient)
* [Questions diverses sur les variations (niveau seconde)](http://labomath.free.fr/qcms/seconde/varfonc/index.html)
