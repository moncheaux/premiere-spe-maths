# III - Produit scalaire avec normes et angle
!!! abstract "Définition III.1"

    <div style="color:red;">

    $$\boxed{\overrightarrow{AB}\cdot\overrightarrow{AC}=AB\times AC\times\cos\left(\theta\right)}$$

    </div>

    où $\theta=\widehat{BAC}$.

    ![Alt text](images/def_ps_angle.png){width=20%; : .center}


!!! tip "Remarque"
    * en notant $||\vec{u}||$ la norme du vecteur $\vec{u}$ (sa longueur), $||\vec{v}||$ celle de $\vec{v}$
    et $(\vec{u},\vec{v})$ une mesure de l'angle entre les vecteurs $\vec{u}$ et $\vec{v}$, ceci peut aussi s'écrire :

    <div style="color:red">

    $$\boxed{\vec{u}\cdot\vec{v}=||\vec{u}||\times||\vec{v}|| \times\cos\left((\vec{u},\vec{v})\right)}$$

    </div>
    
    * Comme la fonction cosinus est paire, le signe de l'angle n'est pas important.

??? tip "Lien entre les deux définitions"
    <iframe scrolling="no" title="" src="https://www.geogebra.org/material/iframe/id/SNrkQzSe/width/1280/height/760/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="1280px" height="600px" style="border:0px;"> </iframe>


!!! exemple "Exemple"
    $ABCD$ est un carré de côté 3.

    ![Alt text](images/expl1.png){width=20%; : .center}

    Voici une autre façon de calculer $\overrightarrow{AB}\cdot \overrightarrow{AC}$ :  
    $\overrightarrow{AB}\cdot \overrightarrow{AC}=AB \times AC \times \cos 45° = 3 \times 3 \sqrt{2} \times \dfrac{\sqrt{2}}{2}=9$.

!!! question "Exercices"
    [Fiche d'exercice](http://mathsenligne.net/telechargement/1STI2D/STI2D_1G1%20-%20Produit%20scalaire%20dans%20le%20plan/STI2D_1G1_ex3a%20-%20D%C3%A9finition%20du%20produit%20scalaire.pdf)
