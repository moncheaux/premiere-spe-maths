# V - Propriétés du produit scalaire

!!! abstract "Propriété V.1 : vecteurs colinéaires"
    Si $\vec{u}$ et $\vec{v}$ sont colinéaires et de même sens alors $\vec{u}\cdot\vec{v}=\|\vec{u}\|\times\|\vec{v}\|$ ;  
    si $\vec{u}$ et $\vec{v}$ sont colinéaires et de sens contraire alors $\vec{u}\cdot\vec{v}=-\|\vec{u}\|\times\|\vec{v}\|$.

!!! exemple "Exemple V.1"
    $ABCD$ est un carré de côté 3.

    ![Alt text](images/fig_carre.png){width=20%; : .center}

    $\overrightarrow{BI}\cdot \overrightarrow{BC}=BI \times BC = 4,5$
    et
    $\overrightarrow{BI}\cdot \overrightarrow{CI}=-BI \times CI = -2,25$.

!!! abstract "Définition"
    Deux vecteurs du plan $\vec{u}=\overrightarrow{AB}$ et $\vec{v}=\overrightarrow{CD}$ sont <span style="color:red">orthogonaux</span> ($\vec{u}\perp \vec{v}$) si les droites $(AB)$ et $(CD)$ sont perpendiculaires.

    ![Alt text](images/fig_ortho.png){width=40%; : .center}


!!! abstract "Propriété V.2 : critère d'orthogonalité"
   
    <div style="color:red">

    $$\boxed{\vec{u}\perp\vec{v} \Longleftrightarrow\vec{u}\cdot\vec{v}=0}$$

    </div>

!!! tip "Remarques"
    * nous verrons que ceci permet parfois de prouver rapidement et efficacement l'existence d'un angle droit ;
    * le vecteur nul est considéré comme orthogonal à tous les vecteurs ;
    * contrairement à la multiplication usuelle, $\vec{u}\cdot\vec{v}=0 \nLeftrightarrow \vec{u}=\vec{0} \mbox{ ou }\vec{v}=\vec{0}$ ;
    * le critère d’orthogonalité s'écrit, dans une base orthonormée de l'espace : $\vec{u} \perp \vec{v} \Longleftrightarrow XX'+YY'+ZZ' = 0$ ;

!!! tip "Exemple V.2"
    $ABCD$ est un carré de côté 3.

    ![Alt text](images/fig_carre.png){width=20%; : .center}

    $(AB)\perp(AD)$ donc $\overrightarrow{AB}\cdot \overrightarrow{AD}=0$ ;  
    $(AC)\perp(BD)$ donc $\overrightarrow{AC}\cdot \overrightarrow{BD}=0$.


!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    17, 18, 2, 15 page 202  
    25 page 203  
    7 page 202


!!! abstract "Propriété V.3 : signe du produit scalaire"
    * $\vec{u}\cdot\vec{v}>0 \Longleftrightarrow (\vec{u}, \vec{v})$ est aigu ;
    * $\vec{u}\cdot\vec{v}<0 \Longleftrightarrow (\vec{u}, \vec{v})$ est obtus ;
    * $\vec{u}\cdot\vec{v}=0 \Longleftrightarrow (\vec{u}, \vec{v})=90°$.

    En effet, le cosinus d'un angle compris entre 0 et 90° est positif et celui d'un angle compris entre 90° et 180° est négatif.

!!! tip "Exemple V.3"
    ![Alt text](images/fig_carre.png){width=20%; : .center}

    $\overrightarrow{AB}\cdot \overrightarrow{CB} = 0$ car $\overrightarrow{AB} \perp \overrightarrow{CB}$ ;  
    $\overrightarrow{AB}\cdot \overrightarrow{AC} >0$ et l'angle $\widehat{BAC}$ est aigu ;  
    $\overrightarrow{AB}\cdot \overrightarrow{BD} <0$ donc l'angle entre $\overrightarrow{AB}$ et $\overrightarrow{BD}$ est obtus (faîtes partir $\overrightarrow{BD}$ du point $A$).


!!! question "Exercices"
    [Exercice 3B.2 de la fiche d'exercices 2](http://mathsenligne.net/telechargement/1STI2D/STI2D_1G1%20-%20Produit%20scalaire%20dans%20le%20plan/STI2D_1G1_ex3b%20-%20Interpr%C3%A9tation%20du%20produit%20scalaire.pdf)  
    ou  
    [Transmath](https://biblio.manuel-numerique.com/) : 26 page 203



!!! abstract "Propriété V.4 : symétrie"
    Le produit scalaire est _symétrique_ : pour tous les vecteurs $\vec{u}$ et $\vec{v}$, 
    $\vec{u}\cdot\vec{v}=\vec{v}\cdot\vec{u}$.

!!! abstract "Propriété V.5 : bilinéarité"
    Le produit scalaire est _bilinéaire_ : pour tous les vecteurs $\vec{u}$, $\vec{v}$, $\vec{w}$ et tout réel $k$, 

    * $\vec{u}\cdot \left(\vec{v}+\vec{w}\right) =\vec{u}\cdot\vec{v}+\vec{u}\cdot\vec{w}$ ;
    * $\left(\vec{u}+\vec{v}\right)\cdot \vec{w} =\vec{u}\cdot\vec{w}+\vec{v}\cdot\vec{w}$ ;
    * $\vec{u}\cdot \left( k\vec{v}\right)=\left( k\vec{u}\right)\cdot\vec{v}=k\,\left( \vec{u}\cdot\vec{v}\right)$.

!!! tip "Remarque"
    La formule $\vec{u}\cdot \left(\vec{v}+\vec{w}\right)=\vec{u}\cdot\vec{v}+\vec{u}\cdot\vec{w}$ rappelle bien sûr la distributivité de la multiplication par rapport à l'addition.

!!! tip "Exemple V.4"
    === "Question"
        Recalculer $\overrightarrow{DI}\cdot \overrightarrow{AD}$ sans utiliser la projection orthogonale.

        ![Alt text](images/fig_carre.png){width=20%; : .center}


    === "Réponse"

        ![Alt text](images/fig_carre.png){width=20%; : .center}

        $\begin{align*}
        \overrightarrow{DI}\cdot \overrightarrow{AD}
        &=^{(1)}
        (\overrightarrow{DC}+\overrightarrow{CI})\cdot \overrightarrow{AD}
        =^{(2)}
        \overrightarrow{DC}\cdot \overrightarrow{AD}+\overrightarrow{CI}\cdot \overrightarrow{AD}
        \\
        &=^{(3)}
        0-CI \times AD
        =
        -1,5 \times 3
        =-4,5.
        \end{align*}$

        $^{(1)}$ relation de Chasles ;  
        $^{(2)}$ distributivité ;  
        $^{(3)}$ $\overrightarrow{DC}\perp\overrightarrow{AD}$ ; $\overrightarrow{CI}$ et $\overrightarrow{AD}$ sont colinéaires et de sens contraires.


??? exemple "Exemple V.5 (un autre exemple de décomposition)"
    $\overrightarrow{AI}\cdot \overrightarrow{AC} = (\overrightarrow{AB}+\overrightarrow{BI})\cdot(\overrightarrow{AB}+\overrightarrow{BC})= \overrightarrow{AB}\cdot\overrightarrow{AB} + \overrightarrow{AB} \cdot \overrightarrow{BC}+\overrightarrow{BI}\cdot\overrightarrow{AB}+\overrightarrow{BI}\cdot\overrightarrow{BC}= AB^2+0+0+BI \times BC=13,5$.



!!! tip "Astuce"
    Nous pouvons toujours transformer les calculs pour n'avoir affaire qu'à des vecteurs horizontaux ou verticaux, ce qui donne des produits scalaires simples à calculer. Nous pouvons aussi plus simplement introduire un repère dans la figure...



!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    3 page 200  
    86 page 208  
    73 page 206 pour les plus forts


