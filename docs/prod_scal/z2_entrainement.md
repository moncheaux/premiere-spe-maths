# Quelques exercices pour vous entraîner
Voici une série d'exercices ludiques, sur Learningapps ou plus "sérieux" sur Wims.

## Calculs de produits scalaires
* par projection orthogonale : [classer suivant la valeur](https://learningapps.org/7639364) et [les calculer](https://learningapps.org/9293010) ;
* en utilisant les normes et l'angle : [niveau 1](https://learningapps.org/12107374) et [niveau 2](https://learningapps.org/12105576), [un autre sur Wims]( https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/geometry/OEFbarypdtsc.fr&cmd=new&exo=cosinus) ;
* avec les coordonnées : [calculer des produits scalaires](https://learningapps.org/9292974) ; [avec des vecteurs de l'espace](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H6/geometry/OEFpdtscalTS.fr&cmd=new&exo=calcesp1) ; [avec des coordonnées de points de l'espace]( https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H6/geometry/OEFpdtscalTS.fr&cmd=new&exo=calcesp2) ; [avec des points dans une figure de l'espace](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H6/geometry/OEFpdtscalTS.fr&cmd=new&exo=calcesp5) 

## Applications du produit scalaire
* [calcul d'un angle dans un repère du plan](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H6/algebra/oefvec2d.fr&cmd=new&exo=angle) et [dans l'espace](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/geometry/oefgeo3D.fr&cmd=new&exo=psangle) ;
* [calcul de la norme d'un vecteur de l'espace](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/geometry/oefgeo3D.fr&cmd=new&exo=normvect) ;
* [savoir si deux vecteurs sont orthogonaux avec leurs coordonnées]( https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/geometry/OEFbarypdtsc.fr&cmd=new&exo=expranalplan) ;
* [classer des couples de vecteurs avec leurs coordonnées](https://learningapps.org/13128660)
