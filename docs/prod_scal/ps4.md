# IV - Calculs en base orthonormée
## Calcul du produit scalaire en base orthonormée
!!! abstract "Propriété IV.1"
    Soient $\vec{u}$ et $\vec{v}$ de coordonnées respectives 
    $\binom{X}{Y}$ et $\binom{X'}{Y'}$ dans une base orthonormée du plan
    $\left(\vec{\imath}\,; \vec{\jmath}\right)$ ($\vec{\imath}\perp \vec{\jmath}$ et 
    \(\vec{\imath}\), \(\vec{\jmath}\) ont pour longueur 1).  
    Alors : 
    
    <div style="color:red">

    $$\boxed{\vec{u}\cdot\vec{v}=XX'+YY'}$$

    </div>
    

!!! tip "Remarque"
    Pour deux vecteurs de l'espace (donc ayant chacun trois coordonnées), la formule devient bien sûr $\vec{u}\cdot\vec{v}=XX'+YY'+ZZ'$.

!!! exemple "Exemple IV.1"
    === "Question"
        Calculer $\vec{u}\cdot\vec{v}$, où $\vec{u}\,\binom{2}{0}$ et $\vec{v}\,\binom{3}{-1}$.

    === "Réponse"
        $\vec{u}\cdot\vec{v} = 2 \times 3 + 0 \times (-1) = 6$.



!!! bug "Attention"
    La formule de la propriété IV.1 ne fonctionne que dans une base orthonormée.

??? exemple "Exemple IV.2"

    Soit $ABCD$ un rectangle de dimensions 5 et 3.

    Dans le repère $(A;\overrightarrow{AB},\overrightarrow{AD})$, les coordonnées des points sont :
    \(A\,(0;0)\) ;
    \(B\,(1;0)\) ;
    \(C\,(1;1\) ;
    \(D\,(0;1)\).

    ![Alt text](images/expl_fig_rect.png){width=30%; : .center}

    Les coordonnées des vecteurs $\overrightarrow{AC}$ et $\overrightarrow{BD}$ sont donc :  
    $\overrightarrow{AC}\,\binom{x_C-x_A}{y_C-y_A}=\binom{1}{1}$ et 
    $\overrightarrow{BD}\,\binom{x_D-x_B}{y_D-y_B}=\binom{-1}{1}$.

    Ici, $XX'+YY' = 1\times (-1)+1\times 1 = 0$ alors que 
    $\overrightarrow{AC}\not\perp \overrightarrow{BD}$ (nous verrons plus tard que le produit scalaire est nul seulement pour les vecteurs orthogonaux) donc 
    $\overrightarrow{AC}\cdot \overrightarrow{BD} \neq XX'+YY'$ (car le repère $(A;\overrightarrow{AB},\overrightarrow{AD})$ n'est pas orthonormé).



!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) : 62 et 53 page 205

!!! exemple "Exemple IV.3"
    === "Question"
        Dans un repère orthonormé $\left(O;\vec{\imath}, \vec{\jmath}\right)$, soient les points : 
        \(A\,(-2;1)\) ;
        \(B\,(3;-6)\) ;
        \(C\,(-1;-5)\).  
        Calculez $\overrightarrow{AB}\cdot \overrightarrow{AC}$ et $\overrightarrow{AC}\cdot \overrightarrow{CB}$.

    === "Réponses"
        Données : \(A\,(-2;1)\) ; \(B\,(3;-6)\) ; \(C\,(-1;-5)\).

        Les coordonnées des vecteurs $\overrightarrow{AB}$ et $\overrightarrow{AC}$ sont : 
        $\overrightarrow{AB}\,\binom{x_B-x_A}{y_B-y_A}=\binom{5}{-7}$ et 
        $\overrightarrow{AC}\,\binom{x_C-x_A}{y_C-y_A}=\binom{1}{-6}$.  
        Donc $\overrightarrow{AB}\cdot \overrightarrow{AC} = XX'+YY' = 5 \times 1+(-7) \times (-6)=47$.

        Les coordonnées des vecteurs $\overrightarrow{AC}$ et $\overrightarrow{CB}$ sont : 
        $\overrightarrow{AC}\,\binom{1}{-6}$ et 
        $\overrightarrow{CB}\,\binom{x_B-x_C}{y_B-y_C}=\binom{4}{-1}$.  
        Donc $\overrightarrow{AC}\cdot \overrightarrow{CB} = XX'+YY' = 1 \times 4+(-6) \times (-1)=10$.

!!! danger "Attention"
    Ne confondez pas les coordonnées de vecteurs et celles des points.

    Dans la propriété IV.1,  $X$, $X'$, $Y$, $Y'$ sont des coordonnées de vecteurs, pas des coordonnées de points !



## Norme d'un vecteur

!!! abstract "Définition"
    La <span style="color:red">norme</span> d'un vecteur \(\vec{u}\) est sa longueur. Elle est notée <span style="color:red">\(\|\vec{u}\|\)</span>.


!!! abstract "Propriété IV.2"
    Soient, dans une base orthonormée $\left(\vec{\imath}; \vec{\jmath}\right)$, le vecteur $\vec{u}\binom{X}{Y}$.
    Alors la norme du vecteur $\vec{u}$ est :

    <div style="color:red;">

    $$\boxed{||\vec{u}|| = \sqrt{X^2+Y^2}}$$

    </div>

    ![Alt text](images/fig_norme.png){width=40%; : .center}


    **Démonstration (en deux dimensions) :**  
    Pour tout $\vec{u}$, nous avons :  
    $\vec{u}.\vec{u}=||\vec{u}|| \times ||\vec{u}|| \times \cos 0 = ||\vec{u}||^2$.

    De plus, 
    $\vec{u}.\vec{u}= X \times X + Y \times Y=X^2+Y^2$.

    Donc $X^2+Y^2 = ||\vec{u}||^2$ d'où $||\vec{u}|| = \sqrt{X^2+Y^2}$.



!!! tip "Remarques"
    * vous aurez bien sûr reconnu la formule de Pythagore...
    * dans l'espace, la norme de $\vec{u}$ est $||\vec{u}|| = \sqrt{X^2+Y^2+Z^2}$.
    

!!! exemple "Exemple IV.4"
    Si $\vec{u}\,\begin{pmatrix}-2\\3\\-1\end{pmatrix}$ dans une base orthonormée de l'espace 
    alors : 
    $\|\vec{u}\|=\sqrt{(-2)^2+3^2+(-1)^2}=\sqrt{14}$.

!!! danger "Attention"
    N'oubliez pas les parenthèses quand vous mettez un nombre négatif au carré !

!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    calculer les normes des vecteurs de l’exercice 62 page 205  
	20 page 202


!!! abstract "Propriété IV.3 : distance entre deux points"
    Soient, dans un repère orthonormé, les points $A\,(x_A;y_A)$ et $B\,(x_B;y_B)$.
    Alors 

    <div style="color:red;">

    $$\boxed{AB = \sqrt{(x_B-x_A)^2+(y_B-y_A)^2}}$$

    </div>

    ![Alt text](images/fig_distance.png){width=50%; : .center}

!!! tip "Remarques"
    * la propriété IV.3 est une conséquence des propriétés I.2 et IV.2 ;
    * dans l'espace, cette distance est $AB = \sqrt{(x_B-x_A)^2+(y_B-y_A)^2+(z_B-z_A)^2}$.


!!! exemple "Exemple IV.5"
    === "Question"
        Dans un repère orthonormé, calculez la distance entre \(A\,(2;-2;3)\) et \(B\,(0;-3;1)\).

    === "Réponse"

        Soit je calcule directement la distance :

        \(\begin{align*}
        AB&=\sqrt{(0-2)^{\strut2}+(-3-(-2))^2+(1-3)^2}
        \\
        &=\sqrt{(-2)^{\strut2}+(-1)^2+(-2)^2}=\sqrt{9}=3.
        \end{align*}\)

        Soit je calcule d'abord les coordonnées du vecteur \(\overrightarrow{AB}\) puis sa norme :
        \(
        \overrightarrow{AB}\begin{pmatrix}x_B-x_A\\y_B-y_A\\z_B-z_A\end{pmatrix}
        =\begin{pmatrix}0-2\\-3-(-2)\\1-3\end{pmatrix}
        =\begin{pmatrix}-2\\-1\\-2\end{pmatrix}
        \)

        donc
        \(
        AB = \|\overrightarrow{AB}\| = \sqrt{(-2)^{\strut2}+(-1)^2+(-2)^2}=\sqrt{9}=3
        \).


!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  19, 21 page 202
