# VI - Quelques utilisations du produit scalaire

!!! tip "Remarques"
    * nous avons déjà vu une application du produit scalaire : prouver l'existence (ou la non-existence) d'un angle droit ;
    * nous avons vu également trois définitions du produit scalaire ce qui permet, en jouant avec celles-ci, de déterminer des angles, des longueurs, des équations de droites ou de réaliser des projections.

## Calculs de mesures d'angles

!!! tip "Remarque importante"
    Pour tous les vecteurs $\vec{u}$ et $\vec{v}$ non nuls, nous pouvons écrire que :

    <div style="color:red">

    $$\cos(\vec{u},\vec{v}) = \dfrac{\vec{u}\cdot\vec{v}}{||\vec{u}||\times||\vec{v}||}$$

    </div>


!!! exemple "Exemple VI.1"
    === "Question"
        ![Alt text](images/fig_carre.png){width=20%; : .center}

        Donner une mesure de l'angle entre $\overrightarrow{DI}$ et $\overrightarrow{AD}$.

    === "Réponse"
        Nous avons vu (exemple V.4) que $\overrightarrow{DI}\cdot \overrightarrow{AD}=-4,5$ or $DI=\sqrt{3^2+1,5^2}=3\sqrt{1,25}$ et $AD=3$ donc 
        $\cos(\overrightarrow{DI},\overrightarrow{AD}) = \dfrac{-4,5}{3\sqrt{1,25}\times3} \simeq -0,447$
        d'où 
        $(\overrightarrow{DI},\overrightarrow{AD}) \simeq \arccos(-0,447)\simeq 116,57°$  
        (remarque : il était possible de trouver cette réponse avec de la trigonométrie de collège...)


??? exemple "Un autre exemple"
    ![Alt text](images/fig_carre.png){width=20%; : .center}

    Nous avons vu (exemple V.5) que $\overrightarrow{AI}\cdot \overrightarrow{AC}= 13,5$ or $AI=\sqrt{3^2+1,5^2}=3\sqrt{1,25}$ et $AC=3\sqrt{2}$ donc 
    $\cos(\overrightarrow{AI},\overrightarrow{AC}) = \dfrac{13,5}{3\sqrt{1,25}\times3\sqrt{2}} \simeq 0,94868$
    d'où 
    $(\overrightarrow{AI},\overrightarrow{AC}) \simeq \arccos(0,94868)\simeq 18,43°$  
    (là encore, la trigonométrie de collège suffisait...)


!!! question "Exercices"
    [Fiche d'exercices 2, Exercice 3B.1](http://mathsenligne.net/telechargement/1STI2D/STI2D_1G1%20-%20Produit%20scalaire%20dans%20le%20plan/STI2D_1G1_ex3b%20-%20Interpr%C3%A9tation%20du%20produit%20scalaire.pdf)
    
    [Transmath](https://biblio.manuel-numerique.com/) :  
    <!-- à la place de la fiche 9, 11 et 12 page 202-->
    79 page 207



!!! exemple "Exemple VI.2 (important)"
    === "Question"
        Soient, dans un repère orthonormé, $A\,(1;-2)$, $B\,(-1;0)$ et $C\,(2;5)$. Donner une mesure en degrés de l'angle $\widehat{BAC}$.

    === "Réponse"
        L'angle $\widehat{BAC}$ est l'angle entre $\overrightarrow{AB}$ et $\overrightarrow{AC}$. Or :  
        $\overrightarrow{AB} \binom{x_B-x_A=-1-1=-2}{y_B-y_A=0-(-2)=2}$, $\overrightarrow{AC} \binom{x_C-x_A=2-1=1}{y_C-y_A=5-(-2)=7}$
        donc $\overrightarrow{AB}\cdot \overrightarrow{AC}=-2\times1+2\times7=12$.

        Comme 
        $\overrightarrow{AB}\cdot \overrightarrow{AC} =AB \times AC \times \cos\widehat{BAC}$,
        je calcule :  
        $AB=\sqrt{(-2)^2+2^2}=\sqrt{8}=2\sqrt{2}$
        et
        $AC=\sqrt{1^2+7^2}=\sqrt{50}=5\sqrt{2}$ ;  
        donc
        $\cos\widehat{BAC} = \dfrac{12}{2\sqrt{2}\times5\sqrt{2}}=\dfrac{3}{5}$
        d'où 
        $\widehat{BAC} = \arccos\dfrac{3}{5} \simeq \boxed{53,13°}$.


!!! tip "Remarques"
    * pensez à prendre deux vecteurs partant du sommet de l'angle dont vous cherchez la mesure, par exemple, l'angle entre $\overrightarrow{AB}$ et $\overrightarrow{CA}$ **n'est pas** l'angle $\widehat{BAC}$ !
    * vérifiez vos calculs (coordonnées de vecteurs, longueurs, produit scalaire puis enfin angle) au fur et à mesure avec une figure ou avec Geogebra.

!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    14 page 202 (trouver les valeurs approchées des trois angles)
    
    



## Calculs de longueurs projetées

!!! example "Exemple VI.3"
    === "Question"
        Soit $ABCD$ un rectangle de dimensions 5 et 3.

        Soient $D'$ et $B'$ les projetés orthogonaux respectifs de $D$ et $B$ sur la droite $(AC)$.

        Calculer la longueur $D'B'$.

        ![Alt text](images/exemple_proj1.png){width=30%; : .center}


    === "Réponse"
        ![Alt text](images/exemple_proj1b.png){width=30%; : .center}

        En utilisant la définition du produit scalaire par projection orthogonale, il vient :
        $\overrightarrow{AC}\cdot \overrightarrow{DB} = AC \times D'B'$
        donc 
        $D'B' = \dfrac{\overrightarrow{AC}\cdot \overrightarrow{DB}}{\strut AC}$.

        Il ne reste plus qu'à calculer $AC$ (avec Pythagore) et 
        $\overrightarrow{AC}\cdot \overrightarrow{DB}$, en décomposant les vecteurs ou avec leurs coordonnées.

        Plaçons nous dans un repère orthonormé $(A;\vec{\imath},\vec{\jmath})$ (cf. figure), les coordonnées des points sont :
        \(A\,(0;0)\) ;
        \(B\,(5;0)\) ;
        \(C\,(5;3)\) ;
        \(D\,(0;3)\).

        Les coordonnées des vecteurs $\overrightarrow{AC}$ et $\overrightarrow{DB}$ sont donc $\overrightarrow{AC}\,\binom{x_C-x_A}{y_C-y_A}=\binom{5}{3}$ et 
        $\overrightarrow{DB}\,\binom{x_B-x_D}{y_B-y_D}=\binom{5}{-3}$.

        J'en déduis que  
        $\overrightarrow{AC}\cdot \overrightarrow{DB} = 5 \times 5+3 \times (-3) = 16$  
        et que  
        $AC = \sqrt{5^2+3^2} = \sqrt{34}$  
        donc  
        $D'B' = \dfrac{16}{\strut \sqrt{34}} = \dfrac{8\sqrt{34}}{17} \simeq 2,74$.

!!! example "Exemple VI.4"
    === "Question"
        Soient, dans un repère orthonormé, les points $A\,(-2;3)$, $B\,(4;2)$ et $C\,(2;-1)$. Soit $C'$ le projeté orthogonal de $C$ sur la droite $(AB)$.  
        Calculer (la valeur exacte de) $BC'$.
    
    === "Réponse"
        ![Alt text](images/exemple_proj2.png){width=30%; : .center}

        $BC'$ s'obtient en projetant orthogonalement $\overrightarrow{BC}$ sur $\overrightarrow{BA}$
        donc $\overrightarrow{BA}\cdot \overrightarrow{BC}=BA \times BC'$ d'où $BC'=\dfrac{\overrightarrow{BA}\cdot \overrightarrow{BC}}{BA}=-2\times1+2\times7=12$.

        Or 
        $\overrightarrow{BA} \binom{x_A-x_B=-2-4=-6}{y_A-y_B=3-2=1}$, $\overrightarrow{BC} \binom{x_C-x_B=2-4=-2}{y_C-y_B=-1-2=-3}$
        donc $\overrightarrow{BA}\cdot \overrightarrow{BC}=-6\times(-2)+1\times(-3)=9$ et 
        $BA=\sqrt{(-6)^2+1^2}=\sqrt{37}$.

        D'où $BC' = \dfrac{9}{\sqrt{37}}=\dfrac{9\sqrt{37}}{37}$.


!!! question "Exercice"
    Avec la même figure que dans l'exemple précédent, calculez $AB'$ et $CA'$.

    ??? tip "Réponses"
        $AB' = \dfrac{7\sqrt{2}}{2}$ et $CA' = \dfrac{4\sqrt{13}}{13}$.


## Recherche d'une équation de droite
!!! tip "Remarque"
    Nous pouvons utiliser le produit scalaire pour trouver des équations de droites (ou de plans) perpendiculaires à une autre droite (exemples : hauteurs dans un triangle, médiatrice d'un segment, tangente à un cercle...).


!!! example "Exemple VI.5"
    === "Question"
        Soient, dans un repère orthonormé, les points $A\,(2;0)$, $B\,(-1;3)$ et $C\,(5;-2)$.  
        Déterminez une équation de la droite perpendiculaire à $(AB)$ et passant par $C$ (donc une équation de la hauteur du triangle $ABC$ issue de $C$).

    === "Réponse"

        ![Alt text](images/droite_perp.gif){width=40%; : .center}

        Appelons $(d)$ cette droite.

        Soit $M\,(x;y)$ un point.
        Alors :

        $$M\in(d)\iff \overrightarrow{CM}\perp\overrightarrow{AB}
        \iff \overrightarrow{CM}\cdot\overrightarrow{AB}=0.$$
        
        Or les coordonnées des deux vecteurs sont :  
        $\overrightarrow{CM}\binom{x-5}{y+2} \text{ et }\overrightarrow{AB}\binom{-1-2=-3}{3-0=3}$
        donc  
        $\overrightarrow{CM}\cdot\overrightarrow{AB}=-3(x-5)+3(y+2)=-3\,x+3\,y+21$.

        Une équation de $(d)$ est donc $-3\,x+3\,y+21=0$ ou encore $-x+y+7=0$.



!!! tip "Remarque"
    Bien sûr, pensez à vérifier la réponse avec un logiciel tel que Geogebra...


!!! question "Exercices"
    [Fiche "Exercice : Produit scalaire et équations de droites"](../exo_prosc_droites.pdf)

    ??? tip "Réponse 2)a)"

        2°) a) Soit \((d)\) la hauteur issue de *E* et \(M\,(x;y)\) un point.  
        Alors :
        \(\overrightarrow{FG}\binom{x_G-x_F}{y_G-y_F} = \binom{-9}{3}\)
        et
        \(\overrightarrow{EM}\binom{x_M-x_E}{y_M-y_E} = \binom{x+2}{y-3} \)
        donc 
        \(M\in(d) \Longleftrightarrow \overrightarrow{FG}\perp\overrightarrow{EM} \Longleftrightarrow \overrightarrow{FG}\cdot\overrightarrow{EM} = 0\)
        \(\Longleftrightarrow -9(x+2)+3(y-3)=0 \Longleftrightarrow -9\,x+3\,y-27=0 \Longleftrightarrow -3\,x+y-9=0\).
        
    ??? tip "Réponse 2)b)"

        b) De même, soit \((d')\) la hauteur issue de *F* et \(M\,(x;y)\) un point.  
        Alors :
        \(\overrightarrow{EG}\binom{x_G-x_E}{y_G-y_E} = \binom{-2}{-4}\)
        et
        \(\overrightarrow{FM}\binom{x_M-x_F}{y_M-y_F} = \binom{x-5}{y+4} \)
        donc 
        \(M\in(d') \Longleftrightarrow \overrightarrow{EG}\perp\overrightarrow{FM} \Longleftrightarrow \overrightarrow{EG}\cdot\overrightarrow{FM} = 0\)
        \(\Longleftrightarrow -2(x-5)+(-4)(y+4)=0 \Longleftrightarrow -2\,x-4\,y-6=0 \Longleftrightarrow x+2\,y+3=0 \).

    ??? tip "Réponse 2)c)"

        L’orthocentre *H* du triangle *MNP* est l'intersection des hauteurs, il suffit donc de résoudre le système 
        \(\left\lbrace \begin{array}{l}-3\,x+y-9=0\\x+2\,y+3=0\end{array} \right.\).  
        Pour cela, je choisis d'éliminer \(x\) par exemple en multipliant la seconde équation par 3 :
        \(\left\lbrace \begin{array}{l}-3\,x+y-9=0\\3\,x+6\,y+9=0\end{array} \right.\).  
        D'où, en ajoutant :  
        \(7\,y=0\) donc \(y=0\)  
        puis je remplace \(y\) par \(0\) dans une équation (la plus simple possible...) :  
        \(x+2\times0+3=0\) ce qui donne \(x = -3\).  
        L'orthocentre  *H* a donc pour coordonnées \(\left(-3;0\right)\).


    ??? tip "Réponse 3)a)"

        Le milieu *I* du segment *[EG]* a pour coordonnées :
        \(x_I=\dfrac{x_E+x_G}{2}=-3\) et
        \(y_I=\dfrac{y_E+y_G}{2}=1\).

        Soit \((\Delta)\) la médiatrice de *[EG]* et \(M\,(x;y)\) un point.
        Alors :
        \(\overrightarrow{EG} \binom{-2}{-4}\)
        et
        \(\overrightarrow{IM}\binom{x_M-x_I}{y_M-y_I} = \binom{x+3}{y-1} \)
        donc 
        \(M\in(\Delta) \Longleftrightarrow \overrightarrow{IM}\perp\overrightarrow{EG} \Longleftrightarrow \overrightarrow{IM}          \cdot\overrightarrow{EG} = 0\)
        \(\Longleftrightarrow -2(x+3)+(-4)(y-1)=0 \Longleftrightarrow -2\,x-4\,y-2=0 \Longleftrightarrow x+2\,y+1=0 \).
        

    ??? tip "Réponse 3)b)"

        Le milieu *J* du segment *[FG]* a pour coordonnées :  
        \(x_J=\dfrac{x_F+x_G}{2}=\dfrac{1}{2}\) et
        \(y_J=\dfrac{y_F+y_G}{2}=-\dfrac{5}{2}\).

        Soit \((\Delta')\) la médiatrice de *[FG]*  et \(M\,(x;y)\) un point.
        Alors :
        \(\overrightarrow{FG}\binom{-9}{3}\)
        et
        \(\overrightarrow{JM}\binom{x_M-x_J}{y_M-y_J} = \binom{x-1/2}{y+5/2} \)
        donc 
        \(M\,(x;y)\in (\Delta') \iff
        \overrightarrow{FG}\perp\overrightarrow{JM} \Longleftrightarrow \overrightarrow{FG}\cdot\overrightarrow{JM} = 0\)
        \(\Longleftrightarrow -9(x-1/2)+3(y+5/2)=0 \Longleftrightarrow -9\,x+3\,y+12=0 \Longleftrightarrow -3\,x+y+4=0\).
        
    ??? tip "Réponse 3)c)"

        Le centre \(\Omega\) du cercle circonscrit au triangle *EFG* est l'intersection
        des médiatrices donc il faut résoudre 
        \(\left\lbrace \begin{array}{l}x+2\,y+1=0\\-3\,x+y+4=0\end{array} \right.\).  
        Pour cela, je choisis d'éliminer \(x\) par exemple en multipliant la première équation par 3 :
        \(\left\lbrace \begin{array}{l}3\,x+6\,y+3=0\\-3\,x+y+4=0\end{array} \right.\).  
        D'où, en ajoutant :  
        \(7\,y+7=0\) donc \(y=-1\)  
        puis je remplace \(y\) par \(-1\) dans une équation (la plus simple possible...) :  
        \(x+2\times(-1)+1=0\) ce qui donne \(x = 1\).  
        Le centre \(\Omega\) du cercle circonscrit a donc pour coordonnées \(\left(1;-1\right)\).
        
    ??? tip "Réponse 3)d)"

        Le rayon est $E\Omega = \sqrt{(x_\Omega-x_E)^2+(y_\Omega-y_E)^2} = \sqrt{(1-(-2)))^2+(-1-3)^2}
        =\sqrt{3^2+(-4)^2}=\sqrt{25}=5$.


    ??? tip "Réponse 4)a)"

        Le coefficient directeur est $\dfrac{y_F-y_G}{x_F-x_G} = -\dfrac{3}{9}=-\dfrac{1}{3}$.  
        L'équation réduite s'écrit donc $y=-\dfrac{1}{3}\,x+p$ puis en remplaçant $x$ et $y$ par les coordonnées de $F$ : $-4=-\dfrac{1}{3}\times5+p$
        d'où $p=-4+\dfrac{5}{3}=-\dfrac{7}{3}$.

        L'équation réduite de $(FG)$ est donc $y=-\dfrac{1}{3}\,x-\dfrac{7}{3}$.

    ??? tip "Réponse 4)b)"
        Le pied $H$ de la hauteur issue de $E$ est l'intersection de $(FG)$ et de la hauteur issue de $E$ donc il faut résoudre  
        \(\left\lbrace \begin{array}{l}y=-\dfrac{1}{3}\,x-\dfrac{7}{3}\\-3\,x+y-9=0\end{array} \right.\).  
        Pour cela, je remplace \(y\) par \(-\dfrac{1}{3}\,x-\dfrac{7}{3}\) dans la seconde équation :  
        \(-3\,x-\dfrac{1}{3}\,x-\dfrac{7}{3}-9=0\).  
        d'où : \(-\dfrac{10}{3}\,x-\dfrac{34}{3}=0\)  
        ce qui donne \(x=\dfrac{34}{3} \div \left(-\dfrac{10}{3}\right) 
        =-\dfrac{34}{3} \times \dfrac{3}{10} = -\dfrac{17}{5}\). 

        Puis je remplace \(x\) par \(-\dfrac{17}{5}\) :  
        \(y=-\dfrac{1}{3}\times \left(-\dfrac{17}{5}\right) -\dfrac{7}{3}
        = \dfrac{17}{15}x-\dfrac{35}{15}=-\dfrac{6}{5}\).
        
        Le pied $H$ de la hauteur issue de $E$ a donc pour coordonnées \(\left(\dfrac{17}{5};-\dfrac{6}{5}\right)\).


    ??? tip "Réponse 4)c)"
        L'aire du triangle $EFG$ est égale à \(\dfrac{EH \times FG}{2}
        =\dfrac{\sqrt{(-7/5)^2+(-21/5)^2}\times\sqrt{(-9)^2+3^2}}{2}
        =\dfrac{7\sqrt{1^2+3^2}\times3\sqrt{3^2+1^2}}{10}
        =\dfrac{21\sqrt{10}^2}{10}
        =21\).
