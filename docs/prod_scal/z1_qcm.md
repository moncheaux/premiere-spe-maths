# ⁉️ QCM
!!! bug "Attention"
    Il peut y avoir plusieurs réponses correctes à chaque question !

#### Q1 - Le produit scalaire de deux vecteurs est :
{{ qcm(["un nombre", "un vecteur", "un segment", "un point"], [1], shuffle = True) }}

#### Q2 - Le produit scalaire de deux vecteurs est :
{{ qcm(["une longueur", "une aire", "un angle", "une équation de droite", "rien de tout cela"], [5]) }}

#### Q3 - Le produit scalaire sert surtout à calculer :
{{ qcm(["des longueurs", "des aires", "des angles", "des équations de droites", "des coordonnées"], [1,3,4], shuffle = True) }}

#### Q4 - Les vecteurs \(\vec{u}\,\binom{4}{-3}\) et \(\vec{v}\,\binom{-6}{5}\) :
{{ qcm(["sont orthogonaux", "sont colinéaires", "sont égaux", "autre réponse"], [4], shuffle = True) }}

#### Q5 - Les vecteurs \(\vec{u}\,\binom{4}{-6}\) et \(\vec{v}\,\binom{12}{8}\) :
{{ qcm(["sont orthogonaux", "sont colinéaires", "sont égaux", "autre réponse"], [1], shuffle = True) }}

