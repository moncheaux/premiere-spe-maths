# I - Rappels de Seconde
!!! abstract "Propriété I.1 : relation de Chasles"

    Pour tous les points $A$, $B$ et $C$ :
    <span style="color:red;">
    $\boxed{\overrightarrow{AB}+\overrightarrow{BC}=\overrightarrow{AC}}$
    </span>

    <iframe scrolling="no" title="Relation de Chasles" src="https://www.geogebra.org/material/iframe/id/RqHzvjpr/width/1280/height/900/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/true/ctl/false" width="1280px" height="700px" style="border:0px;"> </iframe>

!!! abstract "Propriété I.2 : coordonnées d'un vecteur"
    Soient, dans un repère, les points $A\,(x_A;y_A)$ et $B\,(x_B;y_B)$.
    Alors 

    <div style="color:red">

    $$\boxed{\overrightarrow{AB} \text{ a pour coordonnées } \binom{x_B-x_A}{y_B-y_A}}$$

    </div>

    <div style="text-align:center;">
    <iframe scrolling="no" title="Coordonnées d'un vecteur" src="https://www.geogebra.org/material/iframe/id/hrx73duk/width/669/height/795/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/true/rc/false/ld/true/sdz/true/ctl/false" width="669px" height="795px" style="border:0px;"> </iframe>
    </div>

!!! tip "Remarque"
    Dans l'espace, les coordonnées de $\overrightarrow{AB}$ sont $\begin{pmatrix}x_B-x_A\\y_B-y_A\\z_B-z_A\end{pmatrix}$.

!!! example "Exemple I.1"
    Si $N\,(-2;1;5)$ et $U\,(-4;-2;3)$
    alors 
    $\overrightarrow{UN}\,\begin{pmatrix}x_N-x_U\\y_N-y_U\\z_N-z_U\end{pmatrix}=\begin{pmatrix}-2-(-4)\\1-(-2)\\5-3\end{pmatrix}=\begin{pmatrix}2\\3\\2\end{pmatrix}$.

!!! abstract "Propriété I.3 : colinéarité de deux vecteurs"
    Soient, dans un repère, deux vecteurs non nuls $\vec{u}\binom{X}{Y}$ et $\vec{v}\binom{X'}{Y'}$.  
    $\vec{u}$ et $\vec{v'}$ sont colinéaires si l'une des deux propositions équivalentes est vraie :
    
    * il existe un réel $k$ tel que $X'=kX$ et $Y'=kY$ ;
    * <span style="color:red;">$\boxed{XY'-YX'=0}$<span>.
  

!!! tip "Remarques"
    * Dans l'espace, ces propositions deviennent :
        * il existe un réel $k$ tel que $X'=kX$, $Y'=kY$ et $Z'=kZ$ ;
        * $XY'-YX'=0$ et $YZ'-ZY'=0$.
    * Deux vecteurs sont colinéaires si leurs coordonnées sont proportionnelles.
    

!!! example "Exemple I.1"
    Les vecteurs $\vec{u}\binom{-2}{5}$ et $\vec{v}\binom{3}{-7}$ sont-ils colinéaires ?

    Réponse : $XY'-YX'=(-2)\times (-7)-5\times 3 = -1 \neq 0$ donc 
    $\vec{u}$ et $\vec{v}$ ne sont pas colinéaires.
    



!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) : 2 et 6 page 191
