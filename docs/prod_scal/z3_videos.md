# Des vidéos d'explications

## Cours complets avec des exemples
[Une vidéo de cours](https://www.youtube.com/watch?v=cDwE-imSbsg) (la méthode 4 à la fin de la vidéo sera vue plus tard).
[Une autre vidéo](https://www.youtube.com/watch?v=EvB827M56pY) (la définition 3 sera vue plus tard, il faudra avancer dans la vidéo).
[Encore une autre](https://www.youtube.com/watch?v=dII7myZuLvo&list=PLVUDmbpupCaqXSHQxDf2kfOgQAIEDKggp&index=2) (passer la partie intitulée "Produit scalaire et la norme")

## Vidéos points par points
* calculs de produits scalaires avec longueurs et angles dans un triangle : [ici](https://www.youtube.com/watch?v=CJxwKG4mvWs&list=PLVUDmbpupCaqXSHQxDf2kfOgQAIEDKggp) et [là](https://www.youtube.com/watch?v=dfxz40fK0UI&list=PLVUDmbpupCaqXSHQxDf2kfOgQAIEDKggp&index=2) ;
* [calculer un produit scalaire en utilisant Chasles]([https://www.youtube.com/watch?v=FBbTySm7kfk) ;
* [calculer un produit scalaire à partir des coordonnées](https://www.youtube.com/watch?v=aOLRbG0IibY&list=PLVUDmbpupCaqXSHQxDf2kfOgQAIEDKggp&index=16) ;
* prouver que deux droites sont perpendiculaires dans un repère orthonormé : [ici](https://www.youtube.com/watch?v=MQ9I58Cap7g) et [là](https://www.youtube.com/watch?v=cTtV4DsoMLQ&list=PLVUDmbpupCaqXSHQxDf2kfOgQAIEDKggp&index=18) ;
* [prouver que deux droites sont perpendiculaires dans un carré](https://www.youtube.com/watch?v=-Hr28g0PFu0&list=PLVUDmbpupCaqXSHQxDf2kfOgQAIEDKggp&index=19) ;
* [idem en introduisant un repère](https://www.youtube.com/watch?v=N-sWlOcV8j0) ;
* [calcul d'un angle dans un repère orthonormé](https://www.youtube.com/watch?v=Ok6dZG8WIL8&list=PLVUDmbpupCaqXSHQxDf2kfOgQAIEDKggp&index=20) ;
* [exercice : calculs de produits scalaires dans une figure géométrique](https://www.youtube.com/watch?v=tfe2_jzyutY&list=PLVUDmbpupCaqXSHQxDf2kfOgQAIEDKggp&index=12) ;
* [calcul de produits scalaires dans un carré en variant les méthodes](https://www.youtube.com/watch?v=Q7D2GAIwUU0) ;
* trouver des équations de droites définies par une condition de perpendicularité : [une hauteur d'un triangle](https://www.youtube.com/watch?v=YTNN5WhuLyw) (une [autre ici](https://www.youtube.com/watch?v=v7tPngu1EVE)), [une médiatrice d'un segment](https://www.youtube.com/watch?v=q50q9yTJsXs&t=1s) et application pour [trouver le centre d'un cercle circonscrit](https://www.youtube.com/watch?v=A-U-jMrrejM&t=1s) ;
* [un exercice plus difficile avec des équations de droites](https://www.youtube.com/watch?v=8nEinL_C5Os&list=PLVUDmbpupCaqXSHQxDf2kfOgQAIEDKggp&index=17).
