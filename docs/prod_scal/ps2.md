# II - Produit scalaire par projection orthogonale

## Projeté orthogonal
!!! abstract "Définition II.1"
    Soit une droite $(d)$ et un point $M$.

    Le <span style="color:red;">projeté orthogonal</span> de $M$ sur la droite $(d)$ est le point $H$, intersection de $(d)$ et de la perpendiculaire à $(d)$ passant par $M$.


    <iframe scrolling="no" title="projeté orthogonal 2°" src="https://www.geogebra.org/material/iframe/id/ahs78tgk/width/700/height/550/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="700px" height="550px" style="border:0px;"> </iframe>

## Définition du produit scalaire par projection orthogonale


!!! tip "Remarques"
    * Soient $\vec{u}$ et $\vec{v}$ deux vecteurs alors il existe trois points $A$, $B$, $C$  tels $\vec{u}=\overrightarrow{AB}$ et $\vec{v}=\overrightarrow{AC}$.  
    * Un vecteur est un objet mobile ; nous pouvons toujours dire que deux vecteurs partent du même point.  

!!! abstract "Définition II.2"
    Soient $A$, $B$, $C$ trois points et $H$ le projeté orthogonal de $C$ sur $(AB)$.

    Le <span style="color:red;">produit scalaire</span> des vecteurs $\overrightarrow{AB}$ et $\overrightarrow{AC}$ est :

    <div style="color:red;">

    $$\overrightarrow{AB}\cdot \overrightarrow{AC}
    =\left\{
    \begin{array}{ll}    			
    AB \times AH & \text{si } \overrightarrow{AB} \text{ et } \overrightarrow{AH} \text{ ont le même sens}\\
    -AB\times AH & \text{si } \overrightarrow{AB} \text{ et } \overrightarrow{AH} \text{ ont un sens contraire}
    \end{array}
    \right.
    $$

    </div>    
    
    ![Alt text](images/def_ps1.png){width=40% ; .center}


??? tip "Et avec quatre points ?"
    Soient $A$, $B$, $C$, $D$ quatre points et $C'$, $D'$ les projetés orthogonaux de $C$ et $D$ sur $(AB)$.
    Alors :

    $$\overrightarrow{AB}\cdot \overrightarrow{CD}
    =\left\{
    \begin{array}{ll}    			
    AB \times C'D' & \text{si } \overrightarrow{AB} \text{ et } \overrightarrow{C'D'} \text{ ont le même sens}\\
    -AB\times C'D' & \text{si } \overrightarrow{AB} \text{ et } \overrightarrow{C'D'} \text{ ont un sens contraire}
    \end{array}
    \right.
    $$

    ![Alt text](images/def_ps_proj.png){width=90%;}



!!! bug "Attention"
    Le produit scalaire de deux vecteurs est un nombre !



!!! question "Exercice"
    <iframe scrolling="no" title="Produit scalaire: avec le projeté orthogonal" src="https://www.geogebra.org/material/iframe/id/nczne65w/width/673/height/489/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/true/ctl/false" width="673px" height="492px" style="border:0px;"> </iframe>


!!! exemple "Exemple"

    === "Énoncé"
        Soit $ABCD$ un carré de côté 3 et $I$ le milieu de $[BC]$. 

        Calculer les trois produits scalaires $\overrightarrow{AB}\cdot \overrightarrow{AC}$ ; 
        $\overrightarrow{AB}\cdot \overrightarrow{BD}$ et $\overrightarrow{DI}\cdot \overrightarrow{AD}$.

    === "Réponses"

        * Pour $\overrightarrow{AB}\cdot \overrightarrow{AC}$ : je projette le vecteur $\overrightarrow{AC}$ (qui est oblique) sur le vecteur $\overrightarrow{AB}$ qui est horizontal ; 
        le projeté de $A$ est alors $A$ et celui de $C$ est $B$ donc 
        $\overrightarrow{AB}\cdot \overrightarrow{AC}=AB \times AB=9$.

        ![exemple carré](images/expl1.png){width=20% ; .center}

        * Pour $\overrightarrow{AB}\cdot \overrightarrow{BD}$ : je projette le vecteur $\overrightarrow{BD}$ (qui est oblique) sur le vecteur $\overrightarrow{AB}$ qui est horizontal ; 
        le projeté de $B$ est alors $B$ et celui de $D$ est $A$ donc 
        $\overrightarrow{AB}\cdot \overrightarrow{BD}= - AB \times BA=-9$.

        ![exemple carré](images/expl1b.png){width=20% ; .center}

        * Pour $\overrightarrow{DI}\cdot \overrightarrow{AD}$ : je projette le vecteur $\overrightarrow{DI}$ (qui est oblique) sur le vecteur $\overrightarrow{AD}$ qui est vertical ; 
        le projeté de $D$ est alors $D$ et celui de $I$ est le milieu $J$ de $[AD]$ donc 
        $\overrightarrow{DI}\cdot \overrightarrow{AD}=-DJ \times AD=-4,5$.

        ![exemple carré](images/expl1c.png){width=20% ; .center}


!!! tip "Remarques"
    * le produit scalaire n'est pas en général le produit des longueurs (dans l'exemple précédent, $\overrightarrow{AB}\cdot \overrightarrow{AC} \neq AB \times AC$) ;
    * le produit scalaire n'a pas de représentation géométrique : ce n'est ni une longueur, ni un angle, ni une aire...
    * on projette un vecteur sur la direction de l'autre mais pas les deux vecteurs sur une troisième direction (par exemple $\overrightarrow{AI}\cdot \overrightarrow{AC} \neq AB^2$).


!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) : 28, 30 et 36 page 203
