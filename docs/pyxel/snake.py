# import du module
import pyxel
import random

# constantes (à compléter)
LARGEUR = 400
HAUTEUR = 320
TAILLE_CASE = 20
ORANGE = 9
VERT = 11
BLANC = 7
NOIR = 0
MAGENTA = 8
XMAX = (LARGEUR // TAILLE_CASE) - 1
YMAX = (HAUTEUR // TAILLE_CASE) - 1
FRAME_REFRESH = 15

# initialisation de la fenêtre
pyxel.init(LARGEUR, HAUTEUR, title = "Jeu de Snake")

# variables globales
tete = [3,3]
corps = [[2,3], [1,3]]
score = 0
deplacement = [0, -1]
pomme = [7, 5]

# mise à jour des positions des objets
def update():
    global corps, tete, deplacement, pomme, score
    if pyxel.frame_count % FRAME_REFRESH == 0:    
        # mise à jour du serpent qui avance selon le vecteur deplacement
        # supprimer la queue du serpent :
        del corps[len(corps)-1]
        # la tête devient le premier anneau :
        corps.insert(0, tete)
        # nouvelle tête :
        tete = [tete[0]+deplacement[0], tete[1]+deplacement[1]]
    if pyxel.btn(pyxel.KEY_UP):
        deplacement = [0, -1]
    if pyxel.btn(pyxel.KEY_DOWN):
        deplacement = [0, 1]
    if pyxel.btn(pyxel.KEY_LEFT):
        deplacement = [-1, 0]
    if pyxel.btn(pyxel.KEY_RIGHT):
        deplacement = [1, 0]
    if tete in corps or tete[0]<0 or tete[0]>XMAX or tete[1]<0 or tete[1]>YMAX:
        pyxel.quit()
    if tete == pomme:
        # choisir une nouvelle pomme
        while pomme in corps or pomme == tete:
            x_pomme = random.randint(0, XMAX)
            y_pomme = random.randint(0, YMAX)
            pomme = [x_pomme, y_pomme]
        # ajouter un anneau au serpent (en utilisant le déplacement actuel de celui-ci)
        dernier_anneau = corps[len(corps)-1]
        nouvel_anneau = [dernier_anneau[0]-deplacement[0], dernier_anneau[1]-deplacement[1]]
        corps.append(nouvel_anneau)
        # augmenter le score
        score = score + 1
        
    

# fonction de dessin
def draw():
    # à compléter plus tard
    pyxel.cls(NOIR)
    for anneau in corps:
        pyxel.rect(anneau[0]*TAILLE_CASE, anneau[1]*TAILLE_CASE, TAILLE_CASE, TAILLE_CASE, VERT)
    pyxel.rect(tete[0]*TAILLE_CASE, tete[1]*TAILLE_CASE, TAILLE_CASE, TAILLE_CASE, ORANGE)
    pyxel.rect(pomme[0]*TAILLE_CASE, pomme[1]*TAILLE_CASE, TAILLE_CASE, TAILLE_CASE, MAGENTA)
    pyxel.text(1, 1, str(score), BLANC)

# lancement du jeu
pyxel.run(update, draw)


### Vos réponses à l'exercice 2


