---
title:  Snake avec le module pyxel
---


# Le jeu Snake avec Pyxel

> __Source :__ Adaptation du [travail de F. Junier](https://www.frederic-junier.org/NSI/premiere/Projets/Projets2024/snake/snake-pyxel/) et du [tutoriel de Marine Méra](https://nuit-du-code.forge.apps.education.fr/DOCUMENTATION/PYTHON/TUTORIELS/autres-tutoriels/)

## Le jeu Snake

!!! tip "Description du jeu"

    * un serpent se déplace automatiquement, on peut le déplacer avec les flèches du clavier ;
    * s'il mange la pomme, il grandit et celle-ci réapparait dans une case vide ;
    * s'il quitte l'écran ou se mord, il meurt, et le jeu s'arrête ;
    * le score est le nombre de pommes mangées.

    ![alt](./snake.gif)

## Le module Pyxel de Python

!!! tip "Le module [Pyxel](https://github.com/kitao/pyxel)"
    * permet la création assez facile de jeux de style rétro ;
    * les jeux sont exécutables dans un environnement Python (exemple : Thonny) mais aussi dans un navigateur internet, vous pouvez donc facilement partager vos créations !
    * il est utilisé dans le concours [La Nuit du Code](https://www.nuitducode.net/) auquel vous pouvez participer (en mai).


## Principes généraux des jeux vidéos

!!! note "Fonctionnement élémentaire d'un jeu vidéo"

    Une __boucle infinie__ fait progresser le jeu.
    À chaque tour :

    1. **Étape 1 :** le programme de jeu _écoute les interactions_ du joueur
    2. **Étape 2 :** il _met à jour_ l'état du jeu (position du jeu, calcul du score, etc.)
    3. **Étape 3 :** il _dessine_ les éléments à l'écran
    4. **Étape 4 :** il attend quelques millisecondes avant de reprendre à l'étape 1

!!! note "Fonctionnement de Pyxel"
    Dans Pyxel, la boucle infinie est implicite, et l'attente de quelques millisecondes déjà prise en charge.

    Un programme utilisant Pyxel doit :

    * importer le module avec `import pyxel` ;
    * créer la fenêtre du jeu avec `#!python pyxel.init(400, 320, title="Jeu de Snake")` ; vous pouvez aussi stocker les dimensions dans des constantes `LARGEUR` et `HAUTEUR` ;
    * coder les deux fonctions qui vont assurer le déroulement du jeu :  

    |Action|Fonction pyxel|
    |:---:|:---:|
    |Mise à jour du jeu|`update()`|
    |Dessin des éléments|`draw()`|
    
    * à la fin du programme, lancer l'exécution du jeu avec `pyxel.run(update, draw)` qui fait appel aux deux fonctions prédéfinies (qui seront appelées 20 fois par seconde).

!!! question "Exercice 1"

    Ouvrez [l'activité pyxel sur Capytale](https://capytale2.ac-paris.fr/web/c/5b70-3671251), complétez le code en suivant les étapes indiquées par les commentaires puis exécutez votre programme.

??? bug "Pas de Capytale ?"

    __Si__ vous n'avez pas accès à Capytale, vous pouvez lancer cette [interface en ligne de Pyxel Web puis cliquez sur Create](https://www.pyxelstudio.net/) mais il n'y aura pas de sauvegarde en ligne.

    Copiez-collez alors le programme ci-dessous et complétez-le :

    ~~~python
    # import du module
    ...

    # constantes (à compléter)
    LARGEUR = ...
    HAUTEUR = ...

    # initialisation de la fenêtre
    pyxel.init(..., ..., title = "Jeu de Snake")

    # fonction de mise à jour des éléments du jeu
    def update():
        # à compléter plus tard
        pass

    # fonction de dessin
    def draw():
        # à compléter plus tard
        pass

    # lancement du jeu
    pyxel.run(update, draw)
    ~~~

??? danger "A consulter en dernier recours si vous n'arrivez pas à faire l'exercice 1"
    Vous pouvez utiliser le code suivant :

    ~~~python
    # import du module
    import pyxel

    # constantes (à compléter)
    LARGEUR = 400
    HAUTEUR = 320

    # initialisation de la fenêtre
    pyxel.init(LARGEUR, HAUTEUR, title = "Jeu de Snake")

    # fonction de mise à jour des éléments du jeu
    def update():
        # à compléter plus tard
        pass

    # fonction de dessin
    def draw():
        # à compléter plus tard
        pass

    # lancement du jeu
    pyxel.run(update, draw)
    ~~~



## Dessiner le serpent et le score

!!! note "Système de coordonnées en pixels"
    La fenêtre est un ensemble de pixels qui sont repérés par leurs coordonnées dans le repère lié à la fenêtre :
    
    * l'origine est le coin supérieur gauche ;
    * les abscisses sont les colonnes indexées de 0 à `LARGEUR - 1` et l'axe des abscisses est le bord supérieur orienté de gauche à droite ;
    * les ordonnées sont les lignes indexées de 0 à `HAUTEUR - 1` et l'axe des ordonnées est le bord gauche orienté de haut en bas ;
    
    ![alt](./exemple_binaire_cadre.png)

!!! note "Système de coordonnées en cases"
    Nous avons créé dans l'exercice 1 une fenêtre de largeur 400 pixels et de hauteur 320 pixels. Nous choisissons de découper cette fenêtre en cases de 20$\times$20 pixels. Ainsi, la fenêtre devient une grille de 400/20 = 20 cases de largeur sur 320/20 = 16 cases de hauteur.

    |Coordonnées *d'une case* de 20$\times$20|Coordonnées *en pixels* de son coin supérieur gauche|
    |:---:|:---:|
    |`(x, y)`|`(TAILLE_CASE * x, TAILLE_CASE * y)`|

    où TAILLE_CASE vaut ici 20.

!!! question "Exercice 2"

    1. Quelles sont les coordonnées *en case* de la case en bas à gauche de la fenêtre ? et en bas à droite ? Répondre par un commentaire dans l'activité pyxel sur Capytale (à la fin du programme).
    2. Pour le système de coordonnées *en case* définir l'abscisse maximale `XMAX` et l'ordonnée maximale `YMAX` dans les constantes du programme (au début du programme).

??? danger "A consulter en dernier recours si vous n'arrivez pas à faire l'exercice 2"
    1. Il y a 16 cases de hauteur, la première est numérotée 0 et la dernière 15. Donc la case en bas à gauche a pour coordonnées (0, 15). Comme il y a 20 cases en largeur, la case en bas à droite a pour coordonnées (19, 15).
    2. XMAX vaut (LARGEUR // TAILLE_CASE) - 1 et YMAX vaut (HAUTEUR // TAILLE_CASE) - 1.

!!! note "Représentation du serpent"
    Nous représenterons le serpent en distinguant le carré de tête en orange et les carrés du corps en vert :

    * une liste `tete = [x, y]` donne les coordonnées *en cases* de la tête ;
    * une liste de listes : `corps = [[x1, y1], [x2, y2], ...]` où `[x1, y1]`, `[x2, y2]`, etc sont les coordonnées *en cases* des anneaux du corps.

!!! note "Fonctions de dessin utilisées ici"

    Nous n'aurons besoin que de deux fonctions de dessin :

    |Action|Fonction pyxel|
    |:---:|:---:|
    |Dessiner un rectangle de coordonnées `(x, y)`, de largeur `L`,  de hauteur `H`  et de couleur `c`|`pyxel.rect(x, y, L, H, c)`|
    |Colorier tout l'écran en noir|`pyxel.cls(0)`|

    Le module Pyxel propose une palette de 16 couleurs indexées de 0 à 15 :

    ![alt](./05_color_palette.png)

!!! question "Exercice 3"
    Choisissons un serpent défini par : `tete = [3, 3]` et `corps = [[2, 3], [1, 3]]`.

    1. Ajoutez dans l'éditeur de [l'activité pyxel sur Capytale](https://capytale2.ac-paris.fr/web/c/5b70-3671251), dans la partie "Constantes" :      
    ~~~python
    # constantes (à compléter)
    TAILLE_CASE = 20
    ORANGE = 9
    VERT = 11
    BLANC = 7
    NOIR = 0
    ~~~    
    2. Créez les variables `tete` et `corps` dans la partie "Variables globales".
    3. Copiez/collez la fonction `draw` pour qu'elle dessine le carré de tête en orange et les carrés du corps en vert puis exécutez.  
    ~~~python
    # fonction de dessin
    def draw():
        pyxel.cls(NOIR)
        # dessin du serpent
        # dessiner le corps en vert
        for anneau in ...:
            # à compléter
            ...
        # dessiner la tête en orange
        # à compléter
        ...
    ~~~
    4. Nous voulons aussi afficher un score en haut à gauche. Ajoutez dans le code une nouvelle variable globale `score` initialisée à 0 puis une instruction permettant de dessiner le score dans la fonction `draw`.

    |Action|Fonction pyxel|
    |:---:|:---:|
    |Dessiner la chaîne de caractères `s` en `(x, y)` (coordonnées en pixels) avec la couleur `c`|`pyxel.text(x, y, s, col)`|


??? danger "A consulter en dernier recours si vous n'arrivez pas à faire l'exercice 3"
    Vous pouvez utiliser le code suivant :
    ~~~python
    # import du module
    import pyxel

    # constantes (à compléter)
    LARGEUR = 400
    HAUTEUR = 320
    TAILLE_CASE = 20
    ORANGE = 9
    VERT = 11
    BLANC = 7
    NOIR = 0
    XMAX = (LARGEUR // TAILLE_CASE) - 1
    YMAX = (HAUTEUR // TAILLE_CASE) - 1

    # initialisation de la fenêtre
    pyxel.init(LARGEUR, HAUTEUR, title = "Jeu de Snake")

    # variables globales
    tete = [3,3]
    corps = [[2,3], [1,3]]
    score = 0

    # fonction de mise à jour des éléments du jeu
    def update():
        pass

    # fonction de dessin
    def draw():
        # à compléter plus tard
        pyxel.cls(NOIR)
        for anneau in corps:
            pyxel.rect(anneau[0]*TAILLE_CASE, anneau[1]*TAILLE_CASE, TAILLE_CASE, TAILLE_CASE, VERT)
        pyxel.rect(tete[0]*TAILLE_CASE, tete[1]*TAILLE_CASE, TAILLE_CASE, TAILLE_CASE, ORANGE)
        pyxel.text(1, 1, str(score), BLANC)

    # lancement du jeu
    pyxel.run(update, draw)
    ~~~


## Animer le serpent

Dans cette partie, nous allons animer le serpent en déplaçant la tête selon un certain vecteur `deplacement`. Par exemple, en appliquant le déplacement [0, -1] à une tête en [3, 3], celle-ci se retrouve en [3, 2].

!!! question "Exercice 4"
    > Pour les questions qui ne sont pas du code à compléter, répondez par un commentaire dans  [l'activité pyxel sur Capytale](https://capytale2.ac-paris.fr/web/c/5b70-3671251) en indiquant l'exercice et le numéro de la question.

    1. Considérons le serpent initial `tete = [3, 3]` et `corps = [[2, 3], [1, 3]]`. Que deviennent ces variables après un déplacement de la tête de vecteur `[0, -1]` ? 
    2. Comment accède-t-on à la queue du serpent ? 
    3. Ajouter `deplacement = [0, -1]` comme variable globale en dessous de `score`.
    4. Copiez/collez le code ci-dessous dans l'éditeur de [l'activité pyxel sur Capytale](https://capytale2.ac-paris.fr/web/c/5b70-3671251) et complétez la fonction `update` pour qu'elle mette à jour les coordonnées des parties du serpent après le mouvement de vecteur `deplacement` de la tête. Exécutez, que se passe-t-il ?     
    ~~~python
    # mise à jour des positions des objets
    def update():
        global tete, corps # pour que la fonction puisse modifier des variables globales
        # mise à jour du serpent qui avance selon le vecteur deplacement
        # supprimer la queue du serpent :
        del corps[...]
        # la tête devient le premier anneau :
        corps.insert(..., ...)
        # nouvelle tête :
        tete = ...
    ~~~
    5. 30 images par secondes (ou Frames Per Second FPS), ça donne une bonne fluidité d'affichage, mais c'est trop rapide pour le mouvement du serpent. Pour ralentir, on va utiliser le compteur de frames `pyxel.frame_count` intégré à Pyxel, en effectuant le mouvement par exemple uniquement tous les 15 frames.  
    Ajoutez la constante `FRAME_REFRESH = 15` au début avec les constantes, puis dans la fonction `update` effectuer la mise à jour des positions uniquement toutes les 15 Frames en testant la condition `pyxel.frame_count % FRAME_REFRESH == 0`. 
        
    Exécutez et vérifiez que le mouvement est plus lent.



??? danger "A consulter en dernier recours si vous n'arrivez pas à faire l'exercice 4"

    1. Après un déplacement de la tête de vecteur `[0, -1]` : la tête `tete = [3, 3]` devient `[3, 2]` et le corps `[[2, 3], [1, 3]]` devient `[[3, 3], [2, 3]]`. En fait l'ancienne tête devient le premier élément et la queue disparaît.
    2. Pour accèder à la queue du serpent : `corps[len(corps)-1` ou `corps[-1]`.
    

    Voici le code après la question 4 :
    ~~~python
    # import du module
    import pyxel

    # constantes (à compléter)
    LARGEUR = 400
    HAUTEUR = 320
    TAILLE_CASE = 20
    ORANGE = 9
    VERT = 11
    BLANC = 7
    NOIR = 0
    XMAX = (LARGEUR // TAILLE_CASE) - 1
    YMAX = (HAUTEUR // TAILLE_CASE) - 1

    # initialisation de la fenêtre
    pyxel.init(LARGEUR, HAUTEUR, title = "Jeu de Snake")

    # variables globales
    tete = [3,3]
    corps = [[2,3], [1,3]]
    score = 0
    deplacement = [0, -1]

    # mise à jour des positions des objets
    def update():
        global corps, tete
        # mise à jour du serpent qui avance selon le vecteur deplacement
        # supprimer la queue du serpent :
        del corps[len(corps)-1]
        # la tête devient le premier anneau :
        corps.insert(0, tete)
        # nouvelle tête :
        tete = [tete[0]+deplacement[0], tete[1]+deplacement[1]]

    # fonction de dessin
    def draw():
        # à compléter plus tard
        pyxel.cls(NOIR)
        for anneau in corps:
            pyxel.rect(anneau[0]*TAILLE_CASE, anneau[1]*TAILLE_CASE, TAILLE_CASE, TAILLE_CASE, VERT)
        pyxel.rect(tete[0]*TAILLE_CASE, tete[1]*TAILLE_CASE, TAILLE_CASE, TAILLE_CASE, ORANGE)
        pyxel.text(1, 1, str(score), BLANC)

    # lancement du jeu
    pyxel.run(update, draw)
    ~~~

    et celui après la question 5 :

    ~~~python
    # import du module
    import pyxel

    # constantes (à compléter)
    LARGEUR = 400
    HAUTEUR = 320
    TAILLE_CASE = 20
    ORANGE = 9
    VERT = 11
    BLANC = 7
    NOIR = 0
    XMAX = (LARGEUR // TAILLE_CASE) - 1
    YMAX = (HAUTEUR // TAILLE_CASE) - 1
    FRAME_REFRESH = 15

    # initialisation de la fenêtre
    pyxel.init(LARGEUR, HAUTEUR, title = "Jeu de Snake")

    # variables globales
    tete = [3,3]
    corps = [[2,3], [1,3]]
    score = 0
    deplacement = [0, -1]

    # mise à jour des positions des objets
    def update():
        global corps, tete
        if pyxel.frame_count % FRAME_REFRESH == 0:    
            # mise à jour du serpent qui avance selon le vecteur deplacement
            # supprimer la queue du serpent :
            del corps[len(corps)-1]
            # la tête devient le premier anneau :
            corps.insert(0, tete)
            # nouvelle tête :
            tete = [tete[0]+deplacement[0], tete[1]+deplacement[1]]

    # fonction de dessin
    def draw():
        # à compléter plus tard
        pyxel.cls(NOIR)
        for anneau in corps:
            pyxel.rect(anneau[0]*TAILLE_CASE, anneau[1]*TAILLE_CASE, TAILLE_CASE, TAILLE_CASE, VERT)
        pyxel.rect(tete[0]*TAILLE_CASE, tete[1]*TAILLE_CASE, TAILLE_CASE, TAILLE_CASE, ORANGE)
        pyxel.text(1, 1, str(score), BLANC)

    # lancement du jeu
    pyxel.run(update, draw)
    ~~~

!!! question "Exercice 5"
    Pour que le joueur puisse contrôler le mouvement du serpent en modifiant le vecteur `deplacement`, il faut réaliser l'**Étape 1** d'un jeu vidéo : _écouter les interactions du joueur_.  

    On choisit de diriger le serpent avec les quatre flèches du pavé directionnel et on écoute l'événement _appui sur la touche_.  On surveille en permanence dans la boucle implicite un  _événement_ avec un _écouteur_ et si l'événement est capturé on déclenche une action :

    ~~~python
    if ecouteur(evenement):
        action
    ~~~

    Par exemple si on détecte un appui sur la touche avec flèche vers le haut, on modifie `deplacement` en `[0, -1]` :

    ~~~python
    if pyxel.btn(pyxel.KEY_UP):
        direction = [0, -1]
    ~~~

    Voici les quatre événements correspondants aux appuis sur les touches du pavé directionnel :

    |Syntaxe|Événement|Valeur de `deplacement`|
    |:---:|:---:|:---:|
    |`pyxel.KEY_RIGHT`|Appui sur Flèche ➡️|`[1, 0]`|
    |`pyxel.KEY_LEFT`|Appui sur Flèche ⬅️|...|
    |`pyxel.KEY_UP`|Appui sur Flèche ⬆️|`[0, -1]`|
    |`pyxel.KEY_DOWN`|Appui sur Flèche ⬇️|...|

    Modifiez dans [l'activité pyxel sur Capytale](https://capytale2.ac-paris.fr/web/c/5b70-3671251) la fonction `update` avec tous les tests d'écouteurs d'événements qui vont permettre de diriger le serpent au clavier.

    ~~~python
    # mise à jour des positions des objets
    def update():
        global corps, tete, deplacement

        if pyxel.frame_count % FRAME_REFRESH == 0:
            ...
        if pyxel.btn(pyxel.KEY_UP):
            deplacement = [0, -1]
        # compléter avec les tests pour les  trois autres écouteurs d'événements
    ~~~

??? danger "A consulter en dernier recours si vous n'arrivez pas à faire l'exercice 5"
    Vous pouvez utiliser le code suivant :
    ~~~python
    # import du module
    import pyxel

    # constantes (à compléter)
    LARGEUR = 400
    HAUTEUR = 320
    TAILLE_CASE = 20
    ORANGE = 9
    VERT = 11
    BLANC = 7
    NOIR = 0
    XMAX = (LARGEUR // TAILLE_CASE) - 1
    YMAX = (HAUTEUR // TAILLE_CASE) - 1
    FRAME_REFRESH = 15

    # initialisation de la fenêtre
    pyxel.init(LARGEUR, HAUTEUR, title = "Jeu de Snake")

    # variables globales
    tete = [3,3]
    corps = [[2,3], [1,3]]
    score = 0
    deplacement = [0, -1]

    # mise à jour des positions des objets
    def update():
        global corps, tete, deplacement
        if pyxel.frame_count % FRAME_REFRESH == 0:    
            # mise à jour du serpent qui avance selon le vecteur deplacement
            # supprimer la queue du serpent :
            del corps[len(corps)-1]
            # la tête devient le premier anneau :
            corps.insert(0, tete)
            # nouvelle tête :
            tete = [tete[0]+deplacement[0], tete[1]+deplacement[1]]
        if pyxel.btn(pyxel.KEY_UP):
            deplacement = [0, -1]
        if pyxel.btn(pyxel.KEY_DOWN):
            deplacement = [0, 1]
        if pyxel.btn(pyxel.KEY_LEFT):
            deplacement = [-1, 0]
        if pyxel.btn(pyxel.KEY_RIGHT):
            deplacement = [1, 0]

    # fonction de dessin
    def draw():
        # à compléter plus tard
        pyxel.cls(NOIR)
        for anneau in corps:
            pyxel.rect(anneau[0]*TAILLE_CASE, anneau[1]*TAILLE_CASE, TAILLE_CASE, TAILLE_CASE, VERT)
        pyxel.rect(tete[0]*TAILLE_CASE, tete[1]*TAILLE_CASE, TAILLE_CASE, TAILLE_CASE, ORANGE)
        pyxel.text(1, 1, str(score), BLANC)

    # lancement du jeu
    pyxel.run(update, draw)
    ~~~


## Faire mourir le serpent

!!! question "Exercice 6"

    > Pour les questions qui ne sont pas du code à compléter, répondez par un commentaire dans  [l'activité pyxel sur Capytale](https://capytale2.ac-paris.fr/web/c/5b70-3671251) en indiquant l'exercice et le numéro de la question.

    Dans notre version du jeu : le serpent meurt lorsque la tête touche le corps, ou lorsqu'il quitte l'écran. Dans ce cas, le jeu s'arrête, et on quitte la fenêtre avec `pyxel.quit()`.

    1.     
        * _(C1)_ quelle expression permet de tester si les coordonnées de la tête apparaissent  dans le reste du corps du serpent ?
        * _(C2)_  quelle expression permet de tester si l'abscisse de la tête n'est pas dans la fenêtre ?
        * _(C3)_ quelle expression permet de tester si l'ordonnée de la tête n'est pas dans la fenêtre ?
    2. Complétez la fonction `update` avec un test qui déclenche une fermeture de la fenêtre si l'une des conditions précédentes est vérifiée.
    3. Testez avec un serpent un peu plus grand...


??? danger "A consulter en dernier recours si vous n'arrivez pas à faire l'exercice 6"
    1.  
        * _(C1)_ les coordonnées de la tête apparaissent dans le reste du corps du serpent : `tete in corps`
        * _(C2)_ l'abscisse de la tête n'est pas dans la fenêtre : `tete[0]<0 or tete[0]>XMAX`
        * _(C3)_ l'ordonnée de la tête n'est pas dans la fenêtre : `tete[1]<0 or tete[1]>YMAX`

    Voici le code :
    ~~~python
    # import du module
    import pyxel

    # constantes
    LARGEUR = 400
    HAUTEUR = 320
    TAILLE_CASE = 20
    ORANGE = 9
    VERT = 11
    BLANC = 7
    NOIR = 0
    XMAX = (LARGEUR // TAILLE_CASE) - 1
    YMAX = (HAUTEUR // TAILLE_CASE) - 1
    FRAME_REFRESH = 15

    # initialisation de la fenêtre
    pyxel.init(LARGEUR, HAUTEUR, title = "Jeu de Snake")

    # variables globales
    tete = [3,3]
    corps = [[2,3], [1,3]]
    score = 0
    deplacement = [0, -1]

    # mise à jour des positions des objets
    def update():
        global corps, tete, deplacement
        if pyxel.frame_count % FRAME_REFRESH == 0:    
            # mise à jour du serpent qui avance selon le vecteur deplacement
            # supprimer la queue du serpent :
            del corps[len(corps)-1]
            # la tête devient le premier anneau :
            corps.insert(0, tete)
            # nouvelle tête :
            tete = [tete[0]+deplacement[0], tete[1]+deplacement[1]]
        if pyxel.btn(pyxel.KEY_UP):
            deplacement = [0, -1]
        if pyxel.btn(pyxel.KEY_DOWN):
            deplacement = [0, 1]
        if pyxel.btn(pyxel.KEY_LEFT):
            deplacement = [-1, 0]
        if pyxel.btn(pyxel.KEY_RIGHT):
            deplacement = [1, 0]
        if tete in corps or tete[0]<0 or tete[0]>XMAX or tete[1]<0 or tete[1]>YMAX:
            pyxel.quit()

    # fonction de dessin
    def draw():
        # à compléter plus tard
        pyxel.cls(NOIR)
        for anneau in corps:
            pyxel.rect(anneau[0]*TAILLE_CASE, anneau[1]*TAILLE_CASE, TAILLE_CASE, TAILLE_CASE, VERT)
        pyxel.rect(tete[0]*TAILLE_CASE, tete[1]*TAILLE_CASE, TAILLE_CASE, TAILLE_CASE, ORANGE)
        pyxel.text(1, 1, str(score), BLANC)

    # lancement du jeu
    pyxel.run(update, draw)
    ~~~



## Manger des pommes

!!! question "Exercice 7"
    > Pour les questions qui ne sont pas du code à compléter, répondez par un commentaire dans  [l'activité pyxel sur Capytale](https://capytale2.ac-paris.fr/web/c/5b70-3671251) en indiquant l'exercice et le numéro de la question.

    On place une pomme, matérialisée par une case magenta (couleur 8), au hasard dans la fenêtre. Lorsque le serpent mange la pomme, il grandit d'un anneau (sa queue n'est pas effacée), et le score augmente de 1.  
    On importera le module `random` au début du programme avec `import random`.

    Si le serpent mange la pomme, pour en placer une nouvelle, on utilisera le code suivant :

    ~~~python
    while pomme in corps or pomme == tete:
        x_pomme = random.randint(0, XMAX)
        y_pomme = random.randint(0, YMAX)
    pomme = [x_pomme, y_pomme]
    ~~~

    1. Expliquez le code précédent.
    2. Définissez une nouvelle variable globale `pomme` avec les coordonnées de la pomme :
        ~~~python
        # variables globales
        tete = [3, 3]
        corps = [[2, 3], [1, 3]]
        score = 0
        deplacement = [1, 0]
        pomme = [7, 5]
        ~~~
        a. Complétez la fonction `draw` pour dessiner la pomme.
        
        b. Complétez la fonction `update` : si la tête du serpent se trouve sur la pomme alors il grandit d'un anneau et le score augmente de 1 (déclarer `score` avec `global` dans `update`), de plus il faut créer une nouvelle pomme.


??? danger "A consulter en dernier recours si vous n'arrivez pas à faire l'exercice 7"
    1. Tant que la position choisie pour la pomme est à l'intérieur du corps et dans la tête du serpent, re-choisir une position.

    Vous pouvez utiliser le code suivant :
    ~~~python

    # import des modules
    import pyxel
    import random

    # constantes (à compléter)
    LARGEUR = 400
    HAUTEUR = 320
    TAILLE_CASE = 20
    ORANGE = 9
    VERT = 11
    BLANC = 7
    NOIR = 0
    MAGENTA = 8
    XMAX = (LARGEUR // TAILLE_CASE) - 1
    YMAX = (HAUTEUR // TAILLE_CASE) - 1
    FRAME_REFRESH = 15

    # initialisation de la fenêtre
    pyxel.init(LARGEUR, HAUTEUR, title = "Jeu de Snake")

    # variables globales
    tete = [3,3]
    corps = [[2,3], [1,3]]
    score = 0
    deplacement = [0, -1]
    pomme = [7, 5]

    # mise à jour des positions des objets
    def update():
        global corps, tete, deplacement, pomme, score
        if pyxel.frame_count % FRAME_REFRESH == 0:    
            # mise à jour du serpent qui avance selon le vecteur deplacement
            # supprimer la queue du serpent :
            del corps[len(corps)-1]
            # la tête devient le premier anneau :
            corps.insert(0, tete)
            # nouvelle tête :
            tete = [tete[0]+deplacement[0], tete[1]+deplacement[1]]
        if pyxel.btn(pyxel.KEY_UP):
            deplacement = [0, -1]
        if pyxel.btn(pyxel.KEY_DOWN):
            deplacement = [0, 1]
        if pyxel.btn(pyxel.KEY_LEFT):
            deplacement = [-1, 0]
        if pyxel.btn(pyxel.KEY_RIGHT):
            deplacement = [1, 0]
        if tete in corps or tete[0]<0 or tete[0]>XMAX or tete[1]<0 or tete[1]>YMAX:
            pyxel.quit()
        if tete == pomme:
            # choisir une nouvelle pomme
            while pomme in corps or pomme == tete:
                x_pomme = random.randint(0, XMAX)
                y_pomme = random.randint(0, YMAX)
                pomme = [x_pomme, y_pomme]
            # ajouter un anneau au serpent (en utilisant le déplacement actuel de celui-ci)
            dernier_anneau = corps[len(corps)-1]
            nouvel_anneau = [dernier_anneau[0]-deplacement[0], dernier_anneau[1]-deplacement[1]]
            corps.append(nouvel_anneau)
            # augmenter le score
            score = score + 1        

    # fonction de dessin
    def draw():
        pyxel.cls(NOIR)
        for anneau in corps:
            pyxel.rect(anneau[0]*TAILLE_CASE, anneau[1]*TAILLE_CASE, TAILLE_CASE, TAILLE_CASE, VERT)
        pyxel.rect(tete[0]*TAILLE_CASE, tete[1]*TAILLE_CASE, TAILLE_CASE, TAILLE_CASE, ORANGE)
        pyxel.rect(pomme[0]*TAILLE_CASE, pomme[1]*TAILLE_CASE, TAILLE_CASE, TAILLE_CASE, MAGENTA)
        pyxel.text(1, 1, str(score), BLANC)

    # lancement du jeu
    pyxel.run(update, draw)
    ~~~

!!! question "Pour finir"
    Vous pouvez bien sûr modifier ce jeu, changer la vitesse d'exécution, l'épaisseur du serpent et de la pomme, ajouter un écran de Game Over, affecter des touches pour changer la vitesse du serpent, faire en sorte qu'il accélère à chaque fois qu'il mange une pomme, etc. !