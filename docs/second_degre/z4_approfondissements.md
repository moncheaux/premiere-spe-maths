# Approfondissements
## Factorisation d’un polynôme du troisième degré admettant une racine et résolution de l’équation associée.

!!! abstract "Propriété"
    Si un polynôme de degré \(n\) s'annule en un nombre \(k\) alors il se factorise en \((x-k)Q(x)\) où $Q$ est aussi un polynôme de degré \(n-1\).



!!! example "Exemple"

    === "Questions"
        Soit $f$ la fonction définie sur IR par 

        \[f(x)=15x^3+46x^2+25x-14.\]

        1. Calculez $f(-2)$.
        2. Écrivez $f(x)$ comme produit de fonctions affines.
        3. Déduisez-en les solutions de $15x^3+46x^2+25x-14=0$.

    === "Réponse question 1"
        $f(-2)=15(-2)^3+46(-2)^2+25(-2)-14 = -120 +184-50-14 = 0$
        
    === "Réponse question 2"
        
        Comme $-2$ est une racine de $f(x)$, on peut écrire $f(x)=(x+2)g(x)$ où $g$ est une fonction de degré 2.
        Donc :  
        $15x^3+46x^2+25x-14 = (x+2)(ax^2+bx+c) = ax^3+(2a+b)x^2+(c+2b)x+2c$.

        Par identification :
        $a=15$ ; $2a+b=46$ ; $c+2b=25$ ; $2c=-14$

        ce qui donne $b=16$ et $c=-7$ donc $g(x)=15x^2+16x-7$.

        Ce polynôme du second degré se factorise en (calculer le discriminant $\Delta$, etc.) $a(x-x_1)(x-x_2)=15\left (x-\dfrac{1}{3}\right )\left (x+\dfrac{7}{5}\right )=(3x - 1) (5x + 7)$.

        Conclusion : $f(x)=(x+2)(3x-1)(5x+7)$.

    === "Réponse question 3"

        Donc $f(x)=0 \iff (x + 2) (3x - 1) (5x + 7)=0
        \iff x + 2=0 \text{ ou } 3x - 1 =0 \text{ ou } 5x + 7=0
        \iff x=-2 \text{ ou } x=1/3 \text{ ou } x=-7/5$.          
        L'ensemble des solutions est donc :
        \(S=\lbrace-2;1/3;-7/5\rbrace\).









## Factorisation de $x^n-1$ par $x-1$, de $x^n-a^n$ par $x-a$.
La propriété précédente s'applique aux polynômes 
\(x^n-1\) qui s'annule en 1 donc est factorisable par \(x - 1\) 
et à \(x^n - a^n\) qui s'annule en \(a\) donc est factorisable par \(x - a\).


!!! example "Exemple"

    === "Questions"
        Factorisez \(x^5-1\).

    === "Réponse"
        \(x^5-1=(x-1)Q(x)\) où $Q$ est de degré 4 donc $Q(x)=ax^4+bx^3+cx^2+dx+e$.          
        Donc :  
        \(x^5-1=(x-1)(ax^4+bx^3+cx^2+dx+e) = ax^5+bx^4+cx^3+dx^2+ex - ax^4-bx^3-cx^2-dx-e\)

        Par identification :
        $a=1$ ; $b-a=0$ ; $c-b=0$ ; $d-c=0$ ; $e-d=0$ et $-1=-e$
        d'où $a=b=c=d=e=1$ donc $Q(x)=x^4+x^3+x^2+x+1$.

        Conclusion : $x^5-1 =(x-1)(x^4+x^3+x^2+x+1)$.


!!! example "Exemple"

    === "Questions"
        Factorisez \(x^3-8\).

    === "Réponse"
        Remarquez d'abord que \(x^3-8=x^3-2^3 = (x-2)Q(x)\) où $Q$ est de degré 2 donc $Q(x)=ax^2+bx+c$.          
        Donc :  
        \(x^3-8=(x-2)(ax^2+bx+c) = ax^3+bx^2+cx-2ax^2-2bx-2c\)

        Par identification :
        $a=1$ ; $b-2a=0$ ; $c-2b=0$ et $-8=-2c$
        d'où $c=4$ ; $b=2a=2$ donc $Q(x)=x^2+2x+4$.

        Conclusion : $x^3-8 =(x-2)(x^2+2x+4)$.


## Déterminer deux nombres réels connaissant leur somme s et leur produit p comme racines de la fonction polynôme $x \mapsto x^2 - sx + p$.

Nous cherchons deux nombres $x$ et $y$ dont nous connaissons la somme $s$ et le produit $p$. Cela revient à résoudre le système :

$\left\lbrace\begin{array}{l}x+y=s\\xy=p\end{array}\right.$

Transformons ceci : $y=s-x$ donc $x(s-x)=p$ ou encore $x^2-sx+p=0$.

Il suffit alors de résoudre cette équation du second degré pour trouver $x$ puis $y$.

!!! tip "Remarque"
    $x$ et $y$ peuvent être permutés dans le système donc si un couple ($a$ ; $b$) est solution alors le couple ($b$ ; $a$) est aussi solution. 

!!! example "Exemple"

    === "Question"
        Trouver les dimensions d'un rectangle dont l'aire est 8 et le périmètre est 14.

    === "Réponse"
        Ici, $s=7$ et $p=8$. Résolvons donc $x^2-7x+8=0$ :  
        \(\Delta = (-7)^{2}-4\times 1 \times 8=49-32=17\).
        Comme \( \Delta>0 \), l'équation a deux solutions :  
        $x_1=\dfrac{-b-\sqrt{\Delta}}{2\,a}=\dfrac{7-\sqrt{17}}{2}$ et $x_2=\dfrac{7+\sqrt{17}}{2}$.

        Il reste à calculer l'autre dimension : $x+y=7$ donc $y=7-x$, ce qui donne 
        $y_1=7-x_1 = \dfrac{14}{2}-\dfrac{7-\sqrt{17}}{2}=\dfrac{7+\sqrt{17}}{2}$ et $y_2=7-x_ 2=\dfrac{7-\sqrt{17}}{2}$.

        Les dimensions du rectangle sont donc $\dfrac{7-\sqrt{17}}{2}$ en largeur et $\dfrac{7+\sqrt{17}}{2}$ en longueur.