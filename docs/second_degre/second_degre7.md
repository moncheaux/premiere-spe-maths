# Applications de la forme canonique
## Extremum d'une fonction du second degré

La forme canonique permet de trouver le maximum ou le minimum (suivant les cas) d'une fonction du second degré.

!!! example "Exemple III.4"

    === "Questions"
        1. Vérifier que \(2\,x^{2}-6\,x-20=2(x-1,5)^2 -24,5\).
        2. Trouver l'extremum de \(f(x)=2\,x^{2}-6\,x-20\).

    === "Réponse question 1"
        \(2(x-1,5)^2 -24,5
        =2(x^2-3\,x+2,25) -24,5
        =2\,x^2-6\,x+4,5 -24,5
        =2\,x^2-6\,x-20
        \)
        (ou utilisez la complétion du carré).

    === "Réponse question 2"
        Dans la forme canonique  
        \(2(x-1,5)^2 -24,5\),
        nous voyons que 
        \((x-1,5)^2\) est toujours positif 
        donc \(2(x-1,5)^2\) aussi.
        Par conséquent, \(f(x)=2(x-1,5)^2 -24,5 \geqslant -24,5\).
        
        Enfin, \(f(1,5)=2(1,5-1,5)^2 -24,5 = -24,5\)
        donc la valeur \(-24,5\) est atteinte : c'est donc le minimum de la fonction et il est atteint en 1,5.


!!! example "Exemple III.5"

    === "Question"
        1. Trouver la forme canonique de \(-x^{2}+6\,x+5\).
        2. Trouver l'extremum de \(g(x)=-x^{2}+6\,x+5\).

    === "Réponse 1."
        \(-x^{2}+6\,x+5=-(x^2-6\,x)+5
        =-((x-3)^2 -3^2)+5
        =-(x-3)^2 +9+5
        =-(x-3)^2 +14
        \).

    === "Réponse 2."
        \((x-3)^2\) est toujours positif donc \(-(x-3)^2\) est toujours négatif d'où \(g(x)=-(x-3)^2 +14 \leqslant 14\). 
        De plus, \(g(3)=14\) donc la fonction \(g\) a un maximum égal à 14 et atteint en 3.


## Résolution des équations du second degré

!!! danger "Attention"
    Les équations du second degré ne se résolvent pas comme celles du premier degré (il est plus difficile d'isoler l'inconnue $x$).


La connaissance de la forme canonique permet aussi de trouver les racines éventuelles du polynôme.

!!! abstract "Théorème"

    L'équation \(a\,x^2+b\,x+c=0\) avec \(a\neq 0\) :

    * n'a pas de solution dans IR si \( \Delta <0 \) ;  
    * a une seule solution (dite "double") 
    <span style="color:red">$\boxed{x_0=-\dfrac{b}{2\,a}}$</span> si \( \Delta =0 \) ;  
    * a deux solutions 
    <span style="color:red">$\boxed{x_1=\dfrac{-b-\sqrt{\Delta}}{2\,a}}$</span> et <span style="color:red">$\boxed{x_2=\dfrac{-b+\sqrt{\Delta}}{2\,a}}$</span>
    si \( \Delta>0 \).
    

!!! abstract "Preuve à connaître"

    Utilisons la forme canonique :

    \(
    a\,x^2+b\,x+c=0
    \iff
    a\left(x+\dfrac{b}{2\,a}\right)^2 -\dfrac{\Delta}{4\,a}=0
    \iff
    a\left(x+\dfrac{b}{2\,a}\right)^2 =\dfrac{\Delta}{4\,a}
    \iff
    \left(x+\dfrac{b}{2\,a}\right)^2 =\dfrac{\Delta}{4\,a^2}
    \)

    Il faut maintenant éliminer le carré mais trois cas se présentent :

    * si \(\Delta\) est strictement négatif alors \(\dfrac{\Delta}{4\,a^2}\) aussi mais \(\left(x+\dfrac{b}{2\,a}\right)^2\) est positif : l'équation n'a pas de solution ;
    * si \(\Delta=0\) alors \(\dfrac{\Delta}{4\,a^2}=0\) aussi donc  \(\left(x+\dfrac{b}{2\,a}\right)^2=0\)
    \(x+\dfrac{b}{2\,a}=0\)
    ce qui donne une solution unique : \(x=-\dfrac{b}{2\,a}\)
    * si \(\Delta\) est strictement positif alors \(\dfrac{\Delta}{4\,a^2}\) aussi donc \(
    \left(x+\dfrac{b}{2\,a}\right)^2=\dfrac{\Delta}{4\,a^2}
    \iff 
    x+\dfrac{b}{2\,a}=\pm\sqrt{\dfrac{\Delta}{4\,a^2}} = 
    \pm\dfrac{\sqrt{\Delta}}{2\,a}
    \)
    ce qui donne deux solutions : 
    \(x=-\dfrac{b}{2\,a}\pm\dfrac{\sqrt{\Delta}}{2\,a} = \dfrac{-b\pm\sqrt{\Delta}}{2\,a}\)


!!! tip "Remarques"
    * le discriminant $\Delta=b^2-4\,a\,c$ permet donc de savoir dans quel cas l'on se trouve, d'où son nom ; 
    * en remplaçant $\Delta$ par 0 dans les formules, nous trouvons $x_1=x_2=-\dfrac{b}{2\,a}=x_0$ (cela fait une formule en moins à apprendre...)

!!! example "Exemple III.6"

    === "Question"
        Résoudre \(2\,x^{2}-6\,x-20=0\).

    === "Réponse"
        \(2\,x^{2}-6\,x-20=0\) est de la forme \(a\,x^{2}+b\,x+c=0\) avec \(a=2\), \(b=-6\) et \(c=-20\) donc 
        \(\Delta = (-6)^{2}-4\times 2 \times (-20)=36+160=196\).
        Comme \( \Delta>0 \), l'équation a deux solutions :  
        $x_1=\dfrac{-b-\sqrt{\Delta}}{2\,a}=\dfrac{-(-6)-\sqrt{196}}{2\times 2} = \dfrac{6-14}{4}=-2$ et $x_2=\dfrac{-b+\sqrt{\Delta}}{2\,a}=\dfrac{-(-6)+\sqrt{196}}{2\times 2} = \dfrac{20}{4}=5$.

        L'ensemble des solutions est donc : $S=\{-2;5\}$.


!!! tip "Remarque"
    Pensez à vérifier vos solutions, surtout lorsqu'elles sont simples comme dans l'exemple ci-dessus :
    \(2\times (-2)^{2}-6\times(-2)-20=8+12-20=0\) et \(2\times 5^{2}-6\times 5-20=50-30-20=0\).

!!! question "Exercices"
    Transmath :  
    55 à 60 page 77
    103 page 78  
    112 page 79  
    125, 126 page 81  
    130 page 82  
    + 133 page 82 pour les rapides

!!! tip "Remarque"
    Dans certains cas, nous n'avons pas besoin des formules précédentes pour résoudre une équation du second degré.

!!! question "Exercices"
    Transmath : 69 à 72 page 77 ; 82 page 77

!!! question "Exercices sur les équations bicarrées"
    Transmath : 87, 88 page 78

## Factorisation

!!! abstract "Théorème : factorisation"

    Le polynôme \(a\,x^2+b\,x+c=0\) :

    * ne peut pas être factorisé dans IR si \( \Delta <0 \) ;  
    * se factorise en <span style="color:red">$\boxed{a(x-x_0)^2}$</span> si \( \Delta =0 \) ;  
    * se factorise en 
    <span style="color:red">$\boxed{a(x-x_1)(x-x_2)}$</span>
    si \( \Delta>0 \).


!!! example "Exemple III.7"

    === "Question"
        Factorisez \(5\,x^{2}+25\,x-120\).

    === "Réponse"
        \(5\,x^{2}+25\,x-120\) est de la forme \(a\,x^{2}+b\,x+c\) avec \(a=5\), \(b=25\) et \(c=-120\) donc 
        \(\Delta = 25^{2}-4\times 5 \times (-120)=625+2400=3025=55^2\).
        Comme \( \Delta>0 \), l'équation a deux solutions :  
        $x_1=\dfrac{-25-\sqrt{3025}}{2\times 5} = \dfrac{-25-55}{10}=-8$ et $x_2=\dfrac{-b+\sqrt{\Delta}}{2\,a}=\dfrac{-25+55}{10} = 3$.

        Donc \(5\,x^{2}+25\,x-120=5(x+8)(x-3)\).


!!! question "Exercice"
    Factorisez si possible les polynômes suivants :

    * \(-x^2-x+6\)
    * \(2\,x^2-12\,x+18\)
    * \(x^2+x+1\)
    * \(7\,x^2-1\)


## Signe d'une fonction du second degré

!!! abstract "Propriété"

    * si \( \Delta <0 \) :  

    <div style="color:red">

    \[\begin{array}{c|lp{3cm}r}
    x & -\infty & & +\infty \\\hline
    a\,x^{2}+b\,x+c & & \text{signe de } a \\ \end{array}\]

    </div>

    * si \( \Delta =0 \) :

    <div style="color:red">

    \[\begin{array}{c|lp{3cm}cp{3cm}r}
    x & -\infty & & x_0 & & +\infty \\\hline
    a\,x^{2}+b\,x+c & & \text{signe de } a&0& \text{signe de } a \\ \end{array}\]

    </div>

    * si \( \Delta>0 \) : 

    <div style="color:red">

    \[\begin{array}{c|lp{3cm}cccp{3cm}r}
    x & -\infty & & x_1 & & x_2 & & +\infty \\\hline
    a\,x^{2}+b\,x+c & & \text{signe de } a&0& \text{signe de }-a&0&\text{signe de } a& \\ \end{array}\]

    </div>


!!! tip "À retenir"

    La fonction a <span style="color:red">le signe de \(a\) quand \(x\) est _à l'extérieur des racines_</span>.


!!! question "Exercice"
    Faîtes les tableaux de signe des polynômes suivants :

    * \(x^2+5\,x-6\)
    * \(-2\,x^2+8\,x-8\)
    * \(3\,x^2+x+4\)


## Résolution d'inéquations du second degré

!!! danger "Attention"
    Les inéquations du second degré ne se résolvent pas comme celles du premier degré.


!!! tip "Remarque"
    Puisque nous savons maintenant faire le tableau de signe d'un polynôme du second degré, nous savons également résoudre des inéquations du second degré !

!!! example "Exemple III.8"

    === "Question"
        Résoudre \(3\,x^{2}+11\,x-47>-2\,x^2-14\,x+73\).

    === "Réponse"
        Remarquons que 
        \(3\,x^{2}+11\,x-47>-2\,x^2-14\,x+73
        \iff
        5\,x^{2}+25\,x-120>0\).

        D'après l'exemple précédent, les racines de \(5\,x^{2}+25\,x-120\) sont \(-8\) et \(3\) et le coefficient \(a=5\) est positif donc le tableau de signe de \(5\,x^{2}+25\,x-120\) est :

        \(
        \begin{array}{c|lp{8mm}cccp{8mm}r}
        x & -\infty & & -8 & & 3 & & +\infty
        \\\hline
        5\,x^{2}+25\,x-120 & & + & 0 & - & 0 & + &
        \end{array}
        \)

        duquel nous déduisons que \(5\,x^{2}+25\,x-120>0 \iff x<-8 \text{ ou }x>3\) donc l'ensemble des solutions est 
        \(S=]-\infty;-8[\cup]3;+\infty[\).


!!! example "Exemple III.9"

    === "Question"
        Résoudre \(x^2+x+1>0\).

    === "Réponse"

        Le discriminant est \(\Delta = -3\), il est négatif donc \(x^2+x+1\) a toujours le signe de \(a=1\) donc le tableau de signe de \(x^2+x+1\) est :

        \(
        \begin{array}{c|lcr}
        x & -\infty & & +\infty
        \\\hline
        x^2+x+1 & & + & 
        \end{array}
        \)

        duquel nous déduisons que \(x^2+x+1>0\) pour tout réel \(x\) donc l'ensemble des solutions est 
        \(S=]-\infty;+\infty[\) (donc IR).


!!! danger "Attention"
    * \(\Delta\) est négatif donc **l'équation** \(x^2+x+1=0\) n'a pas de solution mais **l'inéquation** \(x^2+x+1>0\) en a ! 
    * par contre, d'après le tableau de signe, si l'inéquation avait été \(x^2+x+1<0\) alors elle n'aurait pas eu de solution.

!!! question "Exercices"
    Transmath : 61 à 63 page 77 ; 66 page 77
