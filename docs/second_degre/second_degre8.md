# Vérifications graphiques
Les solutions peuvent facilement être vérifiées à l'aide d'un logiciel comme Geogebra ou avec une calculatrice graphique.

!!! example "Exemple III.10"
     Équation \(x^{2}-5\,x+6=0\). Les formules donnent deux solutions : 2 et 3.

     Traçons la courbe de la fonction définie sur IR par \(f(x)=x^{2}-5\,x+6\) :

     ![](images/numworks1.png)![](images/numworks2.png)

     celle-ci s'annule bien (\(y\) vaut 0) quand \(x=2\) ou quand \(x=3\).


!!! example "Exemple III.11"
     Équation \(x^{2}-5\,x+6> 0\). Un tableau de signe nous montrerait que les solutions sont les nombres \(x\) inférieurs à 2 ou supérieurs à 3.

     Or sur la courbe de la fonction définie sur IR par \(f(x)=x^{2}-5\,x+6\) :

     ![](images/numworks2.png)

     les points qui ont une ordonnée strictment positive (\(y>0\)) vérifient bien \(x<2\) ou \(x>3\).

!!! question "Exercice"
     Résolvez par lecture graphique :

     * \(4\,x^2+10\,x-6=0\)
     * \(4\,x^2+10\,x-6>0\)
     * \(4\,x^2+10\,x-6\leqslant 0\)
     * \(4\,x^2-x+1=2\,x+8\) de deux façons : avec deux courbes puis avec une seule courbe
     * \(4\,x^2-x+1 \geqslant 2\,x+8\) de deux façons : avec deux courbes puis avec une seule courbe

!!! danger "Attention"
     Évidemment, à moins qu'un énoncé ne spécifie "par lecture graphique" comme ci-dessus, la résolution doit d'abord se faire à la main...
