# II - Forme factorisée d'une expression du second degré

!!! abstract "Définition"

    Une fonction définie sur IR par :

    <div style="color:red">

    \[
    \boxed{f(x)=a(x-x_1)(x-x_2)}
    \]

    </div>

    où $a$, $x_1$, $x_2$ sont des réels fixés, avec $a$ non nul.

    est une fonction du second degré sous <span style="color:red">forme factorisée</span>.


!!! example "Exemple II.1"

    \(-2 \left(x+2\right)\left(x+4\right)\) est la forme factorisée de \(-2\,x^2-12\,x-16\). En effet, en distribuant :


    \[
        -2 \left(x+2\right)\left(x+4\right)
        =
        -2 \left(x^2+4\,x+2\,x+8\right)
        =-2\,x^2-12\,x-16.
    \]


!!! tip "Remarque"
    Certaines fonctions du second degré n'ont pas de forme factorisée (dans IR).
