# I - Fonctions du second degré, forme développée

!!! abstract "Définition" 

    Une <span style="color:red">fonction polynôme du second degré</span> est définie sur IR par

    <div style="color:red">

    \[
    \boxed{f(x)=a\,x^2 + b\,x + c}
    \]

    </div>

    où $a$, $b$, $c$ sont des réels fixés, avec $a$ non nul.


!!! tip "Remarques"

    * une telle fonction est aussi appelée fonction de degré 2 ou trinôme (trois termes) du second degré ; 
    * l'expression \(a,x^2 + b,x + c\) est la <span style="color:red">forme développée</span>, nous verrons qu'il existe deux autres formes ; 
    * si $a$ était nul, nous aurions affaire à une fonction affine.

!!! question "Exercice"

    Dîtes si les expressions suivantes sont celles de fonctions du second degré et, si oui, indiquez les valeurs de $a$, $b$, $c$ :

    === "Propositions"

        - [ ] \(3x-7\), si oui \(a=\dots\), \(b=\dots\) et \(c=\dots\)
        - [ ] \(-3x^2+2x-1\), si oui \(a=\dots\), \(b=\dots\) et \(c=\dots\)
        - [ ] \(2x^2-4x\), si oui \(a=\dots\), \(b=\dots\) et \(c=\dots\)
        - [ ] \(2x^3-7x^2+1\), si oui \(a=\dots\), \(b=\dots\) et \(c=\dots\)
        - [ ] \(1+2x^2\), si oui \(a=\dots\), \(b=\dots\) et \(c=\dots\)


    === "Solution"

        - :x: ~~\(3x-7\)~~ : il n'y a pas de terme en \(x^2\)
        - :white_check_mark: \(-3x^2+2x-1\) avec \(a=-3\), \(b=2\) et \(c=-1\)
        - :white_check_mark: \(2x^2-4x\) avec \(a=2\), \(b=-4\) et \(c=0\)
        - :x: \(2x^3-7x^2+1\) : c'est un polynome du 3^ème^ degré
        - :white_check_mark: \(1+2x^2\) avec \(a=2\), \(b=0\) et \(c=1\)

!!! tip "Courbe"
    La courbe d'une fonction du second degré est une <span style="color:red">parabole</span>.


<iframe scrolling="no" title="Formes développée, canonique et factorisée" src="https://www.geogebra.org/material/iframe/id/fctefraf/width/1200/height/700/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/true/ctl/false" width="1200px" height="500px" style="border:0px;"> </iframe>
