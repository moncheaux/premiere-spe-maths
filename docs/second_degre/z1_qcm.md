# ⁉️ QCM
!!! tip "Attention"
    Il peut y avoir plusieurs réponses correctes à chaque question !


#### Q1 - Parmi les formes suivantes, lesquelles sont des formes canoniques ?
{{ qcm(["$3(x-2)^2+4$", "$2-4(x+1)^2$", "$-[(x-1)^2+3]$", "$3\,x^2+4\,x+2$", "$3(x-2)(x+3)$", "$4\,x^2+x$"], [1,2,3], shuffle = True) }}

#### Q2 - Parmi les formes suivantes, lesquelles sont des formes factorisées ?
{{ qcm(["$-(x+1)(x-5)$", "$x(x-4)$", "$4-(x-5)^2$", "$3\,x^2-x+2$", "$5\,x^2-x$", "$(x-2)^2-9$"], [1,2], shuffle = True) }}

#### Q3 - Le discriminant de $1+2\,x^2-3\,x$ est :
{{ qcm(["1", "16", "$-17$", "25"], [1], shuffle = True) }}

#### Q4 - L'équation $1+2\,x^2=3\,x$ a :
{{ qcm(["une solution", "deux solutions", "aucune solution", "trois solutions"], [2], shuffle = True) }}

#### Q5 - L'équation $-x^2-3\,x+10=0$ a :
{{ qcm(["pour solutions 2 et $-5$", "pour solutions 5 et $-2$", "pour solutions 2 et 3", "n'a pas de solution"], [1], shuffle = True) }}

#### Q6 - L'équation $3\,x^2-2\,x+1=0$ a :
{{ qcm(["pour solutions 1/3 et $-1$", "pour solutions 1 et $-1/3$", "pour solution 1", "n'a pas de solution"], [4], shuffle = True) }}

#### Q7 - Nous admettons que 1 est une racine de $3\,x^2-2\,x-1$. Alors l'autre racine est :
{{ qcm(["$-1/3$", "3", "2/3", "$-4/3$"], [1], shuffle = True) }}

#### Q8 - Nous admettons que 1 et $-7/2$ sont les racines de $2\,x^2+5\,x-7$. Alors l'inéquation $2\,x^2+5\,x-7 \geqslant 0$ a pour ensemble de solutions :
{{ qcm(["$]-\infty;-7/2]\cup[1;+\infty[$", "$[-7/2;1]$", "$]-\infty;-7/2[\cup]1;+\infty[$", "$]-7/2;1[$", "IR", "$\emptyset$"], [1], shuffle = True) }}