# Des vidéos d'explications

Les vidéos suivantes font partie d'une liste qui en comporte beaucoup plus.

* [polynôme du second degré](https://www.youtube.com/watch?v=gkzTKyfCfz4&list=PL_ZtK1TB2InoJ2lEs62JdArQeXKBROOtV&index=1)
* [forme canonique](https://www.youtube.com/watch?v=F4wby1tbgh4&list=PL_ZtK1TB2InoJ2lEs62JdArQeXKBROOtV&index=2)
* [équations du second degré](https://www.youtube.com/watch?v=RP4W2ZDgGFY)
* [équations du second degré sans discriminant](https://www.youtube.com/watch?v=lPtdYIN8blw&list=PL_ZtK1TB2InoJ2lEs62JdArQeXKBROOtV&index=9)
* [signe d'un trinôme](https://www.youtube.com/watch?v=zkUYDSUGDws&list=PL_ZtK1TB2InoJ2lEs62JdArQeXKBROOtV&index=29)
* [résolution d'inéquations](https://www.youtube.com/watch?v=vQlyfkbaFHQ&list=PL_ZtK1TB2InoJ2lEs62JdArQeXKBROOtV&index=54)


