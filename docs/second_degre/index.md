# Introduction aux fonctions du second degré
## Les fonctions du second degré dans la nature
Les fonctions du second degré sont très présentes dans les phénomènes physiques, en particulier lors de chutes (distance parcourue par un objet qui tombe, trajectoire d'une balle ou d'un ballon, etc.)

![](images/paraboles.png)

## Les équations du second degré dans l'histoire
Les problèmes du second degré sont très ancients, nous en trouvons la trace dans la tablette d'argile babylonienne n°13901 du British Museum (BM 13901) qui  comporte des problèmes relatifs aux surfaces de carrés, ainsi que les solutions à ces problèmes. Elle daterait du début du 18^ème^ siècle avant JC.

![Source : http://cdli.ox.ac.uk/wiki/doku.php?id=79._mathematical_text_bm_13901](images/bm13901.png){width=30%; : .center}

!!! example "Exemple"

    Voici le premier problème de cette tablette :

    > J’ai additionné la surface et (le côté de) mon carré : 45´.

    Traduit en équation moderne : \(x^2+x=3/4\) (car 45' représente 3/4 de 60' dans le système sexagésimal).

    La tablette donne ensuite une méthode à suivre pour trouver la longueur du côté du carré.

    ??? question "Combien vaut \(x\) ?"

        Vous pourriez trouver "à tâtons" la (ou les) valeur(s) de \(x\) mais il existe des formules permettant de trouver celle(s)-ci.

        Ici \(x\) vaut \(\dfrac{1}{2}\), en effet : \(\left(\dfrac{1}{2}\right)^2+\dfrac{1}{2} = \dfrac{1}{4} + \dfrac{2}{4}=\dfrac{3}{4}\).

        Il existe une autre solution mais elle est négative donc inexistante pour les babyloniens...

Au 8^ème^ siècle, le mathématicien indien [Sridhara](https://en.wikipedia.org/wiki/Sridhara) donne une méthode pour trouver une solution ([un exemple de résolution ici](https://www.youtube.com/watch?v=g_MHQWr3UmA) et [une preuve là](https://www.slideshare.net/Castellina/ma2-31476676)) puis Al-Khwarizmi (dont le nom a donné le mot _algorithme_) au 9^ème^ siècle fait une étude systématique des équations du second degré, dans un ouvrage intitulé "Abrégé du calcul par la restauration et la comparaison" (c'est du mot _restauration_, en arabe _al-jabr_, que vient le mot _algèbre_).

![](images/AK.jpg){width=20%; : .center}

!!!tip "À quoi ça sert ?"
    Résoudre des équations ou savoir étudier une fonction du second degré permet de répondre à toutes sortes de problèmes liés donc à des surfaces (trouver des dimensions répondant à certaines contraintes), à des mouvements (à quel moment une balle arrivera à une certaine hauteur ou touchera le sol), etc.

## À propos des équations et des inéquations
Un des objectifs prioritaire de ce chapitre est de résoudre des équations ou inéquations.

!!!tip "Deux remarques à propos des équations et inéquations"
    * une équation (a une inconnue) s'écrit de façon générale $f(x)=g(x)$ mais ceci peut aussi s'écrire $f(x)-g(x)=0$ : toutes les équations se ramène à trouver les valeurs $x$ qui annulent une fonction ;
    * une inéquation (a une inconnue) s'écrit de façon générale $f(x)>g(x)$ (ou $f(x)\geqslant g(x)$) mais ceci peut aussi s'écrire $f(x)-g(x)>0$ : toutes les inéquations se ramène à trouver les valeurs $x$ pour lesquels une fonction est positive (ou négative dans le cas $f(x)<g(x)$) ; un tableau de signe sera précieux pour cela.



## Des formules importantes
Les identités remarquables sont à connaître par cœur :


<span style="color:red">
    \(\boxed{(a+b)^2 =a^2+2\,a\,b+b^2}\)
    \(\boxed{(a-b)^2 =a^2-2\,a\,b+b^2}\)
    \(\boxed{(a+b)(a-b) =a^2-b^2}\)
</span>
