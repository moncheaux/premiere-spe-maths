# Applications d'une forme factorisée

## Résolution d'équations du second degré

Quand une fonction du second degré a une forme factorisée, nous pouvons savoir quand elle s'annule.

!!! example "Exemple II.2"

    D'après l'exemple précédent, \(-2\,x^2-12\,x-16 = -2 (x+2)(x+4)\) donc  
    \(
    -2\,x^2-12\,x-16 = 0
    \iff
    -2 (x+2)(x+4) = 0
    \iff
    \)
    ~~–2=0 ou~~
    \(
    x+2 = 0
    \mbox{ ou }
    x+4 = 0
    \iff
    x=-2
    \mbox{ ou }
    x=-4
    \).

    L'ensemble des solutions de \(-2\,x^2-12\,x-16 = 0\) est donc \(S=\{-2;-4\}\).


!!! abstract "Définition"
    Une <span style="color:red">racine</span> d'un polynome \(f\) est une valeur \(u\) telle que \(f(u)=0\).

!!! tip "Remarque"
    Les racines sont parfois appelées des *zéros* du polynôme.

!!! example "Exemple II.3"
    L'expression \(-2\,x^2-12\,x-16\) a deux racines : \(-2\) et \(-4\).

!!! abstract "Propriété" 
    Si un polynôme du second degré se factorise en \(f(x)=a(x-x_1)(x-x_2)\) alors \(f\) a deux racines : \(x_1\) et \(x_2\).

!!! example "Exemple II.4"
    Les racines de \(-3(x+1)(x-4)\) sont \(x_1=-1\) et \(x_2=4\).

<iframe scrolling="no" title="Formes développée, canonique et factorisée" src="https://www.geogebra.org/material/iframe/id/fctefraf/width/1200/height/700/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/true/ctl/false" width="1200px" height="500px" style="border:0px;"> </iframe>

!!! tip "Remarques"
    * un polynôme du second degré peut avoir deux, une ou aucune racine ; 
    * quand il n'y en a qu'une, elle est dite *racine double* et notée \(x_0\) ; 
    * le polynôme \(x^2+1\) n'a pas de racine réelle (car \(x^2+1\) est strictement positif) donc il n'a pas de forme factorisée.

!!! question "Exercice"
    Donnez les racines des polynômes suivants :

    * \(5(x-3)(x+1)\)
    * \(-2(x+3)(x-7)\)
    * \((4\,x-1)(x+5)\)
    * \(2(3\,x-5)(5\,x+1)\)
    * \((x^2-3)(x+4)(2\,x^2+1)\)


## Fonctions s'annulant en deux valeurs

!!! tip "Remarque"
    Inversement, nous pouvons écrire les expressions des fonctions polynômes du second degré s'annulant en deux valeurs.

!!! example "Exemple II.5"
    Les fonctions du second degré s'annulant en \(-5\) et 3 ont pour expression \(a(x+5)(x-3)\) ou encore, en forme développée : \(a\,x^2 +2\,a\,x -15\,a\).

!!! question "Exercices"
    Transmath : 14, 15, 16 page 74

## Tableau de signe d'une fonction du second degré

!!! example "Exemple II.6"
    Faisons le tableau de signes de \(-2\,x^2-12\,x-16\) à l'aide de sa forme factorisée \(-2 (x+2)(x+4)\).


    Remarquons pour cela que \(x+2>0\iff x>-2\) et que \(x+4>0\iff x>-4\).

    \[
    \begin{array}{c|lp{8mm}cccp{8mm}r}
    x & -\infty & & -4 & & -2 & & +\infty
    \\\hline
    -2 & & - & | & - & | & - &
    \\\hline
    x+2 & & - & | & - & 0 & + &
    \\\hline
    x+4 & & - & 0 & + & | & + &
    \\\hline
    -2x^2-12x-16 & & - & 0 & + & 0 & - &
    \end{array}
    \]


!!! abstract "Propriété : signe d'une fonction du second degré"

    Nous supposons ici que \(x_1\leqslant x_2\).  

    <div style="color:red">

    \[\begin{array}{c|lp{3cm}cccp{3cm}r}
    x & -\infty & & x_1 & & x_2 & & +\infty \\\hline
    a(x-x_1)(x-x_2) & & \text{signe de } a&0& \text{signe de }-a&0&\text{signe de } a& \\ \end{array}\]

    </div>

    La fonction a <span style="color:red">le signe de \(a\) quand \(x\) est _à l'extérieur des racines_</span> et le signe de \(-a\) quand \(x\) est _à l'intérieur des racines_.

??? tip "Preuve"
    En utilisant la forme factorisée :

    \[\begin{array}{c|lp{1.6cm}cccp{1.6cm}r}
    x & -\infty & & x_1 & & x_2 & & +\infty \\\hline
    x-x_1 & &  - &0&+&& +& \\\hline
    x-x_2 & &  - &&-&0& +& \\\hline
    (x-x_1)(x-x_2) & &  + &0&-&0& +& \\\hline
    a(x-x_1)(x-x_2) & & \text{signe de } a&0& \text{signe de }-a&0&\text{signe de } a& \\ \end{array}\]


!!! example "Exemple II.7"
    Faisons le tableau de signes de \(-3(x-5)(x+1)\).

    Remarquons que les racines sont \(x_1 = 5\) et \(x_2 = -1\) (ou le contraire) et que le signe de $a=-3$ est négatif. Donc :

    \[
    \begin{array}{c|lp{8mm}cccp{8mm}r}
    x & -\infty & & -1 & & 5 & & +\infty
    \\\hline
    -3(x-5)(x+1) & & - & 0 & + & 0 & - &
    \end{array}
    \]	


!!! question "Exercices"
    Transmath : 5, 6, 7, 8 page 74

## Résolution d'inéquations du second degré

!!! example "Exemple II.8"
    Résolvons l'inéquation \(-2\,x^2-12\,x-16<0\).

    Utilisons pour cela son tableau de signe fait précédemment :

    \[
    \begin{array}{c|lp{8mm}cccp{8mm}r}
    x & -\infty & & -4 & & -2 & & +\infty
    \\\hline
    -2\,x^2-12\,x-16 & & - & 0 & + & 0 & - &
    \end{array}
    \]

    duquel nous déduisons que \(-2\,x^2-12\,x-16<0 \iff x<-4 \text{ ou }x>-2\) donc l'ensemble des solutions est \(S=]-\infty;-4[\cup]-2;+\infty[\).


!!! question "Exercices"
    Transmath : 9, 10 page 74
