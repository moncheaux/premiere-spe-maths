# Quelques exercices pour vous entraîner

Les valeurs utilisées dans ces exercices sont générées par le site Wims, vous pouvez donc refaire plusieurs fois chacun d'entre eux.

## Forme développée
* calculs d'images : [simple](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H4/algebra/oefseconddegree.fr&cmd=new&exo=sdimag) et [plus difficile](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H4/algebra/oefseconddegree.fr&cmd=new&exo=sdimag2)
* [vérifier si une valeur est une racine d'un polynôme](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H4/algebra/oefseconddegree.fr&cmd=new&exo=sdverifrac) ;
* [classer des polynômes suivant leur degré](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/oefsecdeg.fr&cmd=new&exo=degre) ;
* [classer des fonctions suivant leur courbe](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/oefsecdeg.fr&cmd=new&exo=parabole) ;


## Forme factorisée
* [trouver les racines à partir d'une forme factorisée](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H4/algebra/oefseconddegree.fr&cmd=new&exo=sdsigne4)
* [faire un tableau de signe à partir d'une forme factorisée](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H4/algebra/oefseconddegree.fr&cmd=new&exo=sdsigne3) ;
* [choisir la forme factorisée](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H4/algebra/oefseconddegree.fr&cmd=new&exo=sdsigne2) ;
* [trouver la forme factorisée connaissant deux racines](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H4/algebra/oefseconddegree.fr&cmd=new&exo=sdfacto3) ;
* trouver la forme factorisée connaissant une racine : [un exercice](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H4/algebra/oefseconddegree.fr&cmd=new&exo=sdfacto1) et [un autre](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H4/algebra/oefseconddegree.fr&cmd=new&exo=sdfacto2);
* trouver la forme factorisée à partir de la forme développée : [un exercice](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/algebra/oefsecdeg.fr&cmd=new&exo=Factorisationd) et [un autre](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H4/algebra/oefseconddegree.fr&cmd=new&exo=sdfacto2);
* [simplifier une fraction rationnelle](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/algebra/oefsecdeg.fr&cmd=new&exo=simp_frac) ;
* [trouver des fonctions correspondant à une parabole](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/algebra/OEFevalwimssecdeg.fr&cmd=new&exo=expparabole2) ;
* [somme et produit des racines](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/algebra/OEFevalwimssecdeg.fr&cmd=new&exo=sommeprod2)



## Forme canonique
* [chercher la forme canonique](https://euler-ressources.ac-versailles.fr/wims/wims.cgi?wims_window=new&+session=TIED3BEA92_exo&+lang=fr&+module=H4%2Fanalysis%2Ftrisecdeg.fr&+cmd=new&+exo=triformcan) à partir de la forme développée ;
* même question : [ici](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/algebra/OEFevalwimssecdeg.fr&cmd=new&exo=canon1) et [là](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/algebra/OEFevalwimssecdeg.fr&cmd=new&exo=canon4) ;
* [trouver la forme canonique](https://euler-ressources.ac-versailles.fr/wims/wims.cgi?wims_window=new&+session=TIED3BEA92_exo&+lang=fr&+module=H4%2Falgebra%2FcanonicalForms.fr&+cmd=new&+exo=parabole) à partir d'une parabole ;
* [trouver le signe de $a$ et de $\Delta$ à partir d'une parabole](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/algebra/oefsecdeg.fr&cmd=new&exo=allureprb) ;
* [calculer les coordonnées du sommet d'une parabole](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/oefsecdeg.fr&cmd=new&exo=sommet) ;


## Équations du second degré
* [calculs de discriminants](https://euler-ressources.ac-versailles.fr/wims/wims.cgi?wims_window=new&+session=TIED3BEA92_exo&+lang=fr&+module=local%2Falgebra%2Foefacverpdfdiscri.fr&+cmd=new&+exo=calculdiscriminant) ;
* [résolution en plusieurs étapes d'une équation](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/algebra/oefsecdeg.fr&cmd=new&exo=Racinesdunpoly2) ;
* [même question](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/algebra/OEFevalwimssecdeg.fr&cmd=new&exo=solveeq1) ;
* [trouver le signe du discriminant Δ](https://euler-ressources.ac-versailles.fr/wims/wims.cgi?wims_window=new&+session=TIED3BEA92_exo&+lang=fr&+module=local%2Falgebra%2Foefacverpdfdiscri.fr&+cmd=new&+exo=lecturegraphique) à partir d'une parabole ;
* [résolution avec ou sans $\Delta$](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/algebra/OEFevalwimssecdeg.fr&cmd=new&exo=solveeq2) ;
* [intersection droite / parabole](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/algebra/oefsecdeg.fr&cmd=new&exo=Intersection1)
* [intersection droite / hyperbole](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/algebra/oefsecdeg.fr&cmd=new&exo=Intersection2)
* [chercher une valeur pour qu'une droite soit tangente à une parabole](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/algebra/oefsecdeg.fr&cmd=new&exo=posireldrprb) ;
* [trouver deux nombres connaissant leur somme et leur produit](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/algebra/OEFevalwimssecdeg.fr&cmd=new&exo=pbdeg25)



## Inéquations du second degré
* [choisir parmi plusieurs tableaux de signe](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/algebra/OEFevalwimssecdeg.fr&cmd=new&exo=signetrinome1) ;
* [faire un tableau de signe](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/algebra/OEFevalwimssecdeg.fr&cmd=new&exo=signetrinome2)
* [inéquation du second degré](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/algebra/OEFevalwimssecdeg.fr&cmd=new&exo=signetrinome3) ;
* [inéquation avec deux trinômes](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/algebra/oefsecdeg.fr&cmd=new&exo=Inquationsetse) ;


## Pour les plus motivés
* trouver les dimensions d'un triangle rectangle : [ici](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/algebra/OEFevalwimssecdeg.fr&cmd=new&exo=coefracine2) et [là](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/algebra/OEFevalwimssecdeg.fr&cmd=new&exo=coefracine3) ;
* [calculer des résistances](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/algebra/OEFevalwimssecdeg.fr&cmd=new&exo=coefracine5) ;
* [factorisation d'un polynôme du troisième degré](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/algebra/OEFevalwimssecdeg.fr&cmd=new&exo=factdeg33)