# Recherche d'une forme factorisée dans des cas simples

## Trinôme incomplet

!!! tip "Remarque"
    Il est possible de trouver une forme factorisée simplement quand \(b=0\) ou \(c=0\).

!!! exemple "Exemple II.9"

    === "Question"
        Donnez la forme factorisée de $-3\,x^2 + 4\,x$.

    === "Réponse"
        $-3\,x^2 + 4\,x = x(-3\,x+4) = -3\,x \left(x- \dfrac{4}{3}\right)$.

!!! exemple "Exemple II.10"

    === "Question"
        Donnez la forme factorisée de $-3\,x^2 + 12$.

    === "Réponse"
        $-3\,x^2 + 12=-3(x^2-4)=-3(x^2-2^2)=-3(x-2)(x+2)$.

!!! question "Exercices"
    Transmath : 24, 25, 26 page 75

## Factorisation connaissant une racine

!!! example "Exemple II.11"

    === "Questions"
        1. Vérifiez que \(-1\) est une racine "évidente" de $3\,x^2 + 8\,x + 5$.
        2. Donnez la forme factorisée de $3\,x^2 + 8\,x + 5$.

    === "Réponse question 1"
        En remplaçant \(x\) par \(-1\) :  
        $3\times (-1)^2 + 8\times (-1) + 5 = 3-8+5 = 0$ donc \(-1\) est bien une racine de $3\,x^2 + 8\,x + 5$.

    === "Réponse question 2"
        Décidons que \(x_1=-1\). Alors : 
        $3\,x^2 + 8\,x + 5 = 3(x-(-1))(x-x_2)=3(x+1)(x-x_2)$.  
        
        Comme cette égalité doit être vraie pour tout $x$, choisissons $x=0$ :  
        $3\times 0^2 + 8\times 0 + 5 = 3(0+1)(0-x_2)$
        donc $5=-3\,x_2$ ce qui donne $x_2=-\dfrac{5}{3}$.
        
        La forme factorisée de $3\,x^2 + 8\,x + 5$ est donc \(3(x+1)\left(x+\dfrac{5}{3}\right)\) (développez pour vérifier).


<!-- Développons le terme de droite, cela donne : $3,x^2 + 8,x + 5 = 3,x^2 +3,x(-x_2+1) -3,x_2$.

```
    Les deux expressions devant être égale pour tout $x$, nous en déduisons, __par identification__ de la constante, que 
    $5=-3\,x_2$ donc $x_2=-\dfrac{5}{3}$.
```

\-->

!!! question "Exercices"
    Transmath : 28, 29 page 75

## Somme et produit des racines

!!! abstract "Propriété : somme et produit des racines (formules de Viète)"
    Si \(a\,x^2+b\,x+c =a(x-x_1)(x-x_2)\) alors <span style="color:red">\(\boxed{x_1+x_2 = -\dfrac{b}{a}}\)</span> et <span style="color:red">\(\boxed{x_1\times x_2 = \dfrac{c}{a}}\)</spa,>.

!!! tip "Remarque"
    Cette propriété permet de trouver une racine quand on connaît l'autre, puis éventuellement de trouver la forme factorisée.

!!! example "Exemple II.12"

    === "Questions"
        1. Trouvez une racine évidente de $2\,x^2 + 5\,x - 7$.
        2. Donnez la forme factorisée de $2\,x^2 + 5\,x - 7$.

    === "Réponse question 1"
        En remplaçant \(x\) par 1 :  
        $2\times 1^2 + 5\times 1 - 7 = 2+5-7 = 0$ donc 1 est une racine de $2\,x^2 + 5\,x - 7$.
        
    === "Réponse question 2"
        Utilisons la somme des racines (par exemple) : 
        \(x_1+x_2 = -\dfrac{5}{2}\) or \(x_1=1\) donc \(1+x_2 = -\dfrac{5}{2}\) d'où \(x_2 = -\dfrac{5}{2}-1=-\dfrac{7}{2}\).  
        La forme factorisée de \(2\,x^2+5x-7\) est donc \(2(x-1)(x-(-7/2))=(x-1)(2\,x+7)\).


!!! example "Exemple II.13"

    === "Question"
        Donnez la forme factorisée de $f(x)=-2\,x^2 +10\,x -12$.

    === "Réponse"
        Les formules ci-dessus donnent 
        \(x_1+x_2 = -\dfrac{10}{-2} = 5\) et \(x_1x_2 = \dfrac{-12}{-2}=6\).
        Il suffit donc de trouver deux nombres dont la somme est 5 et le produit est 6, ces deux nombres sont 2 et 3 donc :
        \(-2\,x^2 +10\,x -12 = -2(x-x_1)(x-x_2) = -2(x-2)(x-3)\).


!!! question "Exercices" 
    * Donnez la forme factorisée de $f(x)=3\,x^2+9\,x-12$.
    * Transmath : 33 page 75
    
