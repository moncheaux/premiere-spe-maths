# III - Forme canonique d'une fonction du second degré

!!! tip "Méthode de complétion du carré"
    Voyons une méthode permettant d'éliminer un $x$ dans une expression du second degré (ce qui permettra ensuite de résoudre n'importe quelle équation du second degré !).

!!! example "Exemple III.1"
    Vous savez que \((x+3)^2 = x^2+6\,x+9\) donc \(x^2+6\,x = (x+3)^2-9\).

!!! tip "Généralisation"
    De la même façon :
    <span style="color:red">\(x^2+k\,x = \left(x+\dfrac{k}{2}\right)^2-\left(\dfrac{k}{2}\right)^2\)</span>.

!!! example "Exemple III.2" 
    \(x^2+10\,x-3 = (x+5)^2-5^2 -3 = (x+5)^2-28\).

!!! question "Exercice"
    Donnez la forme canonique des trinômes suivants :

    * \(x^2+4\,x-5\)
    * \(x^2+3\,x+1\)
    * \(3\,x^2+4\,x-5\)


!!! abstract "Théorème"

    <span style="color:red">
    \(
    \boxed{a\,x^2+b\,x+c=a\left(x+\dfrac{b}{2\,a}\right)^2 -\dfrac{\Delta}{4\,a}}
    \)
    </span> (<span style="color:red">forme canonique</span> de \(a\,x^2+b\,x+c\))

    où <span style="color:red">$\Delta = b^2-4\,a\,c$</span> (<span style="color:red">discriminant</span> de \(a\,x^2+b\,x+c\)).


??? note "Preuve"

    Étape 1 : factoriser partiellement par \(a\) :  
    \(a\,x^2+b\,x+c=a\left(x^2+\dfrac{b}{a}\,x\right)+c\)

    Étape 2 : appliquer la méthode de complétion du carré sur \(\left(x^2+\dfrac{b}{a}\,x\right)\) :  
    \(\left(x^2+\dfrac{b}{a}\,x\right)
    =\left(x+\dfrac{b}{2\,a}\right)^2- \left(\dfrac{b}{2\,a}\right)^2\)
    =\left(x+\dfrac{b}{2\,a}\right)^2- \dfrac{b^2}{4\,a^2}\)

    Étape 3 : reprendre le calcul de l'étape 1
    \(a\,x^2+b\,x+c
    =a\left(
    \left(x+\dfrac{b}{2\,a}\right)^2- \dfrac{b^2}{4\,a^2}
    \right)+c
    =a\left(x+\dfrac{b}{2\,a}\right)^2- \dfrac{b^2}{4\,a}+c
    =a\left(x+\dfrac{b}{2\,a}\right)^2- \dfrac{b^2-4\,a\,c}{4\,a}
    =a\left(x+\dfrac{b}{2\,a}\right)^2- \dfrac{\Delta}{4\,a}
    \)
    où \(\Delta=b^2-4\,a\,c\).


??? note "Une autre preuve"

    Écrivons \(a\,x^2+b\,x+c=a(x-\alpha)^2+\beta\) et voyons comment déterminer \(\alpha\) et \(\beta\).

    \(a\,x^2+b\,x+c=a(x-\alpha)^2+\beta\) donne en développant :  
    \(a\,x^2+b\,x+c=a(x^2-2\,\alpha\,x+\alpha^2)+\beta\)  
    puis  
    \(a\,x^2+b\,x+c=a\,x^2-2\,a\,\alpha\,x+a\,\alpha^2+\beta\).

    Les termes en \(x^2\) sont les mêmes. Les coefficients des termes en \(x\) nous donnent :  
    \(b=-2\,a\,\alpha\) donc \(\alpha=-\dfrac{b}{2\,a}\).

    Les termes constants (sans \(x\)) nous donnent :  
    \(c=a\,\alpha^2+\beta\) donc \(\beta = c-a\,\alpha^2
    =c-a\times\dfrac{b^2}{4\,a^2}
    =c-\dfrac{b^2}{4\,a}
    =\dfrac{4\,a\,c-b^2}{4\,a}
    =-\dfrac{\Delta}{4\,a}
    \)  
    où \(\Delta=b^2-4\,a\,c\).


!!! example "Exemple III.3"

    L'expression \(-2(x+3)^2+2\) est la forme canonique de \(-2\,x^2-12\,x-16\).

    En effet :

    \(
    -2(x+3)^2+2
    =
    -2(x^2+6x+9)+2
    =
    -2\,x^2-12\,x-18+2
    =
    -2\,x^2-12\,x-16
    \).


!!! tip "Remarques" 
    * la forme canonique est une expression dans laquelle la variable \(x\) n'apparait qu'une fois ; 
    * toutes les fonctions du second degré ont une forme canonique ; 
    * la forme canonique s'écrit aussi \(a\left(\left(x+ \dfrac{b}{2\,a}\right)^2-\dfrac{\Delta}{4\,a^2}\right)\) ; 
    * en écrivant cette forme canonique sous la forme \(a(x-\alpha)^2+\beta\), les valeurs de \(\alpha\) et \(\beta\) sont les coordonnées du sommet de la parabole :

    <iframe scrolling="no" title="Formes développée, canonique et factorisée" src="https://www.geogebra.org/material/iframe/id/fctefraf/width/1200/height/700/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/true/ctl/false" width="1200px" height="550px" style="border:0px;"> </iframe>


<!-- Le calcul effectif de la forme canonique dans le cas général n’est pas un attendu du programme. -->

!!! question "Exercice" 
    [Transmath](https://biblio.manuel-numerique.com/) : 36 page 75
