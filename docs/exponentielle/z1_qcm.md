# ⁉️ QCM
!!! bug "Attention"
    Il peut y avoir plusieurs réponses correctes à chaque question.

#### Q1 - La dérivée de \(\mathrm{e}^{3\,x}\) est :
{{ qcm(["$3\,\mathrm{e}^{3\,x}$", "$\mathrm{e}^{3}$", "$3\,x\,\mathrm{e}^{3\,x}$", "$\mathrm{e}^{3\,x}$", "$0^{3\,x}=0$", "$3\,\mathrm{e}^{2\,x}$"], [1], shuffle = True) }}

#### Q2 - La dérivée de \(\mathrm{e}^{-x}\) est :
{{ qcm(["$-\mathrm{e}^{-x}$", "$-\dfrac{1}{\mathrm{e}^{x}}$", "$\mathrm{e}^{-x}$", "$\mathrm{e}^{-1}$", "$-x\,\mathrm{e}^{-x}$"], [1,2], shuffle = True) }}

#### Q3 - La dérivée de \(5\,\mathrm{e}^{3\,x+2}\) est :
{{ qcm(["$15\,\mathrm{e}^{3\,x+2}$", "$5\,\mathrm{e}^{3}$", "$15\,\mathrm{e}^{2\,x+2}$", "$5\,\mathrm{e}^{3\,x+2}$", "$5\,\mathrm{e}^{5}$"], [1], shuffle = True) }}


#### Q4 - La dérivée de \(x\mathrm{e}^{x}\) est :
{{ qcm(["$(1+x)\mathrm{e}^{x}$", "$\mathrm{e}^{x}+x\mathrm{e}^{x}$", "$\mathrm{e}^{x}$", "$\mathrm{e}$", "$x\mathrm{e}$", "$x\mathrm{e}^{x}$"], [1,2], shuffle = True) }}

#### Q5 - La dérivée de \(\dfrac{\mathrm{e}^{x}}{x}\) est :
{{ qcm(["$\dfrac{\mathrm{e}^{x}(x-1)}{x^2}$", "$\mathrm{e}$", "0",  "$\dfrac{-\mathrm{e}^{x}}{x}$", "$\mathrm{e}^{x}$"], [1], shuffle = True) }}

#### Q6 - La dérivée de \(\sin(x)\) est \(\cos(x)\). Donc la dérivée de \(\mathrm{e}^{x}\sin(x)\) est :
{{ qcm(["$\mathrm{e}^{x}(\cos(x)+\sin(x))$", "$\mathrm{e}(\sin(1))$", "$\mathrm{e}^{x}\cos(x)$", "$\mathrm{e}^{x}\sin(x)$"], [1], shuffle = True) }}

#### Q7 - L'expression \(2\,\mathrm{e}^{2-x}+1\) est :
{{ qcm(["toujours positive", "toujours négative", "positive ou négative suivant les valeurs de $x$", "toujours nulle"], [1], shuffle = True) }}

#### Q8 - La fonction définie sur ℝ par \(f(x)=2\,\mathrm{e}^{2-x}+1\) est :
{{ qcm(["strictement décroissante sur ℝ", "strictement croissante sur ℝ",  "strictement croissante sur \(]-\infty;2]\) puis décroissante", "strictement décroissante sur \(]-\infty;2]\) puis croissante"], [1], shuffle = True) }}

## Autres QCMs
* [10 questions variées](https://www.lumni.fr/quiz/la-fonction-exponentielle)
* [9 questions variées](https://www.bibmath.net/quizz/index.php?action=quizzbdd&id=37)