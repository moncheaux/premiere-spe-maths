# II. Propriétés algébriques. Nombre e

!!! abstract "Propriété II.1"
    Pour tous réels $x$ et $y$, <span style="color:red">$\boxed{\exp(x + y) = \exp(x) \times \exp(y)}$</span>.

    !!! abstract "Démonstration"
        L'astuce de la démonstration est de _fixer_ $y$ (nous écrirons $y=c$ comme constante) 
        et de considérer que $x$ est une variable, en posant :
        $f(x)=\dfrac{\exp(x + c)}{\exp(x) \exp(c)}$ et en prouvant que $f(x)=1$.

        1. Commençons par prouver que $f'(x)=0$ pour tout $x$ :
            * $f$ est de la forme $\dfrac{u}{v}$ ;
            * $u=\exp(x + c)$ est de la forme $g(ax+b)$ ($g=\exp$ ; $a=1$ ; $b=c$)  
            donc $u'=ag'(ax+b)=1(\exp)'(1x+c) = \exp(x+c)$ ;
            * pour dériver $v=\exp(x)\exp(c)$, remarquons que $\exp(c)$ est une constante  
            donc $v'=(\exp(x))' \exp(c) = \exp(x)\exp(c)$ ;
            * D'où 
            $f'(x)=\dfrac{\strut\exp(x + c) \times \exp(x) \exp(c)- \exp(x + c) \times \exp(x) \exp(c)}
            {(\exp(x) \exp(c))^2}=0$.
            * Donc la fonction $f$ est une fonction constante : pour tout $x$, $f(x) = k$.
        2. Prouvons maintenant que $f(x)=1$ :  
        $f(0) = \dfrac{\exp(0 + c)}{\exp(0) \exp(c)} =\dfrac{\exp(c)}{1 \times  \exp(c)}
        =1$ mais $f$ est constante d'où, pour tout $x$, $f(x) = 1$.
        3. Donc $\exp(x + c)=\exp(x) \exp(c)$ et, comme $c$ était quelconque, 
        $\exp(x + y) = \exp(x) \exp(y)$.

!!! abstract "Propriété II.2"
    Pour tout réel $x$, <span style="color:red">$\boxed{ \exp(-x) = \dfrac{1}{\exp(x)}}$</span>.

    !!! abstract "Démonstration"
        D'après la propriété II.1, nous avons  
        $\exp(x) \exp(-x) = \exp(x + (-x)) = \exp(0) = 1$ donc $\exp(-x) = \dfrac{1}{\exp(x)}$.  
        (ceci a aussi été prouvé lors de la démonstration de la propriété du I car $h(x)=\exp(x)\exp(-x)=1$)

!!! abstract "Propriété II.3"
    Pour tout réel $x$ et tout entier $n$, <span style="color:red">$\boxed{(\exp(x))^n = \exp(nx)}$</span>.

    !!! abstract "Démonstration"
        * Nous avons, pour les entiers strictement positifs :  
        pour $n=1$, $(\exp(x))^1 = \exp(x) = \exp(1x)$ ;  
        pour $n=2$, d'après la propriété II.1, $(\exp(x))^2 = \exp(x) \exp(x) = \exp(x + x) = \exp(2x)$ ;  
        pour $n=3$, $(\exp(x))^3 = \exp(x) (\exp(x))^2 = \exp(x) \exp(2x) = \exp(x+2x) =\exp(3x)$  
        etc. (il faut une _démonstration par récurrence_ pour continuer ici...)
        * Pour $n=0$ :  
        $(\exp(x))^n = (\exp(x))^0 = 1$ car $a^0 = 1$ pour tout $a\neq 0$ (et $\exp(x)\neq0$)  
        $\exp(nx) = \exp(0 \times x)=\exp(0)=1$  
        donc $(\exp(x))^0 = \exp(0x)$ ;
        * Pour un entier $n$ strictement négatif :  
        $\begin{align*}
        (\exp(x))^n 
        &=
        \dfrac{1}{(\exp(x))^{-n}}\qquad\text{(propriété sur les puissances)}
        \\
        &=
        \dfrac{1}{\exp(-nx)}\qquad\text{(car }-n \text{ est un entier positif)}
        \\
        &=
        \exp(nx)\qquad\text{(d'après la propriété II.2)}
        \end{align*}$


!!! abstract "Propriété II.4"
    Pour tout réel $x$, <span style="color:red">$\boxed{ \dfrac{\exp(x)}{\exp(y)} = \exp(x-y)}$</span>.

    !!! abstract "Démonstration"
        D'après la propriété II.1, nous avons  
        $\exp(x-y) \exp(y) = \exp(x-y + y)=\exp(x)$ donc $\exp(x-y) = \dfrac{\exp(x)}{\exp(y)}$.

!!! tip "Remarque"

    Les plus affutés auront remarqué que ces propriétés évoquent celles des puissances, par exemple,
    $10^{2+3}=10^{2} \times 10^{3}$ ;
    $10^{-2} = \dfrac{1}{10^{2}}$
    et
    $\dfrac{10^{5}}{10^{2}} = 10^{5-2}= 10^{3}$.


!!! abstract "Définition"
    Le nombre $\mathrm{e}$ est l'image de 1 par la fonction exponentielle :
    <span style="color:red">$\boxed{\mathrm{e} = \exp(1)}$</span>.


!!! tip "Remarques"
    * $\mathrm{e}$ est une constante que l'on retrouve partout en mathématiques et en sciences, au même titre que $\pi$. Elle est à peu près égale à $2,718$.
    * En appliquant la propriété II.3 : pour tout entier $n$, $\exp(n) = \exp(n \times 1) =(\exp(1))^n = \mathrm{e}^n$.   
    De même, on montre assez aisément que $\exp(n)=\mathrm{e}^n$ pour tout $n\in ℤ$.
    * Nous noterons par extension dans la suite : <span style="color:red">$\boxed{\exp(x) = \mathrm{e}^x}$</span> pour tout $x$ réel.
    * Ceci permet surtout de retenir simplement les propriétés précédentes, qui rappellent celles sur les puissances usuelles :   
    <span style="color:red">
    $\mathrm{e}^0 = 1$ ;
    $\mathrm{e}^1 = \mathrm{e}$ ; 
    $\mathrm{e}^{x+y} = \mathrm{e}^x\mathrm{e}^y$ ;
    $\mathrm{e}^{-x}=\dfrac{1}{\mathrm{e}^x}$ ;
    $\dfrac{\mathrm{e}^x}{\mathrm{e}^y} = \mathrm{e}^{x-y}$ ;
    $\left(\mathrm{e}^x\right)^n = \mathrm{e}^{nx}$
    </span>
    * Ceci permet également de parler d'un exposant réel et pas seulement entier !
    Ainsi $\mathrm{e}^{1,5}$ veut dire $\exp(1,5)$.  
    Nous pourrons plus tard grâce à la fonction exponentielle définir également, par exemple, $5^{\pi}$, écriture qui n'avait pas de sens jusqu'à maintenant !

!!! example "Exemples"
    Voici quelques exemples de calculs :

    * \(\dfrac{\strut\mathrm{e}^{4} \times \mathrm{e}}{\strut\mathrm{e}^{8}} = \dfrac{\mathrm{e}^{4} \times \mathrm{e}^{1}}{\mathrm{e}^{8}} = \dfrac{\mathrm{e}^{4+1}}{\mathrm{e}^{8}} = \dfrac{\mathrm{e}^{5}}{\mathrm{e}^{8}} = \mathrm{e}^{5-8} = \mathrm{e}^{-3}\)
    * \(\mathrm{e}^{-x} \times \dfrac{\strut\mathrm{e}^{x-2}}{\strut\mathrm{e}^{3-5\,x}} = \mathrm{e}^{-x} \times \mathrm{e}^{x-2-(3-5\,x)} = \mathrm{e}^{-x+x-2-3+5\,x} = \mathrm{e}^{5\,x-5}\)
    * \(\left(3-\mathrm{e}^{-x}\right)^2 = 3^2-2 \times 3 \times \mathrm{e}^{-x}+ \left(\mathrm{e}^{-x}\right)^2
    = 9-6 \mathrm{e}^{-x}+ \mathrm{e}^{-2x}\)


!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    Lire page 144  
    Exercices : 1, 2, 4, 7, 8, 10, 12, 13, 14 page 149



!!! abstract "Propriété II.5"
    Pour tout réel $a$, la suite $\left( \mathrm{e}^{na}\right)$ est une suite géométrique.

    !!! abstract "Démonstration"
        En effet $\dfrac{\mathrm{e}^{(n+1)a}}{\mathrm{e}^{na}}= \mathrm{e}^{(n+1)a-na}=\mathrm{e}^{a}$ qui est une constante (donc la raison de cette suite géométrique).


!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    58 à 62 page 152
    
