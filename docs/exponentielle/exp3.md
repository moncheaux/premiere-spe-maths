# III. Étude de la fonction exponentielle
## III.1 Signe de la fonction exponentielle

!!! abstract "Propriété III.1"
    Pour tout réel $x$ :  <span style="color:red">$\boxed{\mathrm{e}^{x}>0}$</span>

    !!! abstract "Démonstration"
        En effet, $\mathrm{e}^{x} = \mathrm{e}^{2 \times (x/2)} = \left(\mathrm{e}^{x/2}\right)^2 \ge 0$.  
        Comme la fonction exponentielle ne s'annule pas, nous trouvons que $\mathrm{e}^{x}>0$ pour tout réel $x$.


## III.2 Variation de la fonction exponentielle
!!! abstract "Propriété III.2"

    La fonction exponentielle est <span style="color:red">strictement croissante</span> sur ℝ.
    
    ![alt text](images/var_exp.png){width=40% ; : .center}
    
    !!! abstract "Démonstration"
        Pour tout $x$ réel, $\left(\mathrm{e}^x\right)' = \mathrm{e}^x >0$ d'après la propriété III.1.


!!! tip "Remarques"
    Les deux _valeurs_ aux extrémités de la flèche ne sont pas à connaître, néanmoins, pour les plus curieux, voilà quelques explications intuitives ; considérons un entier $n$ :

    * comme $\mathrm{e}\simeq2,718>1$, le nombre $\mathrm{e}^n$ deviendra de plus en plus grand quand $n$ tend vers $+\infty$ et puisque la fonction est croissante, $\mathrm{e}^x$ deviendra de plus en plus grand quand $x$ tendra vers $+\infty$ ;
    * quand $n$ tend vers $-\infty$, remarquons que $\mathrm{e}^n = \frac{1}{\mathrm{e}^{-n}}$, avec $-n$ qui tend vers $+\infty$ donc $\mathrm{e}^{-n}$ tend vers $+\infty$ et $\mathrm{e}^n$ tend vers 0 ;
    * ces valeurs se retrouvent aussi par analogie avec les puissances de 10, par exemple $10^{40}$ est un nombre très grand tandis que $10^{-40}$ est un nombre très proche de 0.


## III.3 Courbe de la fonction exponentielle

![alt text](images/courbe_exp.png){width=50% ; : .center}

!!! tip "Remarque"
    La connaissance de la courbe ci-dessus permet de retrouver les variations et le signe de la fonction exponentielle (ainsi que les _limites_).
