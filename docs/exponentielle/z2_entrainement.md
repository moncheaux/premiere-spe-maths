# Quelques exercices pour vous entraîner
Les valeurs utilisées dans les exercices du site Wims sont générées aléatoirement, vous pouvez donc refaire plusieurs fois chacun d'entre eux.

* [propriétés algébriques de l'exponentielle](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H6/analysis/oefexp.fr&cmd=new&exo=proprietes_algebriques_exp) ;
* mettre en correspondance les différentes écritures d'une même expression : [exercice 1](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H6/analysis/OEFexpalgTS.fr&cmd=new&exo=sumprod1), [exercice 2](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H6/analysis/OEFexpalgTS.fr&cmd=new&exo=sumprod2), [exercice 3](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H6/analysis/OEFexpalgTS.fr&cmd=new&exo=sumprod3) ; 
* [dérivée du produit d'une fonction exponentielle par une constante](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/oefpolexpo.fr&cmd=new&exo=1k_exp) ;
* [dérivée du produit des fonctions exponentielle et affine](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/oefpolexpo.fr&cmd=new&exo=2aff_exp) ;
* [dérivée du produit des fonctions exponentielle et du 2nd degré](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/oefpolexpo.fr&cmd=new&exo=3secdeg_exp) ;
* [dérivée du quotient d'une fonction exponentielle par une constante](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/oefpolexpo.fr&cmd=new&exo=4Qexp_k) ;
* [dérivée du quotient des fonctions exponentielle et affine](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/oefpolexpo.fr&cmd=new&exo=5Qexp_aff) ;
* [dérivée du quotient des fonctions exponentielle et du 2nd degré](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/oefpolexpo.fr&cmd=new&exo=6Qexp_secdeg) ;
* [calculs de dérivées](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H6/analysis/oefexp.fr&cmd=new&exo=derivee_avec_exp_1) ;
* [idem](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H6/analysis/oefexp.fr&cmd=new&exo=derivee_avec_exp_2) ;
* [étude d'une fonction avec exponentielle](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H6/analysis/oefexp.fr&cmd=new&exo=etude_fonction_avec_exp) ;
* [associer graphes et fonctions](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=U1/analysis/oefexpcurve.fr&cmd=new&exo=expocorresp) ;
* [reconnaître le graphe d'une fonction](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=U1/analysis/oefexpcurve.fr&cmd=new&exo=exporadio) ;
 


