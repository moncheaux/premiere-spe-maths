# I. Définition de la fonction exponentielle

!!!tip "Remarque"
    Nous avons vu dans le chapitre sur la dérivation que la dérivée d'une fonction est une autre fonction.  
    Par exemple, la dérivée de la fonction carrée ($f(x)=x^2$) est la fonction double ($f'(x)=2\,x$).

    Existe-t-il une fonction qui _ne change pas_ quand on la dérive ?

!!! abstract "Propriété"
    Il existe une unique fonction $f$ dérivable sur ℝ et telle que $f'=f$ et $f(0)=1$.

    !!! abstract "Démonstration"
        Nous admettons ici que cette fonction existe (ce qui est plus difficile à prouver).

        * Prouvons d'abord qu'une telle fonction ne peut pas s'annuler.  
        Soit donc $f$ une fonction vérifiant $f'=f$ et $f(0)=1$ et posons $h(x)=f(x)f(-x)$ pour tout $x$.  
        Alors 
        $h$ est dérivable sur ℝ et 
        $\begin{align*}
        h'(x)&=(f(x))'f(-x)+f(x)(f(-x))' \qquad\text{(dérivée d'un produit)}
        \\
        &=f'(x)f(-x)+f(x)(-f'(-x)) \qquad\text{(dérivée de } x\mapsto g(ax+b)\text{ avec }a=-1\text{ et }b=0)
        \\
        &=f'(x)f(-x)-f(x)f'(-x)
        \\
        &=f(x)f(-x)-f(x)f(-x) \qquad\text{(car } f'=f)
        \\
        &=0
        \end{align*}$  
        donc la fonction $h$ est constante : $h(x)=c$ pour tout réel $x$.  
        De plus, comme $h(0)=f(0)f(-0)=1 \times 1 = 1$, il vient $c=1$
        donc $h(x)=1$ pour tout réel $x$.  
        La fonction $f$ ne peut donc pas s'annuler, sinon on aurait $h(x) = 0 \times f(-x)=0$ pour un certain $x$.
        * Démontrons l'unicité de cette fonction :  
        supposons au contraire qu'il existe deux telles fonctions $f$ et $g$.  
        Comme $g$ ne peut pas s'annuler, la fonction \(\dfrac{f}{g}\) est définie et dérivable sur ℝ et :  
        $\left(\dfrac{f}{g}\right)' = \dfrac{f'g-fg'}{g^2} 
        =\dfrac{fg-fg}{g^2} =0$
        car $f'=f$ et $g'=g$.  
        Donc 
        la fonction $\dfrac{f}{g}$ est une fonction constante : pour tout $x$, $\left(\dfrac{f}{g}\right)(x) = k$.  
        Or $\left(\dfrac{f}{g}\right)(0) = \dfrac{f(0)}{g(0)} = \dfrac{1}{1}=1$ donc $k=1$ d'où, pour tout $x$, 
        $\left(\dfrac{f}{g}\right)(x) = 1$ donc $f(x)=g(x)$ : les fonctions $f$ et $g$ sont une seule et même fonction.

!!! abstract "Définition"
    Cette fonction est appelée <span style="color:red">fonction exponentielle</span> et notée <span style="color:red">exp</span>.


!!!tip "Remarques"
    * Nous avons donc : <span style="color:red">$\boxed{(\exp(x))'=\exp(x)} \qquad \boxed{\exp(0) = 1}$</span>.
    * La fonction exponentielle intervient dans de nombreux domaines, que ce soit en sciences, en économie, etc.
