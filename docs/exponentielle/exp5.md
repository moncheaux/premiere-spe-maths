# V. Résolution d'(in)équations comportant des exponentielles

!!! abstract "Propriétés"

    Pour tous les réels $a$ et $b$ :

    <div style="color:red">
    
    \[\boxed{\mathrm{e}^{a} = \mathrm{e}^{b} \iff a = b}\]

    \[\boxed{\mathrm{e}^{a} > \mathrm{e}^{b} \iff a > b}\]
    
    </div>

    !!! abstract "Démonstration"
        Ce sont des conséquences de la croissance stricte de la fonction exponentielle.


!!! example "Exemple V.1"
    === "Question"
        Résoudre \(\mathrm{e}^{3x-1} = \mathrm{e}^{x+2}\).

    === "Réponse"
        \(
            \mathrm{e}^{3\,x-1} = \mathrm{e}^{x+2} 
            \iff
            3\,x-1 = x+2
            \iff
            3\,x-x = 2+1
            \iff
            2\,x=3
            \iff
            x=\dfrac{3}{2}
        \)
        donc 
        \(S=\left\{\dfrac{3}{2}\right\}\).


!!! example "Exemple V.2"
    === "Question"
        Résoudre \(\mathrm{e}^{3\,x-1} = 1\).

    === "Réponse"
        \(
            \mathrm{e}^{3\,x-1} = 1
            \iff
            \mathrm{e}^{3\,x-1} = \mathrm{e}^{0}
            \iff
            3\,x-1 = 0
            \iff
            3\,x=1
            \iff
            x=\dfrac{1}{3}
        \)
        donc 
        \(S=\left\{\dfrac{1}{3}\right\}\).

!!! example "Exemple V.3"
    === "Question"
        Résoudre \(\mathrm{e}^{2-3x} < \mathrm{e}\).

    === "Réponse"
        \(
            \mathrm{e}^{2-3\,x} < \mathrm{e}
            \iff
            \mathrm{e}^{2-3\,x} < \mathrm{e}^{1} 
            \iff
            2-3\,x < 1
            \iff
            -3\,x<-1
            \iff
            x>\dfrac{1}{3}
        \)
        donc 
        \(S=\left]\dfrac{1}{3};\infty\right[\).




!!! example "Exemple V.4"
    === "Question"
        Résoudre \(5\,\mathrm{e}^{x-3} +2= 7\).

    === "Réponse"
        \(
            5\,\mathrm{e}^{x-3} +2= 7
            \iff
            5\,\mathrm{e}^{x-3} = 7-2=5
            \iff
            \mathrm{e}^{x-3} = 5\div 5 =1 = \mathrm{e}^{0}
            \iff
            x-3 = 0
            \iff
            x = 3
        \)
        donc 
        \(S=\left\{3\right\}\).




!!! example "Exemple V.5"
    === "Question"
        Résoudre \(\mathrm{e}^{2\,x^2-x} \geqslant \mathrm{e}\).

    === "Réponse"
        \(
            \mathrm{e}^{2\,x^2-x} \geqslant \mathrm{e}
            \iff
            \mathrm{e}^{2\,x^2-x} \geqslant \mathrm{e}^{1} 
            \iff
            2\,x^2-x \geqslant 1
            \iff
            2\,x^2-x -1 \geqslant 0
        \)  
        Les racines de \(2\,x^2-x-1\) sont (calculer \(\Delta\), etc. ou racine évidente : 1 puis utilisation de \(x_1x_2=\dfrac{c}{a}\)) : \(x_1 = -\dfrac{1}{2}\) et \(x_2 = 1\).  
        Le signe de \(2\,x^2-x-1\) est celui de \(a=2>0\) sauf entre les racines donc \(2\,x^2-x -1 \geqslant 0\) si \(x \leqslant -\dfrac{1}{2}\) ou si \(x \geqslant 1\) donc \(S=\left]-\infty;-\dfrac{1}{2}\right]\cup \left[1;+\infty\right[\).	



!!! example "Exemple V.6"
    === "Question"
        Résoudre \(\mathrm{e}^{2\,x} + \mathrm{e}^{x} -2=0\).

    === "Réponse"
        Remarquons d'abord que \(\mathrm{e}^{2\,x}=\left(\mathrm{e}^{x}\right)^2\), l'équation se ré-écrit alors :

        \[\left(\mathrm{e}^{x}\right)^2 + \mathrm{e}^{x} -2=0\]

        Envisageons maintenant que l'inconnue est \(\mathrm{e}^{x}\) en posant \(X=\mathrm{e}^{x}\) (_changement d'inconnue_) :

        \[X^2+X-2=0\]

        Ceci est une équation du second degré, qui a pour solutions (...) \(X_1=1\) et \(X_2=-2\).  
        Mais comme \(X=\mathrm{e}^{x}\), nous trouvons 
        \(\mathrm{e}^{x}=1=\mathrm{e}^{0}\) ou \(\mathrm{e}^{x}=-2\) (impossible car \(\mathrm{e}^{x}>0\))
        d'où \(x=0\).
        
        Conclusion : \(S=\left\lbrace0\right\rbrace\).


!!! question "Exercices (in)équations"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    64, 65, 66, 68, 71, 72, 74 page 152  
    75 page 152 pour ceux qui veulent progresser encore plus


!!! question "Exercices (in)équations"
    Résoudre les équations :

    * \(3\,\mathrm{e}^{2\,x} + \mathrm{e}^{x} -4=0\)
    * \(\mathrm{e}^{2\,x} - \mathrm{e}^{x} +2=0\)
    * \(\mathrm{e}^{2\,x} + 3\,\mathrm{e}^{x} +2=0\)
    * \(2\,\mathrm{e}^{2\,x} + 3\,\mathrm{e}^{x} =2\,\mathrm{e}^{x+1}+3\,\mathrm{e}\)
    * \(\mathrm{e}^{x} - 3\, \mathrm{e}^{-x} +2=0\)


!!! question "Exercice"
    === "Question"
        Étudier les variations des fonctions définies sur ℝ par :  
        \(f(x)=\mathrm{e}^{x} - \mathrm{e}\,x\)  
        \(g(x)=\mathrm{e}^{x}(-x^2+2\,x-4)\)

    === "Réponse"
        \(f'(x)=\mathrm{e}^{x} - \mathrm{e}\) 
        donc \(f'(x)> 0 \iff \mathrm{e}^{x} > \mathrm{e} \iff x>1\) : la fonction \(f\) est strictement croissante sur \([1;+\infty[\) et strictement décroissante sur \(]-\infty;1]\).

        \(g(x)=\mathrm{e}^{x}(-x^2+2\,x-4)\) est de la forme \(uv\) donc  
        \(g'(x)=\mathrm{e}^{x}(-x^2+2\,x-4)+\mathrm{e}^{x}(-2\,x+2)=
        \mathrm{e}^{x}(-x^2+2\,x-4-2\,x+2) = \mathrm{e}^{x}(-x^2-2)\).  
        Cette dérivée est le produit de \(\mathrm{e}^{x}\), qui est toujours positif, et de \((-x^2-2)\), qui est toujours négatif du fait du carré donc \(g'(x)<0\) sur ℝ : la fonction \(g\) est strictement décroissante sur ℝ.
    




!!! question "Exercices (modélisations)"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    37 page 150   
    105, 106 page 154  
    111 page 156  
    En plus : 38 page 150, 100 et 101 page 153
    <!--110 page 155 (il faut ln donc non !)-->
    
