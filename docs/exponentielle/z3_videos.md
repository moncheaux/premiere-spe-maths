# Des vidéos d'explications
## La fonction exponentielle
* [introduction de la fonction exponentielle et des équations différentielles (Lumni)](https://www.lumni.fr/video/presentation-de-la-specialite-autour-des-fonctions-exponentielles) ;
* [histoire du nombre e](https://www.youtube.com/watch?v=-T9qge2ckLU)

## (In)équations
* [6 équations à résoudre](https://www.youtube.com/watch?v=S3IJYmS7XJI) ;
* [une équation avec exponentielles et 2nd degré](https://www.youtube.com/watch?v=dA73-HT-I_Y&list=PLVUDmbpupCaofwlm6oHUQa0YArPzMZ4M7&index=5) et [une autre plus difficile](https://www.youtube.com/watch?v=ItHWX02UKmk&list=PLVUDmbpupCaofwlm6oHUQa0YArPzMZ4M7&index=7) ;
* [une inéquation simple](https://www.youtube.com/watch?v=d28Fb-zBe4Y&list=PLVUDmbpupCaofwlm6oHUQa0YArPzMZ4M7&index=8) puis [trois autres moins simples](https://www.youtube.com/watch?v=X6n4T9c9ww4&list=PLVUDmbpupCaofwlm6oHUQa0YArPzMZ4M7&index=9)

## Études de fonction
* [quelques calculs de dérivées](https://www.youtube.com/watch?v=RlyFEcx5Y3E&list=PLVUDmbpupCaofwlm6oHUQa0YArPzMZ4M7&index=12) ;
* [étude de $(x+1)\mathrm{e}^x$](https://www.youtube.com/watch?v=_MA1aW8ldjo) ;
* [étude d'une fonction dans une situation concrète](https://www.youtube.com/watch?v=lsLQwiB9Nrg&list=PLVUDmbpupCaofwlm6oHUQa0YArPzMZ4M7&index=13)

## Un peu de tout
* [quelques questions](https://www.youtube.com/watch?v=y5GfPIZqOCM) ;
* [cours complet avec beaucoup d'exemples](https://www.youtube.com/watch?v=xhUDmCBTjAY)

