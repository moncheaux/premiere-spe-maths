# IV. Étude de fonctions liées à l'exponentielle
## IV.1 Calculs de dérivées de fonctions comportant des exponentielles

!!! tip "Remarque"
    Les propriétés vues dans les chapitres sur la dérivation s'applique encore évidemment ici.

!!! example "Exemples IV.1"
    Les fonctions suivantes sont toutes définies et dérivables sur ℝ.

    * Si \(f(x)=-5\,\mathrm{e}^{x}\) alors \(f^\prime(x) = -5\,\mathrm{e}^{x}\) (dérivée de \(k\,u\)).
    * Si \(f(x)=x^2+3+\mathrm{e}^{x}\) alors \(f^\prime(x) = 2\,x+\mathrm{e}^{x}\) (dérivée de \(u+v\)).
    * Si \(f(x)=x^2\, \mathrm{e}^{x}\) alors \(f^\prime(x) = 2\,x \mathrm{e}^{x} + x^2\, \mathrm{e}^{x} = \mathrm{e}^{x}(x^2+2x)\) (dérivée de \(u\,v\)).
    * Si \(f(x)=\dfrac{\strut 2\,x-1}{\strut \mathrm{e}^{x}}\) alors \(f^\prime(x) = \dfrac{\strut 2\mathrm{e}^{x} - (2\,x-1)\mathrm{e}^{x}}{\strut \left(\mathrm{e}^{x}\right)^2} = \dfrac{\strut \mathrm{e}^{x}(2 - 2\,x+1)}{\strut \mathrm{e}^{2x}} = \mathrm{e}^{-x}(3-2\,x) \) (dérivée de \(\dfrac{u}{v}\)).
    * Si \(f(x)=\mathrm{e}^{4-3\,x}\) alors \(f^\prime(x) = -3\mathrm{e}^{4-3\,x}\) (dérivée de \(g(ax+b)\)).


!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    77, 81, 86, 87, 92 page 152


!!! tip "Remarque"
    Retenez que : <span style="color:red">\(\left(\mathrm{e}^{a\,x+b}\right)^\prime = a\, \mathrm{e}^{a\,x+b}\)</span>.


!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    78, 79 page 152






## IV.2 Représentation graphique des fonctions $t \mapsto \mathrm{e}^{-k\,t}$ et $t \mapsto \mathrm{e}^{k\,t}$

!!! abstract "Propriété IV.1"
    Soit $k$ un réel strictement positif. Voici les courbes des fonctions $f$ définies sur ℝ par :

    ![alt text](images/courbes_exp_kt.png){width=60% ; : .center}

    !!! abstract "Démonstration des variations"
        Si \(f(t)=\mathrm{e}^{-k\,t}\) alors \(f^\prime(t)=-k\,\mathrm{e}^{-k\,t}<0\) car $k>0$ et $\mathrm{e}^x >0$ pour tout réel $x$ donc (\f\) est strictement décroissante.

        Si \(f(t)=\mathrm{e}^{k\,t}\) alors \(f^\prime(t)=k\,\mathrm{e}^{k\,t}>0\) car $k>0$ et $\mathrm{e}^x >0$ pour tout réel $x$ donc (\f\) est strictement croissante.

!!! example "Exemples IV.2"

    * La fonction \(f\) définie sur ℝ par \(f(x)=\mathrm{e}^{-3\,x}\) est strictement décroissante car \(-3<0\).
    * La fonction \(f\) définie sur ℝ par \(f(x)=\mathrm{e}^{4\,x}\) est strictement croissante car \(4>0\).
    * Soit la fonction \(f\) définie sur ℝ par \(f(x)=\mathrm{e}^{-1-2\,x}\).  
    Alors 
    \(f(x)=\mathrm{e}^{-1} \times \mathrm{e}^{-2\,x}\) donc, comme \(\mathrm{e}^{-1}>0\), \(f\) a les mêmes variations que la fonction \(g\) définie sur ℝ par \(g(x)=\mathrm{e}^{-2\,x}\).  
    Or cette fonction \(g\) est strictement décroissante car \(-2<0\) donc \(f\) l'est également.

!!! tip "Remarque"
    Ces fonctions sont très utiles dans différents phénomènes, par exemple la désintégration radioactive.

    ![illustration radioactivité](https://static1.assistancescolaire.com/ele/images/t_t103i03.png)

    [Source : assistancescolaire.com](https://static1.assistancescolaire.com/ele/images/t_t103i03.png)

!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    23, 24, 25, 28, 29 page 149

