# Accueil

Vous trouverez ici mes cours de Première spécialité mathématiques mis à jour en 2023/2024 au format MkDocs.

Pour accéder aux cours au format PDF, à quelques fiches d'exercices et aux fiches réussites, <a href="https://clogique.fr/maths/afficher.php?id=122" target="_blank">suivez ce lien</a>.

**Table des matières**

* [Généralités sur les suites](suite/)
* [Équations et inéquations du second_degre](second_degre/)
* [Trigonométrie dans le cercle trigonométrique](trigo/)
* [Suites arithmétiques. Suites géométriques](suites_ag/)
* [Dérivation (1)](derivation1/)
* [Probabilités conditionnelles](proba_cond/)
* [Produit scalaire](prod_scal/)
* [Dérivation (2)](derivation2/)
* [Fonction exponentielle](exponentielle/)
* [Compléments sur les suites](suites_complements/)
* [Variables aléatoires](var_alea/)
* [Géométrie repérée](geom_rep/)
* [Compléments sur le produit scalaire](prod_scal_compl/)
