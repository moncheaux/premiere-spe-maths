# ⁉️ QCM
!!! bug "Attention"
    Il peut y avoir plusieurs réponses correctes à chaque question !

#### Q1 - Le taux de variation de \(f\) entre \(a\) et \(a+h\) est :
{{ qcm(["la pente d'une sécante", "la pente d'une tangente", "l'ordonnée du point d'abscisse $a$", "l'ordonnée du point d'abscisse $a+h$"], [1], shuffle = True) }}

#### Q2 - Le nombre \(f'(a)\) est :
{{ qcm(["la pente d'une sécante", "la pente d'une tangente", "l'ordonnée du point d'abscisse $a$", "l'ordonnée du point d'abscisse $a+h$"], [2], shuffle = True) }}

#### Q3 - Le nombre \(f(a)\) est :
{{ qcm(["la pente d'une sécante", "la pente d'une tangente", "l'ordonnée du point d'abscisse $a$", "l'ordonnée du point d'abscisse $a+h$"], [3], shuffle = True) }}

#### Q4 - Le taux de variation de \(2\,x^2-1\) entre \(1\) et \(1+h\) est :
{{ qcm(["$4+2\,h$", "$2\,h$", "$1+2\,h$", "$2\,h^2$"], [1], shuffle = True) }}

#### Q5 - Le taux de variation de \(\dfrac{1}{1+x}\) entre \(2\) et \(2+h\) est :
{{ qcm(["$-\dfrac{1}{3(3+h)}$", "$\dfrac{1}{h^2}$", "$\dfrac{1}{3+h}$", "$\dfrac{1}{3}$", "$-\dfrac{h}{3+h}$"], [1], shuffle = True) }}

#### Q6 - Sur le graphique ci-dessous :
{{ qcm(["$f(-1)=0$", "$f(1)=2$", "$f'(-1)=7$", "$f'(1)=-1$", "$f'(-1)=0$", "$f'(1)=2$", "$f'(1)=3$", "$f'(1)=1$", "$f(2)=1$", "$f(7)=0$", "$f'(7)=-1$", "$f(3)=0$", "$f(0)=7$", "$f'(-1)=-7$"], [1,2,3,4], shuffle = True) }}

![](images/pente_tangente.png){width=50%; : .center}

#### Q7 - Sur le graphique ci-dessus, les équations de deux tangentes représentées sont :
{{ qcm(["$y=7\,x+7$", "$y=-x+3$", "$y=7\,x-1$", "$y=-x+7$", "$y=3\,x-1$", "$y=2\,x+1$", "$y=x+2$"], [1,2], shuffle = True) }}

#### Q8 - Une fonction polynome est dérivable :
{{ qcm(["partout", "partout sauf en 0", "sur ℝ$^+$", "sur ℝ$^{+*}$", "nulle part"], [1], shuffle = True) }}

#### Q9 - La fonction racine carrée est dérivable :
{{ qcm(["partout", "partout sauf en 0", "sur ℝ$^+$", "sur ℝ$^{+*}$", "nulle part"], [4], shuffle = True) }}

#### Q10 - La fonction inverse est dérivable :
{{ qcm(["partout", "partout sauf en 0", "sur ℝ$^+$", "sur ℝ$^{+*}$", "nulle part"], [2], shuffle = True) }}

#### Q11 - La dérivée de $3\,x^2-5$ est :
{{ qcm(["$6\,x$", "$x^2-5$", "$x^2$", "$2\,x-5$", "$2\,x$", "$6\,x-5$"], [1], shuffle = True) }}

#### Q12 - La dérivée de $4\sqrt{x} -6\,x^3+1$ est :
{{ qcm(["$8\sqrt{x} -3\,x^2+1$", "$\dfrac{2}{\sqrt{x}}-18\,x^2$", "$8\,\sqrt{1}-3\,x^2$", "$\dfrac{1}{2\sqrt{x}}-18\,x^2+1$", "$\dfrac{1}{\sqrt{x}}-3\,x$", "$-\dfrac{4}{x^2}-18\,x+1$"], [2], shuffle = True) }}

#### Q13 - La dérivée de $5\,x+\dfrac{5}{x}$ est :
{{ qcm(["$5-\dfrac{5}{x^2}$", "$5-\dfrac{5}{x}$", "$1-\dfrac{1}{x^2}$", "$1-\dfrac{5}{x}$", "$1+\dfrac{1}{x^2}$", "$5+\dfrac{1}{x^2}$"], [1], shuffle = True) }}


