% !TEX program = latexmk
\documentclass[12pt,2col]{cours}

\begin{document}

\titre{\'Equations de droites et de cercles (dans le plan)}

\partie{Rappels sur les équations de droites}
\spartie{\'Equations cartésiennes, vecteurs directeurs}

\begin{propriete}
Toute droite $(d)$ du plan a des \rouge{équations cartésiennes} de la forme

\centerline{
\rouge{\encadre{$ax+by+c=0$}}.
}
\end{propriete}


\begin{remarque}
Une droite a une infinité d'équations cartésiennes (proportionnelles entre elles).
\end{remarque}




\begin{exemple}
Trouvez une équation cartésienne de la droite \((AB)\) où \(A\,\coordh{-4}{1}\) et \(B\,\coordh{4}{3}\).


\underline{Réponse :}

\begin{dingautolist}{172}
\item \(\vecteur{AB}\,\Coord{x_B-x_A}{y_B-y_A}\) donc \(\vecteur{AB}\,\Coord{8}{2}\) et 
\(\vecteur{AM}\,\Coord{x_M-x_A}{y_M-y_A}\) donc \(\vecteur{AM}\,\Coord{x+4}{y-1}\) ;
\item 
\(
%\ssi 
%\vecteur{AB}\,\Coord{8}{2} \text{ est colinéaire à }\vecteur{AM}\,\Coord{x+4}{y-1}
\det\left(\vecteur{AB};\vecteur{AM}\right)=
%\)
%\newline
%\(
\begin{vmatrix}
8 	&	x+4
\\
2	&	y-1
\end{vmatrix}
\) ;

\item 
\(
M\,\coordh{x}{y} \in (AB)\ssi
8(y-1)-2(x+4)=0
%\ssi
%8y-8-2x-8=0
\)
\newline
\(
\ssi
-2x+8y-16=0
\ssi
x-4y+8=0
\).
\end{dingautolist}
\end{exemple}


\begin{remarque}
J'ai ici utilisé le fait que le déterminant de deux vecteurs est nul si et seulement si ces vecteurs sont colinéaires.
\end{remarque}

\begin{definition}
Un vecteur non nul \(\vec{u}\) est un \rouge{vecteur directeur} d'une droite \(d\) s'il existe deux points \(A\) et \(B\) de \(d\) tels que 
\(\vecteur{AB}=\vec{u}\).
\end{definition}


\medskip
\centerline{
\psset{unit=7mm,algebraic=true,arrowsize=6pt}
\begin{pspicture*}(-4.6,-1)(5.5,4.6)
\psgrid[subgriddiv=0,gridlabels=0,gridcolor=lightgray](0,0)(-5,-1)(5.5,4.5)
\psaxes[labelFontSize=\small]{->}(0,0)(-4.6,-0.6)(5.5,4.6)
\psplot[algebraic=true]{-5}{5}{x/4+2}
\psdots(-4,1)(4,3)
\uput[120](-4,1){$A$}
\uput[120](4,3){$B$}
\uput[120](1,1){$\vec{u}$}
\psline{->}(-3,0)(5,2)
\uput[-90](5.4,0){$x$}
\uput[180](0,4.4){$y$}
\uput[-135](0,0){$O$}
\end{pspicture*}
}



\begin{propriete}
\centerline{
\newrgbcolor{srsrsr}{0.13 0.13 0.13}
\psset{unit=0.3cm,algebraic=true}
\begin{pspicture*}(-4.78,-8.46)(24,7.5)
\psplot{-4.78}{14}{(5-3*x)/5}
\psline{->}(-1,5)(4,2)
\rput[bl](10,-5){$(d):ax+by+c=0$}
\rput[bl](1.3,3.95){$\vec{u}\Coord{-b}{a}$}
\end{pspicture*}
}

Si $(d)$ a pour équation \(a\,x+b\,y+c=0\) alors le vecteur 
\rouge{$\vec{u}\Coord{-b}{a}$} est un \rouge{vecteur directeur} de $(d)$.
\end{propriete}

\medskip






\begin{exemple}
La droite d'équation $4\,x-3\,y+5=0$ a pour vecteur directeur $\vec{u}\Coord{3}{4}$.
\end{exemple}

\begin{remarque}
\ding{235}
Dans l'exemple précédent, tout vecteur colinéaire à $\vec{u}$ sera aussi un vecteur directeur de la droite.

\ding{235}
Une droite a une infinité de vecteurs directeurs.
\end{remarque}

\medskip

\begin{exemple}
Trouver une équation cartésienne de la droite $(d)$, de vecteur directeur $\vec{u} \Coord{3}{4}$ et passant par $T\, \coordh{2}{-1}$.

\underline{Réponse :}

Le vecteur $\vec{u} \Coord{3}{4}$ est un vecteur directeur de la droite $(d)$ donc 
une équation de $(d)$ s'écrit 
(puisque $-b=3$ et $a=4$) :
$4\,x-3\,y+c=0$.

En remplaçant $x$ et $y$ par les coordonnées de $T$ : 
$4 \times 2-3\times (-1)+c=0$ donne $11+c=0$ donc $c=-11$.

$(d)$ a donc pour équation cartésienne $4\,x-3\,y-11=0$.
\end{exemple}




\begin{exemple}[1]

Retrouver une équation cartésienne de la droite $(AB)$ où \(A\,\coordh{-4}{1}\) et \(B\,\coordh{4}{3}\).

\underline{Réponse :}

Le vecteur $\vecteur{AB} \Coord{8}{2}$ est un vecteur directeur de la droite $(AB)$ donc une équation de $(AB)$ s'écrit 
(puisque $-b=8$ et $a=2$) :
\linebreak $2x-8y+c=0$.

En remplaçant $x$ et $y$ par les coordonnées de $A$ (ou de $B$) : 
\linebreak
$2 \times (-4)-8\times 1+c=0$ donne $-16+c=0$ donc $c=16$.

$(AB)$ a donc pour équation $2x-8y+16=0$ ou encore $x-4y+8=0$.
\end{exemple}

\newpage

\begin{exemple}[3]
Trouver une équation cartésienne de la droite $(d')$, parallèle à la droite $(d)$ : 
$4\,x-3\,y+11=0$ et passant par $E\, \coordh{5}{-2}$.

\underline{Réponse :}

Le vecteur $\vec{u} \Coord{3}{4}$ est un vecteur directeur de la droite $(d)$ donc 
c'est aussi un vecteur directeur de $(d')$.

Une équation de $(d')$ s'écrit donc 
(puisque $-b=3$ et $a=4$) :
\linebreak $4\,x-3\,y+c=0$.

En remplaçant $x$ et $y$ par les coordonnées de $E$ : 
\linebreak
$4 \times 5-3\times (-2)+c=0$ donne $26+c=0$ donc $c=-26$.

$(d')$ a donc pour équation cartésienne $4\,x-3\,y-26=0$.
\end{exemple}





\spartie{\'Equations réduites, coefficients directeurs}

\begin{propriete}
\tabfig{
L'équation $ax+by+c=0$ peut s'écrire, selon les cas, sous l'une des deux formes \rouge{réduites} suivantes : \rouge{\fbox{$y=mx+p$}} ou \rouge{\fbox{$x=k$}}.
\newline
Le coefficient $m$ est le \rouge{coefficient directeur} de la droite et $p$ est son \rouge{ordonnée à l'origine}.% (ordonnée du point d'intersection de la droite avec l'axe des ordonnées).
}
{
\psset{unit=0.8cm}
\begin{pspicture}(-2.5,-2)(4,2.5)
\psaxes[labels=none,ticks=none]{->}(0,0)(-2.5,-2)(4,2.5)
\psline(-1.5,-2)(-1.5,2)
\uput[-225](-1.5,0){$k$}
\uput[90](-1.5,2){$\boxed{x=k}$}
\psline(-2,-2)(3.5,0.75)
\uput[135](0,-1){$p$}
\psdot(0,-1)
\psline[linestyle=dotted](0.5,-0.75)(1.5,-0.75)(1.5,-0.25)
\uput[-90](1,-0.75){1}
\uput[0](1.5,-0.5){$m$}
\uput[135](3,0.5){$\boxed{y=mx+p}$}
\end{pspicture}
}
\end{propriete}

\medskip

\begin{propriete}
Si $x_A \neq x_B$ alors la droite $(AB)$ a un coefficient directeur égal à 
\rouge{\fbox{$m=\dfrac{\Delta y}{\Delta x} = \dfrac{y_B-y_A}{x_B-x_A}$}}.
\end{propriete}

\medskip

\begin{exemple}[1]

Trouver l'équation réduite de la droite $(AB)$.

\underline{Réponse :}

\begin{itemize}
\item je calcule le coefficient directeur 
$m=\dfrac{y_B-y_A}{x_B-x_A}=\dfrac{2}{8}=\dfrac{\strut1}{\strut4}$ ;
\item l'équation réduite s'écrit donc $y=\dfrac{\strut1}{\strut4}\,x+p$ ;
\item pour trouver $p$, je remplace $x$ et $y$ par les coordonnées de $A$ (ou de $B$) :
$1=\dfrac{\strut1}{\strut4}\times(-4)+p$ donc $p=2$ ;
\item l'équation réduite de la droite $(AB)$ est $y=\dfrac{\strut1}{\strut4}\,x+2$ (ce qui est équivalent à $x-4y+8=0$).
\end{itemize}
\end{exemple}

\begin{remarque}
\ding{235} 
Si je connais une équation cartésienne, je peux trouver l'équation réduite, ainsi, dans l'exemple précédent :

$
x-4y+8=0
\iff
4y = x+8
\iff
y = \dfrac{\strut1}{\strut 4}\,x+2
$.

\ding{235} Une droite n'a qu'une seule équation réduite mais une infinité d'équations cartésiennes.
\end{remarque}

\newpage

\ttwpnouvellepage

\partie{Vecteurs normaux. Projeté orthogonal}

\spartie{Vecteurs normaux}


\begin{propriete}
\centerline{
\newrgbcolor{srsrsr}{0.13 0.13 0.13}
\psset{unit=2.5mm,algebraic=true}
\begin{pspicture*}(-1.78,-8.46)(30,7.2)
\pspolygon[linecolor=srsrsr,fillcolor=srsrsr,fillstyle=solid,opacity=0.1](8.77,-3.72)(8.37,-3.48)(8.13,-3.88)(8.53,-4.12)
\psplot{-4.78}{14}{(5-3*x)/5}
\psline{->}(11,0)(14,5)
\psline[linestyle=dotted](8.53,-4.12)(11,0)
\rput[bl](14.23,-7.11){$(d):ax+by+c=0$}
\rput[bl](13,0.2){$\vec{n}\Coord{a}{b}$}
\end{pspicture*}
}

Si $(d)$ a pour équation \(a\,x+b\,y+c=0\) alors le vecteur 
\rouge{$\vec{n}\Coord{a}{b}$} est un \rouge{vecteur normal} de $(d)$.
\end{propriete}





\begin{exemple}[3]
La droite $(d):4\,x-3\,y+11=0$, a pour vecteur normal $\vec{n}\Coord{4}{-3}$.
\end{exemple}


\begin{remarque}
\ding{235}
Dans l'exemple précédent, tout vecteur colinéaire à $\vec{n}$ sera aussi un vecteur normal de $(d)$.

\ding{235}
Une droite a une infinité de vecteurs normaux.
\end{remarque}




\begin{exemple}
Trouver une équation de la droite $(d)$, de vecteur normal $\vec{n}\Coord{-4}{7}$
et passant par $K\, \coordh{6}{-5}$.

\underline{Réponse :}

D'après la propriété 5, une équation de $(d)$ s'écrit $-4\,x+7\,y+c=0$.

En remplaçant $x$ et $y$ par les coordonnées de $K$, nous trouvons \linebreak
$-24-35+c=0$ d'où $c=59$ donc $(d):-4\,x+7\,y+59=0$.
\end{exemple}



\begin{exemple}[1]
Trouver une équation cartésienne de la droite $(d'')$, perpendiculaire à la droite $(AB)$ et passant par $C\, \coordh{-1}{2}$.

\underline{Réponse :}
Le vecteur $\vecteur{AB}\Coord{8}{2}$ est un vecteur normal de la droite~$(d'')$ donc une équation de $(d'')$ s'écrit $8\,x+2\,y+c=0$.

En remplaçant $x$ et $y$ par les coordonnées de $C$, nous trouvons \linebreak
$-8+4+c=0$ d'où $c=4$ donc $(d'')$ a pour équation $8\,x+2\,y+4=0$ ou encore 
$4\,x+y+2=0$.
\end{exemple}



\begin{exemple}[3]
Trouver une équation cartésienne de la droite $(\Delta)$, perpendiculaire à la droite $(d)$ : 
$4\,x-3\,y+11=0$ et passant par $E\, \coordh{5}{-2}$.

\smallskip
\underline{Réponses :}

\smallskip
\underline{Méthode 1 :}

Le vecteur $\vec{u}\Coord{3}{4}$ est un vecteur directeur de la droite $(d)$ donc un vecteur normal de $(\Delta)$ ; une équation de $(\Delta)$ s'écrit donc 
(puisque $a=3$ et $b=4$) : $3\,x+4\,y+c=0$.

En remplaçant $x$ et $y$ par les coordonnées de $E$, nous trouvons \linebreak $c=-7$ donc $(\Delta)$ a pour équation $3\,x+4\,y-7=0$.


\smallskip
\underline{Méthode 2 :}

\begin{dingautolist}{172}
\item 
Le vecteur $\vec{n}\Coord{4}{-3}$ est un vecteur normal de la droite $(d)$ ;

\item 
\(
M\in (\Delta)
\iff
\vecteur{EM}
\) colinéaire à \(\vec{n}
\iff
\det\left(\vecteur{EM};\vec{n}\right)=0
\) ;

\item 
\(\vecteur{EM}\,\Coord{x-5}{y+2}\) et
\(
\det\left(\vecteur{EM};\vec{n}\right)
=
\begin{vmatrix}
x-5 	&	4
\\
y+2	&	-3
\end{vmatrix}
=
-3\,x-4\,y+7
\) ;

\item 
\(
M\in (\Delta)
\iff
-3\,x-4\,y+7 = 0
\iff
3\,x+4\,y-7 = 0
\) ; ceci est donc une équation de \(\Delta\).
\end{dingautolist}






\smallskip
\underline{Méthode 3 :} trouver deux points de la droite $(d)$ et procéder comme dans l'exemple précédent.
\end{exemple}




\begin{remarques}
\ding{235} 
Il est vivement conseillé de faire une figure, au minimum au brouillon et au mieux sur Geogebra.

\ding{235} Nous pouvions aussi utiliser le produit scalaire pour trouver l'équation de $(d'')$.
\end{remarques}





\spartie{Coordonnées d'un projeté orthogonal}


\begin{exemple}[1]
Trouver les coordonnées du projeté orthogonal de $C$ sur la droite~$(AB)$.

\underline{Réponse :}
soit $H$ ce point ; alors $H$ est le point d'intersection de $(AB)$ et de $(d'')$.
Je résous donc le système :
$
\begin{systeme}
x-4\,y+8=0
\\
4\,x+y+2=0
\end{systeme}
$

en multipliant la première équation par 4 :
$
\begin{systeme}
4\,x-16\,y+32=0
\\
4\,x+y+2=0
\end{systeme}
$
puis en soustrayant les deux lignes :
$-17\,y+30=0$
donc $y=\dfrac{30}{17}$
puis 
$
4\,x + \dfrac{30}{17}+2=0
$
donne (\dots)
$x=-\dfrac{\strut64}{\strut17}\div4 = -\dfrac{16}{17}$.

Donc les coordonnées du projeté orthogonal du point $C$ sur la droite~$(AB)$ sont
$\coordh{-\dfrac{\strut16}{\strut17}}{\dfrac{30}{17}}$.
\end{exemple}




\begin{remarque}
\ding{235} Il est parfois nécessaire de multiplier chaque ligne du système par un coefficient différent.

\ding{235} Une fois les coordonnées du projeté orthogonal connues, il est simple de calculer la distance entre un point et une droite (ainsi que l'aire d'un triangle).
\end{remarque}







\partie{\'Equation d'un cercle dans le plan}
\spartie{Définition et expression}
%Soit $\Omega$ un point de coordonnées $\coordh{a}{b}$ et $R$ un réel positif.

\begin{definition}
Une \rouge{équation de cercle} est une égalité que doivent vérifier les coordonnées d'un point pour que ce point soit sur le cercle.
\end{definition}

\medskip
\begin{propriete}
Le cercle de centre $\Omega\coordh{a}{b}$ et de rayon $R$ a pour équation 

\centerline{\rouge{$(x - a)^2+(y - b)^2 =R^2$}.}
\end{propriete}

\medskip
\begin{demonstration}
Le cercle $\cal C$ de centre $\Omega$ et de rayon $R$ est l'ensemble des points~$M$ tels que $\Omega M=R$. Donc :

$M\, \coordh xy \in \cal C \ssi \Omega M^2=R^2\ssi (x - a)^2+(y - b)^2 =R^2$.
\end{demonstration}

\medskip
\begin{remarque}
L'équation d'un cercle est une écriture de la relation de Pythagore.
\end{remarque}


\begin{exemple}
Le cercle de centre $\Omega\coordh{1}{-6}$ et de rayon $10$ a pour équation \linebreak
$(x - 1)^2+(y +6)^2 =100$.
\end{exemple}

\newpage


\spartie{Cercle défini par un diamètre}

\begin{exemple}
Déterminer l'équation du cercle $\cal C$ de diamètre $[FG]$, où 
$F\, \coordh{-4}{-1}$ et $G\, \coordh{2}{5}$.

\smallskip
\underline{Réponses :}

\smallskip
\underline{Méthode 1 :}

\begin{dingautolist}{172}
\item 
Je calcule les coordonnées du centre du cercle $\Omega$, qui est le milieu de $[FG]$ :

\(
x_{\Omega} = \dfrac{x_F+x_G}{2} 
= 
\dfrac{-4+2}{2}=-1
\)
et
\(
y_{\Omega} = \dfrac{y_F+y_G}{2} 
= 
\dfrac{-1+5}{2}=2
\)
donc 
\(\Omega\,\coordh{-1}{2}\) ;

\item Je calcule le rayon du cercle :

\(R = \Omega G = \sqrt{(2-(-1))^2+(5-2)^2} = \sqrt{18}\) ;

\item 
L'équation du cercle $\cal C$ est donc 
\((x+1)^2 + (y-2)^2 = 18\).


\end{dingautolist}

\smallskip
\underline{Méthode 2 :}

Je peux aussi utiliser une propriété géométrique :
\og Un point $M$ appartient au cercle de diamètre $[AB]$ si et seulement si le triangle $ABM$ est rectangle en ce point \fg.

Donc :

\(
M\in \cal C
\iff
FGM \text{  est rectangle en }M
\iff
\vecteur{FM}\cdot \vecteur{GM} = 0
\iff
(x+4)(x-2)+(y+1)(y-5) = 0
\iff
x^2 +2x+y^2-4y-13=0
\iff
(x+1)^2-1 +(y-2)^2-4-13=0
\iff
(x+1)^2 +(y-2)^2=18
\).

Voir le 3) pour une explication de la fin du calcul.
\end{exemple}




\spartie{Reconnaître une équation de cercle}

L'objectif est ici de savoir si une équation donnée est celle d'un cercle, et, dans l'affirmative, de donner son centre et son rayon.

%\medskip
%
%Pour cela, remarquons que :
%
%$
%(x-a)^2+(y-b)^2=R^2
%\ssi
%x^2-2ax+a^2+y^2-2by+b^2=R^2
%\ssi
%\rouge{x^2+y^2-2ax-2by+a^2+b^2-R^2=0}
%$

La technique est celle de la mise sous forme canonique : %, déjà vue pour les fonctions du second degré.
 il faut voir si l'on peut utiliser l'identité remarquable $(x-a)^2=x^2-2ax+a^2$.




\begin{exemple}
Vérifier que $x^2+y^2-4x+y+3=0$ est l'équation d'un cercle dont on précisera le centre et le rayon.

\underline{Réponse :} 

%En comparant $x^2+y^2-4x-y+3=0$
%avec $x^2+y^2-2ax-2by+a^2+b^2-R^2=0$
%on voit que :
%
%$-2a=-4$ donc $a=2$ ; 
%
%$-2b=1$ donc $b=-1/2$ ; 
%
%$a^2+b^2-R^2=3$
%donc $R^2=a^2+b^2-3=4+1/4-3=5/4$
%donc $R=\dfrac{\sqrt{5}}{2}$.



Nous avons :\\[1ex]
$x^2-4x = (x^2-4x+4)-4 = (x-2)^2-4$

$y^2-y = \left(y^2-2\times y\times \dfrac{1}{2}+\left(\dfrac{1}{2}\right)^2\right)-\left(\dfrac{1}{2}\right)^2
=\left(y-\dfrac{1}{2}\right)^2-\dfrac{1}{4}$

donc 

$x^2+y^2-4x+y+3=0 \ssi (x-2)^2+\left(y+\dfrac{1}{2}\right)^2 = \dfrac{5}{4}$ : l'équation est bien celle d'un cercle, de centre $\Omega$ \coordh{2}{-\dfrac{1}{2}} et de rayon $\dfrac{\sqrt{5}}{2}$.
\end{exemple}







\spartie{Intersection avec une droite parallèle aux axes}

%\begin{exemple}
%Soit le cercle $\cal C$ de centre $\Omega\coordh{10}{3}$ et de rayon $R=\sqrt{65}$.
%
%Déterminer les coordonnées des points d'intersection de la droite $(d)$ d'équation $2\,x+y-8=0$ et du cercle~$\cal C$.
%
%\underline{Réponses :}
%le système 
%$
%\left\{
%\begin{array}{l}
%\boxed{y}=\boxed{-2\,x+8}
%\\
%(x-10)^2+(\boxed{y}-3)^2=65
%\end{array}
%\right.
%$
%donne 
%
%$(x-10)^2+(-2\,x+8-3)^2=65$
%donc
%$(x-10)^2+(-2\,x+5)^2 - 65=0$
%d'où
%$5\,x^2-40\,x+60=0$ donc
%$x^2-8\,x+12=0$.
%Cette équation du second degré a pour solutions (\dots)
%$x_1 = 2$ et $x_2 = 6$.
%
%Comme $y=-2\,x+8$, 
%$y_1 = 4$ et $y_2 = -4$
%d'où
%\(M_1\,\coordh{2}{4}\)
%et
%\(M_2\,\coordh{6}{-4}\).
%\end{exemple}
%
\begin{exemple}
Soit le cercle $\cal C$ de centre $\Omega\coordh{-5}{2}$ et de rayon $R=4$.

Déterminer les coordonnées des points du cercle~$\cal C$ d'abcisse 0 ou d'ordonnée 5.


\underline{Réponses :}

Pour les points d'abscisse 0 :
je résous le système 

\[
\left\{
\begin{array}{l}
\boxed{x}=\boxed{0}
\\
(\boxed{x}+5)^2+(y-2)^2 = 16
\end{array}
\right.
\]
qui donne 
$(0+5)^2+(y-2)^2=16$
donc
$(y-2)^2=16-25 = -9$.

Le carré d'un réel est toujours positif donc cette équation n'a pas de solution : le cercle 
$\cal C$ n'a pas de points d'abscisse 0.


Pour les points d'ordonnée 5 :
je résous le système 
\[
\left\{
\begin{array}{l}
\boxed{y}=\boxed{5}
\\
(x+5)^2+(\boxed{y}-2)^2 = 16
\end{array}
\right.
\]
qui donne 
$(x+5)^2+(5-2)^2=16$
donc
$(x+5)^2=16-9 = 7$
d'où
$x+5 = \pm \sqrt{7}$
donc
$x = -5\pm \sqrt{7}$.

Le cercle $\cal C$ a deux points d'ordonnée 5 : 
\(M_1\,\coordh{-5-\sqrt{7}}{5}\)
et
\(M_2\,\coordh{-5+\sqrt{7}}{5}\).

\end{exemple}


\begin{remarque}
Il est également possible de déterminer l'intersection d'une droite quelconque et d'un cercle ou de deux cercles grâce aux équations du second degré.
\end{remarque}






\end{document}