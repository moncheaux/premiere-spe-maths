# II - Probabilité d'un événement (rappels)
## Définitions
!!! abstract "Définition II.1"
    Nous définissons une <span style="color:red;">probabilité</span> sur l'univers $\Omega$ en associant à tout événement $A$ un nombre <span style="color:red;">$P(A)$</span> tel que :  

    <div style="color:red">

    $P(\Omega)=1$

    Si $A\cap B=\emptyset$ alors $P(A\cup B)=P(A)+P(B)$

    </div>

!!! abstract "Définition II.2"
    Lorsque les éventualités ont la même probabilité (exemple : lancer d'un dé non truqué), nous sommes dans une situation d'<span style="color:red;">équiprobabilité</span>.



## Propriétés des probabilités
!!! abstract "Propriété II.1"
    Si $A$ est un ensemble fini alors <span style="color:red;">$P(A)$ est la somme des probabilités des éventualités contenues dans $A$</span>.


!!! example "Exemple II.1"
    Supposons qu'un dé soit truqué, avec une chance sur deux d'avoir un 6, les autres résultats étant équiprobables.

    Nous avons alors le tableau suivant (ou **loi de probabilité**) :

    Résultat | 1 | 2 | 3 | 4 | 5 | 6  
    - | - | - | - | - | - | -  
    Probabilité | 1/10 | 1/10 | 1/10 | 1/10 | 1/10 | 1/2
    
    La probabilité d'obtenir un nombre pair est alors :  
    $P({2})+P({4})+P({6})=\dfrac{7}{10}$.

!!! abstract "Propriété II.2"
    Dans un cas d'<span style="color:red;">équiprobabilité</span>, la probabilité d'un événement A est :

    <div style="color:red">
    
    $$\boxed{P(A) =\dfrac{\mbox{nombre d'éventualités dans $A$}}{\mbox{nombre total d'éventualités}} =\dfrac{\text{card } A} {\text{card } \Omega} }$$

    </div>

    (nombre de cas favorables / nombre de cas possibles).

!!! example "Exemple II.2"
    Si le dé n'est pas truqué, la probabilité d'obtenir un nombre pair est $\dfrac{1}{2}$ car il y a 6 cas possibles (nombre total d'éventualités) et 3 cas favorables (nombre d'éventualités réalisant « chiffre pair »).

!!! abstract "Propriétés II.3"
    Soient $A$ et $B$ deux événements. Nous admettons les propriétés suivantes :

    <div style="color:red">

    $P(\emptyset)=0$

    Si $A\subset B$ alors $P(A)\leqslant P(B)$

    $\boxed{0\leqslant P(A)\leqslant 1}$

    $\boxed{P(A\cup B)=P(A)+P(B)-P(A\cap B)}$

    $\boxed{P\left(\bar A\right)=1-P(A)}$

    </div>

!!! tip "Remarque"
    Comme vu dans la définition II.1, si $A$ et $B$ sont incompatibles alors $P(A\cup B)=P(A)+P(B)$.
    
    Cette propriété sera très utile dans la suite.


!!! example "Exemple II.3"
    Supposons que le dé est non truqué. Soient les événements  
    $A$ = « le chiffre est pair », $B$ = « le chiffre est inférieur à 5 », $C$ = « on obtient un 5 ». 
    Nous avons $P(A)=\dfrac{3}{6}$ et $P(B)=\dfrac{4}{6}$.

    De plus $A\cap B$ est l'événement « le chiffre est pair et inférieur à 5 » donc $A\cap B=\{2;4\}$ d'où $P(A\cap B) =\dfrac{2}{6}$.
    L'événement $A\cup B$ est « le chiffre est pair ou inférieur à 5 » donc $A\cup B=\{1;2;3;4;6\}$ d'où $P(A\cup B)=\dfrac{5}{6}$.

    En utilisant le cours, nous retrouvons le même résultat :

    $P(A\cup B)=P(A)+P(B)-P(A\cap B)=\dfrac{3}{6}+\dfrac{4}{6}-\dfrac{2}{6}= \dfrac{5}{6}$.

    Nous avons $P(C)=\dfrac{1}{6}$ et $A\cap C = \emptyset$.
    L'événement « le chiffre est pair ou est 5 » est $A\cup C$ et $P(A\cup C)=P(A)+P(C)=\dfrac{3}{6}+\dfrac{1}{6}=\dfrac{4}{6}=\dfrac{2}{3}$.

!!! example "Exemple II.4"
    Nous lançons deux fois un dé non truqué. Les éventualités sont des couples tels que $(1\,;\,4)$ ou $(5\,;\,5)$, etc. et il y a équiprobabilité. Il y a 36 couples possibles donc 36 éventualités pour cette expérience.

    Soit $A$ l'événement « On obtient un double 5 ».
    Alors $A$ ne contient qu'une éventualité : $A=\{(5\,;\,5)\}$ donc 
    $P(A)=\dfrac{\strut 1}{\strut 36}$.

    Soit $B$ l'événement « On obtient au moins un chiffre supérieur à 2 ».
    Alors $\bar B$ est l'événement « On n'obtient que des 1 ou des 2 ».

    Au lieu de calculer $P(B)$, on calcule $P\left(\bar B\right)$ car 
    $\bar B$ contient moins d'éventualités que $A$.  
    On a $\bar B=\{(1\,;\,1),(1\,;\,2),(2\,;\,1),(2\,;\,2)\}$ donc 
    $P(\bar B)=\dfrac{\strut 4}{\strut 36}=\dfrac{1}{9}$, on en déduit que $P(B)=1-\dfrac{1}{9}=\dfrac{8}{9}$.

!!! example "Exemple II.5"
    Supposons que $P(A)=0,8$, $P(\bar B)=0,3$ et que $P(A\cap B)=0,69$. Quelle est alors la valeur de $P(A\cup B)$ ?

    Réponse :  
    D'abord, $P(B)=1-P(\bar B)=0,7$ puis  
    $P(A\cup B)=P(A)+P(B)-P(A\cap B) = 0,8+0,7-0,69 = 0,81$.

## Tableau à double entrée
!!! example "Exemple II.6"
    Avec les données de l'exemple II.5, nous cherchons $P(\bar A\cap\bar B)$.

    **1^ière^ approche :**

    $\bar A\cap\bar B$ est l'ensemble des éventualités qui ne sont ni dans $A$ ni dans $B$, c'est donc le contraire de $A\cup B$ (voir figure).

    ![figure exemple II.6](images/fig_exemple.png){: .center}

    Comme $P(A\cup B)=0,81$, il vient
    $P(\bar A\cap\bar B)=1-P(A\cup B)=0,19$.

    **2^ième^ approche :**

    Le tableau qui suit est un tableau à double entrée (ou tableau de Karnaugh) :

    ![tableau Karnaugh](images/fig_exemple_Karnaugh.png){: .center}
   
    dans lequel j'ai indiqué en gras les valeurs données dans l'énoncé de $P(A)$, de $P(\bar B)$ et de $P(A\cap B)$.

    Il ne reste plus qu'à le compléter pour trouver $P(\bar{A}\cap\bar{B})=0,19$.

!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    4 page 248 (exemple)  
    9 page 254 avec $P(\bar{A}\cup B)$  
    6 page 254  
    14 page 255


