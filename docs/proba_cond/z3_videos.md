# Des vidéos d'explications

## Cours complet
[Une vidéo de cours avec beaucoup d'exemples sur le site Lumni](https://www.lumni.fr/video/les-probabilites-conditionnelles-et-lindependance-de-deux-evenements)

## Probabilité conditionnelle
* [explication du principe](https://www.youtube.com/watch?v=BbD9Dpk3E5Y) ;
* compléter un arbre et faire quelques calculs : [ici](https://www.youtube.com/watch?v=FBbTySm7kfk) et [là](https://www.youtube.com/watch?v=vOgSZWi_tHk) ;
* [utilisation d'un tableau pour trouver quelques probabilités](https://www.youtube.com/watch?v=HLyp6SInbQQ) ;
* [manipulation des formules et un exemple de calcul](https://www.youtube.com/watch?v=LdGhHyfFepg) ;
* construire un arbre à partir d'un énoncé puis calculs de probabilités : [ici](https://www.youtube.com/watch?v=fTkpJseiW9k) et [là](https://www.youtube.com/watch?v=YnM9-9K1fWY) ;
* [formule des probabilités totales](https://www.youtube.com/watch?v=izEY7ggfykM).
