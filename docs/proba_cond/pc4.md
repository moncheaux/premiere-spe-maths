# IV - Indépendance
!!! abstract "Définition"
    Deux événements $A$ et $B$ sont <span style="color:red;">indépendants</span> si <span style="color:red;">$P(A\cap B)=P(A)\times P(B)$</span>.


!!! tip "Remarque"
    Le fait que $A$ soit réalisé ou non n'influe alors pas sur la probabilité que $B$ se réalise. En effet, si $A$ et $B$ sont indépendants alors  $P_A(B)=\dfrac{P(B\cap A)}{P(A)} =\dfrac{P(B)\times P(A)}{P(A)}=P(B)$.



!!! exemple "Exemple IV.1"
    Prenons l'univers ci-dessous, dans une situation d'équiprobabilité :
    
    ![Alt text](images/pc3.png){ width=30%; : .center }
    
    Nous avons alors :

    $P(A)=\dfrac{3}{6}=\dfrac{1}{2}$, $P(B)=\dfrac{4}{6}=\dfrac{2}{3}$ et  $P(A\cap B)=\dfrac{2}{6}=\dfrac{1}{3}$  
    donc
    $P(A)\times P(B) = \dfrac{1}{2} \times \dfrac{2}{3} = \dfrac{1}{3} = P(A\cap B)$
    donc $A$ et $B$ sont indépendants.

    Remarquez également que 
    $P_A(B)=\dfrac{2}{3}=P(B)$, ce qui prouve également l'indépendance.


!!! exemple "Exemple IV.2"
    Nous tirons une carte d'un jeu de 32 cartes. Soit $A$ l'événement « c'est un valet ».  
    Soit $B$ l'événement « c'est un carreau ».
    Les évenements $A$ et $B$ sont indépendants.
    En effet, le fait de savoir que la carte est un carreau 
    n'augmente pas la probabilité d'avoir un valet.

    Vérifions le : $P(A)=\dfrac{1}{\strut8}$, $P(B)=\dfrac{1}{4}$ et $P(A\cap B)=\dfrac1{32}$ et nous avons bien
    $P(A\cap B)=P(A)\times P(B)$.


!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    92, 94, 95, 96, 98, 100 page 263  
    131 page 269  
    


!!! tip "Remarque"
    Les tirages avec remises donnent lieu à des résultats indépendants.

!!! exemple "Exemple IV.3"
    Dans une boîte contenant 20 boules dont 13 rouges et 7 noires, je prélève successivement deux boules et je cherche la probabilité d'obtenir deux boules rouges.
    Notons $R_1$ et $R_2$ les événements :

    $R_1$ : « la première boule est rouge » ;
    $R_2$ : « la seconde boule est rouge ».

    Nous cherchons donc $P(R_1\cap R_2)$.

    * 1^ier^ cas : avec remise. Je prends une première boule que je remets dans la boîte puis j'en tire une seconde (qui peut être éventuellement la même que la première).      
    Le résultat du premier tirage n'influe pas sur celui du second : les événements $R_1$ et $R_2$ sont indépendants donc :      
    $P(R_1\cap R_2)=P(R_1)\times P(R_2)=\left(\dfrac{13}{20}\right)^2.$
    * 2^ième^ cas : sans remise. Je prends une première boule puis une seconde sans remettre la première dans l'urne.  
    Si $R_1$ est réalisé alors il reste 12 boules rouges sur 19 restantes pour le second tirage et donc 
    $P_{R_1}(R_2)=\dfrac{12}{19}$ d'où      
    $P(R_1\cap R_2)=P(R_1)\times P_{R_1}(R_2)=\dfrac{13}{20}\times\dfrac{12}{19}=\dfrac{156}{380}.$
    

!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    109 page 265  
    125 page 268  
    Pour les plus rapides : 129 page 268