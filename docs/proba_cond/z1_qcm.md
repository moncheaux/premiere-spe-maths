# ⁉️ QCM

Quelques quiz en ligne :

* [sur le site Lumni](https://www.lumni.fr/quiz/les-probabilites-conditionnelles)
* [sur le site maths-cours](https://www.maths-cours.fr/quiz/1re-probabilites-conditionnelles)
* [sur le site Kartable](https://www.kartable.fr/ressources/mathematiques/quiz/probabilites-conditionnelles-et-independance-1/56024/140974)
