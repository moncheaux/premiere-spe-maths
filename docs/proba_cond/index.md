# I - Événements (rappels)
## Notion d'événement
!!! example "Exemple I.1"

    Je lance une fois un dé à six faces numérotées de 1 à 6 et
    je m'intéresse au chiffre apparaissant sur la face du dessus.

    Il y a ici six <span style="color:red;">éventualités</span> (six résultats possibles) et l'ensemble de toutes les éventualités est l'<span style="color:red;">univers</span> $\Omega=\{1,2,3,4,5,6\}$.

    Un <span style="color:red;">événement</span> est un ensemble d'éventualités, son __cardinal__ est le nombre d'éventualités qui y sont contenues.

    Par exemple soit l'événement $A$ :
    « le chiffre est pair ». Alors $A$ peut être représenté par l'ensemble $\{2,4,6\}$ et on a $\text{card }A=3$.

    De même, l'événement $B$ : « le chiffre est inférieur à 5 » peut s'écrire $B=\{1,2,3,4\}$ et on a $\text{card }B=4$.


    ![](images/evts.png){ width=25%; : .center }

    Un événement peut être __impossible__ : par exemple, « le chiffre est 7 » est impossible, on note cet événement $\emptyset$.

    Un événement peut être __certain__ : par exemple, « le chiffre est un entier » est certain et contient toutes les éventualités.  Cet événement est l'univers $\Omega$.


!!! example "Exemple I.2"
    Je lance le dé deux fois.  
    Les éventualités peuvent alors être représentées sous forme de couples $(a;b)$ ($a$ : 1er chiffre, $b$ : second chiffre). Il y a alors $6\times6=36$ éventualités.


## Opérations sur les événements
### Réunion et intersection de deux événements

Dans la suite $A$ et $B$ sont deux événements.

!!! abstract "Définition I.1"
    La <span style="color:red;">réunion</span> des événements $A$ et $B$, notée <span style="color:red;">$A\,\cup\,B$</span> (ce qui se lit $A$ union $B$), est l'ensemble des éventualités qui sont dans $A$ <span style="color:red;">ou</span> dans $B$.

    ![](images/union.png){ width=25%; : .center }

!!! bug "ou ?"
    Le « ou » est ici un « ou » inclusif.  
    Cela signifie que la réunion réunit les éventualités qui sont dans un événement ou dans l'autre **ou dans les deux**.

!!! example "Exemple I.3"
    $\{2,4,6\}\cup\{1,2,3,4\}=\{1,2,3,4,6\}$.


!!! abstract "Définition I.2"
    L'<span style="color:red;">intersection</span> des événements $A$ et $B$, notée <span style="color:red;">$A\,\cap\,B$</span> (ce qui se lit $A$ *inter* $B$), est l'ensemble des éventualités qui sont dans $A$ <span style="color:red;">et</span> dans $B$.

    ![](images/inter.png){ width=25%; : .center }

!!! bug "et ?"
    Le « et » signifie ici dans les deux événements **en même temps**. 

!!! example "Exemple I.4"
    $\{2,4,6\}\cap\{1,2,3,4\}=\{2,4\}$.


!!! example "Exemple I.5"
    Choisissons au hasard une personne dans une foule.

    Soient les événements :   
    $A$ : « la personne est une femme » ;  
    $B$ : « la personne est adulte » et  
    $C$ : « la personne est du 3^ième^ âge ».

    $A \cup B$ est l'événement « la personne est une femme ou une personne adulte ».  
    $A \cap B$ est l'événement « la personne est une femme adulte ».

    Remarquons que dans cet exemple $B\cap C=C$ et que $B \cup C = B$.

### Événements incompatibles

!!! abstract "Définition I.3"
    Deux événements $A$ et $B$ sont <span style="color:red;">incompatibles</span> (ou disjoints)    <span style="color:red;">si $A \cap B = \emptyset$</span> donc s'il est impossible que $A$ et $B$ se réalisent en même temps.
    
    ![](images/incomp.png){ width=25%; : .center }

!!! example "Exemple I.6"
    Quand je lance un dé, les événements « le chiffre est un 5 » et « le chiffre est pair » sont incompatibles.


### Événement contraire

!!! abstract "Définition I.4"
    Soit $A$ un événement. L'<span style="color:red;">événement contraire</span> de $A$, noté $\bar A$, est l'événement constitué des éventualités qui ne sont pas dans $A$.
    
    ![](images/contraire.png){ width=25%; : .center }

!!! example "Exemple I.7"
    Quand je lance un dé, le contraire de l'événement $A=\{2,5\}$ est $\bar A=\{1,3,4,6\}$.

!!! tip "Remarque"
    Deux événements contraires sont incompatibles mais deux événements incompatibles ne sont pas forcément contraires.
    Ainsi, dans le cas du lancer d'un dé, $A=\{2\,;\,5\}$ et $B=\{1\,;\,3\}$ sont incompatibles mais non contraires.

!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) : 2 page 245 ; 2 et 12 page 254
