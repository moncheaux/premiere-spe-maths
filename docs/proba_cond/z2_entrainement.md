# Quelques exercices pour vous entraîner
Voici une série d'exercices ludiques, sur Learningapps.

## Rappels (de Seconde) sur les probabilités
* [vocabulaire en mots croisés](https://learningapps.org/9813960)
* [vocabulaire des événements](https://learningapps.org/1982453)
* [memory (associer des cartes)](https://learningapps.org/11876960)
* [compléter un tableau à double entrée](https://learningapps.org/11376078)
* [trouver des probabilités avec un tableau](https://learningapps.org/11380955)
* [compléter et utiliser un tableau](https://learningapps.org/5317246)
* [calcul divers](https://learningapps.org/11779038)

## Probabilité conditionnelle
* [petit QCM](https://learningapps.org/13478877)
* [quelques questions avec un arbre](https://learningapps.org/11713685)
* [une série d'exercices](https://learningapps.org/8134847)

## Indépendance
* [deux exercices avec un arbre pour commencer](https://learningapps.org/7176911)

## Sur Wims
[Une série d'exercices variés](https://wims.math.cnrs.fr/wims/fr_H5~probability~oefprobacond.fr.html)