# III - Probabilité conditionnelle
## Définition
!!! abstract "Définition III.1"
    Nous nous intéressons ici aux cas où la réalisation d'un événement $B$ dépend de celle d'un autre événement $A$.

    Nous notons <span style="color:red;">$P_A(B)$</span> la probabilité que $B$ se réalise **sachant que** $A$ est déjà réalisé.

    $P_A(B)$ se lit « probabilité de $B$ sachant $A$ », c'est une <span style="color:red;">probabilité conditionnelle</span>.


!!! example "Exemple III.1"
    === "Énoncé"
        Supposons que nous soyons dans une situation où il n'y a que quelques éventualités, représentées par des croix ci-contre et toutes équiprobables (pour simplifier) :

        ![Alt text](images/pc4.png){ width=30%; : .center }

        Donnez $P(B)$, $P(A\cap B)$, $P_A(B)$ et $P_B(A)$.
    
    === "Réponses"

        === "$P(B)$"
            Je choisis une éventualité dans l'univers (9 cas possibles) et il y en a 4 dans $B$ (4 cas favorables) donc la probabilité que $B$ se réalise est $P(B)=\dfrac{4}{9}$.

            ![Alt text](images/pc4b.png){ width=40%; : .center }

        === "$P(A\cap B)$"
            Je choisis une éventualité dans l'univers (9 cas possibles) et il y en a 2 dans $A\cap B$ (4 cas favorables) donc la probabilité que $A$ et $B$ se réalisent (en même temps) est $P(A\cap B)=\dfrac{\strut 2}{\strut 9}$.

            ![Alt text](images/pc4c.png){ width=40%; : .center }

        === "$P_A(B)$"
            Si **je sais** que $A$ est réalisé alors j'ai choisi une éventualité dans $A$ (qui devient l'univers, 5 cas possibles) et il y a 2 éventualités de $A$ qui sont dans $B$ (2 cas favorables) donc la probabilité que $B$ se réalise sachant que $A$ est réalisé est $P_A(B)=\dfrac{\strut 2}{\strut 5}$.

            ![Alt text](images/pc4d.png){ width=45%; : .center }

        
        === "$P_B(A)$"
             Si **je sais** que $B$ est réalisé alors j'ai choisi une éventualité dans $B$ (qui devient l'univers, 4 cas possibles) et il y a 2 éventualités de $B$ qui sont dans $A$ (2 cas favorables) donc la probabilité que $A$ se réalise sachant que $B$ est réalisé est $P_B(A)=\dfrac{\strut 2}{\strut 4}$.

            ![Alt text](images/pc4e.png){ width=40%; : .center }


!!! question "Exercice"
    === "Énoncé"
        Dans la situation suivante (avec équiprobabilité) :

        ![Alt text](images/pc3.png){ width=30%; : .center }

        Donnez $P(A)$, $P(B)$, $P(A\cap B)$, $P(A\cup B)$, $P_A(B)$, $P_B(A)$ et $P_{\bar{A}}(B)$.

    === "Réponses"
        $P(A)=\dfrac{3}{6}=\dfrac{1}{2}$, $P(B)=\dfrac{4}{6}=\dfrac{2}{3}$, $P(A\cap B)=\dfrac{2}{6}=\dfrac{1}{3}$, $P(A\cup B)=\dfrac{5}{6}$, $P_A(B)=\dfrac{2}{3}$, $P_B(A)=\dfrac{2}{4}=\dfrac{1}{2}$ et $P_{\bar{A}}(B)=\dfrac{2}{3}$.


!!! danger "Attention"
    Vous aurez remarqué sur cet exemple que :

    * $P(A\cap B)$, $P(B)$ et $P_A(B)$ <span style="color:red;">sont trois nombres différents</span> ;
    * <span style="color:red;">$P_A(B) \neq P_B(A)$</span> en général.
    

!!! tip "Comment savoir de quelle probabilité l'énoncé parle"
    Souvent les sujets seront écrits en langue courante et il faut bien interpréter ce qui est demandé.

    Les mots clés « sachant » ou « si » peuvent vous indiquer qu'il s'agît d'une probabilité conditionnelle.



!!! example "Exemple III.2"
    === "Enoncé"
        Une entreprise reçoit des pièces mécaniques de deux fournisseurs A et B, la première fournissant 70 % des pièces.

        Parmi les pièces du fournisseur A, il y en a 40 % de première qualité ; parmi les pièces du fournisseur B, il y en a 20 % de première qualité.

        Nous choisissons une pièce au hasard.

        Notons $A$ l'événement « La pièce provient du fournisseur A » 
        ($\bar{A}$ est donc l'événement « La pièce provient du fournisseur B »)
        et $Q$ l'événement « La pièce est de première qualité ».

        Donnez les valeurs de $P(A)$, de $P_A(Q)$ et de $P_{\bar{A}}(\bar{Q})$.

    === "Réponses"

        * $P(A)$ est la probabilité que la pièce provienne du fournisseur A donc $P(A)=0,7$ ;
        * $P_A(Q)$ est la probabilité que la pièce soit de première qualité sachant qu'elle vient du fournisseur A donc $P_A(Q) = 0,4$ ;
        * $P_{\bar{A}}(\bar{Q})$ est la probabilité que la pièce ne soit pas de première qualité sachant qu'elle ne vient pas du fournisseur A or 80 % des pièces du fournisseur B ne sont pas de première qualité donc $P_{\bar{A}}(\bar{Q})=0,8$.
        
        L'<span style="color:red;">arbre pondéré</span> ci-dessous résume les différents cas (dans un tel arbre, la somme des probabilités des branches partant d'un même noeud est 1).

        ![arbre pondéré](images/pc2_arbre.png){ width=40%; : .center }

??? tip "Une autre façon de voir cet exemple avec un tableau d'effectifs"
    Supposons qu'il y a 100 pièces dans la commande totale (en admettant que cette supposition ne change rien aux calculs de proportions...). 

    Alors nous avons les effectifs suivants :

    &nbsp;  | A | B | Totaux
    --- | --- | --- | 
    Q | 28 | 6 | 34 
    $\bar{\text{Q}}$ | 42 | 24 | 66
    Totaux  | 70 | 30 | 100

    Donc il y a 28 pièces de première qualité parmi les 70 pièces du fournisseur A donc $P_A(Q)=\dfrac{28}{70}=0,4$  
    et il y a 24 pièces pas de première qualité parmi les 30 pièces du fournisseur B donc $P_{\bar{A}}(\bar{Q})=\dfrac{24}{30}=0,8$.


!!! danger "Attention à une confusion fréquente"

    <div style="color:red">

    $$P(A\cap B) \neq P_A(B)$$

    </div>

    Dans l'exemple III.2, comparez les deux questions suivantes :

    * « Déterminer la probabilité qu'une pièce provienne du fournisseur A et soit de première qualité » : on demande alors le calcul de $P(A\cap Q)$ ; l'univers est l'ensemble des pièces.  
    <!--La réponse est donc 0,28.-->
    * « On choisit une pièce du fournisseur A. Déterminer alors la probabilité qu'elle soit de première qualité » : on demande ici le calcul de $P_A(Q)$ ; l'univers est l'ensemble des pièces du fournisseur A.  
    <!--La réponse est donc 0,4.-->


!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    2 page 246 (exemple)  
	87, 88, 89, 90, 91 page 263  
    79 page 262  
	35 et 36 page 257 <!--vers la formule donnant PA(B)-->  
    1 page 245 (exemple)  <!-- arbres -->  
    66, 67, 71 page 260  


!!! tip "Remarque importante"
    Les tableaux contiennent les probabilités d’intersection mais pas les probabilités conditionnelles ; les arbres contiennent des probabilités conditionnelles mais pas des probabilités d’intersection.

    ![tableau à double entrée](images/pc_tableau.png){ width=40%; : .center }

    ![arbre pondéré](images/pc_arbre.png){ width=40%; : .center }


!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    81, 82 et 83 page 262 (une erreur dans le 82 : lire $P_{\bar{B}}(\bar{A})=0,5$)  





## Lien entre $P_A(B)$ et $P(A\cap B)$

!!! abstract "Propriété III.1"
    La probabilité que $B$ se réalise sachant que $A$ est réalisé est :

    <div style="color:red;">

    $$\boxed{P_A(B)=\dfrac{P(B\cap A)}{P(A)} = \dfrac{P(A\cap B)}{P(A)}}$$
    
    </div>

!!! abstract "Remarque"
    Ceci peut en fait être considéré comme une définition de $P_A(B)$.


!!! example "Exemple III.3"
    === "Énoncé"
        Supposons que $P(A)=0,8$, $P(\bar B)=0,3$ et que $P(A\cap B)=0,69$.  
        Donnez : 
        $P_A(B)$ ; $P_B(A)$ ; $P_{\bar{A}}(B)$ ; $P_{\bar{B}}(A\cap B)$ ; $P_{\bar{B}}(A\cap \bar{B})$.

    === "Réponses"

        * $P_A(B)=\dfrac{P(A\cap B)}{P(A)} = \dfrac{0,69}{0,8}=\dfrac{69}{80}$ ;
        * $P_B(A)=\dfrac{P(A\cap B)}{P(B)} = \dfrac{0,69}{0,7}=\dfrac{69}{70}$ ;
        * $P_{\bar{A}}(B) = \dfrac{P(\bar{A}\cap B)}{P(\bar{A})} = \dfrac{0,01}{0,2} = 0,05$ ;
        * $P_{\bar{B}}(A\cap B) =\dfrac{P(A\cap B \cap \bar{B})}{P(\bar{B})} = \dfrac{0}{0,3}=0$ ;
        * $P_{\bar{B}}(A\cap \bar{B}) =\dfrac{P(A\cap \bar{B}\cap \bar{B})}{P(\bar{B})} =\dfrac{P(A\cap \bar{B})}{P(\bar{B})} =\dfrac{0,11}{0,3} = \dfrac{11}{30}$.

!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
	37 page 257  
    39 et 40 page 258  
    86 page 262





## Conséquence : calcul de $P(A\cap B)$

!!! abstract "Propriété III.2"

    <div style="color:red;">

    $$\boxed{P(A\cap B)=P(A)\times P_A(B)=P(B)\times P_B(A)}$$

    </div>

!!! example "Exemple III.4"
    Dans l'exemple III.2 :

    ![arbre pondéré](images/pc2_arbre.png){ width=40%; : .center }

    $P(A\cap Q)=P(A)\times P_A(Q) =  0,7 \times 0,4 = 0,28$ et 

    $P(B\cap \bar{Q})= P(B)\times P_B(\bar{Q})=0,3 \times 0,8 = 0,24$.

    Pour trouver des probabilités d'intersection avec un arbre il faut parcourir l'arbre de la gauche vers la droite et multiplier les probabilités.


??? tip "Une autre façon de voir cet exemple avec un tableau d'effectifs"

    Supposons qu'il y a 100 pièces dans la commande totale (en admettant que cette supposition ne change rien aux calculs de proportions...). 

    Alors nous avons les effectifs suivants :

    &nbsp;  | A | B | Totaux
    --- | --- | --- | 
    Q | 28 | 6 | 34 
    $\bar{\text{Q}}$ | 42 | 24 | 66
    Totaux  | 70 | 30 | 100

    Les cases intérieures donnent alors rapidement les réponses :  
    $P(A\cap Q)=\dfrac{28}{100}=0,28$ et $P(B\cap \bar{Q})=\dfrac{24}{100}=0,24$.
    

!!! tip "Remarque"
    La propriété III.2 est parfois appelée « règle du produit ».

!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    74, 76 page 261  
    85 page 262  
    3 page 247



## Formule des probabilités totales

!!! abstract "Définition III.2"
    Des événements $A_1$, $A_2$, $\dots$ $A_n$ forment une <span style="color:red;">partition</span> de l'univers $\Omega$ si :

    * ils sont incompatibles deux à deux : $A_i \cap A_j = \emptyset$ pour tous $i\neq j$ ;
    * leur réunion est l'univers : $A_1 \cup A_2 \cup  \dots \cup A_n = \Omega$.


!!! tip "Remarque importante"
    Une partition $A_1$, $A_2$, $\dots$ $A_n$ de l'univers découpe un événement $B$ en plusieurs parties : $B = (B \cap A_1) \cup (B \cap A_2) \cup \dots  \cup (B \cap A_n)$.

    ![partition](images/partition.png){ width=40%; : .center }

    De plus, ces événements $B \cap A_1$, $B \cap A_2$, ..., $B \cap A_n$ sont incompatibles deux à deux.

!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    27 page 256



!!! abstract "Propriété III.4"
    Si $A_1$, $A_2$, $\dots$, $A_n$ forment une partition de l'univers alors 

    <div style="color:red;">

    $$P(B) =
    P(A_1\cap B) + P(A_2\cap B) + \dots + P(A_n\cap B)
    =P(A_1) \times P_{A_1}(B) + P(A_2) \times P_{A_2}(B) + \dots + P(A_n) \times P_{A_n}(B).
    $$

    </div>


!!! tip "Remarques"
    * Cette propriété est parfois appelée « règle de la somme ».
    * Dans le cas de deux événements $A$ et $B$, la formule des probabilités totales permet le calcul de $P(B)$ en commençant par séparer deux cas possibles : soit $A$ est réalisé soit il ne l'est pas. 
    * Interprétation de la formule : pour calculer $P(B)$, il suffit de suivre chaque chemin d'un arbre pondéré qui mène à $B$ en multipliant les probabilités le long de ce chemin (propriété III.2) puis d'ajouter tous les résultats obtenus (définition II.1 : les chemins sont incompatibles).

!!! example "Exemple III.5"
    Dans l'exemple III.2 et III.4 :

    ![arbre pondéré](images/pc2_arbre.png){ width=40%; : .center }

    $P(Q)= P(A)\times P_A(Q)+P(\bar{A})\times P_{\bar{A}}(Q) = 0,7 \times 0,4 + 0,3 \times 0,2 = 0,34$.

    Nous pouvons de plus calculer maintenant $P_Q(A)$ :

    $P_Q(A) = \dfrac{P(A\cap Q)}{P(Q)} = \dfrac{0,7\times 0,4}{0,34} = \dfrac{14}{17}$.

!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    6 page 250 (exemple)  
 	28, 30 page 256  
    32 page 257  
    112 page 265  
