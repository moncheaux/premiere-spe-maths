# Suites et tableur

!!! example "Exemple"

    === "Questions"
        Voici une capture d'écran venant d'un tableur :

        ![](images/tableur1.png)

        1) Quelle valeur va s'afficher dans la cellule B3 après validation ?

        2) En recopiant la formule dans les cellules B4, B5, ..., nous obtenons les premiers termes d'une suite.  
        Comment est définie cette suite ? Par quelle égalité ?

    === "Réponses"
    
        1) La valeur en B3 sera \(3\times 5-2=13\).

        2) La suite est définie par récurrence car un terme (celui dans B3 etc.) se calcule à partir du précédent (celui dans B2 etc.).
        Le premier terme est \(u_0=5\) et la relation de récurrence est \(u_{n+1}=3u_n-2\).


!!! example "Exemple"

    === "Questions"
        Voici une capture d'écran venant d'un tableur :

        ![](images/tableur2.png)

        1) Quelle valeur va s'afficher dans la cellule B3 après validation ?

        2) En recopiant la formule dans les cellules B4, B5, ..., nous obtenons les premiers termes d'une suite.  
        Comment est définie cette suite ? Par quelle égalité ?

    === "Réponses"
    
        1) La valeur en B3 sera \(4\times 1+5=9\).

        2) La suite est définie explicitement car un terme (valeur de la colonne) se calcule à partir du rang de la même ligne (présent dans la colonne A).
        L'expression est \(u_n=4\,n+5\).


!!! example "Exemple"

    === "Question"
        Soit la suite définie par \(u_1=6\) et la relation de récurrence \(u_{n+1}=u_{n}-n\).

        Comment compléter les cellules ci-dessous pour obtenir les premiers termes de cette suite ?

        ![](images/tableur3.png)

    === "Réponse"
    
        ![](images/tableur4.png)


!!! example "Exemple"

    === "Question"
        Soit la suite définie par \(u_0=-3\), \(u_1=2\) et la relation de récurrence \(u_{n+2}=u_{n+1}-u_n-2\,n\).

        Comment compléter les cellules ci-dessous pour obtenir les premiers termes de cette suite ?

        ![](images/tableur3.png)

    === "Réponse"
    
        ![](images/tableur5.png)
