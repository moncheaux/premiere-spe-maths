# Utilisation de la calculatrice Numworks 

Dans tous les cas, cherchez l'application Suites avec les flèches et lancez-là avec le bouton OK. Appuyez à nouveau sur OK pour "Ajouter une suite".

![](images/numworks1.png)

=== "Suites explicites"
    Validez le premier choix ("$u_n$ Explicite") avec OK.

    ![](images/numworks2.png)

    Tapez l'expression de $u_n$, le bouton "x,n,t" donne accès ici à la lettre n directement.

    ![](images/numworks3.png)

    Une fois fini, validez avec OK puis avec les flèches allez sur "Tracer le graphique" ou sur "Afficher les valeurs" pour obtenir des termes de la suite.


=== "Suites par récurrence"
    Validez le deuxième choix ("$u_{n+1}$ Récurrente d'ordre 1") avec OK.
    
    ![](images/numworks4.png)

    Par commodité, la machine affiche déjà $u_n$, mais si vous avez besoin d'un autre $u_n$, utilisez le bouton de la boîte à outils.

    ![](images/numworks5.png)

!!! question "Exercices"
    
    Transmath :  
    13 (vérifiez vos réponses), 15 page 47  
    21 (vérifiez vos réponses), 24 page 47  

!!! question "Somme des inverses des carrés"
    Calculez la somme des inverses des carrés des entiers compris entre 1 et 100 :

    * à l'aide d'un programme en Python ;
    * à l'aide de la calculatrice en mode Suite.
    
    Calculez \(\dfrac{\pi^2}{6}\)...

!!! question "Pour les plus rapides"
    Programmer le calcul des premiers termes de la suite de Collatz en Python.