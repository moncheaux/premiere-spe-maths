# Suites définies explicitement

!!! abstract "Définition"

     Une suite est définie <span style = "color:red">explicitement</span> si on connaît la relation entre $n$ et $u_n$, de la forme $u_n=f(n)$ où $f$ est connue.

!!! example "Exemples"
     * Soit $u_n= \dfrac{n-1}{\strut n+1}$ pour tout $n\in {\rm IN}^*$ (rappel : IN^*^ désigne l'ensemble des entiers naturels non nuls). Alors, par exemple, $u_6=\dfrac{5}{7}$.
     * La suite $(v_n)$ des nombres pairs (0 ; 2 ; 4 ; 6 ; ...) peut être vue comme une suite définie explicitement : $v_{n}=2n$ pour tout $n\in {\rm IN}$.
     * La suite $(w_n)$ définie explicitement par $w_{n}=\sqrt{n-42}$ ne peut être définie que pour des entiers $n\geqslant 42$.





!!! tip "Pour calculer des termes"
    Il suffit de remplacer $n$ par la valeur demandée dans l'expression de $u_n$.

!!! example "Exemple"

    Soit la suite \((u_n)\) définie sur IN par $u_n = 3\,n^2+2$.

    Alors :  
    $u_0 = 3\times 0^2+2 = 2$,  
    $u_1=3\times 1^2+2 = 5$,  
    $u_2=3\times 2^2+2 = 14$,  
    ...,  
    $u_{100}=3\times 100^2+2 = 30002$.

!!! example "Exemple important"

    Soit la suite \((u_n)\) définie sur IN par $u_n = 2\,n+5$.

    Alors :  
    $u_{n+1}=2(n+1)+5=2\,n+7$  
    $u_{n+2}=2(n+2)+5=2\,n+9$  
    $u_{2\,n}=2(2\,n)+5=4\,n+5$.

!!! tip "Remarques"
    * Ceci sera très utile cette année pour étudier la nature ou les variations d'une suite.
    * Si un terme est noté $u_n$ alors le terme suivant est $u_{n+1}$, pas $u_n+1$ !  
    Attention donc à écrire correctement les indices...

!!! example "Exemple"

    Si $u_n=2\,n+3$ alors $u_n+1=(2\,n+3)+1 = 2\,n+4$ tandis que $u_{n+1}=2(n+1)+3=2\,n+5$.


!!! question "Exercices"
     Transmath :  
     13, 14, 16 page 47  
     90 page 51
     3, 5, 8, 9 page 47  

