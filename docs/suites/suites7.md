# Suites définies par un algorithme

!!! example "Exemple"
     
    Voici un algorithme en langage naturel :

    $N \leftarrow 10$  
    **pour** $n$ **allant de** 0 **à** $N$  
    **faire**  
    $u \leftarrow 4 \times n + 11$  
    **afficher** $u$  
    **fin pour**

    Cet algorithme calcule et affiche les onze premiers termes de la suite définie explicitement par \(u_{n}=4\,n+11\).  
    Il suffit de changer la première ligne de l'algorithme pour calculer plus de termes.

    Voici la transcription en langage Python :

    {{IDEv('scripts/suites_algo1')}}

    Remarques :
    
    * Essayez de retirer le décalage vers la droite de la dernière instruction...
    * Améliorez l'affichage en remplaçant `#!python print(u)` par `#!python print('u', n, '=', u)`



!!! example "Exemple"
      
    L'algorithme :

    $N \leftarrow 20$  
    $u \leftarrow 7$  
    **pour** $n$ **allant de** 0 **à** $N$  
    **faire**  
    $u \leftarrow 6 - 0,5 \times u$  
    **afficher** $u$  
    **fin pour**

    calcule et affiche les vingt premiers termes de la suite définie par récurrence par \(u_0=7\) et \(u_{n+1}=6-0,5\,u_n\).  
    Il suffit de changer la première ligne de l'algorithme pour calculer plus de termes.


    Sa transcription en Python est

    {{IDEv('scripts/suites_algo2')}}



!!! example "Exemple"
     
    Soit $(T_{n})$ la suite des sommes des entiers de 0 à \(n\) :
    \(
    T_n = 1+2+3+4+...+n
    \)
    (c'est la suite des "nombres triangulaires", voir le dernier exercice de la fiche d'introduction).

    L'algorithme suivant permet le calcul d'un terme de la suite, ici \(T_{20}\) :

    $N \leftarrow 20$  
    $T \leftarrow 0$  
    **pour** $n$ **allant de** 1 **à** $N$  
    **faire**  
    $T \leftarrow T + n$  
    **fin pour**   
    **afficher** $T$  

    La variable \(T\) contiendra la valeur de \(T_{20}\) à la fin de l'algorithme.  
    Il suffit ensuite de changer la première ligne de l'algorithme pour obtenir un autre terme.


    {{IDEv('scripts/suites_algo3')}}

    
!!! question "Exercices"
    
    Transmath :  
     25 page 48  
