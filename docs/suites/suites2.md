# Quelques exemples de suites dans l'histoire

=== "Archimède (III^ème^ siècle av. JC)"

    Archimède cherchait à calculer une valeur approchée du nombre $\pi$ à l'aide de deux suites de polygones.
    
    <iframe scrolling="no" title="Méthode d'exhaustion d'Archimède" src="https://www.geogebra.org/material/iframe/id/PASHZtf7/width/491/height/487/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/true/rc/false/ld/false/sdz/true/ctl/false" width="491px" height="487px" style="border:0px;"> </iframe>

    ??? info "Les formules de calcul"

        Dans les notations actuelles les formules permettant de passer d'un polygone inscrit au polygone inscrit ayant deux fois plus de côtés sont :

        \(c_{2n}=\sqrt{\dfrac{1}{2}(1-\sqrt{1-c_n^2})}\) pour le côté du polygone (il suffit d'utiliser la relation de Pythagore...)

        \(p_{2n}=2nc_{2n}\) pour le périmètre du polygone.


        <table>
        <tbody>
        <tr>
            <td>nombre de côtés \(n\)</td>
            <td>6</td><td>12</td><td>24</td><td>48</td><td>96</td>
        </tr>
        <tr>
            <td>longueur d'un côté \(c_n\)</td>
            <td>0,5</td><td>0,258819</td><td>0,130526</td><td>0,065403</td><td>0,032719</td>
        </tr>
        <tr>
            <td>périmère du polygone \(p_n\)</td>
            <td>3</td><td>3,105828</td><td>3,132628</td><td>3,139350</td><td>3,141031</td>
        </tr>
        </tbody>
        </table>

    Il a obtenu $\dfrac{223}{71}<\pi <\dfrac{22}{7}$, soit une précision de 3 décimales ($\pi\simeq 3,14$).


=== "Fibonaci (Inde 6^ième^ siècle, Italie 1202)"

    Suite étudiée à titre récréatif pour illustrer une croissance de populations de lapins. Un terme est égal à la somme des deux précédents :
    1 ; 1 ; 2 ; 3 ; 5 ; 8 ; 13 ; 21 ; ...

    ![lapins Fibonaci](https://upload.wikimedia.org/wikipedia/commons/a/af/Fibonacci_Rabbits.svg)

    Choses étonnantes, ces nombres apparaissent dans la plupart des spirales de la nature.

    ![spirale Fibonaci](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/Fibonacci_Spiral.svg/320px-Fibonacci_Spiral.svg.png)
    ![fleur Fibonaci](https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/Centre_d%27une_fleur.JPG/320px-Centre_d%27une_fleur.JPG)

    ??? info "Fibonaci et les chiffres indo-arabes"
        C'est aussi grâce à Fibonaci que nous devons l'arrivée en Europe des chiffres que nous utilisons, issus de l'Inde en passant par le monde arabe.
    
=== "Suite de Collatz (1928)"

    Choisissons un nombre entier strictement positif :

    * s’il est pair, je le divise par 2 ;  
    * sinon on le multiplie par 3 puis j'ajoute 1 ;  
    * puis je réitère sur le nombre obtenu.

    Appliqué par exemple au nombre 7, nous obtenons la suite :
    7 ; 22 ; 11 ; 34 ; 17 ; 52 ; 26 ; 13 ; 40 ; 20 ; 10 ; 5 ; 16 ; 8 ; 4 ; 2 ; 1 ; 4 ; 2 ; 1 ; ...

    Dans tous les tests effectués avec d'autres entiers initiaux, on arrive toujours à 1 à un moment donné mais personne n'a réussi à le prouver dans le cas général !


<!--
=== "Héron (I^er^ siècle ap. JC)"

    (Re)trouve une méthode rapide pour calculer la racine carrée d'un nombre, par exemple pour $\sqrt{7}\simeq 2,645751311$ :

    étape 1 : nous partons de $r_1 = 2$ par exemple ;  
    étape 2 : en appliquant la formule de Héron à $r_1$ nous trouvons $r_2 = 2,75$ ;  
    étape 3 :  en appliquant la formule de Héron à $r_2$ nous trouvons $r_3 = 2,647727...$ ;  
    étape 4 :  en appliquant la formule de Héron à $r_3$ nous trouvons $r_4 = 2,645752...$
    etc.

    Nous étudierons cette méthode plus tard.
-->


!!! info "Combien y a-t-il de suites connues ?"
    Le site OEIS recense des centaines de milliers de suites de nombres entiers et en enregistre des milliers supplémentaires chaque année.

