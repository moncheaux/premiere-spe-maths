# Représentation graphique d'une suite 

!!! tip "Comment représenter graphiquement une suite \((u_{n})\)"
    Il suffit de placer les points d'abscisse $n$ et d'ordonnée $u_n$.

!!! example "Exemple"
     Représentation graphique (partielle) de la suite $(u_n)$ définie par $u_n=\sqrt{n}$ pour tout $n\in{\rm IN}$.
     
    ![Figure suite](images/fig_suites3.png)
