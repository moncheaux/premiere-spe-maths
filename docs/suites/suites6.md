# Suites définies par récurrence

!!! abstract "Définition"

     Une suite est définie <span style = "color:red">par récurrence</span> si on connaît son premier terme $u_0$ (ou $u_1$, ...) et la relation entre un terme $u_n$ et son suivant $u_{n+1}$ (de la forme $u_{n+1}=g(u_n)$).

     ![Figure suite récurrente](images/fig_suites2.png)


!!! example "Exemple"
     * Soit $(u_n)$ la suite définie par $u_0=2$ et par la relation de récurrence $u_{n+1}=2\,u_n-3$ (ici, $g(x)=2\,x-3$).  
     Alors :   
     pour $n=0$ : $u_1=2u_0-3=1$,  
     pour $n=1$ : $u_2=2u_1-3=-1$,  
     pour $n=2$ : $u_3=2u_2-3=-5$,  
     pour $n=3$ : $u_4=2u_3-3=-13$,  
     pour $n=4$ : $u_5=2u_4-3=-29$,  
     pour $n=5$ : $u_6=2u_5-3=-61$,  
     etc.
     * La suite $(v_n)$ des nombres pairs (2 ; 4 ; 6 ; ...) peut être vue comme une suite définie par récurrence : son premier terme est $v_0=2$ et la relation de récurrence est $v_{n+1}=v_n+2$ pour tout $n\in{\rm IN}$.

!!! tip "Remarques"
     * Nous ne pouvons pas calculer directement un terme quelconque de la suite sans avoir calculé tous les précédents...
     * Il existe d'autres formes de récurrence, comme dans le cas de la suite de Fibonacci où un terme se calcule à partir des deux précédents ($F_0=F_1=1$ et $F_{n+2}=F_{n+1}+F_n$).
     * Une relation de récurrence ne permet pas toujours de définir une suite.

!!! example "Exemple"
     Si $u_0=-2$ et $u_{n+1}=\dfrac{1}{u_n+1}$ alors $u_1=\dfrac{1}{u_0+1}=-1$ et $u_2$ n'existe pas.

!!! question "Exercices"
     Transmath :  
     21, 23 page 48  
     95 page 51  