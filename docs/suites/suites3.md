# Notion de suite numérique

!!! abstract "Rappels"

    * ℕ est l'ensemble des <span style = "color:red">entiers naturels</span>, c'est-à-dire l'ensemble des entiers positifs ou nuls : ℕ = {0;1;2;3;4;...}.
    * On définit une <span style = "color:red">fonction numérique</span> $f$ sur un ensemble $\cal D$ (souvent un intervalle ou une réunion d'intervalles) en associant à un $x$ quelconque de $\cal D$, un réel et un seul, appelé image de $x$ et noté $f(x)$.


!!! abstract "Définition"

     Une <span style = "color:red">suite numérique</span> $u$ est une fonction définie sur ℕ (ou parfois sur ℕ privé de quelques entiers : 0 ; 1 ou autres) et à valeurs réelles.

!!! abstract "Remarques et notations"

     * $u(n)$ sera plus simplement noté $u_n$ ("$u$ indice $n$"). 
      C'est le <span style = "color:red">terme</span> de <span style = "color:red">rang</span> $n$ (ou d'indice $n$) de la suite $u$.
     * Une suite numérique est une succession de nombres numérotés 
      avec des entiers :  
      à partir du rang 0 : $u_0,u_1,u_2,...$  
      ou à partir du rang 1 : $u_1,u_2,u_3,...$   
      (ou du rang 2, etc.)
     * La suite $u$ est plus souvent notée $(u_n)$ ou encore, plus précisement, $(u_n)_{n\in ℕ}$ (ou, suivant les cas, $(u_n)_{n\ge1}$, etc.).  
      La notation $(u(n))$ est aussi valable mais rarement utilisée.

![Figure suites](images/fig_suites.png)


!!! example "Exemple"

    Soit la suite \((u_n)\) des nombres pairs : 2 ; 4 ; 6 ; 8 ; ...

    Le premier terme 2 peut être noté $u_1$, 
    le second $u_2$, le troisième $u_3$, etc. :

    $u_1=2$, $u_2=4$, $u_3=6$, ...

    Le terme général de cette suite est alors $u_n=2n$.

    On aurait pu aussi commencer la numérotation à zéro : 

    $u_0=2$, $u_1=4$, $u_2=6$, ... ce qui donnerait alors $u_n=2(n+1)$.

!!! danger "Une erreur fréquente"
     Ne confondez pas $n$ (le rang d'un terme) avec $u_n$ (un terme de la suite) !

!!! tip "Une autre représentation"
    Une suite peut aussi être vue comme un tableau de valeurs infini dans lequel la première ligne contient les entiers successifs. Par exemple, pour la suite des nombres premiers :

    <table>
    <tbody>
    <tr>
        <td>\(n\)</td>
        <td>0</td><td>1</td><td>2</td><td>3</td><td>...</td>
    </tr>
    <tr>
        <td>\(u_n\)</td>
        <td>\(u_0=2\)</td><td>\(u_1=3\)</td><td>\(u_2=5\)</td><td>\(u_3=7\)</td><td>...</td>
    </tr>
    </tbody>
    </table>

!!! danger "Une autre erreur fréquente"
     Le 3^ième^ terme n'est pas forcément le terme de rang 3, c'est-à-dire \(u_3\) ; par exemple dans le tableau ci-dessus le 3^ième^ terme est \(u_2=5\).
