# ⁉️ QCM
!!! tip "Attention"
    Il peut y avoir plusieurs réponses correctes à chaque question !


#### Q1 - Soit la suite définie sur IN par $u_n = 5-4\,n$.

Alors :

{{ qcm(["La suite est définie explicitement", "La suite est définie par récurrence", "Je peux calculer $u_{50}$", "Je ne peux pas calculer $u_{50}$ sans avoir calculé d'abord les termes précédents"], [1,3], shuffle = True) }}


#### Q2 - Soit la suite définie sur IN^*^ par $u_n = 5-\dfrac{1}{n}$.

Alors :

{{ qcm(["Le premier terme est $u_0$", "Le premier terme est $u_1$", "Le terme de rang 4 est $u_4$", "Le terme de rang 4 est $u_3$"], [2,3], shuffle = True) }}

#### Q3 - Soit la suite définie sur IN par $u_{n+1} = 2\,u_n-1$ et $u_0=4$.

Alors :

{{ qcm(["La suite est définie explicitement", "La suite est définie par récurrence", "Je peux calculer $u_{50}$", "Je ne peux pas calculer $u_{50}$ sans avoir calculé d'abord les termes précédents"], [2,4], shuffle = True) }}

#### Q4 - Soit la suite définie sur IN par $u_{n} = 5\,n-u_{n-1}$ et $u_0=3$.

Alors :

{{ qcm(["La suite est définie explicitement", "La suite est définie par récurrence", "Je peux calculer $u_{100}$", "Je ne peux pas calculer $u_{100}$ sans avoir calculé d'abord les termes précédents"], [2,4], shuffle = True) }}



#### Q5 - Soit la suite définie sur IN par $u_n = n^2+3$.

Alors :

{{ qcm(["$u_{n+1}=(n+1)^2+3$", "$u_{n+1}=n^2+4$", "$u_{2\,n}=(2\,n)^2+3$", "$u_{2\,n}=2\,(n)^2+6$"], [1,3], shuffle = True) }}

#### Q6 - Soit le script Python suivant :

```python
u = -1
for n in range(5):
    u = u + 3
    print(u)
```		

Ce script calcule et affiche les termes d'une suite \((u)\). Alors :

{{ qcm(["La suite est définie explicitement", "La suite est définie par récurrence", "Le dernier terme calculé est 14", "Le dernier terme calculé est 17"], [2,3], shuffle = True) }}


#### Q7 - Soit le script Python suivant :

```python
for n in range(10):
    u = 5*n + 12
    print(u)
```		

Ce script calcule et affiche les termes d'une suite \((u)\). Alors :

{{ qcm(["La suite est définie explicitement", "La suite est définie par récurrence", "Le dernier terme calculé est 62", "Le dernier terme calculé est 57"], [1,4], shuffle = True) }}