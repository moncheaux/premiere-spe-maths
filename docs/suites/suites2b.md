# Mais à quoi ça sert en vrai ?
Les suites sont omniprésentes dans les mathématiques mais voici quelques exemples d'applications concrètes :

=== "Écologie"
    Études d'évolutions de populations.

    Taux de radiation suite à une réaction nucléaire ; datation carbone 14.


=== "Économie"

    Connaître le solde de votre compte en banque (intérêts composés) ou le nombre d'années pendant lesquelles vous allez rembourser un emprunt, etc.

=== "Médecine"

    Traitement de l'image en radiologie, IRM.

    Étude de la propagation d'une épidémie.

    


=== "Informatique"

    Conversion analogique &rarr; numérique (exemple : discussion via Internet)

    Fractales

    ![Flocon Von Koch](https://upload.wikimedia.org/wikipedia/commons/f/fd/Von_Koch_curve.gif)
    
    
!!! tip "Cas d'utilisation"
    Les suites servent à modéliser des phénomènes étudiés à intervalle de temps régulier (par exemple tous les ans), ou qui se répètent (à chaque rebond d'une balle au sol, etc.).
