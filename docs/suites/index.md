# Première approche
Une suite est une **succession d'objets**.

!!! example "Exemple"
     Voici une suite de nombres choisis au hasard :  
     2,34   ;   -5,47 ; -8,21 ; 6,55 ; -0,67 ; ...

     Cette suite n'a pas de logique donc ne peut pas être vraiment un objet d'étude.

!!! example "Exemple"
     La suite :  
     2 ; 3 ; 5 ; 7 ; 11 ; 13 ; 17 ; ...  
     a-t-elle une logique ?

    ??? question "Réponse"
         C'est la suite des nombres premiers, très étudiée dans l'histoire des mathématiques et qui l'est encore.


!!! example "Exemple"
     Voici la suite (finie) des lettres de l'alphabet :  
     a ; b ; c ; d ; e ; ... ; z

     Nous pouvons numéroter les termes de cette suite et écrire par exemple que L~1~ = a, L~2~ = b, etc.

!!! tip "Un objectif essentiel"
     Un des objectifs principaux dans ce chapitre est de réussir à calculer n'importe quel terme d'une suite donnée, par exemple le 100^ième^...


!!! question "Fiche d'introduction"
    Faîtes [cette fiche d'exercice](/maths/premiere/documents/intro_suites_gen.pdf) avant de passer à la suite.


!!! question "Pour les plus rapides"
    === "Énoncé"
        ![Château de cartes](images/exo_chingmath.png)

         (source : chingmath.fr)

    === "Correction question 1"
        ![Correction 1](images/exo_chingmath_corr1.png)

    === "Correction question 2"
        ![Correction 2](images/exo_chingmath_corr2.png)

    === "Correction question 3"
        ![Correction 3](images/exo_chingmath_corr3.png)
