# II. Nombre dérivé d’une fonction en un point


!!! note "Définition"
    Soit $f$ une fonction définie sur un intervalle ouvert $I$ et $a\in I$.

    La fonction $f$ est <span style="color:red">dérivable</span> en $a$ si le taux de variation $\dfrac{f(a+h)-f(a)}{\strut h}$ se rapproche d'un nombre quand $h$ se rapproche de 0. 

    Ce nombre est alors appelé <span style="color:red">nombre dérivé</span> de $f$ en $a$ et noté <span style="color:red">$f'(a)$</span> :

    <div style="color:red">

    \[
    \boxed{f'(a)=\lim\limits_{h\to 0} \dfrac{\strut f(a+h)-f(a)}{h}}
    \]

    </div>

!!! exemple "Exemple II.1"

    Le taux de variation de la fonction carré entre 1 et \(1+h\) est :
    \(
    \frac{\strut f(1+h)-f(1)}{\strut h}
    =
    2+h
    \), qui tend vers 2 quand \(h\) tend vers 0.

    La fonction carré est donc dérivable en 1 et le nombre dérivé de cette fonction en 1 est \(f'(1)=2\).



!!! exemple "Exemple II.2"
    La fonction valeur absolue n'est pas dérivable en 0.

    En effet,
    \(
    \frac{\strut f(0+h)-f(0)}{h}
    =
    \frac{\strut |h|}{h}
    \)
    qui est égal à 1 si \(h>0\) et à \(-1\) si \(h<0\) donc 
    \(
    \frac{\strut f(0+h)-f(0)}{h}
    \)
    ne tend pas vers _un_ nombre quand \(h\) tend vers 0.

    ![](images/fig_val_absolue.png){width=50%; : .center}




!!! note "Démonstration remarquable"

    La fonction racine carrée n'est pas dérivable en 0.

    En effet,
    \(
    \frac{\strut f(0+h)-f(0)}{h}
    =
    \frac{\strut \sqrt{h}}{h}
    =
    \frac{\strut \sqrt{h}}{(\sqrt{h})^2}
    =
    \frac{\strut 1}{\sqrt{h}}
    \)
    et ce nombre devient très grand (il tend vers \(+\infty\)) quand \(h\) se rapproche de 0.

    ![](images/fig_racine_carree.png){width=50%; : .center}



!!! exemple "Exemple II.3"

    Soit \(f\) la fonction définie sur ℝ par \(f(x)=x^3-3\,x^2+1\).

    Nous aimerions savoir si \(f\) est dérivable en 3.

    Remarquons d'abord que \(f(3)=3^3-3 \times 3^2+1 = 1\) puis observons le taux de variation entre \(a=3\) et \(3+h\) avec \(h\) qui tend vers 0 :

    \(h\) | 1 | 0,5 | 0,1 | 0,01 | 0,001
    --- | --- | --- | --- | --- | ---
    \(f(3+h)\) | 17 | 7,125 | 1,961 | 1,09 | 1,009
    \(f(3+h)-f(3)\) | 16 | 6,125 | 0,961 | 0,09 | 0,009 
    \(\dfrac{\strut f(3+h)-f(3)}{\strut h}\) | 16 | 12,25 | 9,61 | 9,06 | 9,006

    Ainsi, __il semblerait que__ \(f\) soit dérivable en 3 et que \(f'(3)=9\).

    Pour un exemple de démonstration rigoureuse, voir les exemples II.1 et II.2.


!!! tip "Remarques"

    * Si \(f\) est affine, définie sur ℝ par \(f(x)=a\,x+b\) alors le taux de variation est le coefficient \(a\) donc \(f\) est dérivable en tout \(x\) et \(f'(x)=a\).
    * Si l'on considère que $f(x)=y$ alors le nombre dérivé de \(f\) en \(a\) s'écrit aussi \(f'(a)=\dfrac{\strut d y}{\strut dx}(a)\) (notation de Leibniz).  
    En sciences physiques, les fonctions dépendent souvent du temps, ainsi si \(q=f(t)\) alors le nombre dérivé de \(q\) en \(a\) s'écrit \(q'(a)=\dfrac{\strut dq}{dt}(a)\).


!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :

    * question 1 des exercices 42 et 44 page 101 ;
    * exercice 45 page 101
