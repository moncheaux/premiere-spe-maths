# IV. Calculs de (fonctions) dérivées


## Principe

!!! tip "Nombre dérivé en un point quelconque"
    Nous avons vu comment calculer le nombre dérivé \(f'(a)\) quand \(a\) est connu. Nous aimerions maintenant faire ce calcul pour un \(a\) quelconque.

!!! exemple "Exemple IV.1"
    Observons la pente de la tangente (donc \(f'(a)\)) en un point quelconque de la courbe de la fonction carré :

    <iframe scrolling="no" title="Fonction dérivée" src="https://www.geogebra.org/material/iframe/id/JMZ3E8tj/width/1000/height/608/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="1000px" height="608px" style="border:0px;"> </iframe>

    Il semblerait que \(f'(a)=2\,a\). Ainsi, si \(f(x)=x^2\), il semblerait que \(f'(x)=2\,x\).
    Nous prouverons cela dans la suite.


!!! abstract "Définition"
    $f$ est <span style="color:red">dérivable sur un intervalle</span> $I$ si $f$ est dérivable en tout réel $a$ de $I$. La fonction qui associe à tout \(a\) de $I$ le nombre dérivé \(f'(a)\) est appelée (fonction) <span style="color:red">dérivée</span> de $f$ et notée <span style="color:red">\(f'\)</span>.

!!! exemple "Exemple IV.2"
    Soit la fonction \(f\) définie sur ℝ par \(f(x)=4\).  
    Calculons le taux de variation entre \(a\) et \(a+h\) :

    \[
    \dfrac{f(a+h)-f(a)}{h}
    =
    \dfrac{4-4}{h}
    =
    0
    \]
    
    donc \(f\) est dérivable sur ℝ et sa fonction dérivée \(f'\) est définie sur ℝ par \(f'(x)=0\).

    (pensez au fait que la fonction étant constante, ses taux de variation sont nuls car ce sont les pentes de tangentes horizontales)

!!! exemple "Exemple IV.3"
    Soit la fonction \(f\) définie sur ℝ par \(f(x)=x\).  
    
    Calculons le taux de variation entre \(a\) et \(a+h\) :

    \[
    \dfrac{f(a+h)-f(a)}{h}
    =
    \dfrac{a+h-a}{h}
    =
    \dfrac{h}{h}
    =1
    \]
    
    donc \(f\) est dérivable sur ℝ et sa fonction dérivée \(f'\) est définie sur ℝ par \(f'(x)=1\).

    (la fonction étant affine, ses tangentes sont confondues avec la courbe donc leur pente est celle de la courbe, ici 1)


!!! bug "Abus de notation"
    Nous écrirons plus rapidement \((x)'=1\) mais gardez à l'esprit que nous dérivons bien une fonction et pas un nombre !

    Appliqué à un nombre, cela donne des absurdités : par exemple, à partir de l'égalité $x=4$ nous obtiendrions $1=0$ !  
    (ne dérivez jamais une équation).


## Fonctions dérivées de quelques fonctions usuelles

!!! note "Démonstration remarquable"
    Soit \(f\) la fonction carré, définie sur ℝ par \(f(x)=x^2\).

    Alors, pour tout \(a\),

    \[
    \begin{eqnarray}
    \dfrac{f(a+h)-f(a)}{\strut h}
    &=&
    \dfrac{(a+h)^2-a^2}{h}
    =
    \dfrac{a^2+2ah+h^2-a^2}{h}
    \\
    &=&
    \dfrac{2ah+h^2}{h}
    =
    2a+h.
    \end{eqnarray}
    \]

    Quand \(h\) tend vers 0, \(2a+h\) tend vers \(2a\) donc la fonction carré est dérivable en tout réel \(a\) et sa fonction dérivée est définie sur ℝ par \(f'(x)=2\,x\).



!!! note "Démonstration remarquable"
    Soit \(f\) la fonction inverse, définie sur \(ℝ^*\) par \(f(x)=\dfrac{1}{x}\).

    Alors, pour tout réel \(a\) non nul, 

    \[
    \begin{eqnarray}
    \dfrac{f(a+h)-f(a)}{\strut h}
    &=&
    \dfrac{\frac{1}{a+h}-\frac{1}{a}}{h}
    =
    \dfrac{\strut \frac{a}{(a+h)a}-\frac{a+h}{a(a+h)}}{\strut h}
    \\
    &=&
    \dfrac{\strut \frac{\strut a-(a+h)}{\strut a(a+h)}}{\strut h}
    =
    \dfrac{-h}{a(a+h)} \times \dfrac{1}{h}
    \\
    &=&
    -\dfrac{1}{a(a+h)}
    .
    \end{eqnarray}
    \]

    Quand \(h\) tend vers 0, \(-\dfrac{1}{\strut a(a+h)}\) tend vers \(-\dfrac{1}{a^2}\) donc la fonction inverse est dérivable en tout réel \(a\neq 0\) et sa fonction dérivée est définie sur \(ℝ^*\) par \(f'(x)=-\dfrac{\strut1}{x^2}\).


!!! abstract "Propriété IV.1"
    Le tableau des dérivées des fonctions de base est :
 
    $$\begin{array}{|c|c|c|}
        \hline
        \mbox{Si }f(x)=... & \mbox{alors } f'(x)=...& \mbox{quand } x \in ...\\ \hline
        k\mbox{ (constante)}&0& ℝ \\ \hline
        x&1& ℝ \\ \hline
        x^2&2\,x& ℝ \\ \hline
        x^3&3\,x^2& ℝ \\ \hline
        x^n\ (n \in ℕ)&nx^{n-1}& ℝ \\ \hline
        \dfrac{\strut1}{\strut x} &-\dfrac{1}{x^2} & ℝ^* \\ \hline
    %    \dfrac{1}{x^n} \ (n>0)&-\dfrac{n}{x^{n+1}} & ℝ^* \\ \hline
        \sqrt{x}&\dfrac{\strut 1}{\strut 2\sqrt{x}} & ]0;+\infty[ \\ \hline
    %    \cos x &-\sin x& ℝ \\ \hline
    %    \sin x &\cos x& ℝ \\ \hline
    %%    \tan x &1+\tan^2 x& ℝ \\ \hline
    %    \ln x &1/x&\intoo0\pinfini\\ \hline
    %    \e^x &\e^x& ℝ \\ \hline
    \end{array}$$


!!! exemple "Exemple IV.4"
    La fonction dérivée de la fonction \(f\) définie sur ℝ par \(f(x)=x^7\) est la fonction \(f'\) définie sur ℝ par \(f'(x)=7\,x^6\).

!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) : questions 2 des exercices 42 et 44 page 101 (vérifier avec les questions 1, déjà faîtes)


## Dérivées de $ku$ et de $u+v$

!!! note "Définitions"
    * Soient $u$ une fonction définie sur $I$ et $k$ une constante. Nous notons $k\,u$ la fonction définie sur $I$ par $(k\,u)(x)=k  \times u(x)$.
    * Soient $u$ une fonction définie sur $I$ et $v$ une fonction définie sur $J$. Soit $u + v$ la fonction définie sur $I\cap J$ par $(u + v)(x)=u(x)+v(x)$ (remarque : il n'y a pas de distribution ici car il n'y a pas de multiplication).


!!! exemple "Exemples IV.5"
    Soit $u$ la fonction définie sur $[0;+\infty[$ par $u(x)=\sqrt{x}$.  
    Alors la fonction $3\,u$ est définie sur $[0;+\infty[$ par $3\,u(x)=3\sqrt{x}$.

    Soit $v$ la fonction définie sur $]-\infty;2]$ par $v(x)=\sqrt{2-x}$.  
    Alors la fonction $u+v$ est définie sur $[0;2]$ par $(u+v)(x)=\sqrt{x}+\sqrt{2-x}$.

!!! abstract "Propriétés IV.2"
    Soient $u$ et $v$ deux fonctions dérivables sur un intervalle $I$ et $k$ une constante.  
    Alors $k\,u$ et $u+v$ sont dérivables sur $I$ et :

    <div style="color:red">

    \[
    \boxed{(k.u)'=k.u'}\qquad\boxed{(u+v)'=u'+v'}
    \]

    </div>


!!! tip "Remarques"
    * Des deux formules précédentes, il en vient une troisième : <span style="color:red">$\boxed{(u-v)'=u'-v'}$</span>.
    * D'autres formules viendront dans un chapitre ultérieur...


!!! bug "Encore un abus de notation"
    Vous verrez souvent écrit \(u=x^2\) par exemple alors qu'il faudrait écrire \(u(x)=x^2\)...


!!! exemple "Exemples IV.6"
    === "Questions"
        Calculez les fonctions dérivées des fonctions définies sur un certain intervalle par :

        * \(f(x)=5\,x^3\) ;
        * \(g(x)=x^2+x^5\) ;
        * \(h(x)=4-3\,x^4\) ;
        * \(i(x)=\dfrac{2}{x}-\sqrt{x}\).

    === "Réponses"
        * \(f(x)=5\,x^3\) donc \(f\) est de la forme \(ku\) où \(k=5\) et \(u=x^3\) donc \(f'(x)=ku'(x) = 5 \times 3\,x^2 = 15\,x^2\).
        * \(g(x)=x^2+x^5\) donc \(g\) est de la forme \(u+v\) où \(u=x^2\) et \(v=x^5\) donc \(g'(x)=u'(x)+v'(x) = 2\,x+5\,x^4\).
        * \(h(x)=4-3\,x^4\) donc \(h\) est de la forme \(u+kv\) où \(u=4\), \(k=-3\) et \(v=x^4\) donc \(h'(x)=0 -3 \times 4\,x^3 = -12\,x^3\).
        * \(i(x)=\dfrac{2}{x}-\sqrt{x}=2\times \dfrac{1}{x}-\sqrt{x}\) donc  
        \(\begin{eqnarray}
        i'(x)
        &=&
        2\times \left(-\dfrac{1}{x^2}\right)-\dfrac{1}{2\sqrt{x}}
        =
        -\dfrac{2}{x^2}-\dfrac{1}{2\sqrt{x}}
        =
        -\dfrac{4}{2\,x^2}-\dfrac{x\sqrt{x}}{2\,x^2}
        \\
        &=&
        \dfrac{\strut-4-x\sqrt{x}}{2\,x^2}
        .
        \end{eqnarray}\)



!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    51, 46, 47 page 101  
    118 page 105  
    131 page 105

!!! tip "Transformation d'écriture avant de dériver"
    Vous ne savez dériver qu'une somme ou une différence mais il est parfois possible de transformer l'écriture d'une fonction dont l'expression n'en est pas une avant de dériver.

!!! exemple "Exemples de transformations"
    * \(f(x)=\dfrac{x+1}{x}\) ne se dérive pas en \(\dfrac{1}{1}\) ! Il faut remarquer que 
    \(f(x)=\dfrac{x+1}{x}=\dfrac{x}{x}+\dfrac{1}{x} = 1 + \dfrac{1}{x}\)
    donc \(f'(x)=1-\dfrac{1}{x^2}\).
    * \(g(x)=x(x+1)\) ne se dérive pas en \(1\times 1\) ! Il faut remarquer que 
    \(g(x)=x^2+x\) donc \(g'(x)=2\,x+1\).
    * \(h(x)=(3\,x+1)^2\) ne se dérive pas en \(3^2\) ! En utilisant une identité remarquable, 
    \(h(x)=9\,x^2+6\,x+1\) donc \(h'(x)=18\,x+6\).
    * \(i(x)=\dfrac{x}{2}\) ne se dérive pas en \(\dfrac{1}{0}\) !! Comme diviser revient à multiplier par l'inverse, \(i(x)=\dfrac{1}{2}\,x\) donc \(i'(x)=\dfrac{1}{2}\).


!!! exemple "Exemple IV.7"
    === "Questions"
        Soit \(f\) la fonction définie sur ℝ par \(f(x)=x^3-3\,x^2+1\).  
        Construire la tangente à la courbe de \(f\) en son point d'abscisse 3.  
        Déterminer l'équation réduite de cette tangente.


    === "Réponses"

        Pour tout réel \(x\), nous avons \(f'(x)=3\,x^2-3(2\,x)\) 
        donc \(f'(3)=3 \times 3^2-3 \times (2 \times 3) = 9\) (valeur conjecturée plus tôt dans l'exemple II.3 avec un tableau de valeurs).

        La connaissance de \(f(3)= 1\) et de \(f'(3)= 9\) suffit pour tracer la tangente :

        ![](images/derivee_tangente_fin.png){width=50%; : .center}

        L'équation réduite de la tangente est alors :

        \(y
        =f(3)+f'(3)(x-3)
        =1+9(x-3)
        =9\,x-26
        \).


!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    77, 78 page 103  
    81, 82 page 103



??? tip "Bonus : démonstration de la dérivée de la fonction racine carrée."

    Soit \(f\) la fonction racine carré, définie sur $ℝ^+$ par \(f(x)=\sqrt{x}\).

    Alors, pour tout \(a\), 

    \[
    \begin{eqnarray}
    \dfrac{f(a+h)-f(a)}{\strut h}
    &=&
    \dfrac{\sqrt{a+h}-\sqrt{a}}{h}
    \\
    &=&
    \dfrac{\strut(\sqrt{a+h}-\sqrt{a})(\sqrt{a+h}+\sqrt{a})}{\strut h(\sqrt{a+h}+\sqrt{a})}
    \\
    &=&
    \dfrac{\strut(\sqrt{a+h})^2-(\sqrt{a})^2}{\strut h(\sqrt{a+h}+\sqrt{a})}
    =
    \dfrac{\strut a+h-a}{\strut h(\sqrt{a+h}+\sqrt{a})}
    \\
    &=&
    \dfrac{\strut h}{\strut h(\sqrt{a+h}+\sqrt{a})}
    =
    \dfrac{1}{\sqrt{a+h}+\sqrt{a}}
    .
    \end{eqnarray}
    \]

    Quand \(h\) tend vers 0, \(\dfrac{1}{\sqrt{a+h}+\sqrt{a}}\) tend vers \(\dfrac{1}{2\sqrt{a}}\) donc la fonction racine carrée est dérivable en tout réel \(a>0\) et sa fonction dérivée est définie sur \(]0 ; +\infty[\) par \(f'(x)=\dfrac{\strut1}{2\sqrt{x}}\).












<!--


!!! question "Exercice"

    Dîtes si les expressions suivantes sont celles de fonctions du second degré et, si oui, indiquez les valeurs de $a$, $b$, $c$ :

    === "Propositions"

        - [ ] \(3x-7\), si oui \(a=\dots\), \(b=\dots\) et \(c=\dots\)
        - [ ] \(-3x^2+2x-1\), si oui \(a=\dots\), \(b=\dots\) et \(c=\dots\)
        - [ ] \(2x^2-4x\), si oui \(a=\dots\), \(b=\dots\) et \(c=\dots\)
        - [ ] \(2x^3-7x^2+1\), si oui \(a=\dots\), \(b=\dots\) et \(c=\dots\)
        - [ ] \(1+2x^2\), si oui \(a=\dots\), \(b=\dots\) et \(c=\dots\)


    === "Solution"

        - :x: ~~\(3x-7\)~~ : il n'y a pas de terme en \(x^2\)
        - :white_check_mark: \(-3x^2+2x-1\) avec \(a=-3\), \(b=2\) et \(c=-1\)
        - :white_check_mark: \(2x^2-4x\) avec \(a=2\), \(b=-4\) et \(c=0\)
        - :x: \(2x^3-7x^2+1\) : c'est un polynome du 3^ème^ degré
        - :white_check_mark: \(1+2x^2\) avec \(a=2\), \(b=0\) et \(c=1\)

-->
