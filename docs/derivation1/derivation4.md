# III. Tangente à la courbe d’une fonction en un point
## Définition

Considérons les sécantes passant par un point \(A\) et notons \(B\) un autre point de la courbe.

Alors, quand \(B\) s'approche de \(A\), les sécantes peuvent se rapprocher d'une droite appelée <span style="color:red">tangente</span> à la courbe en \(A\).

<iframe scrolling="no" title="Sécantes et tangente en un point" src="https://www.geogebra.org/material/iframe/id/k2hrxpzz/width/1472/height/704/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="1000px" height="460px" style="border:0px;"> </iframe>


!!! tip "Remarques"
    * Dire que $B$ se rapproche revient ici à dire que $h$ tend vers 0.
    * Certaines courbes n'ont pas de tangente en un point, par exemple la courbe de la fonction valeur absolue n’a pas de tangente en son point \(A\) d'abscisse 0 :  
    ![](images/courbe_val_abs.png){width=50%; : .center}

!!! question "Exercices"
    [Exercices 1A.1 et 1A.2](derivation_STI_1N6_ex1a.pdf)


## Dérivabilité et tangente

!!! abstract "Propriété III.1"
    Si $f$ est dérivable en $a$ alors la courbe représentative de $f$ admet une tangente en son point d'abscisse $a$.

??? "Et la réciproque ?"
    La réciproque de cette propriété est fausse : la courbe de la fonction racine carrée a une tangente en \(O\,(0;0)\) mais la fonction racine carrée n'est pas dérivable en 0 car la tangente n'a pas de coefficient directeur (elle est "verticale").



!!! exemple "Exemple III.1"
    Nous avons prouvé que la fonction carré est dérivable en 1 donc sa courbe a une tangente en son point d'abscisse 1.


!!! abstract "Propriété III.2"
    Si $f$ est dérivable en $a$ alors le coefficient directeur de la tangente au point d'abscisse \(a\) est <span style="color:red">\(f'(a)\)</span>.

    ![](images/derivee_tangente.png){width=50%; : .center}

!!! tip "Remarque"
    En effet, si la fonction est dérivable en \(a\) alors le taux de variation (la pente de la sécante) se rapproche de \(f'(a)\), qui est donc la pente de la tangente.

!!! danger "Attention"
    Ne confondez pas \(f(a)\) (l'ordonnée du point d'abscisse \(a\)) avec \(f'(a)\) (la pente de la tangente en ce point).


!!! exemple "Exemple III.2"
    La fonction carré est dérivable en 1 et \(f'(1)=2\).  
    La tangente à sa courbe en son point d'abscisse 1 a donc pour coefficient directeur 2.

    ![](images/derivee_tangente2.png){width=50%; : .center}

    Remarquez que \(f(1)=1\) n'est pas égal à \(f'(1)=2\).


!!! question "Exercices"
    [Exercice 1A.3](derivation_STI_1N6_ex1a.pdf)  
    [Transmath](https://biblio.manuel-numerique.com/) : 16 page 99


## Équation réduite d'une tangente

!!! abstract "Propriété III.3"
    Si $f$ est dérivable en $a$ alors la  tangente  à  la  courbe  représentative  de $f$ au  point d’abscisse $a$ a pour équation
    <span style="color:red">\(\boxed{y=f(a)+f'(a)(x-a)}\)</span>.

!!! note "Démonstration remarquable"
    Si $f$ est dérivable en $a$ alors la  tangente  à  la  courbe  représentative  de $f$ au  point d’abscisse $a$ a pour coefficient directeur $f'(a)$ donc son équation réduite s'écrit \(y=f'(a)\,x+p\).

    Elle passe par \(A\,(a;f(a))\) donc \(f(a)=f'(a)\times a+p\) d'où \(p=f(a)-a\,f'(a)\).

    L'équation réduite de cette tangente est donc : \(y=f'(a)\,x+f(a)-a\,f'(a) = f(a)+f'(a)(x-a)\).

!!! exemple "Exemple III.2"
    Cherchons l'équation réduite de la tangente à la courbe de la fonction carré en son point d'abscisse 1 (nous savons déjà que \(f(1)=1\) et \(f'(1)=2\)) :

    \(y=f(1)+f'(2)(x-1)=1+2(x-1)=2\,x-1\)

!!! question "Exercice"
    [Exercices 1A.4 et 1A.5](derivation_STI_1N6_ex1a.pdf)  
    [Transmath](https://biblio.manuel-numerique.com/) : 95 page 104 et 34 page 100

