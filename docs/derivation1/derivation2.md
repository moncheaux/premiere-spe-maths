# I. Sécantes à une courbe en un point

!!! abstract "Définition I.1"
    Une sécante à une courbe coupe cette courbe en deux points :

    ![](images/secante.png){width=50% ; : .center}

!!!tip "Remarques"
    * Si nous notons \(a\) et \(b\) les abscisses respectives des points \(A\) et \(B\) alors les coordonnées de ces points s'écrivent : \(A\,(a;f(a))\) et \(B\,(b;f(b))\).
    * Le coefficient directeur d'une sécante s'écrit donc :

    \[m = \dfrac{y_B-y_A}{x_B-x_A} = \dfrac{f(b)-f(a)}{b-a}.\]

!!! abstract "Définition I.2" 

    Le <span style="color:red">taux de variation</span> d'une fonction \(f\) entre deux nombres \(a\) et \(b\) est :

    <div style="color:red">

    \[
    \boxed{\dfrac{f(b)-f(a)}{b-a}}
    \]

    </div>


!!! exemple "Exemple I.1"
    === "Question"
        Calculez le taux de variation entre 2 et 6 de la fonction \(f\) définie sur IR par \(f(x)=-4\,x+3\).

    === "Réponse"    
        \[\frac{\strut f(6)-f(2)}{6-2} = \frac{-21-(-5)}{4} = \frac{-16}{4}=-4.\]

        Ce résultat est logique car la sécante et la courbe de la fonction affine \(f\) sont confondues donc ont la même pente !


!!! exemple "Exemple I.2"
    === "Question"
        Calculez le taux de variation entre 2 et 5 de la fonction \(g\) définie sur IR par \(g(x)=x^2-4\,x+2\).
    
    === "Réponse"
        \[\frac{\strut g(5)-g(1)}{5-1} = \frac{7-(-1)}{4} = \frac{8}{4}=2.\]

        ![](images/pente_secante.png){width=50%; height=30%; : .center}



!!! danger "Une erreur fréquente"
    Si \(f\) est croissante sur l'intervalle \([a;b]\) alors le taux de variation de \(f\) entre \(a\) et \(b\) est positif. La réciproque est __fausse__.

    Regardez la figure de l'exemple I.2 : le coefficient directeur de la sécante \((AB)\) est positif (il vaut 2) mais la fonction n'est pas croissante entre \(a=1\) et \(b=5\) !





!!! tip "Remarques"
    * Afin de faire un "zoom" autour du point \(A\), nous allons maintenant rapprocher le point \(B\) du point \(A\). Dans la suite du cours, nous considérerons les sécantes passant par un point \(A\) qui reste fixe (seul le point \(B\) se déplace alors sur la courbe).
    * Si nous notons \(a\) et \(a+h\) les abscisses respectives des points \(A\) et \(B\) alors les coordonnées de ces points s'écrivent : \(A\,(a;f(a))\) et \(B\,(a+h;f(a+h))\).
    * Le coefficient directeur d'une sécante passant par \(A\) s'écrit alors \(m=\dfrac{f(b)-f(a)}{b-a}=\dfrac{f(a+h)-f(a)}{h}\).

    ![](images/secante3.png){width=30% ; : .center}


!!! note "À retenir" 

    Le taux de variation</span> d'une fonction \(f\) entre deux nombres \(a\) et \(a+h\) est :

    <div style="color:red">

    \[
    \dfrac{f(a+h)-f(a)}{h}
    \]

    </div>

<!--
!!! abstract "Définition" 

    Le <span style="color:red">taux de variation</span> d'une fonction \(f\) entre deux nombres \(a\) et \(a+h\) est :

    <div style="color:red">

    \[
    \boxed{\dfrac{f(a+h)-f(a)}{h}}
    \]

    </div>


!!! tip "Remarques"
    * Ce nombre est donc le coefficient directeur de la sécante passant par les points \(A\,(a;f(a))\) et \(B\,(a+h;f(a+h))\).
    * Pour une fonction affine, définie sur IR par \(f(x)=a\,x+b\), le taux de variation est le coefficient \(a\) (voir exemple I.1) puisque la sécante est confondue avec la courbe représentative de la fonction !

-->


??? tip "Utilisations dans des situations concrètes"
    __Exemple en sciences physiques__  
    Une balle lâchée d'une certaine hauteur parcourt en un temps \(t\) (en secondes) une distance 
    \(d(t)=4,9\,t^2\) (en mètres).
    Après 1 seconde, elle parcourt \(d(1)=4,9\times 1^2=4,9\) mètres.  
    Après 4 secondes, elle parcourt \(d(4)=4,9\times 4^2=78,4\) mètres.  
    Entre ces deux temps, elle a parcouru \(78,4-4,9=73,5\) mètres en \(4-1=3\) secondes, soit une vitesse moyenne de \(73,5 \div 3 = 24,5\) m/s.

    Or le taux de variation de \(d\) entre les temps \(t=1\) et \(t=4\) est \(\frac{\strut d(4)-d(1)}{4-1} = \frac{78,8-4,9}{3} = \frac{73,5}{3}=24,5\).

    Le taux de variation correspond donc à la notion de <span style="color:red">vitesse moyenne</span>.

    __Exemple en économie__  
    Le coût marginal est le coût engendré par la fabrication d'un objet supplémentaire :
    \(C_m(n)=C(n+1)-C(n) = \frac{C(n+1)-C(n)}{(n+1)-n}\) : c'est le taux de variation du coût entre \(n\) et \(n+1\).


!!! tip "Étude des variations d'une fonction"
    Pour connaître les variations d'une fonction, il va falloir laisser \(a\) et \(h\) quelconques.  
    Commençons par \(h\)...
    

!!! exemple "Exemple I.3"

    Soit \(f\) la fonction carré. Alors le taux de variation de cette fonction entre 1 et \(1+h\) est :

    \[
    \dfrac{f(1+h)-f(1)}{h}
    =
    \dfrac{(1+h)^2-1^2}{h}
    =
    \dfrac{1^2+2h+h^2-1}{h}
    =
    \dfrac{2h+h^2}{h}
    =
    2+h.
    \]

!!! tip "Remarque"
    Quand \(h\simeq0\), ce taux de variation est proche de \(2\)...


!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) : 3 à 7 page 99
