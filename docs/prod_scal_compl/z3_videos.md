# Des vidéos d'explications
## Dérivation locale, taux de variation et nombre dérivé
* histoire et explication du principe de dérivation locale : [ici](https://www.lumni.fr/video/leibniz-et-newton-le-calcul-infinitesimal) et [là](https://www.youtube.com/watch?v=RLEE-iSBimc) ;
* calcul du nombre dérivé avec le taux de variation (explications et exemples) : 
  * [avec des fonctions simples](https://www.youtube.com/watch?v=Ovooolgp7q4) ;
  * [avec des fractions](https://www.youtube.com/watch?v=YxZzSEp7Mgk&list=PLUfJOEwIb2c6V6AfToJBo1aYy9MgwLI3-&index=3) ;
  * [avec des polynomes](https://www.youtube.com/watch?v=4C4KMzt26Ko&list=PLUfJOEwIb2c6V6AfToJBo1aYy9MgwLI3-&index=4) ;
  * [avec des racines carrés](https://www.youtube.com/watch?v=XhE2FtzuL5U&list=PLUfJOEwIb2c6V6AfToJBo1aYy9MgwLI3-&index=5) ;
* [un cours complet filmé](https://www.youtube.com/watch?v=L6SjDXMIvMw) ;
* [calculer des nombres dérivés avec un graphique](https://www.youtube.com/watch?v=G-B2PEZhXCc&list=PLUfJOEwIb2c6V6AfToJBo1aYy9MgwLI3-&index=6) ;
* [trouver une équation d'une tangente avec un graphique](https://www.youtube.com/watch?v=dmX5Gp-Wx30) ;
* [trouver une équation d'une tangente avec l'expression de la fonction](https://www.youtube.com/watch?v=n8k1048FHPc&list=PLUfJOEwIb2c6V6AfToJBo1aYy9MgwLI3-&index=7) ;

## Dérivation globale
* [calculer la dérivée d'une fonction polynome](https://www.youtube.com/watch?v=NYIjC5fl4ZI&list=PLUfJOEwIb2c5Uvvlp2day-6nNG0CdppPk) ;
* [calculer la dérivée d'une fonction comportant un inverse ou une racine carrée](https://www.youtube.com/watch?v=HJo3uWJbVvg&list=PLUfJOEwIb2c5Uvvlp2day-6nNG0CdppPk&index=2) ;
* idem : [ici](https://www.youtube.com/watch?v=ehHoLK98Ht0) et [là](https://www.youtube.com/watch?v=1fOGueiO_zk) ;
* [trouver l'équation d'une tangente avec l'expression de la fonction](https://www.youtube.com/watch?v=lNeSqzcy_Ag&list=PLUfJOEwIb2c5Uvvlp2day-6nNG0CdppPk&index=10) ;
* [tracer une tangente en un point avec l'expression de la fonction](https://www.youtube.com/watch?v=9tHUh01aSKo)