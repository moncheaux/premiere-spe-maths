# Quelques exercices pour vous entraîner
Les valeurs utilisées dans ces exercices sont générées par le site Wims, vous pouvez donc refaire plusieurs fois chacun d'entre eux.

## Nombre dérivé
* [calculer un nombre dérivé avec le taux de variation](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/oefderivee1S.fr&cmd=new&exo=nbderiv) ;
* [idem et trouver l'équation d'une tangente](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/oefderivee1S.fr&cmd=new&exo=eqtgte1) ;
* [calculer un nombre dérivé avec un graphique](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/derivation1ere.fr&cmd=new&exo=nbdergraph)
 et [là](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/derivation1ere.fr&cmd=new&exo=nbdergraph2) ;


## Calculs de dérivées

* [dérivée d'un monome](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H6/analysis/OEFderivee.fr&cmd=new&exo=form1) ;
* [dérivée d'un polynome](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H6/analysis/OEFderivee.fr&cmd=new&exo=der2) et [là](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/derivation1ere.fr&cmd=new&exo=deriverPoly) ;


## Tangente et calcul de dérivées
* [lecture graphique puis calcul de la pente d'une tangente](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H6/analysis/OEFderivee.fr&cmd=new&exo=tan0) ;
* [calcul de l'équation d'une tangente (guidé)](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H6/analysis/OEFderivee.fr&cmd=new&exo=tan1) ;
* [calcul de l'équation d'une tangente (non guidé)](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H6/analysis/OEFderivee.fr&cmd=new&exo=tan2)
et [là](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/oefderivee1S.fr&cmd=new&exo=eqtgte2)
