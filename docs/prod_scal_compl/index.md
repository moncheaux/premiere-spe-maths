# Introduction
## Petit historique
Se basant sur des recherches mathématiques menées depuis l'Antiquité (Archimède, Thābit ibn Qurra, Cavalieri, John Wallis, René Descartes, Pierre de Fermat, Blaise Pascal, Isaac Barrow), la notion de dérivée est définie au XVIIème par deux grands penseurs en même temps : Leibniz et Newton. Des accusations de plagiat fusent alors, deux camps se forment pour défendre leur champion mais les historiens considérent maintenant que Leibniz a pu s'inspirer de Newton mais que sa méthode était au final plus pratique. De toute façon, l'idée était "dans l'air du temps".

![Source de l'image : https://s3-us-west-2.amazonaws.com/courses-images/wp-content/uploads/sites/2332/2018/01/11205144/CNX_Calc_Figure_03_01_001.jpg](images/Newton_Leibniz.jpg){width=30%; : .center}


## Rappels utiles
### Coefficient directeur d'une droite

!!! abstract "Définition"
    Le <span style="color:red;">coefficient directeur</span> d'une droite est égal à :

    <div style="color:red">

    \[
    m=\dfrac{\Delta y}{\Delta x}
    \]

    </div>

    où \(\Delta x\) et \(\Delta y\) sont les déplacements horizontaux et verticaux entre deux points de la droite.

    En particulier, \(m=\Delta y\) quand \(\Delta x\) vaut 1.

    <iframe scrolling="no" title="Coefficient directeur sans coordonnées de points" src="https://www.geogebra.org/material/iframe/id/jpa4qs44/width/1472/height/704/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="1000px" height="460px" style="border:0px;"> </iframe>

!!! abstract "Remarque utile"
    Le coefficient directeur indique la vitesse de croissance (ou de décroissance) d'une droite.

??? tip "La pente d'une route"
    ![Source de l'image : https://de.serlo.org/mathe/59968/prozentangaben-bei-steigungen](images/pente.png){ width=5%; align=right }
    La pente d'une droite indique son inclinaison par rapport à l'horizontale. Dans le langage courant, elle est donnée par un angle ou par un pourcentage :

    <iframe scrolling="no" title="Pente (pourcentage et angle)" src="https://www.geogebra.org/material/iframe/id/cvd85k7j/width/1472/height/704/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="1000px" height="500px" style="border:0px;"> </iframe>

    Ainsi une pente de 20 % correspond à un coefficient directeur de \(\dfrac{20}{100}=0,2\) et à un angle avec l'horizontale de \(\arctan 0,2 \simeq 11,3°\).

  


!!! abstract "Propriété"
    Le coefficient directeur d'une droite $(AB)$ est égal à :

    <div style="color:red">

    \[
    \boxed{m=\dfrac{y_B-y_A}{x_B-x_A}}
    \]

    </div>

    <iframe scrolling="no" title="Coefficient directeur avec coordonnées de points" src="https://www.geogebra.org/material/iframe/id/khrsaaz8/width/1472/height/704/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="1000px" height="460px" style="border:0px;"> </iframe>

!!! exemple "Exemple"
    Si \(A\,(4;-1)\) et \(B\,(-2;-5)\) alors le coefficient directeur de la droite \((AB)\) est 
    \(m=\dfrac{-5-(-1)}{-2-4}=\dfrac{-4}{-6}=\dfrac{2}{3}\).


!!! danger "Et si $x_A=x_B$ ?"
    La fraction ci-dessus n'est pas calculable si $x_A=x_B$, cela se produit quand la droite est parallèle à l'axe des $y$ : de telles droites **n'ont pas de coefficient directeur**.



### Variation d'une fonction affine

!!! abstract "Propriété"
    Soit \(f\) une fonction affine, définie sur IR par \(f(x)=a\,x+b\).
    Alors \(a\) est le coefficient directeur de la droite représentant la fonction \(f\).

!!! abstract "Propriété"
    Soit \(f\) une fonction affine, d'expression \(f(x)=a\,x+b\).

    * si \(a>0\) alors \(f\) est strictement croissante sur IR ;
    * si \(a<0\) alors \(f\) est strictement décroissante sur IR ;
    * si \(a=0\) alors \(f\) est constante sur IR.

    <iframe scrolling="no" title="La fonction affine et ses paramètres" src="https://www.geogebra.org/material/iframe/id/VJsUYxmx/width/1024/height/580/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="1024px" height="580px" style="border:0px;"> </iframe>

!!! bug "Attention"
    Seules les droites ont un coefficient directeur donc la propriété ci-dessus ne s'applique qu'aux fonctions affines.

    Un des buts de la dérivation est d'utiliser quand même cette propriété pour étudier les variations de fonctions non affines.

    L'idée est que lorsque nous zoomons sur un point d'une courbe alors, au voisinage de ce point, la courbe _ressemble_ souvent à une droite ayant un coefficient directeur.

    <iframe scrolling="no" title="Zoom sur un point d'une courbe avec curseur" src="https://www.geogebra.org/material/iframe/id/ygydatuk/width/1920/height/918/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="1100px" height="460px" style="border:0px;"> </iframe>

    Il ne reste plus qu'à trouver un moyen de calculer ce coefficient directeur !

