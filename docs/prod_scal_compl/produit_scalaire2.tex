\documentclass[12pt,2col]{cours}
\begin{document}

\pieddepagecomplet[Première]

\titre{Compléments sur le produit scalaire}

\partie{Rappels}

Voilà les trois définitions du produit scalaire vues cette année  :


\begin{definition}
\tabfig{
Soient \(\vecteur{AB}\) et \(\vecteur{CD}\) deux vecteurs.
Soient $C'$ et $D'$ les projections orthogonales de $C$ et $D$ 
sur la droite ($AB$). Le produit scalaire des vecteurs \(\vecteur{AB}\) et \(\vecteur{CD}\) est :
}{
\psset{unit=1mm}
\begin{pspicture}(70,20)
\psline[linestyle=dashed](0,5)(70,5)
\put(18,1){$C'$}
\put(16,9){$C$}
\put(29,1){$A$}
\put(58,1){$B$}
\put(49,1){$D'$}
\put(51,20){$D$}
\psframe(20,5)(22,7)
\psframe(50,5)(52,7)
\psline[linestyle=dotted](20,5)(20,10)
\psline[linestyle=dotted](50,5)(50,20)
% \put(50,5){\dashbox{1}(0,15){}}
{\thicklines
\put(20,10){\vector(3,1){30}}
\put(30,5){\vector(1,0){30}}
}
% \psframe(20,5)(51,6)
% \put(20,5){\framebox(1,1){}}
% \put(50,5){\framebox(1,1){}}
\end{pspicture}
}

\enc{$
\rouge{
\vecteur{AB}\cdot\vecteur{CD}=\left\{
\begin{tabular}{ll}    			
$AB.C'D'$&si \(\vecteur{AB}\) et \(\vecteur{C'D'}\) ont le même sens\\
$-AB.C'D'$&si \(\vecteur{AB}\) et \(\vecteur{C'D'}\) ont un sens contraire
\end{tabular}\right.
}$}
\end{definition}


\begin{definition}
% http://xymaths.free.fr/Lycee/1S/Cours-1S/Cours-Angles-Orientes-Produit-Scalaire.php?lx#src
\centerline{
\fbox{
\psset{unit=1cm,arrowsize=7pt}
\begin{pspicture}(-0.2,-1.2)(7.5,1.3)
  \psline[arrowsize=5pt,linewidth=1.4pt]{->}(0,0)(2,1)
  \rput(0.9,0.8){$\vec{v}$}
  \psline[arrowsize=5pt,linewidth=1.4pt]{->}(0,0)(1.5,-1)
  \rput(0.7,-0.8){$\vec{u}$}
  \psarc{->}(0,0){1.2}{-29}{25}\rput(1.5,0){$\theta$}
  \rput[l](3,0){\rouge{$\vec{u}\cdot\vec{v}=\left\|\vec{u}\right\|\times\left\|\vec{v}\right\|  \times \cos\theta$}}
\end{pspicture}
}
}
\end{definition}


\begin{definition}
Dans un repère orthonormé, si $\vec{u}\,\Coordsp XYZ$ et $\vec{v}\,\Coordsp{X'}{Y'}{Z'}$ alors \encpoint{$\rouge{\vec{u}\cdot\vec{v}=XX'+YY'+ZZ'}$}
\end{definition}




\partie{Carré scalaire et applications}
\spartie{Définition}

\begin{definition}
Le nombre $\vec{u}\cdot\vec{u}$ peut être noté $\vec{u}\,^2$ (carré scalaire de $\vec{u}$).
\end{definition}


\begin{propriete}
Pour tout vecteur $\vec{u}$:
\encpoint{$\vec{u}\,^2=||\vec{u}||^2$}

\end{propriete}



\begin{demonstration}
En effet, 
$
\vec{u}^2
=
\vec{u}\cdot\vec{u}
=
\left\|\vec{u}\right\|\times\left\|\vec{u}\right\|  \times \cos0
=
\left\|\vec{u}\right\|^2  \times 1
=
\left\|\vec{u}\right\|^2
$.
\end{demonstration}




\begin{exemple}
Si un vecteur $\vec{u}$ a pour norme 6 alors $\vec{u}\,^2=6^2=36$.
\end{exemple}

\begin{remarque}
C'est un des seuls cas où l'on peut remplacer un vecteur par sa longueur.
\end{remarque}


\spartie{Développement de $\boldsymbol{\|\vec{u}+\vec{v}\|^2}$}

%Notations usuelles pour cette sous-partie 

\begin{propriete}
Pour tous les vecteurs $\vec{u}$ et $\vec{v}$ :
\encpoint{\rouge{$\|\vec{u}+\vec{v}\|^2 = \|\vec{u}\|^2+2\,\vec{u}\cdot\vec{v}+\|\vec{v}\|^2$}}

\end{propriete}

\begin{demonstration}
\begin{align*}
\|\vec{u}+\vec{v}\|^2
	&=
	\left(\vec{u}+\vec{v}\right)^2
	= 
	\left(\vec{u}+\vec{v}\right)\cdot \left(\vec{u}+\vec{v}\right)
	= \vec{u}\cdot\vec{u}+\vec{u}\cdot\vec{v}+\vec{v}\cdot\vec{u}+\vec{v}\cdot\vec{v}
	\\
	&=
	\vec{u}^2+\vec{u}\cdot\vec{v}+\vec{u}\cdot\vec{v}+\vec{v}^2
	=
	\|\vec{u}\|^2+2\,\vec{u}\cdot\vec{v}+\|\vec{v}\|^2	
\end{align*}
\end{demonstration}


\begin{remarques}
\ding{235} cette propriété ressemble à une identité remarquable mais notez bien la présence d'un produit scalaire ;

\ding{235} remarquez qu'en isolant ce produit scalaire, nous trouvons :

$\vec{u}\cdot\vec{v} = \dfrac{1}{2} \left( \|\vec{u}+\vec{v}\|^2 - \|\vec{u}\|^2 - \|\vec{v}\|^2\right)$ ;

\ding{235} cette formule constitue une quatrième (et dernière !) définition du produit scalaire.
\end{remarques}






\spartie{Formule d'Al-Kashi}
\tabfig{Les notations suivantes sont souvent utilisées : 
$a=BC$, $b=AC$, $c=AB$ ; \newline
$\hat{A}$ est une mesure de l'angle $\strut$\angle BAC, $\hat{B}$~de l'angle \angle ABC et 
$\hat{C}$ de l'angle \angle ACB (de sorte que le côté de longueur $c$ soit
celui qui est \og en face \fg de l'angle $\hat{C}$).}
{\setlength{\unitlength}{9mm}
\begin{picture}(5.1,2.5)
\put(1.36,2.81){$A$}
\put(0,0.277){$B$}
\put(5.05,0.310){$C$}
\put(2.37,0.040){$a$}
\put(3.42,1.66){$b$}
\put(0.581,1.7){$c$}
\put(1.53,2.68){\line(3,-2){3.33}}
\put(4.85,0.461){\line(-1,0){4.45}}
\put(0.4,0.461){\line(1,2){1.11}}
\qbezier(1.41,2.46)(1.6,2.41)(1.7,2.56)
\qbezier(0.519,0.68)(0.73,0.67)(0.72,0.461)
\qbezier(4.48,0.71)(4.40,0.61)(4.47,0.461)
\put(0.75,0.564){$\hat{B}$}
\put(1.4,1.9){$\hat{A}$}
\put(3.8,0.55){$\hat{C}$}
\end{picture}}





\begin{theoreme}
Formule d'Al-Kashi :

\centerline{\rouge{\fbox{$a^2=b^2+c^2-2b.c.\cos \hat{A}$}}}

\end{theoreme}

\begin{demonstrationremarquable}
\begin{align*}
a^2&=BC^2 = \vecteur{BC}\,^2 = \left(\vecteur{BA}+\vecteur{AC}\right)^2
       =\vecteur{BA}\,^2+2\vecteur{BA}\cdot \vecteur{AC} + \vecteur{AC}\,^2\\
       &=BA^2+2\left(-\vecteur{AB}\right)\cdot \vecteur{AC}+AC^2
       =AB^2+AC^2-2\vecteur{AB}\cdot \vecteur{AC}\\
       &=c^2+b^2 - 2 \times AB \times AC \times \cos \left(\vecteur{AB}, \vecteur{AC}\right)\\
       &= c^2+b^2 - 2 \times c \times b \times \cos \hat{A}.
\end{align*}
\end{demonstrationremarquable}




\medskip
\begin{remarques}
\ding{235} si je connais deux côtés et l'angle entre les deux alors je peux calculer le troisième côté ;

\ding{235} si je connais les trois côtés alors je peux calculer n'importe quel angle ;

\ding{235} 
le triangle étant quelconque, la formule d'Al-Kashi peut aussi s'écrire  
$b^2=a^2+c^2-2a.c.\cos \hat{B}$
ou
$c^2=a^2+b^2-2a.b.\cos \hat{C}$.

\ding{235} si le triangle est rectangle en $A$, la formule d'Al-Kashi devient 
$a^2=b^2+c^2-2b.c.\cos 90°=b^2+c^2-2\times b \times  c \times 0=b^2+c^2$ : nous retrouvons là la relation de Pythagore, qui n'est donc qu'un cas particulier de la formule d'Al-Kashi ; pour cette raison cette dernière est parfois appelée formule de Pythagore généralisée.
\end{remarques}




\begin{exemple}
Soit $ABC$ un triangle tel que $AB=5$, $BC=4$ et $\angle CBA=60°$.

Calculez une valeur approchée de $AC$ et des autres angles du triangle.


\underline{Réponses :}

Avec les notations, 
$c=5$, $a=4$ et $\hat{B}=60°$ et nous cherchons $b$.
\begin{align*}
b^2&=a^2+c^2-2a.c.\cos \hat{B} = 16+25-2 \times 4 \times 5 \times \cos 60°
\\
&= 41-20 = 21
\end{align*}
donc 
$AC = \sqrt{21} \simeq 4,58$.

Calculons maintenant l'angle $\hat{A}$ en écrivant la formule d'Al-Kashi ainsi :
$a^2=b^2+c^2-2b.c.\cos \hat{A}$
donc 

$\cos \hat{A} = \dfrac{a^2-b^2-c^2}{-2bc} = \dfrac{\strut 16-21-25}{\strut -2 \times \sqrt{21} \times 5} 
=\dfrac{3}{\sqrt{21}}$

d'où
$\hat{A} = \arccos \dfrac{\strut 3}{\strut \sqrt{21}} \simeq 49,11°$.

Enfin, $\hat{C}=180-\hat{A}-\hat{B} \simeq 70,89°$.
\end{exemple}

\newpage
\renewcommand*{\overrightarrow}[1]{\vbox{\halign{##\cr 
  \tiny\rightarrowfill\cr\noalign{\nointerlineskip\vskip1pt} 
  $#1\mskip2mu$\cr}}}


\partie{Transformation de $\boldsymbol{\protect\vecteur{MA}\cdot\protect\vecteur{MB}}$ et application}

\begin{propriete}
Soit $[AB]$ un segment de milieu $I$.

Alors, pour tout point $M$, nous avons :

\centerline{
\fbox{\textcolor{red}{$\vecteur{MA}\cdot\vecteur{MB} = MI^2-\dfrac{AB^2}{4}$}}.
}

\end{propriete}

\begin{demonstrationremarquable}

\begin{align*}
\vecteur{MA}\cdot\vecteur{MB}
&=^{(1)}
\left(\vecteur{MI}+\vecteur{IA}\right)\cdot\left(\vecteur{MI}+\vecteur{IB}\right)
\\
&=^{(2)}
\vecteur{MI}\cdot\vecteur{MI}+\vecteur{MI}\cdot\vecteur{IB}
+\vecteur{IA}\cdot\vecteur{MI}+\vecteur{IA}\cdot\vecteur{IB}
\\
&=^{(3)}
\vecteur{MI}^2+\vecteur{MI}\cdot\vecteur{IB}
+\vecteur{MI}\cdot\vecteur{IA}+\vecteur{IA}\cdot \vecteur{IB}
\\
&=^{(4)}
MI^2+\vecteur{MI}\cdot\vecteur{IB}
+\vecteur{MI}\cdot\vecteur{IA}+\vecteur{IA}\cdot \left(-\vecteur{IA}\right)
\\
&=^{(5)}
MI^2+\vecteur{MI}\cdot(\vecteur{IB}+\vecteur{IA})-\vecteur{IA}^2
\\
&=^{(6)}
MI^2+\vecteur{MI}\cdot\vec{0}-IA^2
\\
&=^{(7)}
MI^2- \left(\dfrac{AB}{2}\right)^2
=
MI^2-\dfrac{AB^2}{4}
\end{align*}

$^{(1)}$ relation de Chasles

$^{(2)}$ distributivité du produit scalaire

$^{(3)}$ commutativité du produit scalaire ($\vec{u}\cdot\vec{v}=\vec{v}\cdot\vec{u}$)

$^{(4)}$ propriété 1 et $I$ milieu de $[AB]$ donc $\vecteur{IB}=-\vecteur{IA}$

$^{(5)}$ factorisation du produit scalaire

$^{(6)}$ $I$ milieu de $[AB]$ donc $\vecteur{IA}+\vecteur{IB}=\vec{0}$ et propriété 1

$^{(7)}$ $I$ milieu de $[AB]$ donc $IA = \dfrac{AB}{2}$.
\end{demonstrationremarquable}




\begin{propriete}
Soit $[AB]$ un segment.

L'ensemble des points $M$ tels que 
\textcolor{red}{$\vecteur{MA}\cdot\protect\vecteur{MB} = 0$} est le \textcolor{red}{cercle de diamètre $[AB]$}.

\end{propriete}

\begin{demonstrationremarquable}
\begin{align*}
\vecteur{MA}\cdot\vecteur{MB} = 0
&\iff
MI^2-\dfrac{AB^2}{4} = 0
\iff
MI^2 = \dfrac{AB^2}{4}
\\
&\iff
IM = \dfrac{\strut AB}{2}
\\
&\iff
M \text{ est sur le cercle de centre $I$, de rayon $\dfrac{AB}{2}$}
\\
&\iff
M \text{ appartient au cercle de diamètre $[AB]$}.
\end{align*}
\end{demonstrationremarquable}




\begin{remarque}
Nous retrouvons un théorème de géométrie bien connu, attribué à Thalès :
\og un point $M$ appartient au cercle de diamètre $[AB]$ si et seulement si le triangle $AMB$ est rectangle en $M$ \fg (j'exclus ici les points $A$ et $B$ pour simplifier).
\end{remarque}





\end{document}



















$\boldsymbol{\overrightarrow{MA}}$

%Transformation del’expression MA.MB.



\begin{propriete}
L'ensemble des points $M$ du plan tels que $\vecteur{MA}\cdot\vecteur{MB} = 0$ est le cercle de diamètre $[AB]$.
\end{propriete}


\begin{demonstrationremarquable}

\begin{align*}
\vecteur{MA}\cdot\vecteur{MB} = 0
		&\Longleftrightarrow BC^2 = \vecteur{BC}\,^2 = \left(\vecteur{BA}+\vecteur{AC}\right)^2
       =\vecteur{BA}\,^2+2\vecteur{BA}\cdot \vecteur{AC} + \vecteur{AC}\,^2\\
       &=BA^2+2\left(-\vecteur{AB}\right)\cdot \vecteur{AC}+AC^2
       =AB^2+AC^2-2\vecteur{AB}\cdot \vecteur{AC}\\
       &=c^2+b^2 - 2 \times AB \times AC \times \cos \left(\vecteur{AB}, \vecteur{AC}\right)\\
       &= c^2+b^2 - 2 \times c \times b \times \cos \hat{A}.
\end{align*}

\end{demonstrationremarquable}



\end{document}