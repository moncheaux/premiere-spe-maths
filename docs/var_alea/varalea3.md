# IV - Espérance

!!! abstract "Définition IV.1"
    L'<span style="color:red">espérance</span> d'une variable aléatoire $X$ prenant $n$ valeurs $x_1$, $x_2$, ..., $x_n$ est égale à 

    <div style="color:red">

    \[\boxed{E(X) =
    P(X=x_1) \times x_1 + 
    P(X=x_2) \times x_2 + 
    \dots +
    P(X=x_n) \times x_n
    =\sum_{i=1}^{n}p_ix_i}
    \]

    </div>

    C'est, en probabilité, la moyenne des valeurs que prendrait $X$ 
    si l'on répétait un grand nombre de fois l'expérience.


!!! example "Exemple IV.1"
    Calculons-la dans le cas de l'exemple II.1 (somme de deux dés tétraèdriques) :

    \(\begin{align*}
    E(X)&=
    p_1\times x_1
    +p_2\times x_2
    +p_3\times x_3
    +p_4\times x_4
    +p_5\times x_5
    +p_6\times x_6
    +p_7\times x_7
    \\
    &=
    2\times\dfrac{1}{16}
    +3\times\dfrac{2}{16}
    +4\times\dfrac{3}{16}
    +5\times\dfrac{4}{16}
    +6\times\dfrac{3}{16}
    +7\times\dfrac{2}{16}
    +8\times\dfrac{1}{16}
    =\dfrac{80}{16}=5
    \end{align*}\)

    (sur beaucoup de lancers de deux dés, la somme moyenne des deux dés devrait être de 5).

!!! example "Exemple IV.2"
    === "Question"
        Un forain me propose ce jeu : je mise 1 € et le forain lance un dé à six faces une fois, s'il obtient un~5 alors il me donne 8 €, sinon j'ai perdu.

        Ais-je intérêt à jouer ?

    === "Réponse"
        Soit $X$ le gain à l'issue d'une partie.

        Alors la loi de $X$ est :

        \[\begin{array}{|c|c|c|c|c|c|c|}\hline
        \text{Résultat du lancer du dé} &1&2&3&4&5&6\\\hline
        \text{Gain }X &-1&-1&-1&-1&7&-1\\\hline
        p_i
        &
        \dfrac{\strut 1}{\strut 6}&\dfrac{1}{6}&\dfrac{1}{6}
        &
        \dfrac{1}{6}&\dfrac{1}{6}&\dfrac{1}{6}
        \\\hline
        \end{array}\]

        donc

        \[
        \begin{align*}
        E(X)&=
        -1\times\dfrac{1}{6}
        +(-1)\times\dfrac{1}{6}
        +(-1)\times\dfrac{1}{6}
        +(-1)\times\dfrac{1}{6}
        +7\times\dfrac{1}{6}
        +(-1)\times\dfrac{1}{6}
        =\dfrac{2}{6}=\dfrac{1}{3}.
        \end{align*}
        \]

        Mon gain moyen se montera à $\dfrac{1}{3}$ € soit environ 33 cents : ce gain moyen est positif (pour moi).

!!! tip "Remarques"
    * l'espérance permet donc de savoir si un jeu est équitable (quand elle est nulle) ;
    * bien sûr, avec du bon sens nous pouvions trouver la réponse sans l'espérance mais d'autres exemples sont plus retords... ;
    * si je suis malchanceux, je peux perdre plus souvent que je gagne mais _statistiquement_ le jeu est plus intéressant pour moi que pour le forain.

!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    24, 25, 29, 33 page 283  
    70 page 288

