# II - Variables aléatoires
## Exemples et définition

!!! example "Exemple II.1"
    Lançons un dé à quatre faces (dé tétraèdrique) deux fois de suite, l'univers peut être ici modélisé par :

    $\Omega=\{(1;1);(1;2);(1;3);(1;4);(2;1);(2;2);(2;3);(2;4);(3;1);(3;2);(3;3);(3;4);(4;1);(4;2);(4;3);(4;4)\}$.

    Intéressons-nous à la somme des deux résultats, notée $X$.  
    Les valeurs que peut prendre $X$ sont 2 ; 3 ; 4 ; 5 ; 6 ; 7 ; 8.

    La valeur de $X$ dépend du résultat de l'expérience, nous dirons que $X$ est une _variable aléatoire_.

!!! example "Exemple II.2"
    Lançons un dé à six faces jusqu'à obtenir un 6 et appelons $X$ le nombre de lancers nécessaires. 

    Le 6 peut mettre beaucoup de temps à apparaître (par exemple dans le cas d'un dé truqué), de ce fait $X$ peut théoriquement prendre n'importe quelle valeur dans $\{1\,;\,2\,;\,3\,;\,4\,;\,5\,;\,6\,;\,7\,;\,\dots \} = ℕ^{*}$.


!!! example "Exemple II.3"
    Soit $X$ la durée d'attente à un guichet.  
    En supposant que le guichet reste ouvert pendant 4 heures, $X$ peut prendre n'importe quelle valeur de l'intervalle [0 ; 4].


!!! abstract "Définition II.1"
    Une <span style="color:red">variable aléatoire</span> est une fonction $X$ définie sur l'univers $\Omega$ de l'expérience et à valeurs réelles.


!!! example "Exemple II.1"
    Dans l'exemple précédent II.1, l'image de l'éventualité $(2\,;\,3)$ par $X$ est $X((2\,;\,3))=5$.

!!! tip "Remarques"
    * dans les exemples II.1 et II.2, la variable est _discrète_ : il est possible de numéroter les valeurs que prend $X$ ; celle de l'exemple II.3 est par contre _continue_ ;
    * dans l'exemple II.1, l'univers est _fini_ (pas infini), seul ce cas est au programme de première ;
    * sachez quand même pour information que, dans le cas d'une variable aléatoire non discrète, la dérivation et la fonction exponentielle jouent souvent un rôle...

## Événements $\{X=k\}$, $\{X \leqslant k\},\dots$

!!! abstract "Définitions II.2"
    L'événement $\{X=k\}$ est l'ensemble des éventualités de $\Omega$ qui ont pour image $k$ par $X$.

    L'événement $\{X \leqslant k\}$ est l'ensemble des éventualités de $\Omega$ qui ont une image inférieure ou égale à $k$ par $X$.


!!! example "Exemple II.1"
    L'événement $\{X=4\}$ est l'ensemble des éventualités ayant pour image 4 par $X$, il s'agit donc de l'événement $\{(1\,;\,3);(2\,;\,2);(3\,;\,1)\}$.

    L'événement $\{X\leqslant4\}$ est l'ensemble des éventualités ayant pour image 2 ou 3 ou 4 par $X$, il s'agit donc de l'événement $\{(1\,;\,1);(1\,;\,2);(1\,;\,3);(2\,;\,1);(2\,;\,2);(3\,;\,1)\}$.


!!! tip "Remarque"
    La probabilité de $\{X=k\}$ est notée $P(X=k)$ (au lieu de $P(\{X=k\})$).  
    De même, nous écrirons $P(X\leqslant k)$ (au lieu de $P(\{X\leqslant k\})$).


!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    53 page 285  
    1, 2, 3, 5, 6 page 281

