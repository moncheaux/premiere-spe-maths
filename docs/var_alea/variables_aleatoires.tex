\documentclass[12pt]{cours}
\usepackage{mathrsfs,pst-plot}

\begin{document}
\titre{Variables aléatoires}

\partie{Rappels sur les probabilités}
\spartie{\'Evénements}

\ding{235} 
Une \textcolor{red}{éventualité} (ou événement élémentaire) est un résultat possible lors d'une expérience aléatoire.

\ding{235} 
L'ensemble de toutes les éventualités est l'\textcolor{red}{univers} $\Omega$.

\ding{235} 
Un \textcolor{red}{événement} est un ensemble d'éventualités, donc une partie de l'univers.

\ding{235} 
L'\textcolor{red}{intersection} des événements $A$ et $B$, notée \textcolor{red}{$A\inter B$} ("$A$ inter $B$") est l'ensemble des éventualités qui se trouvent dans $A$ {\bf et} dans~$B$.

\ding{235} 
La \textcolor{red}{réunion} des événements $A$ et $B$, notée \mbox{\textcolor{red}{$A\,\union\,B$}} (\og $A$ union $B$ \fg), est l'ensemble des éventualités qui sont dans $A$ {\bf ou} dans~$B$ (ou dans les deux).


\spartie{Probabilités}

\ding{235} 
Si $A$ est un ensemble fini alors la \textcolor{red}{probabilité} de l'événement $A$, notée $P(A)$, est la somme des probabilités 
des éventualités contenues dans $A$.

\ding{235} 
Dans un cas d'\textcolor{red}{équiprobabilité}, la probabilité d'un événement A est :
$$\textcolor{red}{\boxed{P(A) =\dfrac{\mbox{nombre d'éventualités dans $A$}}{\mbox{nombre total d'éventualités}}}}$$
(nombre de cas favorables / nombre de cas possibles).

\ding{235} Propriétés indispensables :

\rouge{\enc{$0\le P(A)\le 1$}}
\rouge{\enc{$P(A\union B)=P(A)+P(B)-P(A\cap B)$}}
\rouge{\encpoint{$P\left(\bar A\right)=1-P(A)$}}


\spartie{Probabilités conditionnelles}
\ding{235} La probabilité que $B$ se réalise sachant que $A$ est réalisé est :

\rouge{\enc{$P_A(B)=\dfrac{P(B\cap A)}{P(A)} = \dfrac{P(A\cap B)}{P(A)}$}}

$P_A(B)$ se lit \og probabilité de $B$ sachant $A$ \fg, c'est une \rouge{probabilité conditionnelle}.

\ding{235} Calcul de $P(A\cap B)$ : 
\textcolor{red}{\fbox{$P(A\cap B)=P(A)\times P_A(B)=P(B)\times P_B(A)$}}


\ding{235} Formule des probabilités totales :

Si $A_1$, $A_2$, \dots $A_n$ forment une partition de l'univers alors 

\centerline{
\textcolor{red}{
\fbox{
\multiligne{
P(B) 
&=&
P(A_1\cap B) + P(A_2\cap B) + \dots + P(A_n\cap B)
\\
&=&
P(A_1) \times P_{A_1}(B) + P(A_2) \times P_{A_2}(B) + \dots + P(A_n) \times P_{A_n}(B).
}}}
}

\newpage

\partie{Variables aléatoires}
\spartie{Exemples et définition}

\begin{exemple}
Lançons un dé à quatre faces (dé tétraèdrique) deux fois de suite et intéressons-nous à la somme des deux résultats, notée $X$. L'univers peut être ici modélisé par :

{
\small
$\Omega=\{(1;1);(1;2);(1;3);(1;4);(2;1);(2;2);(2;3);(2;4);(3;1);(3;2);(3;3);(3;4);(4;1);(4;2);(4;3);(4;4)\}$.
}

Les valeurs que peut prendre $X$ sont 2 ; 3 ; 4 ; 5 ; 6 ; 7 ; 8.

La valeur de~$X$ dépend du résultat de l'expérience, nous dirons que $X$ est une \emph{variable aléatoire}.
\end{exemple}

\begin{exemple}
Lançons un dé à six faces jusqu'à obtenir un 6 et appelons $X$ le nombre de lancers nécessaires. 

Le 6 peut mettre beaucoup de temps à apparaître (par exemple dans le cas d'un dé truqué), de ce fait $X$ peut prendre n'importe quelle valeur dans 
$\{1\,;\,2\,;\,3\,;\,4\,;\,5\,;\,6\,;\,7\,;\,\dots \} = \N^{*}$.
\end{exemple}



\begin{exemple}
Soit $X$ la durée d'attente à un guichet.
En supposant que le guichet reste ouvert pendant 4 heures, 
$X$ peut prendre n'importe quelle valeur de l'intervalle \intff{0}{4}.
\end{exemple}




\medskip
\begin{definition}
Une \textcolor{red}{variable aléatoire} est une fonction $X$ définie sur l'univers $\Omega$ de l'expérience et à valeurs réelles.
\end{definition}

\begin{exemple}[1]
Dans l'exemple précédent, l'image de l'éventualité $(2\,;\,3)$ par $X$ est $X((2\,;\,3))=5$.
\end{exemple}

\begin{remarques}
\ding{235} dans les exemples 1 et 2, la variable est \emph{discrète} : il est possible de numéroter les valeurs que prend $X$ ; celle de l'exemple 3 est par contre \emph{continue} ;

\ding{235} dans l'exemple 1, l'univers est \emph{fini} (pas infini), seul ce cas est au programme de première ;

\ding{235} sachez quand même pour information que, dans le cas d'une variable aléatoire non discrète, la dérivation et la fonction exponentielle jouent souvent un rôle\dots.
\end{remarques}



\spartie{\'Evénements $\boldsymbol{\{X=k\}}$, $\boldsymbol{\{X \le k\}},\dots$}

\begin{definitions}
L'événement $\{X=k\}$ est l'ensemble des éventualités de $\Omega$ qui ont pour image $k$ par $X$.

L'événement $\{X \le k\}$ est l'ensemble des éventualités de $\Omega$ qui ont une image inférieure ou égale à~$k$ par $X$.
\end{definitions}



\begin{exemple}[1]

L'événement $\{X=4\}$ est 
%l'ensemble des éventualités ayant pour image 4 par $X$, il s'agit donc de 
l'événement $\{(1\,;\,3);(2\,;\,2);(3\,;\,1)\}$.

L'événement $\{X\le4\}$ est 
%l'ensemble des éventualités ayant pour image 2 ou 3 ou 4 par $X$, il s'agit donc de 
l'événement $\{(1\,;\,1);(1\,;\,2);(1\,;\,3);(2\,;\,1);(2\,;\,2);(3\,;\,1)\}$.
\end{exemple}



\begin{remarque}
La probabilité de $\{X=k\}$ est notée $P(X=k)$ (au lieu de $P(\{X=k\})$).

De même, on écrira $P(X\le k)$ (au lieu de $P(\{X\le k\})$).
\end{remarque}



\newpage

\partie{Loi d'une variable aléatoire}

\begin{definition}
La \textcolor{red}{loi d'une variable aléatoire} discrète $X$ est l'ensemble des couples ($x_i$ ; $P(X=x_i)$) où les $x_i$ sont les valeurs pouvant être prises par $X$.

Une loi de variable aléatoire peut être représentée par un tableau :

\smallskip
\centerline{%
$\begin{array}{|c|c|c|c|c|}\hline
\tvi\mbox{Valeur }x_i&x_1&x_2&\dots\dots &x_n\\\hline
\tvi P(X=x_i)&P(X=x_1)&P(X=x_2)&\dots\dots&P(X=x_n)
\\\hline
\end{array}$}

\end{definition}


\begin{remarque}
La notion de loi a déjà été vue mais il s'agissait d'une loi de probabilité, où l'on associait à chaque éventualité une probabilité.
\end{remarque}


\begin{propriete}
Si $X$ peut prendre les valeurs $x_1$, $x_2$, \dots, $x_n$ alors 
\fbox{$\displaystyle\sum_{i=1}^{n}P(X=x_i)=1$}.

\end{propriete}


\begin{remarque}
La notation \og $\displaystyle\sum_{i=1}^{n}\dots\dots$ \fg signifie \og somme pour $i$ allant de 1 à $n$ \fg.
\end{remarque}



\smallskip

\begin{exemple}[1] Déterminer la loi de la variable aléatoire $X$ de l'exemple précédent (somme de deux dés tétraèdriques).

\medskip
Les valeurs pouvant être prises par $X$ sont $x_1=2$, $x_2=3$, $x_3=4$, $x_4=5$,  
$x_5=6$, $x_6=7$, $x_7=8$.

Détaillons, par exemple, le calcul de $P(X=3)$ : 

$P(X=3)=P(\{(2\,;\,1);(1\,;\,2)\})=\dfrac{\strut2}{\strut16}$ car les 16 couples $(a\,;\,b)$ sont équiprobables. On procède de même pour les autres valeurs $x_i$. La loi de $X$ est donc :

\smallskip
\centerline{%
\(\begin{array}{|c|c|c|c|c|c|c|c|}\hline
\tvi\mbox{Valeurs de }X\,:\,x_i&2&3&4&5&6&7&8\\\hline
\tvi P(X=x_i)\,:\,p_i
&
\dfrac{\strut 1}{\strut 16}&\dfrac{2}{16}&\dfrac{3}{16}
&
\dfrac{4}{16}&\dfrac{3}{16}&\dfrac{2}{16}&\dfrac{1}{16}
\\\hline
\end{array}\)}
\smallskip

que l'on peut représenter par un graphique :

\centerline{%
\psset{unit=0.5}
\begin{pspicture}(-0.5,-1)(8.5,5.7)
\psaxes[labels=x]{->}(0,0)(-0.5,-0.5)(8.5,5)
\uput[0](9,0){valeurs}
\uput[180](0,4.7){probabilités}
{\psset{linewidth=0.2}
\psline(2,0)(2,1)
\psline(3,0)(3,2)
\psline(4,0)(4,3)
\psline(5,0)(5,4)
\psline(6,0)(6,3)
\psline(7,0)(7,2)
\psline(8,0)(8,1)}
\end{pspicture}
}




\medskip
Calculons maintenant la probabilité de l'événement \og $X$ est pair \fg donc de 
\og $X=2$ ou $X=4$ ou $X=6$ ou $X=8$ \fg.

$P((X=2)\union(X=4)\union(X=6)\union(X=8))=P(X=2)+P(X=4)+P(X=6)+P(X=8)=\dfrac{8}{16}=\dfrac{1}{2}$ car les événements  $\{X=2\}$,  $\{X=4\}$,  $\{X=6\}$ et  $\{X=8\}$ sont incompatibles deux à deux.
\end{exemple}

\newpage

\partie{Espérance}

\begin{definition}
L'\textcolor{red}{espérance} d'une variable aléatoire $X$ prenant $n$ valeurs $x_1$, $x_2$, \dots, $x_n$ est égale à 

\enc{\textcolor{red}{$\displaystyle 
E(X)
=
P(X=x_1) \times x_1 + 
P(X=x_2) \times x_2 + 
\dots +
P(X=x_n) \times x_n
=\sum_{i=1}^{n}p_ix_i
%=
%\sum_{i=1}^{n}P(X=x_i).x_i
$
}}

\smallskip
C'est, en probabilité, la moyenne des valeurs que prendrait $X$ 
si l'on répétait un grand nombre de fois l'expérience.
\end{definition}

\begin{exemple}[1]
Calculons-la dans le cas précédent :
\begin{align*}
E(X)&=
p_1\times x_1
+p_2\times x_2
+p_3\times x_3
+p_4\times x_4
+p_5\times x_5
+p_6\times x_6
+p_7\times x_7
\\
&=
2\times\dfrac{1}{16}
+3\times\dfrac{2}{16}
+4\times\dfrac{3}{16}
+5\times\dfrac{4}{16}
+6\times\dfrac{3}{16}
+7\times\dfrac{2}{16}
+8\times\dfrac{1}{16}
=\dfrac{80}{16}=5
\end{align*}
(sur beaucoup de lancers de deux dés, la somme moyenne des deux dés devrait être de 5).
\end{exemple}


\begin{exemple}
Un forain me propose ce jeu : je mise 1 € et le forain lance un dé à six faces une fois, s'il obtient un~5 alors il me donne 8 €, sinon j'ai perdu.

Ais-je intérêt à jouer ?

\underline{Réponse}

Soit $X$ le gain à l'issue d'une partie.

Alors la loi de $X$ est :

\smallskip
\centerline{%
\(\begin{array}{|c|c|c|c|c|c|c|}\hline
\tvi\mbox{Résultat du lancer du dé} &1&2&3&4&5&6\\\hline
\tvi\mbox{Gain }X &-1&-1&-1&-1&7&-1\\\hline
\tvi p_i
&
\dfrac{\strut 1}{\strut 6}&\dfrac{1}{6}&\dfrac{1}{6}
&
\dfrac{1}{6}&\dfrac{1}{6}&\dfrac{1}{6}
\\\hline
\end{array}\)}

donc
\begin{align*}
E(X)&=
-1\times\dfrac{1}{6}
+(-1)\times\dfrac{1}{6}
+(-1)\times\dfrac{1}{6}
+(-1)\times\dfrac{1}{6}
+7\times\dfrac{1}{6}
+(-1)\times\dfrac{1}{6}
=\dfrac{2}{6}=\dfrac{1}{3}.
\end{align*}

Mon gain moyen se montera à $\dfrac{1}{3}\ $€ soit environ 33 cents : ce gain moyen est positif (pour moi).

\end{exemple}


\begin{remarques}
\ding{235} l'espérance permet donc de savoir si un jeu est équitable (quand elle est nulle).

\ding{235} bien sûr, avec du bon sens nous pouvions trouver la réponse sans l'espérance mais d'autres exemples sont plus retords\dots

\ding{235} si je suis malchanceux, je peux perdre plus souvent que je gagne mais \emph{statistiquement} le jeu est plus intéressant pour moi que pour le forain.
\end{remarques}




%\begin{proprietes}%[espérance et constantes]
%Soit $k$ une constante réelle. On admet que :
%\lis Si $X$ ne prend qu'une valeur $k$ alors $E(X)=E(k)=k$.
%\lis $E(X+k)=E(X)+k$.
%\lis $E(kX)=kE(X)$.
%\end{proprietes}
%
%\begin{remarque}
%Soit $m=E(X)$ et la variable aléatoire $Y=X-m$ (définie par $y_i=x_i-m$). $m$ étant une constante (pour une variable aléatoire donnée),
%on a $E(Y)=E(X-m)=E(X)-m=0$. On dit que la variable $Y$ est {\bf centrée} 
%(car $E(Y)=0$).
%\end{remarque}


\newpage

\partie{Variance et écart type}


\begin{definition}
La \textcolor{red}{variance} de $X$ est égale à \encpoint{\textcolor{red}{$V(X)=E((X-E(X))^2)$}}
\end{definition}

\begin{exemple}[1]

Rappelons que $E(X)=5$.

Calculons d'abord les valeurs de la variable aléatoire $(X-E(X))^2$ puis son espérance :

\smallskip
\centerline{%
\(\begin{array}{|c|c|c|c|c|c|c|c|}\hline
\tvi\mbox{Valeurs de }X\,:\,x_i&2&3&4&5&6&7&8\\\hline
\tvi\mbox{Valeurs de }X-E(X)&-3&-2&-1&0&1&2&3\\\hline
\tvi\mbox{Valeurs de }(X-E(X))^{\strut2}\,:\,x'_i&9&4&1&0&1&4&9\\\hline
\tvi p_i
&
\dfrac{\strut 1}{\strut 16}&\dfrac{2}{16}&\dfrac{3}{16}
&
\dfrac{4}{16}&\dfrac{3}{16}&\dfrac{2}{16}&\dfrac{1}{16}
\\\hline
\end{array}\)}
\begin{align*}
E((X-E(X))^2)&=
p_1\times x'_1
+p_2\times x'_2
+p_3\times x'_3
+p_4\times x'_4
+p_5\times x'_5
+p_6\times x'_6
+p_7\times x'_7
\\
&=
9\times\dfrac{1}{16}
+4\times\dfrac{2}{16}
+1\times\dfrac{3}{16}
+0\times\dfrac{4}{16}
+1\times\dfrac{3}{16}
+4\times\dfrac{2}{16}
+9\times\dfrac{1}{16}
=\dfrac{40}{16}=2,5
\end{align*}
donc $V(X)=2,5$.
\end{exemple}

\begin{propriete}
\enc{\textcolor{red}{$V(X)=E(X^2)-(E(X))^2$}}
\end{propriete}

\begin{remarques}
\ding{235} cette propriété, admise, est la formule de König-Huygens ; elle donne une façon plus simple de calculer la variance ;

\ding{235} la partie droite peut se lire ainsi : \og l'espérance du carré moins le carré de l'espérance \fg.
\end{remarques}


\begin{exemple}[1]

Pour l'exemple 1, la loi de $X^2$ est :

\smallskip
\centerline{%
\(\begin{array}{|c|c|c|c|c|c|c|c|}\hline
\tvi\mbox{Valeurs de }X^{\strut2}&4&9&16&25&36&49&64\\\hline
\tvi p_i
&
\dfrac{\strut 1}{\strut 16}&\dfrac{2}{16}&\dfrac{3}{16}
&
\dfrac{4}{16}&\dfrac{3}{16}&\dfrac{2}{16}&\dfrac{1}{16}
\\\hline
\end{array}\)}
\smallskip

donc 
\begin{align*}
V(X)&=
%p_1\times x_1^2 
%+p_2\times x_2^2
%+p_3\times x_3^2
%+p_4\times x_4^2
%+p_5\times x_5^2
%+p_6\times x_6^2
%+p_7\times x_7^2
%-(E(X))^2
%\\
%&=
4\times\dfrac{1}{16}
+9\times\dfrac{2}{16}
+16\times\dfrac{3}{16}
+25\times\dfrac{4}{16}
+36\times\dfrac{3}{16}
+49\times\dfrac{2}{16}
+64\times\dfrac{1}{16}
-5^2
\\
&=\dfrac{40}{16}=2,5.
\end{align*}
\end{exemple}


\begin{definition}
L'\textcolor{red}{écart-type} de $X$ est la racine carrée de sa variance :

\encpoint{\textcolor{red}{$\sigma(X)=\sqrt{V(X)}$}}
\end{definition}


\begin{exemple}[1]
Calcul de $\sigma(X)$ pour l'exemple 1 :

$V(X)=2,5$ donc $\sigma(X)=\sqrt{V(X)}=\sqrt{2,5}\env 1,581$.
\end{exemple}


\begin{remarque}
L'écart-type est un indicateur de la dispersion.

Par exemple, si j'appelle $X$ la moyenne annuelle d'un élève choisi au hasard dans une classe, alors plus $\sigma(X)$ sera petit, plus les résultats de la classe seront homogènes.
\end{remarque}



%\medskip
%\begin{proprietes}%[variance et constantes]
%Soit $k$ une constante réelle. On admet que :
%\lis Si $X$ ne prend qu'une valeur $k$ alors $V(X)=V(k)=0$ donc $§s(k)=0$.
%\lis $V(X+k)=V(X)$ donc $§s(X+k)=§s(X)$.
%\lis $V(kX)=k^2V(X)$ donc $§s(kX)=|k|§s(X)$.
%\end{proprietes}
%
%\begin{remarque}
%Ces propriétés sont intuitives sachant que $§s(X)$ indique la dispersion
%des valeurs de $X$ (si $X$ est constante, pas de dispersion, etc.).
%\end{remarque}
%
%\medskip
%\begin{exemple} Considérons une variable aléatoire $X$ d'espérance 3 et d'écart-type 2. Soit alors $Z$ la variable aléatoire définie par $Z=\dfrac{X-3}{2}$. Nous avons alors :
%
%$\begin{array}{rcll}
%E(Z)&
%=&
%\dfrac{1}{\strut 2}\fois E(X-3)
%&
%\mbox{car }E(kX)=kE(X)\\
%&=&
%\dfrac{1}{\strut 2}\fois (E(X)-3)
%&
%\mbox{car }E(X+k)=E(X)+k\\
%&=&0.
%\end{array}$
%
%et 
%
%$\begin{array}{rcll}
%§s(Z)&
%=&
%\left|\dfrac{1}{\strut 2}\right| §s(X-3) &
%\mbox{car }§s(kX)=|k|§s(X)\\
%&=&\dfrac{\strut 1}{\strut 2}.§s(X-3)&
%\\
%&=&\dfrac{1}{\strut 2}.§s(X)&
%\mbox{car }§s(X+k)=§s(X)\\
%&=&1.
%\end{array}$
%
%Quand $E(Z)=0$ et $§s(Z)=1$ on dit que $Z$ est {\bf centrée réduite}.
%
%On démontre de façon générale que si l'on définit $Z$ par $Z=\dfrac{X-E(X)}{§s(X)}$ alors $Z$ 
%est centrée réduite.
%\end{exemple}
%
%\begin{remarque}
%Nous verrons des applications de cette propriété dans le
%paragraphe concernant la loi normale.
%\end{remarque}

\end{document}
