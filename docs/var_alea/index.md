# I - Rappels sur les probabilités
## Événements
* Une <span style="color:red;">éventualité</span> est un résultat d'une expérience aléatoire.
* L'ensemble de toutes les éventualités est l'<span style="color:red;">univers</span> $\Omega$.
* Un <span style="color:red;">événement</span> est un ensemble d'éventualités, donc une partie de l'univers.
* L'<span style="color:red;">intersection</span> des événements $A$ et $B$, notée <span style="color:red;">$A\cap B$</span> (« $A$ inter $B$ ») est l'ensemble des éventualités qui se trouvent dans $A$ __et__ dans $B$.
* La <span style="color:red;">réunion</span> des événements $A$ et $B$, notée <span style="color:red;">$A\,\cup\,B$</span> (« $A$ union $B$ »), est l'ensemble des éventualités qui sont dans $A$ __ou__ dans $B$ (ou dans les deux).

![](images/inter.png){ width=25%;}
![](images/union.png){ width=25%;}

## Probabilités

* Si $A$ est un ensemble fini alors la <span style="color:red;">probabilité</span> de l'événement $A$, notée <span style="color:red;">$P(A)$</span>, est la somme des probabilités des éventualités contenues dans $A$.
* Dans un cas d'<span style="color:red;">équiprobabilité</span>, la probabilité d'un événement A est :

    <div style="color:red;">

    \[
    \boxed{P(A) =\dfrac{\text{nombre d'éventualités dans }A}{\text{nombre total d'éventualités}}}
    \]

    </div>

    (nombre de cas favorables / nombre de cas possibles).

* Propriétés indispensables :

    <div style="color:red;">

    \[
    \boxed{0\leqslant P(A)\leqslant 1} \qquad \boxed{P(A\cup B)=P(A)+P(B)-P(A\cap B)} \qquad\boxed{P\left(\bar{A}\right)=1-P(A)}
    \]

    </div>

## Probabilités conditionnelles
* La probabilité que $B$ se réalise sachant que $A$ est réalisé est :  

    <div style="color:red;">

    \[
    \boxed{P_A(B)=\dfrac{P(B\cap A)}{P(A)} = \dfrac{P(A\cap B)}{P(A)}}
    \]

    </div>

    $P_A(B)$ se lit « probabilité de $B$ sachant $A$ », c'est une <span style="color:red;">probabilité conditionnelle</span>.

* Calcul de $P(A\cap B)$ : <span style="color:red">$\boxed{P(A\cap B)=P(A)\times P_A(B)=P(B)\times P_B(A)}$</span>
* Formule des probabilités totales : si $A_1$, $A_2$, \dots $A_n$ forment une partition de l'univers alors 

    <div style="color:red;">

    \[
    \boxed{P(B) = P(A_1\cap B) + P(A_2\cap B) + \dots + P(A_n\cap B) = P(A_1) \times P_{A_1}(B) + P(A_2) \times P_{A_2}(B) + \dots + P(A_n) \times P_{A_n}(B)}.
    \]

    </div>