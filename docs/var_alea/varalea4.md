# V - Variance et écart type

!!! abstract "Définition V.1"
    La <span style="color:red">variance</span> de $X$ est égale à :
    
    <div style="color:red">

    \[\boxed{V(X)=E((X-E(X))^2)}\]

    </div> 
    

!!! example "Exemple V.1"
    Reprenons l'exemple II.1 (somme de deux dés tétraèdriques).
    Rappelons que $E(X)=5$.

    Calculons d'abord les valeurs de la variable aléatoire $(X-E(X))^2$ puis son espérance :

    
    \[\begin{array}{|c|c|c|c|c|c|c|c|}\hline
    \mbox{Valeurs de }X\,:\,x_i&2&3&4&5&6&7&8\\\hline
    \mbox{Valeurs de }X-E(X)&-3&-2&-1&0&1&2&3\\\hline
    \mbox{Valeurs de }(X-E(X))^{\strut2}\,:\,x'_i&9&4&1&0&1&4&9\\\hline
    p_i
    &
    \dfrac{\strut 1}{\strut 16}&\dfrac{2}{16}&\dfrac{3}{16}
    &
    \dfrac{4}{16}&\dfrac{3}{16}&\dfrac{2}{16}&\dfrac{1}{16}
    \\\hline
    \end{array}\]

    \(\begin{align*}
    E((X-E(X))^2)&=
    p_1\times x'_1
    +p_2\times x'_2
    +p_3\times x'_3
    +p_4\times x'_4
    +p_5\times x'_5
    +p_6\times x'_6
    +p_7\times x'_7
    \\
    &=
    9\times\dfrac{1}{16}
    +4\times\dfrac{2}{16}
    +1\times\dfrac{3}{16}
    +0\times\dfrac{4}{16}
    +1\times\dfrac{3}{16}
    +4\times\dfrac{2}{16}
    +9\times\dfrac{1}{16}
    =\dfrac{40}{16}=2,5
    \end{align*}\)

    donc $V(X)=2,5$.

!!! abstract "Propriété V.1"
    <div style="color:red">

    \[\boxed{V(X)=E(X^2)-(E(X))^2}\]

    </div> 



!!! tip "Remarques"
    * cette propriété, admise, est la formule de König-Huygens ; elle donne une façon plus simple de calculer la variance ;
    * la partie droite peut se lire ainsi : « l'espérance du carré moins le carré de l'espérance ».


!!! example "Exemple V.2"
    Toujours pour l'exemple II.1, la loi de $X^2$ est :

    \[\begin{array}{|c|c|c|c|c|c|c|c|}\hline
    \mbox{Valeurs de }X^{\strut2}&4&9&16&25&36&49&64\\\hline
    p_i
    &
    \dfrac{\strut 1}{\strut 16}&\dfrac{2}{16}&\dfrac{3}{16}
    &
    \dfrac{4}{16}&\dfrac{3}{16}&\dfrac{2}{16}&\dfrac{1}{16}
    \\\hline
    \end{array}\]

    donc  
    \(\begin{align*}
    V(X)&=
    %p_1\times x_1^2 
    %+p_2\times x_2^2
    %+p_3\times x_3^2
    %+p_4\times x_4^2
    %+p_5\times x_5^2
    %+p_6\times x_6^2
    %+p_7\times x_7^2
    %-(E(X))^2
    %\\
    %&=
    4\times\dfrac{1}{16}
    +9\times\dfrac{2}{16}
    +16\times\dfrac{3}{16}
    +25\times\dfrac{4}{16}
    +36\times\dfrac{3}{16}
    +49\times\dfrac{2}{16}
    +64\times\dfrac{1}{16}
    -5^2
    \\
    &=\dfrac{40}{16}=2,5.
    \end{align*}\)


!!! abstract "Définition V.2"
    L'<span style="color:red">écart-type</span> de $X$ est la racine carrée de sa variance :

    <div style="color:red">

    \[
    \boxed{\sigma(X)=\sqrt{V(X)}}
    \]

    </div>


!!! example "Exemple V.3"
    Calcul de $\sigma(X)$ pour l'exemple II.1 :

    $V(X)=2,5$ donc $\sigma(X)=\sqrt{V(X)}=\sqrt{2,5}\simeq 1,581$.

!!! tip "Remarque"
    L'écart-type est un indicateur de la dispersion.

    Par exemple, si j'appelle $X$ la moyenne annuelle d'un élève choisi au hasard dans une classe, alors plus $\sigma(X)$ sera petit, plus les résultats de la classe seront homogènes.


!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    26, 27 page 283  
    68 page 288  
    76 page 289


