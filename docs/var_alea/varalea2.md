# III - Loi d'une variable aléatoire

<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>


!!! abstract "Définition III.1"
    La <span style="color:red">loi d'une variable aléatoire</span> discrète $X$ est l'ensemble des couples ($x_i$ ; $P(X=x_i)$) où les $x_i$ sont les valeurs pouvant être prises par $X$.

    Une loi de variable aléatoire peut être représentée par un tableau :

    \(\begin{array}{|c|c|c|c|c|}\hline
    \text{Valeur }x_i&x_1&x_2&\dots\dots &x_n\\\hline
    P(X=x_i)&P(X=x_1)&P(X=x_2)&\dots\dots&P(X=x_n)
    \\\hline
    \end{array}\)

!!! tip "Remarque"
    La notion de loi a déjà été vue en classe de Seconde mais il s'agissait d'une loi de probabilité, où l'on associait à chaque éventualité une probabilité.

!!! abstract "Propriété III.1"
    Si $X$ peut prendre les valeurs $x_1$, $x_2$, ... , $x_n$ alors $\boxed{\displaystyle\sum_{i=1}^{n}P(X=x_i)=1}$.

!!! tip "Remarque"
    La notation « $\displaystyle\sum_{i=1}^{n}\dots\dots$ » signifie « somme pour $i$ allant de 1 à $n$ ».

!!! example "Exemple III.1"
    Déterminer la loi de la variable aléatoire $X$ de l'exemple précédent II.1 (somme de deux dés tétraèdriques).

    Les valeurs pouvant être prises par $X$ sont $x_1=2$, $x_2=3$, $x_3=4$, $x_4=5$, $x_5=6$, $x_6=7$, $x_7=8$.

    Détaillons, par exemple, le calcul de $P(X=3)$ : 

    $P(X=3)=P(\{(2\,;\,1);(1\,;\,2)\})=\dfrac{\strut2}{\strut16}$ car les 16 couples $(a\,;\,b)$ sont équiprobables. On procède de même pour les autres valeurs $x_i$. La loi de $X$ est donc :

    \[\begin{array}{|c|c|c|c|c|c|c|c|}\hline
    \text{Valeurs de }X\,:\,x_i&2&3&4&5&6&7&8\\\hline
    P(X=x_i)\,:\,p_i
    &
    \dfrac{\strut 1}{\strut 16}&\dfrac{2}{16}&\dfrac{3}{16}
    &
    \dfrac{4}{16}&\dfrac{3}{16}&\dfrac{2}{16}&\dfrac{1}{16}
    \\\hline
    \end{array}\]


    que l'on peut représenter par un graphique :

    <div class="chart-container" style="height:40vh; width:80vw; text-align: center;">
    <canvas id="myChart"></canvas>
    </div>

    Calculons maintenant la probabilité de l'événement « $X$ est pair » donc de « $X=2$ ou $X=4$ ou $X=6$ ou $X=8$ ».

    $P((X=2)\cup(X=4)\cup(X=6)\cup(X=8))=P(X=2)+P(X=4)+P(X=6)+P(X=8)=\dfrac{8}{16}=\dfrac{1}{2}$ car les événements  $\{X=2\}$,  $\{X=4\}$,  $\{X=6\}$ et  $\{X=8\}$ sont incompatibles deux à deux.



!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    50, 52 page 285  
    18 page 282  
    64 page 287  
    58 page 285  
    65 page 287  
    62 page 286  
    60, 63 page 286



<script>
  const data = [
    { val: 2, proba: 1/16 },
    { val: 3, proba: 2/16 },
    { val: 4, proba: 3/16 },
    { val: 5, proba: 4/16 },
    { val: 6, proba: 3/16 },
    { val: 7, proba: 2/16 },
    { val: 8, proba: 1/16 },
  ];

  new Chart(
    document.getElementById('myChart'),
    {
      type: 'bar',
      data: {
        labels: data.map(row => row.val),
        datasets: [
          {
            label: 'probabilité',
            data: data.map(row => row.proba)
          }
        ]
      }
    }
  );
</script>