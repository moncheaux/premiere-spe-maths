def somme(N):
    S = 0
    for n in range(N+1):
        S = S + n
    return S

print('1+2+...+7=', somme(7))
print('1+2+...+50=', somme(50))
print('1+2+...+1000=', somme(1000))
