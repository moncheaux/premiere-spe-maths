# Des vidéos d'explications
## Les suites
* [cours complet sur les suites (Lumni)](https://www.lumni.fr/video/les-suites-numeriques) ;
* [même chose](https://www.youtube.com/watch?v=Y5Z7RitDNvM) ;
* [une fiche de révision](https://www.youtube.com/watch?v=rdSxmqatY1g)

## Sommes de termes de suites
* [exercices : calcul de termes de suites arithmétiques](https://www.youtube.com/watch?v=XiKVQa8BWko) ;
* [exercices : calcul de termes de suites géométriques](https://www.youtube.com/watch?v=OUFkINbcHrI) ;

## Utilisation d'outils logiciels
* [étude de suite géométrique avec un algorithme](https://www.youtube.com/watch?v=ejdcrblFRsk) ;
* [étude de suite géométrique avec un tableur](https://www.youtube.com/watch?v=SjJZn_uCybU) ;
* [calcul de termes d'une suite définie par récurrence avec Python](https://www.youtube.com/watch?v=EYPDAe9T3BA&list=PLVUDmbpupCaoqExMkHrhYvWi4dHnApgG_&index=29) ;
* [algorithme de seuil avec Python sur Numworks](https://www.youtube.com/watch?v=vJmpzwhaka8&list=PLVUDmbpupCaoqExMkHrhYvWi4dHnApgG_&index=33)
* [algorithme de seuil avec Python sur Numworks](https://www.youtube.com/watch?v=4pf7bQkb46k&list=PLVUDmbpupCaoqExMkHrhYvWi4dHnApgG_&index=32)

## Variations de suites
* [étude d'une suite du second degré avec la méthode de la différence](https://www.youtube.com/watch?v=DFz8LDKCw9Y) ;
* [étude d'une suite avec la méthode du rapport](https://www.youtube.com/watch?v=R8a60pQwiOQ)
* [étude d'une suite avec la méthode de la dérivation](https://www.youtube.com/watch?v=dPR3GyQycH0&list=PLVUDmbpupCaoqExMkHrhYvWi4dHnApgG_&index=21) ;
* [utilisation de deux méthodes](https://www.youtube.com/watch?v=YVCqdK3hBLg&list=PLVUDmbpupCaoqExMkHrhYvWi4dHnApgG_&index=22)

## Limites de suites
* [comprendre la notion de limite d'une suite sur trois exemples](https://www.youtube.com/watch?v=0CC-EqOH92c) ;
* [un exemple](https://www.youtube.com/watch?v=NB15JWlzeW0) ;
* [comment conjecturer la limite d'une suite avec différentes méthodes](https://www.youtube.com/watch?v=Z-EspJlOlGc)


