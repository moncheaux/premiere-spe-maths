# ⁉️ QCM
!!! bug "Attention"
    Il n'y a qu'une bonne réponse correcte à chaque question.

#### Q1 - La suite définie sur ℕ par \(u_n=n-5\) est :
{{ qcm(["croissante", "décroissante", "croissante à partir d'un certain rang", "décroissante à partir d'un certain rang", "autre réponse"], [1], shuffle = False) }}

#### Q2 - La suite définie sur ℕ par \(u_{n+1}=u_n-5\) et \(u_0=6\) est :
{{ qcm(["croissante", "décroissante", "croissante à partir d'un certain rang", "décroissante à partir d'un certain rang", "autre réponse"], [2], shuffle = False) }}

#### Q3 - La suite définie sur ℕ par \(u_n=n^2-10\,n+4\) est :
{{ qcm(["croissante", "décroissante", "croissante à partir d'un certain rang", "décroissante à partir d'un certain rang", "autre réponse"], [3], shuffle = False) }}

#### Q4 - La suite définie sur ℕ par \(u_n=\dfrac{n+1}{n+3}\) est :
{{ qcm(["croissante", "décroissante", "croissante à partir d'un certain rang", "décroissante à partir d'un certain rang", "autre réponse"], [1], shuffle = False) }}

#### Q5 - La suite définie sur ℕ par \(u_{n+1}=u_n-2\,n+8\) et \(u_0=-4\) est :
{{ qcm(["croissante", "décroissante", "croissante à partir d'un certain rang", "décroissante à partir d'un certain rang", "autre réponse"], [4], shuffle = False) }}

#### Q6 - \(1+2+3+\dots+60=\)
{{ qcm(["1830", "66", "1770", "12360", "1789", "autre réponse"], [1], shuffle = True) }}

#### Q7 - \(1+4+16+\dots+4^{20} \simeq \)
{{ qcm(["$1,47\\times10^{12}$", "$4,4\\times10^{12}$", "$1,1\\times10^{12}$", "$3,665\\times10^{11}$", "autre réponse"], [1], shuffle = True) }}

#### Q8 - \(30+35+40+\dots+280=\)
{{ qcm(["7905", "7875", "38905", "38875", "", "autre réponse"], [1], shuffle = True) }}

#### Q9 - \(3+6+12+\dots+6144 = \)
{{ qcm(["12285", "6141", "797160", "265719", "autre réponse"], [1], shuffle = True) }}

#### Q10 - Après avoir recopié, complété et exécuté le programme ci-dessous, donnez la valeur de \(S=59+60+61+62+\dots+365\) :

```python
S = ...
for n in range(...):
    S = ... + 59 + ...

print('Résultat :', ...)
```

{{ IDEv() }}

{{ qcm(["65084", "18420", "18360", "64719", "46322", "autre réponse"], [1], shuffle = True) }}

#### Q10 - Après avoir recopié, complété et exécuté le programme ci-dessous, donnez la valeur de la somme des carrés des entiers, ces carrés devant être inférieurs à 100000 : \(S=1+4+9+\dots\) :

```python
S = 0
n = 1
while n......:
    S = ... + ...
    n = ... + ...
print('Résultat :', ...)
```

{{ IDEv() }}

{{ qcm(["50086", "317", "316", "4999950000", "6420120", "autre réponse"], [1], shuffle = True) }}


#### Q12 - La limite de \(5\,n+\dfrac{1}{n^2}\) est :
{{ qcm(["$+\infty$", "$-\infty$", "0", "un nombre fini non nul", "il n'y a pas de limite"], [1], shuffle = False) }}

#### Q13 - La limite de \(4-\dfrac{5}{2^n}\) est :
{{ qcm(["$+\infty$", "$-\infty$", "0", "un nombre fini non nul", "il n'y a pas de limite"], [4], shuffle = False) }}

#### Q14 - La limite de la suite arithmétique de premier terme \(u_0=-2\) et de raison 0,1 est :
{{ qcm(["$+\infty$", "$-\infty$", "0", "un nombre fini non nul", "il n'y a pas de limite"], [1], shuffle = False) }}

#### Q15 - La limite de la suite arithmétique de premier terme \(u_0=-2\) et de raison -0,4 est :
{{ qcm(["$+\infty$", "$-\infty$", "0", "un nombre fini non nul", "il n'y a pas de limite"], [2], shuffle = False) }}

#### Q16 - La limite de la suite géométrique de premier terme \(u_0=-2\) et de raison 0,1 est :
{{ qcm(["$+\infty$", "$-\infty$", "0", "un nombre fini non nul", "il n'y a pas de limite"], [3], shuffle = False) }}

#### Q17 - La limite de la suite géométrique de premier terme \(u_0=-2\) et de raison 3 est :
{{ qcm(["$+\infty$", "$-\infty$", "0", "un nombre fini non nul", "il n'y a pas de limite"], [2], shuffle = False) }}

#### Q18 - La limite de la suite géométrique de premier terme \(u_0=0,5\) et de raison \(-2\) est :
{{ qcm(["$+\infty$", "$-\infty$", "0", "un nombre fini non nul", "il n'y a pas de limite"], [5], shuffle = False) }}

#### Q19 - La limite de la suite géométrique de premier terme \(u_0=0,5\) et de raison 2 est :
{{ qcm(["$+\infty$", "$-\infty$", "0", "un nombre fini non nul", "il n'y a pas de limite"], [1], shuffle = False) }}
