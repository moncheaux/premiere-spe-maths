# Quelques exercices pour vous entraîner
Les valeurs utilisées dans les exercices du site Wims sont générées aléatoirement, vous pouvez donc refaire plusieurs fois chacun d'entre eux.

## Somme de termes de suites

* [somme des premiers termes d'une suite arithmétique](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/oefseq.fr&cmd=new&exo=sommearith) ;
* [somme des premiers termes d'une suite géométrique](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/oefseq.fr&cmd=new&exo=sommegeo) ;
* [somme de termes de suites](https://wims.univ-cotedazur.fr/wims/wims.cgi?session=Q702F77889.12&lang=fr&cmd=new&module=H4%2Falgebra%2Foefsuites1S.fr&exo=sommeterme&qnum=1&scoredelay=&seedrepeat=0&qcmlevel=1&special_parm2=&special_parm4=) ;
* [calculs de sommes](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/oefsequence.fr&cmd=new&exo=calcul_somme_geo) ;
* [distance totale parcourue pendant plusieurs semaines d'entraînement](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H6/analysis/numericseq.fr&cmd=new&exo=formulesomme) ;
* [nombre de places dans une arène](https://learningapps.org/12819985) ;
* [construire un escalier double](https://learningapps.org/33979851) ;
* [remboursement d'un four à poterie](https://learningapps.org/13206254) ;

## Variation de suites
* [trouver le sens de variation d'une suite](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H4/algebra/oefsuites1S.fr&cmd=new&exo=sensvarsuite) ;

## Limites de suites
* [trouver (conjecturer) des limites](https://learningapps.org/21182573) ;
* [classer des suites en fonction de leur convergence ou divergence](https://learningapps.org/5676195)
* [trouver si une suite a une limite et la donner](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H4/algebra/oefsuites1S.fr&cmd=new&exo=limfrac1) ;
* [trouver des limites parmi des propositions](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/oefsequence.fr&cmd=new&exo=limite_suite) ;
* [idem avec des suites arithmético-géométriques](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/oefsequence.fr&cmd=new&exo=limite_suite_arithmetico_geometrique) ;
* [somme de termes consécutifs d'une suite géométrique, limite](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/oefsequence.fr&cmd=new&exo=somme_de_terme_consecutif_geometrique_et_limite) ;
* [variation et limite d'une suite géométrique](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/oefsequence.fr&cmd=new&exo=variation_et_limite_suite_geometrique)

## Algorithmes et suites
* [collection d'exercices](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/algo/oefsequence.fr)
