# II. Variations d'une suite
## II.1 Suites monotones

!!! abstract "Définitions"

    Soit $n_0$ un entier. La suite ($u_n$) est :

    * <span style='color:red'>croissante</span> (respectivement strictement croissante) à partir de $n_0$ si et seulement si pour tout $n\ge n_0$, on a $u_{n+1}\geqslant u_n$ (resp. $u_{n+1}> u_n$) 
    * <span style='color:red'>décroissante</span> (resp. strictement décroissante) à partir de $n_0$ si et seulement si pour tout $n\ge n_0$, on a $u_{n+1}\leqslant u_n$ (resp. $u_{n+1}< u_n$) 
    * <span style='color:red'>constante</span> à partir de $n_0$ si et seulement si pour tout $n\ge n_0$, on a $u_{n+1}=u_n$
    * <span style='color:red'>monotone</span> si elle est soit croissante, soit décroissante (strictement monotone si elle est strictement croisante ou strictement décroissante).


!!! example "Exemple II.1"
    Soit $(u_n)$  définie par $u_n=n^2-5\,n$ pour $n\geqslant 0$. 

    Un tableau de valeurs donne :

    \[\begin{array}{c|c|c|c|c|c|c|c|c|c|c|c|c|c}
    n &0 &1 &2 &3 &4 &5 &6 &7 &8 & 9& 10 &11 &12
    \\\hline
    u_n &0 &-4 &-6 &-6 &-4 &0 &6 & 14 &24& 36& 50& 66 &84
    \end{array}\]

    ![](../images/suite_parab.png){width=50%; : .center}

    Il semblerait que $(u_n)$ soit croissante à partir du rang $n=2$ (et strictement croissante à partir du rang $n=3$).

    Nous verrons dans la suite deux preuves de cette conjecture.


## II.2 Étude des variations
Nous disposons de trois techniques, à utiliser suivant la suite que l'on étudie.

!!! abstract "Technique 1 : Signe de la différence de deux termes consécutifs"
    La technique la plus universelle consiste à étudier le signe de $u_{n+1} - u_n$. En effet :

    * $(u_n)$ est croissante à partir de $n_0$ si et seulement si pour tout <b>$n\geqslant n_0$</b>, $u_{n+1}- u_n\geqslant0$ ;
    * $(u_n)$ est décroissante à partir de $n_0$ si et seulement si pour tout $n\geqslant n_0$, $u_{n+1} - u_n\leqslant0$.


!!! example "Exemple II.2"
    === "Question"
        Étudier les variations de la suite $(u_n)$ définie par $u_n=n^2-5\,n$ pour $n\geqslant 0$.

    === "Réponse"

        \[u_{n+1}- u_n = (n+1)^2-5(n+1) - (n^2-5\,n) = 2\,n-4\]

        donc

        \[u_{n+1}- u_n \geqslant 0 \iff 2\,n-4\geqslant0\iff n\geqslant 2.\]

        La suite $(u_n)$ est donc bien croissante à partir du rang $n=2$.


!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    134, 135, 136, 137, 138 page 55



!!! abstract "Technique 2 : Comparaison du quotient de deux termes consécutifs avec 1"
    __Dans le cas où tous les termes de la suite sont strictement positifs__, il est possible de calculer $\dfrac{u_{n+1}}{u_{n}}$ et de comparer le résultat à 1.

    En effet, pour les suites à termes strictement positifs,
    $u_{n+1} \geqslant u_{n} \iff \dfrac{u_{n+1}}{u_{n}} \geqslant \dfrac{u_{n}}{u_{n}} \iff \dfrac{u_{n+1}}{\strut u_{n}} \geqslant 1$.

    Notez bien que l'inégalité ne change pas de sens quand je divise par $u_n$ car $u_n>0$ (et que je peux faire cette division car $u_n\neq 0$).


!!!tip "Remarque"
    Cette technique s'applique bien aux suites comportant des puissances.

!!! example "Exemple II.3"
    === "Question"
        Étudier les variations de la suite $(u_n)$ définie par $u_n=\dfrac{2^n}{n}$ pour $n\geqslant 1$.

    === "Réponse"
        Si $u_n=\dfrac{2^n}{n}$ pour $n\geqslant 1$ alors $u_n$ reste positif et 

        \[\dfrac{u_{n+1}}{u_{n}} =
        \dfrac{\frac{2^{n+1}}{n+1}}{\frac{2^n}{n}} =\dfrac{2^{n+1}}{n+1} \times \dfrac{n}{2^n} = \dfrac{2^{n+1}}{2^n} \times \dfrac{n}{n+1} = \dfrac{2\,n}{n+1}\]

        donc

        \[\dfrac{u_{n+1}}{u_{n}} \geqslant 1
        \iff
        \dfrac{2\,n}{n+1} \geqslant 1
        \iff 2\,n\ge n+1\iff n\geqslant 1\]
        
        donc $(u_n)$ est croissante (à partir du rang 1).


!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    140 page 55

!!! abstract "Technique 3 : Étude des variations d'une fonction"

    Si $u_n=f(n)$ alors nous pouvons étudier les variations de $f$ sur $[0;+\infty[$.

    Par exemple, si $f$ est croissante sur $[0;+\infty[$ alors ($u_n$) l'est aussi.

!!! example "Exemple II.4"
    === "Question"
        Étudier les variations de la suite $(u_n)$  définie par $u_n=n^2-5\,n$ pour $n\geqslant 0$.

    === "Réponse"
        La valeur de $u_n$ est écrite en fonction de $n$ (expression explicite du terme général de la suite) c'est-à-dire $u_n=f(n)$ où $f$ peut être définie sur $[0;+\infty[$ par $f(x)=x^2-5\,x$.

        J'étudie donc les variations de $f$ :

        $f^\prime(x) = 2\,x-5$ donc 
        $f^\prime(x) >0 \iff 2\,x-5>0 \iff x > \dfrac{5}{\strut2}$.

        La fonction $f$ est donc strictement croissante sur $\left[\dfrac{5}{2};+\infty\right[$
        et la suite $(u_n)$ est strictement croissante à partir du rang 3.


!!!tip "Remarques"
    * Cette troisième technique ne s'applique qu'aux suites définies explicitement, pas aux suites définies uniquement par récurrence.
    * Pour les suites définies par récurrence, par exemple par $u_{n+1}=f(u_n)$, il est possible que $f$ soit croissante sans que ($u_n$) le soit.
    En fait, si $f$ est croissante, pour que $u_{n+1}\geqslant u_n$, il faudrait que $u_n\geqslant u_{n-1}$, donc que $u_{n-1}\geqslant u_{n-2}$ etc. et donc que $u_1\geqslant u_0$ ce qui n'est pas forcément le cas.

!!! example "Exemple II.5"
    Soit ($u_n$) la suite définie par $u_0=2$ et $u_{n+1}=u_n-3$. Alors la fonction associée à la relation de récurrence (définie sur $[0;+\infty[$ par $f(x)=x-3$) est croissante mais la suite ($u_n$) est clairement décroissante.


???tip "Raisonnement par récurrence..."
    Pour étudier les variations de suites définies par récurrence, il faut souvent faire appel à un type de raisonnement logique appelé "raisonnement par récurrence" (au programme de la spécialité mathématiques de Terminale).

!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    Refaire le 137 page 55
    
