# III. Limites de suites (exemples)


!!! tip "Remarque"
    Nous ne ferons cette année que conjecturer les limites des suites, sans les prouver.

## III.1 Suites ayant une limite infinie

!!! example "Exemple III.1"
    Soit $(u_n)$ définie par $u_n=n^2-5\,n$ pour $n\geqslant0$. 

    Un tableau de valeurs donne :

    \[\begin{array}{c|c|c|c|c|c|c}
    n &0 &10 &100 &1000 &10000 &100000
    \\\hline
    u_n &0 &50 &9500 &995 000 &99 950 000 &9 999 500 000
    \end{array}
    \]

    ![](../images/suite_parab2.png){width=50%; : .center}

    Il semblerait que les valeurs de $(u_n)$ dépassent n'importe quel seuil à partir d'un certain rang.

    Par exemple, un tableau de valeurs plus précis suggère que tous les termes de la suite dépasseront un milliard à partir du rang $n=31626$, ce qui peut être prouvé en résolvant une inéquation du second degré.

    Nous dirons que la suite $(u_n)$ <span style='color:red'>tend vers</span> $+\infty$ ou que sa <span style='color:red'>limite</span> est $+\infty$.

    Ceci peut s'écrire ainsi : <span style='color:red'>$\lim\limits_{n \to+\infty}u_n$</span>$=+\infty$.


!!! example "Exemple III.2"
    Soit $(u_n)$ définie par $u_n=5-8\,n$ pour $n\ge0$. 

    Un tableau de valeurs donne :

    \[\begin{array}{c|c|c|c|c|c|c}
    n &0 &10 &100 &1000 &10000 &100000
    \\\hline
    u_n &0 &-75 &-795 &-7995 &-79995 &-799995
    \end{array}\]

    ![](../images/suite_droite.png){width=50%; : .center}

    Il semblerait donc que la suite $(u_n)$ tende vers $-\infty$ donc que sa limite soit $-\infty$. 

    Ceci peut s'écrire ainsi : $\lim\limits_{n \to+\infty}u_n=-\infty$.

!!! tip "Remarque"
    Dans le cas des suites, nous pourrions juste écrire $\lim u_n$ ou $\lim(u_n)$, sans préciser que $n$ tend vers $+\infty$.

    Cependant vous verrez en Terminale les limites de fonctions, dans ce cas, des écritures telles que 
    $\lim\limits_{x \to-\infty}f(x)$ ou $\lim\limits_{x \to-3}f(x)$
    pourront être utilisées.


### III.2 Suites ayant une limite finie

!!! example "Exemple III.3"
    Soit $(u_n)$ définie par $u_n=3-\dfrac{1}{n}$ pour $n\ge1$. 

    Un tableau de valeurs donne :

    \[\begin{array}{c|c|c|c|c|c|c}
    n &1 &10 &100 &1000 &10000 &100000
    \\\hline
    u_n &2 &2,9 &2,99 &2,999 &2,9999 &2,99999
    \end{array}\]

    ![](../images/suite_hyperb.png){width=50%; : .center}

    Il semblerait ici que les valeurs de $(u_n)$ se rapprochent de 3 quand $n$ tend vers l'infini, autrement dit que la limite de $(u_n)$ est 3.

    Ceci peut s'écrire ainsi : $\lim\limits_{n \to+\infty}u_n=3$.


## III.3 Suites n'ayant pas de limite

!!! example "Exemple III.4"
    Soit $(u_n)$ définie par $u_n=(-1)^{n}$ pour $n\in ℕ$.

    Un tableau de valeurs donne :

    \[\begin{array}{c|c|c|c|c|c|c}
    n &0 &1 &2 &3 &4 &5
    \\\hline
    u_n &1 &-1 &1 &-1 &1 &-1
    \end{array}\]

    ![](../images/suite_alt.png){width=50%; : .center}

    Les valeurs de $(u_n)$ alternent entre $-1$ et 1 donc elle ne se rapprochent ni d'un nombre, ni de l'infini.

    Nous dirons que la suite $(u_n)$ n'a pas de limite.


!!! tip "Remarques"
    * des suites telles que $(\cos n)$ ou $(\sin n)$ n'ont pas non plus de limite ;
    * une suite qui a une limite finie est dite __convergente__ ;
    * une suite non convergente (pas de limite ou une limite infinie) est dite __divergente__.

    ![](../images/suite_cos.png){width=50%; : .center}


## III.4 Cas des suites arithmétiques

!!! example "Exemple III.5"
    Soit $(u_n)$ la suite arithmétique de premier terme $u_0=-700$ et de raison 5.

    Pour passer d'un terme à son suivant, il faut ajouter 5 : la suite aura pour limite $+\infty$.

    Plus précisement,
    $u_n = u_0+n\,r = 5\,n-700$.  
    Si nous voulons que, par exemple, $u_n > 1 000 000$, il suffit que 
    $5\,n-700 > 1 000 000$ donc que $n>\dfrac{1000000+700}{5}$, ce qui se produit à partir du rang $n=200141$.


!!! tip "Remarque"
    Les suites arithmétiques ont toujours pour limite soit $+\infty$, soit $-\infty$, suivant le signe de $r$.

    Seules exceptions : les suites arithmétiques de raison $r=0$ !


## III.5 Cas des suites géométriques

!!! example "Exemple III.6"
    Soit $(u_n)$ la suite géométrique de premier terme $u_0=700$ et de raison 0,1.

    Un tableau de valeurs donne :

    \[\begin{array}{c|c|c|c|c|c|c}
    n &0 &1 &2 &3 &10 &100
    \\\hline
    u_n &700 &70 &7 &0,7 &0,00000007 &0,000\dots00007
    \end{array}\]

    (la dernière valeur comporte 98 zéros).
    Il semblerait donc que $\lim\limits_{n \to+\infty}u_n=0$.


!!! example "Exemple III.7"
    Soit $(u_n)$ la suite géométrique de premier terme $u_0=700$ et de raison 2.

    Un tableau de valeurs donne :

    \[\begin{array}{c|c|c|c|c|c|c}
    n &0 &1 &2 &3 &10 &100
    \\\hline
    u_n &700 &1400 &2800 &5600 &716800 &8,87 \times 10^{\strut32}
    \end{array}\]

    (la dernière valeur est arrondie).
    Il semblerait donc que $\lim\limits_{n \to+\infty}u_n=+\infty$.


!!! example "Exemple III.8"
    Soit $(u_n)$ la suite géométrique de premier terme $u_0=700$ et de raison $-2$.

    Un tableau de valeurs donne :

    \[\begin{array}{c|c|c|c|c|c|c|c}
    n &0 &1 &2 &3 &10 &100&101
    \\\hline
    u_n &700 &-1400 &2800 &-5600 &716800 &8,87 \times 10^{32} &-1,77 \times 10^{\strut33}
    \end{array}\]

    (les dernières valeurs sont arrondies).
    Il semblerait donc que cette suite n'ait pas de limite.

!!! tip "Remarque"
    Pour une suite géométrique de premier terme $u_0\neq0$, il y a trois cas de figure :

    * Si $-1<q<1$ alors $\lim\limits_{n \to+\infty}q^n=0$ donc 
    $\lim\limits_{n \to+\infty}u_n=
    \lim\limits_{n \to+\infty}u_0\times  q^n
    =0$ ;
    * Si $q>1$ alors $\lim\limits_{n \to+\infty}q^n=+\infty$ donc 
    $\lim\limits_{n \to+\infty}u_n =
    \lim\limits_{n \to+\infty}u_0\times  q^n
    =\pm \infty$ (suivant le signe de $u_0$) ;
    * Si $q<-1$ alors $(u_n)$ n'a aucune limite (alternance des signes et valeur absolue qui augmente).

!!! example "Exemple III.9"
    Soit $(u_n)$ la suite géométrique de premier terme $u_0=-4$ et de raison 3.
    Alors $u_n = -4 \times 3^n$.

    Comme $q=3>1$, nous avons 
    $\lim\limits_{n \to+\infty}3^n=+ \infty$
    donc 
    $\lim\limits_{n \to+\infty}u_n=- \infty$.


!!! example "Exemple III.10"
    === "Question"
        Trouver la limite de la suite $(S_n)$ de sommes :
        
        \[S_n
        =
        1+\dfrac{1}{2}+\dfrac{1}{4}+\dfrac{1}{8}+\dfrac{1}{16}
        +\dots+\dfrac{1}{2^n}.\]

    === "Réponse"
        D'après la propriété I.2 :

        \[S_n = \dfrac{1-(1/2)^{n+1}}{1-1/2} = 2 \left(1-\left(\dfrac{1}{2}\right)^{n+1}\right).\]

        Comme $-1<\dfrac{1}{2}<1$, nous avons 
        $\lim\limits_{n \to+\infty}\left(\dfrac{1}{2}\right)^{n+1}=0$
        donc
        $\lim\limits_{n \to+\infty}S_n=2(1-0)=2$.

        Nous pourrions écrire :
        $1+\dfrac{1}{2}+\dfrac{1}{4}+\dfrac{1}{8}+\dfrac{1}{16}
        +\dots
        =2$.

!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    142 à 146 page 55