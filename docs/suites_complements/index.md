# I. Sommes de termes consécutifs de suites
## I.1 Sommes de termes consécutifs d'une suite arithmétique

!!! question "Une question pour commencer"
    === "Question"
        Quelle est la somme des entiers de 1 à 1000 ?
        Faire cette somme à la main (même aidé d'une calculatrice) prendrait trop de temps !

    === "Première possibilité"
        Trouver une formule mathématique permettant de calculer cela. Nous y reviendrons.

    === "Deuxième possibilité"
        Utiliser un tableur. Ça ira plus vite mais que se passe-t-il si nous voulons la somme des entiers de 1 à 10000000 ?

    === "Dernière possibilité"
        Utiliser un langage de programmation.

        Appelons $S$ cette somme : $S=1+2+3+\dots+1000$. Cette somme se calcule étape par étape :

        * étape 0 : $S$ vaut 0
        * étape 1 : $S$ vaut 1
        * étape 2 : $S$ vaut $1+2=3$
        * étape 3 : $S$ vaut $3+3=6$
        * étape 4 : $S$ vaut $6+4=10$
        * étape 5 : $S$ vaut $10+5=15$
        * etc.

        À chaque étape nous ajoutons à $S$ un entier $n$, __pour tout__ $n$ __allant de__ 1 à 1000.

!!! tip "Programmes en Python"

    Voici un premier programme, qui calcule cette somme :

    {{IDE('somme_entiers_1000', SIZE=7)}}

    Modifions le programme précédent pour en faire une fonction donnant la somme des entiers de 1 à un entier N quelconque (pas forcément 1000), que nous testons ensuite :

    {{IDE('somme_entiers')}}            

    Observons quelques résultats :

    N | S
    --- | ---
    7 | 28
    10 | 15
    50 | 1275
    100 | 5050
    1000 | 500500

    Quelle formule pouvez-vous conjecturer ?




!!! abstract "Propriété I.1"
    Pour tout $n\in ℕ^*$, <span style='color:red'>$\boxed{1+2+3+\dots+n=\dfrac{n(n+1)}{2}}$</span>.

    !!! abstract "Démonstration remarquable"

        Soit $S=1+2+3+\dots+n$, où $n\in ℕ^*$. Alors :

        \[\begin{array}{rccccccccccccccc}
        S=&1&+&2&+&\dots&+&p&+&\dots&+&n-1&+&n\\
        S=&n&+&n-1&+&\dots&+&n-p+1&+&\dots&+&2&+&1\\\hline
        2\,S=&n+1&+&n+1&+&\dots
        &+&n+1&+&\dots
        &+&n+1&+&n+1
        \end{array}\]

        Chaque terme de la somme $2\,S$ s'écrit $n+1$ et la somme comporte $n$ termes donc $2\,S=n\times (n+1)$, d'où la formule.

!!! example "Exemple I.1"
    $1+2+3+\dots+100=\dfrac{100 \times101}{2}=5050$.


!!!tip "Remarque"
    Grâce à la propriéte I.1, il est possible de calculer une somme de termes consécutifs d'une suite arithmétique.

!!! abstract "Rappels sur les suites arithmétiques"

    * ($u_n$) est __arithmétique__ s'il existe un nombre $r$ tel que, pour tout $n\in ℕ$ : $\boxed{u_{n+1}=u_n+r}$.
    ![Suite arithmétique](images/suite_arithm.png){width=70%; : .center}
    * Dans ce cas, il y a une formule explicite : $\boxed{u_n=u_0+n\,r}$.



!!! example "Exemple I.2"
    === "Question"
        Soit ($u_n$) arithmétique de premier terme $u_0=5$ et de raison $r=3$.  
        Calculer la somme $u_0+u_1+u_2+\dots+u_{100}$.

    === "Réponse"

        $\begin{align*}
        u_0+u_1+u_2+\dots+u_{100} 
        &= 
        u_0+(u_0+r)+(u_0+2r)+\dots+(u_{0}+100r)
        \\
        &=
        101 u_0 + r \times (1+2+\dots+100)
        \\
        &=
        101 u_0 + r \times \dfrac{100 \times 101}{2}
        \\
        &=
        101 u_0 + 5050 r
        =
        101 \times 5+5050 \times 3
        =
        15655.
        \end{align*}$

!!! example "Exemple I.3"
    === "Question"
        Soit ($u_n$) une suite arithmétique de premier terme $u_2=10$ et de raison $r=-1$.  
        Calculer la somme des vingt premiers termes de cette suite.

    === "Réponse"

        $\begin{align*}
        u_2+u_3+u_4+\dots+u_{21} 
        &= 
        u_2+(u_2+r)+(u_2+2r)+\dots+(u_{2}+19r)
        \\
        &=
        20 u_2 + r \times (1+2+\dots+19)
        \\
        &=
        20 u_2 + r \times \dfrac{19 \times 20}{2}
        \\
        &=
        20 u_2 + 190 r
        =
        20 \times 10+190 \times (-1)
        =
        10.
        \end{align*}$

??? tip "Formule directe"
    En fait,
    $u_0+u_1+u_2+\dots+u_{n}=(n+1) \times \dfrac{u_0+u_n}{2}$

    Il suffit de remplacer chaque terme par la moyenne du premier et du dernier terme et de multiplier par le nombre de termes.

!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    Exercice 122 page 54, question 2) (montrer le c) avec la formule directe).
    



## I.2 Sommes de termes consécutifs d'une suite géométrique

!!! abstract "Propriété I.2"
    Soit $q$ un réel différent de 1. 
    Alors, pour tout $n\in ℕ$, <span style='color:red'>$\boxed{1+q+q^2+q^3+\dots+q^n=\dfrac{1-q^{n+1}}{1-q}}$</span>.

    !!! abstract "Démonstration remarquable"

        Soit $S=1+q+q^2+q^3+\dots+q^n$ avec $q\neq 1$.
        Alors :

        \[\begin{array}{rccccccccccccc}
        S=&1&+&q&+&q^{2}&+&\dots&+&q^{n-1}&+&q^n\\
        qS=&&&q&+&q^{2}&+&\dots&+&q^{n-1}&+&q^n&+&q^{n+1}\\\hline
        S-qS=&1&+&0&+&0&+&\dots&+&0&+&0
        &+&(-q^{n+1})
        \end{array}\]

        donc $(1-q)S=1-q^{n+1}$. 

        Comme $q\neq 1$, nous avons $1-q\neq 0$ et il suffit de diviser l'égalité précédente par $1-q$ pour trouver $S$.

!!!tip "Remarques"
    * La formule de la propriété n'est pas applicable quand $q=1$ (division par zéro...).
    * Pour $q=1$, nous avons  $1+q+q^2+q^3+\dots+q^n= 1+1+1+\dots+1 = (n+1) \times 1 =n+1$.
    * Dans la formule de la propriété I.2, $n+1$ est le nombre de termes dans la somme.

!!! example "Exemple I.4"
    === "Question"
        Calculer $1+3+9+27+\dots+531441$.

    === "Réponse"
        En cherchant à tâtons, nous trouvons que $531441 = 3^{12}$
        (une fonction vue en classe de Terminale permettrait de trouver ce nombre rapidement...).

        Donc $1+3+9+27+\dots+531441 = 1+3+3^2+3^3+\dots+3^{12} = \dfrac{1-3^{12+1}}{1-3} = 797161$.

!!!tip "Remarque"
    Grâce à la propriété I.2, il est possible de calculer une somme de termes consécutifs d'une suite géométrique.

!!! abstract "Rappels sur les suites géométriques"

    * ($u_n$) est __géométrique__ s'il existe un nombre $q$ tel que, pour tout $n\in ℕ$ : $\boxed{u_{n+1}=q \times u_n}$.  
    ![Suite arithmétique](images/suite_geom.png){width=70%; : .center}
    * Dans ce cas, il y a une formule explicite : $\boxed{u_n=q^n \times u_0}$.

!!! example "Exemple I.5"
    === "Question"
        Soit ($u_n$) une suite géométrique de premier terme $u_0=5$ et de raison $q=3$.  
        Calculer la somme $u_0+u_1+u_2+\dots+u_{10}$.

    === "Réponse"

        $\begin{align*}
        u_0+u_1+u_2+\dots+u_{10} 
        &= 
        u_0+q\,u_0+q^2\,u_0+\dots+q^{10}\,u_{0}
        \\
        &=
        u_0 \left(1+q+q^2+q^3+\dots+q^{10}\right)
        =
        u_0 \times \dfrac{1-q^{11}}{1-q}
        \\
        &=
        5 \times \dfrac{1-3^{11}}{1-3}
        =
        442865.
        \end{align*}$


!!! question "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) :  
    124, 125, 129, 132, 133 page 54
