# Quelques exercices pour vous entraîner
Les valeurs utilisées dans ces exercices sont générées par le site Wims, vous pouvez donc refaire plusieurs fois chacun d'entre eux.

## Suites arithmétiques

* [deviner si une suite peut être arithmétique](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H4/algebra/oefsuites1S.fr&cmd=new&exo=aritm1) ;
et [là](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H4/algebra/oefsuites1S.fr&cmd=new&exo=aritm2) ;
* [calculer un terme](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/suites.fr&cmd=new&exo=Suitesarithmti) ;
* [trouver la raison ou un terme](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H6/algebra/oefsuites.fr&cmd=new&exo=suite_arith) et [là](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H6/analysis/numericseq.fr&cmd=new&exo=calctersuite7) ;
* [calculs sur une suite explicite et arithmétique](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H4/algebra/oefsuites1S.fr&cmd=new&exo=raisonarit) ;


## Suites géométriques

* [deviner si une suite peut être géométrique](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H4/algebra/oefsuites1S.fr&cmd=new&exo=geom1) ;
* [calculer un terme](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/oefsequence.fr&cmd=new&exo=calcul_terme_suite_geometrique) ;
* [trouver la raison ou un terme](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H6/algebra/oefsuites.fr&cmd=new&exo=suite_geo_entier) et [là](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H6/analysis/numericseq.fr&cmd=new&exo=calctersuite8) ;
* [pourcentage et coefficient multiplicateur](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/oefsequence.fr&cmd=new&exo=pourcentage_et_cm_1) et [là](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/oefsequence.fr&cmd=new&exo=pourcentage_et_cm_2) ;
* [pourcentages et suites](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/oefsequence.fr&cmd=new&exo=est_elle_geometrique) ;
* [calculs sur une suite explicite et géométrique](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H4/algebra/oefsuites1S.fr&cmd=new&exo=raisongeo) ;

## Autres exercices
* [suite arithmétique ou géométrique (avec des valeurs) ?](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H6/algebra/oefsuitesExosSyn.fr&cmd=new&exo=nature_suite) ;
* [suite arithmétique ou géométrique (avec des expressions explicites) ?](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H4/algebra/oefsuites1S.fr&cmd=new&exo=classersuitesB) ;
* [suite arithmétique ou géométrique (avec des relations de récurrence) ?](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H4/algebra/oefsuites1S.fr&cmd=new&exo=classersuitesA) ;
* [calculer un terme (suite arithmétique ou géométrique)](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/suites.fr&cmd=new&exo=Suitearithmtiq) ;
* [QCM sur les suites](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H6/analysis/numericseq.fr&cmd=new&exo=suitesQCM1) ;
* [une suite arithmético-géométrique](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/oefsequence.fr&cmd=new&exo=etude_suite_arithmetico_geometrique)
