# Suites arithmétiques ; suites géométriques
## I - Rappels sur les pourcentages

!!! abstract "Propriété"
    Augmenter une valeur de $t$ % revient à multiplier cette valeur par \(1+\dfrac{t}{100}\).

!!! example "Exemple I.1"
    Le prix d'un article coûtant 50 € est augmenté de 30 %.

    Comme \(1+\dfrac{30}{100}=1,3\) le nouveau prix est alors \(50 \times 1,3 = 65\) €.

!!! abstract "Propriété"
    Diminuer une valeur de $t$ % revient à multiplier cette valeur par \(1-\dfrac{t}{100}\).

!!! example "Exemple I.2"
    Le prix d'un article coûtant 50 € est réduit de 30 %.

    Comme \(1-\dfrac{30}{100}=0,7\) le nouveau prix est alors \(50 \times 0,7 = 35\) €.



!!! tip "Remarques"
    Voici deux exemples de pièges liés aux pourcentages :

    * augmenter un prix de 10 % puis réduire le nouveau prix de 10 % revient à multiplier le prix initial par \(1,1 \times 0,9 = 0,99\) donc à le réduire de 1 % (ce qui ne donne pas le prix de départ !) ;
    * augmenter cinq fois un prix de 10 % revient donc à le multiplier par \(1,1^5 = 1,61051\) donc à l'augmenter d'environ 61 % (pas de 50 % ...).



## II - Suites arithmétiques
### Définition

!!! abstract "Définition"
    Une suite ($u_n$) est dite <span style="color: red;">arithmétique</span> si l'on passe d'un terme à son suivant en ajoutant un nombre constant appelé <span style="color: red;">raison</span> et noté $r$, c'est-à-dire si, pour tout $n\in ℕ$ :
    <span style="color: red;">\(\boxed{u_{n+1}=u_n+r}\)</span>

    ![Suite arithmétique](images/suite_arithm.png){width=70%; : .center}


!!! question "Exercice"
    Exercice I de la [fiche d'exercices](exos_suites_a_g.pdf)


!!! example "Exemple II.1"

    === "Question"
        Soit $(u_n)$ la suite arithmétique de premier terme $u_0=-7$ et de raison 5. Donnez les cinq premiers termes de cette suite.

    === "Réponses"
        $u_{0}=-7$ ;  
        $u_{1}=u_{0}+5 = -7+5=-2$ ;  
        $u_{2}=u_{1}+5 = -2+5=3$ ;  
        $u_{3}=u_{2}+5 = 3+5=8$ ;  
        $u_{4}=u_{3}+5 = 8+5=13$.


        ![Suite arithmétique](images/suite_arithm2.png){width=50%; : .center}


!!! question "Exercice"
    Exercice II de la [fiche d'exercices](exos_suites_a_g.pdf)



### Expression du terme général, d'un terme en fonction d'un autre
Une suite arithmétique est à priori définie par récurrence. Cependant, il est
possible de calculer un terme sans avoir à calculer ceux qui précèdent.

!!! abstract "Propriété"
    Soit ($u_n$) une suite arithmétique de raison $r$ alors, pour tous les entiers naturels $n$ et $p$ :

    <span style="color: red;">
    $\boxed{u_n=u_0+n\,r}$
    $\boxed{u_n=u_1+(n-1)r}$
    $\boxed{u_n=u_p+(n-p)r}$
    </span>

    (la dernière formule résume tous les cas possibles)


!!! example "Exemples II.2"

    === "Questions"
        * Soit $(u_n)$ une suite arithmétique de premier terme $u_1=100$ et de raison $r=-0,5$.  
        Calculez le cinquantième terme de la suite.
        * Soit $(v_n)$ une suite arithmétique telle que $u_{30}=10$ et $u_5=-2$.  
        Trouver la raison de cette suite.
    === "Réponses"
        * Le cinquantième terme est $u_{50}=u_1+49\,r=75,5$.
        * $u_{30}=u_5+25\,r$ donc $r=\dfrac{u_{30}-u_5}{25} =0,48$.

!!! question "Exercices"
    Exercices III et IV de la [fiche d'exercices](exos_suites_a_g.pdf)



!!! tip "Remarque"
    Les suites arithmétiques apparaissent naturellement quand on s'intéresse à l'évolution d'un capital à accroissement fixe.

!!! example "Exemple II.3"

    === "Question"
        Un enfant dispose d'un capital de 70 € et économise 10 € par mois.  
        Quel sera ce capital dans 2 ans ?

    === "Réponse :"    
        comme l'enfant ajoute toujours la même somme d'argent tous les mois, la suite $(C_n)$ (où $n$ est le nombre de mois écoulés) des capitaux est une suite arithmétique de premier terme $C_{0}=70$ et de raison $r=10$.

        Le capital dans 2 ans sera alors 
        \(C_{120}=C_{0} + 120 \times 10 =1270\) €.

!!! tip "Remarque"
    La relation $u_n=u_0+n\,r$ montre que les suites arithmétiques sont reliées aux fonctions affines.

!!! example "Exemple II.4"
    Dans l'exemple précédent, \(C_{n}=C_{0} + n\times r = 70 + 10\,n = 10\,n+70\), qui est de la forme $a\,n+b$.

    La représentation graphique (partielle) de cette suite $(C_n)$ est donc une succession de points alignés :

    ![Suite arithmétique figure](images/suite_arithm3.png){width=40%; : .center}

### Déterminer si une suite est arithmétique

!!! tip "Astuce"
    Remarquez que \(u_{n+1}=u_n+r \iff u_{n+1}-u_n=r\) : il suffit de prouver que la différence entre un terme et son précédent reste constante.

!!! example "Exemple II.5"

    === "Question"
        Soit $(u_n)$ la suite définie par $u_n=15+20\,n$ pour tout entier naturel $n$.  
        Prouver que $(u_n)$ est arithmétique.

    === "Réponse"
        Nous avons $u_{n+1}=15+20(n+1)=35+20\,n$ d'où $u_{n+1}-u_n=(35+20\,n)-(15+20\,n)=20$ donc $(u_n)$ est arithmétique de raison $r=20$ (et de premier terme $u_0=15$).

!!! question "Exercice"
    Exercice V de la [fiche d'exercices](exos_suites_a_g.pdf)





## III - Suites géométriques
### Définition

!!! abstract "Définition"
    Une suite ($u_n$) est dite <span style="color: red;">géométrique</span> si l'on passe d'un terme à son suivant en le multipliant par un nombre constant appelé <span style="color: red;">raison</span> et noté $q$, c'est-à-dire si, pour tout $n\in ℕ$ :
    <span style="color: red;">$\boxed{u_{n+1}=q \times u_n}$</span>

    ![Suite géométrique](images/suite_geom.png){width=70%; : .center}

!!! question "Exercice"
    Exercice VI de la [fiche d'exercices](exos_suites_a_g.pdf)


!!! example "Exemple III.1"
    === "Question"
        Soit $(u_n)$ la suite géométrique de premier terme $u_0=-7$ et de raison $-2$.  
        Donnez les cinq premiers termes de cette suite.
    === "Réponses"
        $u_{0}=-7$ ;  
        $u_{1}=u_{0} \times (-2) = -7 \times (-2) = 14$ ;  
        $u_{2}=u_{1}\times (-2) = 14 \times (-2) = -28$ ;  
        $u_{3}=u_{2}\times (-2) = -28 \times (-2) = 56$ ;  
        $u_{4}=u_{3}\times (-2) = 56 \times (-2) = -112$.

        ![Suite géométrique](images/suite_geom2.png){width=50%; : .center}


!!! question "Exercice"
    Exercice VII de la [fiche d'exercices](exos_suites_a_g.pdf)


### Expression du terme général

!!! abstract "Propriété"
    Soit ($u_n$) une suite géométrique de raison $q$. Alors, pour tous les entiers naturels $n$ et $p$, on a :

    <span style="color: red;">$\boxed{u_n=q^n \times u_0}$
    $\boxed{u_n=q^{n-1}\times u_1}$
    $\boxed{u_n=q^{n-p}\times u_p}$
    </span>

    La dernière formule résume tous les cas possibles.

!!! example "Exemples III.2"
    
    === "Questions"
        * Soit $(u_n)$ une suite géométrique de premier terme $u_0=35$ et de raison $q=2$.  
        Calculez son 11ième terme.
        * Soit $(v_n)$ une suite géométrique telle que $v_{5}=16$ et $v_{10}=-512$.  
        Trouvez la raison de cette suite.
    
    === "Réponses"
        * Le 11ième terme est $u_{10}=u_0 \times 2^{10} = 35 \times 1024 = 35840$.
        * $v_{10}=v_5\times q^5$ donc $q^5=\dfrac{v_{10}}{v_{5}}=\dfrac{-512}{16}=-32$ d'où $q=\sqrt[5]{-32}=-2$.
    
!!! question "Exercices"
    Exercices VIII et IX de la [fiche d'exercices](exos_suites_a_g.pdf)


!!! tip "Remarque"
    Les suites géométriques apparaissent naturellement quand on s'intéresse à l'évolution d'un capital à taux fixe.



!!! example "Exemple III.3"

    === "Question"
        Un capital de 2000 € est placé sur un compte bancaire qui rapporte 2 % d'intérêts tous les ans.
        Quel sera ce capital dans 10 ans ?

    === "Réponse"
        Comme augmenter de 2 % revient à multiplier par \(1+\dfrac{2}{\strut100}=1,02\), la suite $(C_n)$ des capitaux est une suite géométrique de premier terme $C_{0}=2000$ et de raison $q=1,02$.

        Le capital dans 10 ans sera alors \(C_{10}=C_{0} \times 1,02^{\strut10} \simeq 2438\) €.


### Déterminer si une suite est géométrique

!!! tip "Astuce"
    Remarquez que \(u_{n+1}=q \times u_n \iff \dfrac{u_{n+1}}{u_n}=q\) : il suffit de prouver que le quotient d'un terme par son précédent reste constant (mais il faut que \(u_n\) ne s'annule jamais !).

!!! example "Exemple III.4"

    === "Question"
        Prouver que la suite $(u_n)$ définie par $u_n=-2\times5^n$ est géométrique.

    === "Réponse"
        Calculons le rapport $\dfrac{\strut u_{n+1}}{u_{n}}$ (en remarquant que $u_n=-2\times5^n \neq0$ pour tout $n$) :

        $\dfrac{u_{n+1}}{u_{n}}=\dfrac{-2 \times 5^{n+1}}{-2 \times 5^{n}}=\dfrac{5^{n}\times 5^{1}}{5^{n}} = 5$ donc
        $(u_n)$ est une suite géométrique de raison 5 et de premier terme 
        $u_{0}=-2 \times 5^{\strut 0} = -2 \times 1=-2$.

!!! question "Exercice"
    Exercice X de la [fiche d'exercices](exos_suites_a_g.pdf)

!!! question "Exercices d'application"
    Exercices XI à XV de la [fiche d'exercices](exos_suites_a_g.pdf)


!!! question "Encore deux exercices"
    [Transmath](https://biblio.manuel-numerique.com/) : 119 page 53, 106 page 52