# Quelques exercices pour vous entraîner
Les valeurs utilisées dans ces exercices sont générées par le site Wims, vous pouvez donc refaire plusieurs fois chacun d'entre eux.

* [placer un point sur le cercle](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/cercletrigo.fr&cmd=new&exo=placerangle1)
* [lire un angle](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/cercletrigo.fr&cmd=new&exo=lireangle1) ;
* [trouver des valeurs de cosinus et sinus](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/cercletrigo.fr&cmd=new&exo=liresincos) ou [ici](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/sincos.fr&cmd=new&exo=valrem1) et [là](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/sincos.fr&cmd=new&exo=valrem2) ;
* [idem mais en QCM](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H4/geometry/anglesCercleTrigo.fr&cmd=new&exo=chgrtrigo1) ;
* [placer un point à partir du cosinus et du sinus](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/cercletrigo.fr&cmd=new&exo=placersincos) ;
* [angles associés (symétries)](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/analysis/cercletrigo.fr&cmd=new&exo=placerassoc1) ;
* [trouver le cosinus connaissant le sinus (ou l'inverse)](https://wims.univ-cotedazur.fr/wims/wims.cgi?module=H5/geometry/oefangles1S.fr&cmd=new&exo=chgrtrigo1)