# IV - Fonctions cosinus et sinus
## IV.1 Définitions
!!! abstract "Définitions"
     La <span style="color:red">fonction cosinus</span> associe, à tout réel \(\alpha\), le nombre \(\cos\alpha\).

     La <span style="color:red">fonction sinus</span> associe, à tout réel \(\alpha\), le nombre \(\sin\alpha\).

!!! example "Exemples IV.1"
     * l'image de \(\dfrac{\pi}{6}\) par la fonction sinus est \(\dfrac{1}{2}\) ; 
     * \(\dfrac{\strut1}{\strut2}\) a une infinité d'antécédents par la fonction sinus, à savoir les nombres s'écrivant $\dfrac{\strut\pi}{\strut6}+2k\pi$ et $\dfrac{5\pi}{6}+2k\pi$, $k\in ℤ$ ;
     * 3 n'a pas d'antécédent par la fonction cosinus car, pour tout \(x\) réel, \(-1 \leqslant \cos x \leqslant 1\).

## IV.2 Parité

!!! abstract "Définitions"
     Soit \(I\) un ensemble symétrique par rapport à 0 (si \(x\in I\) alors \(-x\in I\)) et \(f\) une fonction définie sur \(I\).

     La fonction \(f\) est <span style="color:red">paire</span> si, pour tout \(x\) de \(I\), <span style="color:red">\(\boxed{f(-x)=f(x)}\)</span>.

     La fonction \(f\) est <span style="color:red">impaire</span> si, pour tout \(x\) de \(I\), <span style="color:red">\(\boxed{f(-x)=-f(x)}\)</span>.

!!! example "Exemples IV.2"
     * Les fonctions définies sur ℝ par :  
     \(f_1(x)=3\) ; 
     \(f_2(x)=5\,x^2\) ; 
     \(f_3(x)=2\,x^4+x^2+1\)  
     sont paires.

     * Les fonctions définies sur ℝ par :  
     \(f_4(x)=2\,x\) ; 
     \(f_5(x)=x^3-5\,x\) ; 
     \(f_6(x)=1/x~(x\neq0)\)  
     sont impaires.

     * Les fonctions définies sur ℝ par :  
     \(f_7(x)=4x+2\) ; 
     \(f_8(x)=x^2-x\) ; 
     \(f_9(x)=x^3+x^2\)  
     sont ni paires, ni impaires.


??? tip "Fonctions ni paires, ni impaires"
     * Une fonction "choisie au hasard" aura de fortes chances d'être ni paire, ni impaire !
     * Une fonction définie sur un intervalle non symétrique par rapport à 0 (exemple : la fonction racine carrée, définie sur $[0;+\infty[$) n'est ni paire, ni impaire.

!!! abstract "Propriété"

     <span style="color:red">La fonction cosinus est paire</span> et <span style="color:red">la fonction sinus est impaire</span>.

     __Démonstration__  
     En effet, nous avons vu que $\cos(-\alpha)=\cos\alpha$ et $\sin(-\alpha)=-\sin\alpha$.


## IV.3 Périodicité
!!! abstract "Définition"
     Soit \(f\) une fonction définie sur ℝ.  
     La fonction \(f\) est <span style="color:red">périodique</span> s'il existe un nombre \(T\) tel que, pour tout \(x\) réel, <span style="color:red">\(\boxed{f(x+T)=f(x)}\)</span>.

!!! tip "Remarques"
     * le nombre $T$ est alors appelé période de la fonction \(f\) ;
     * cela se traduit graphiquement par une répétition de la courbe :

     <iframe scrolling="no" title="Fonction périodique" src="https://www.geogebra.org/material/iframe/id/rsmyazek/width/1400/height/516/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="1400px" height="400px" style="border:0px;"> </iframe>

!!! abstract "Propriété"
     Les fonctions cosinus et sinus sont <span style="color:red">périodiques de période \(2\pi\)</span>.

     __Démonstration__  
     En effet, nous avons vu que $\cos(\alpha+2\pi)=\cos\alpha$ et  $\sin(\alpha+2\pi)=\sin\alpha$ souvenez-vous que \(2\pi\) représente un tour complet).

!!! question "Exercices"
     [Transmath](https://biblio.manuel-numerique.com/) : 29 à 32 pages 178, 179 ; 69 page 181 ; 97 page 184  
     Exercice 9 de la fiche
