# III - Cosinus et sinus d'un réel
## III.1 Définitions
!!! abstract "Définition"
    Soit $M$ le point image d'un réel $\alpha$.
    
    Le <span style="color:red">cosinus</span> et le <span style="color:red">sinus</span> de $\alpha$ sont alors l'abscisse et l'ordonnée de $M$ dans le repère $(O;\overrightarrow{OI},\overrightarrow{OJ})$ :

    <div style="color:red">

    \[\boxed{\cos \alpha = x_{M}} \qquad ; \qquad
    \boxed{\sin \alpha = y_{M}}\]

    </div>

    ![](images/trigo4.png){width=30% ; .center}

!!! tip "Remarques"
    * pour tout \(\alpha\) réel, <span style="color:red">\(\boxed{-1 \leqslant \cos \alpha \leqslant 1}\)</span> et <span style="color:red">\(\boxed{-1 \leqslant \sin \alpha \leqslant 1}\)</span> ;
    * si l’angle est en degrés alors il faut le préciser :  
    $\cos \dfrac{\strut \pi}{4}= \cos 45°$ (mais $\dfrac{\pi}{4}\simeq 0,785 \neq 45$ !).

!!! example "Exemple III.1"

    ![](images/trigo4.png){width=20% ; .center}

    $\alpha$ | 0 | $\dfrac{\strut\pi}{\strut2}$ | $-\pi$ | $\dfrac{7\pi}{2}$ | $2023\pi$ |$\dfrac{215\pi}{2}$  
    --- | --- | --- | --- | --- | --- | ---  
    Point image de $\alpha$ | I | J| I'| J'| I'| J'  
    $\cos\alpha$ | 1 | 0| -1| 0| -1| 0  
    $\sin\alpha$ | 0 | 1| 0| -1| 0| -1

    Pour la dernière colonne, il fallait remarquer que 

    \(
    \dfrac{215\pi}{2}
    =
    \dfrac{216\pi}{2}-\dfrac{\pi}{2}
    =
    108\pi-\dfrac{\pi}{2}
    =
    54 \times (2\pi)-\dfrac{\pi}{2}
    \)
    ce qui revient à
    \(-\dfrac{\pi}{2}\).

??? tip "Lien avec les définitions du collège"

    Considérons un triangle rectangle, ses angles non droits sont alors aigus (entre 0 et 90°).
    
    Sur la figure ci-dessous, j'ai placé un point $M$
    tel que $0 < \widehat{IOM} < 90°$ et $H$, $K$ les projetés orthogonaux de $M$ sur les axes.

    ![](images/trigo5.png){width=20% ; .center}

    Alors, dans le triangle rectangle $OHM$ :  
    $\cos\alpha=\dfrac{\strut\text{côté adjacent à }\alpha}{\strut\text{hypoténuse}} = \dfrac{OH}{OM}=\dfrac{x_M}{1}=x_M$  
    et  
    $\sin\alpha=\dfrac{\strut\text{côté opposé à }\alpha}{\strut\text{hypoténuse}} = \dfrac{HM}{OM}=\dfrac{OK}{OM}=\dfrac{y_M}{1}=y_M$.

    Nous retrouvons donc les définitions du collège. Pourquoi introduire de nouvelles définitions alors ? Pour pouvoir parler du cosinus et du sinus d’angles quelconques (pas forcément aigus) !


!!! faq "Exercices"
    Fiche d'exercices : Exercices 3, 4 et 5


## III.2 Recherche de valeurs de cosinus et sinus
### Valeurs remarquables

!!! abstract "Propriété"

    Pour tout réel $\alpha$, 
    <span style="color:red">$\boxed{\cos^2\alpha+\sin^2\alpha=1}$</span>
    où $\cos^2\alpha=(\cos\alpha)^2$ et $\sin^2\alpha=(\sin\alpha)^2$.


    __Démonstration__  
    Dans le cas d'un angle \(\alpha\in \left[0;\dfrac{\pi}{2}\right]\), dans le triangle rectangle $OHM$, d’après le théorème de Pythagore :

    $OH^2+HM^2=OM^2=1^2=1$.

    Or $OH^2=(x_M)^2=(\cos\alpha)^2$ et $HM^2=(y_M)^2=(\sin\alpha)^2$.

    ![](images/trigo5.png){width=20% ; .center}

    Le raisonnement est analogue pour tous les réels \(\alpha\).

!!! abstract "Démonstration remarquable"

    Supposons que $\alpha=\dfrac{\pi}{3}$ (angle au centre de 60°).
    Alors on montre aisément que $OIM$ est équilatéral donc la hauteur issue de $M$ est aussi une médiane donc 
    $OH=\dfrac{1}{2}$ d'où $\cos \dfrac{\pi}{3}=\dfrac{1}{2}$.

    Par conséquent $\sin^2\dfrac{\pi}{3} = 1-\left(\dfrac{1}{2}\right)^2 = 1-\dfrac{\strut 1}{4} = \dfrac{3}{4}$.

    Comme \(\dfrac{\pi}{3}\in \left[0;\dfrac{\pi}{2}\right]\), 
    \(\sin \dfrac{\pi}{3}>0\), nous obtenons
    $\sin\dfrac{\pi}{3} = \sqrt{\dfrac{3}{4}} = \dfrac{\sqrt{3}}{2}$.


!!! abstract "Démonstration remarquable"


    Supposons que $\alpha=\dfrac{\pi}{4}$ (angle au centre de 45°).
    Alors on montre aisément que $OHM$ est rectangle et isocèle donc \(OH=HM=OK\) d'où
    $\cos \dfrac{\pi}{4}=\sin \dfrac{\pi}{4}$. Notons \(x\) cette valeur.

    Comme \(\cos^2\alpha+\sin^2\alpha=1\), il vient \(x^2+x^2=1\) donc \(2x^2=1\) puis 
    \(x^2=\dfrac{1}{2}\) donc \(x=\sqrt{\dfrac{1}{2}}=\dfrac{\sqrt{1}}{\sqrt{2}}=
    \dfrac{1}{\sqrt{2}}=
    \dfrac{\sqrt{2}}{(\sqrt{2})^2}=
    \dfrac{\sqrt{2}}{2}
    \).

    Conclusion :
    \(\cos \dfrac{\pi}{4}=\sin \dfrac{\pi}{4}=\dfrac{\sqrt{2}}{2}\).

!!! abstract "Propriété : valeurs remarquables"

    \(\alpha\) | 0 | \(\dfrac{\strut\pi}{\strut6} \)| \( \dfrac{\pi }{4} \)| \( \dfrac{\pi }{3} \)| \( \dfrac{\pi }{2} \)| \(\pi\)  
    --- | --- | --- | --- | --- | --- | ---   
    \( \cos \alpha \)| 1| \( \dfrac{\strut \sqrt{3}}{\strut 2} \)| \( \dfrac{\sqrt{2}} 2 \)| \( \dfrac 1 2 \)| 0| \(-1\)  
    \( \sin \alpha \)| 0| \( \dfrac{\strut 1}{\strut 2} \)| \( \dfrac {\sqrt{2}} 2 \)| \( \dfrac {\sqrt{3}} 2 \)| 1| 0

    <iframe scrolling="no" title="Angles remarquables - sinus et cosinus" src="https://www.geogebra.org/material/iframe/id/eakn9drz/width/1426/height/704/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="1426px" height="704px" style="border:0px;"> </iframe>



### Exemples d'utilisation de symétries

!!! example "Exemple III.2"

    Considérons le point $M$ associé à $\dfrac{\pi}{6}$ et $N$, $P$, $Q$ les symétriques de $M$ par rapport à l'axe des abscisses, à $O$ et à l'axe des ordonnées.

    ![](images/trigo6.png){width=40% ; .center}

    Point | $M$ | $N$ | $P$ | $Q$  
    --- | --- | --- | --- | ---  
    Un réel associé | \(\pi/6\) | \(-\pi/6\) | \(7\pi/6\) | \(5\pi/6\)  
    cosinus | \(\sqrt{3}/2\) | \(\sqrt{3}/2\) | \(-\sqrt{3}/2\) | \(-\sqrt{3}/2\)  
    sinus | \(1/2\) | \(-1/2\) | \(-1/2\) | \(1/2\)

    * $\cos\left(-\dfrac{\pi}{6}\right)=\cos\dfrac{\pi}{6}$ et $\sin\left(-\dfrac {\pi} 6 \right)=-\sin\dfrac{\pi}{6}$
    * $\cos\left(\dfrac {7\pi} 6 \right)=\cos\left(\pi+\dfrac{\pi}{6}\right)=-\cos\dfrac{\pi}{6}$ et $\sin\left(\dfrac {7\pi} 6 \right)=-\sin\dfrac{\pi}{6}$
    * $\cos\left(\dfrac {5\pi} 6 \right)=\cos\left(\pi-\dfrac{\pi}{6}\right)=-\cos\dfrac{\pi}{6}$ et $\sin\left(\dfrac {5\pi} 6 \right)=\sin\dfrac{\pi}{6}$.

!!! abstract "Propriétés"
    Pour tout réel \(\alpha\) :

    <span style="color:red">\(\boxed{\cos\left(-\alpha\right)=\cos\left(\alpha\right)}\)</span> et 
    <span style="color:red">\(\boxed{\sin\left(-\alpha\right)=-\sin\left(\alpha\right)}\)</span>

    <span style="color:red">\(\boxed{\cos\left(\pi-\alpha\right)=-\cos\left(\alpha\right)}\)</span>
    et
    <span style="color:red">\(\boxed{\sin\left(\pi-\alpha\right)=\sin\left(\alpha\right)}\)</span>

    <span style="color:red">\(\boxed{\cos\left(\alpha+\pi\right)=-\cos\left(\alpha\right)}\)</span>
    et
    <span style="color:red">\(\boxed{\sin\left(\alpha+\pi\right)=-\sin\left(\alpha\right)}\)</span>
    
    <span style="color:red">\(\boxed{\cos\left(\alpha+2\pi\right)=\cos\left(\alpha\right)}\)</span>
    et
    <span style="color:red">\(\boxed{\sin\left(\alpha+2\pi\right)=\sin\left(\alpha\right)}\)</span>
    

    <iframe scrolling="no" title="Angles associés" src="https://www.geogebra.org/material/iframe/id/SzDZqDwY/width/653/height/328/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="1000px" height="328" style="border:0px;"> </iframe>


!!! tip "Remarque"
    Au vu de l'exemple précédent, remarquons que \(\cos(5\alpha)\) n'est pas forcément égal à \(5\cos(\alpha)\) (ils sont rarement égaux en fait).



!!! faq "Exercices"
    Fiche d'exercices : Exercices 6 et 7



### Valeurs approchées du cosinus et du sinus
!!! example "Exemple III.3"
    En utilisant la calculatrice en mode radians, 

    $\cos 1 \simeq 0,54$  
    $\sin 1,46 \simeq 0,99$  
    $\cos \dfrac{\pi}{12} \simeq 0,97$.


## III.3 Exemple d'équation trigonométrique

!!!tip "Des tours"
    Soit $k$ un entier. Le nombre $k \times 2\pi=2k\pi$ indique un certain nombre $k$ de tours.


!!! example "Exemple III.4"

    Nous voulons résoudre dans IR l'équation $\sin x = \dfrac{1}{2}$. Pour cela :

    * plaçons le ou les point(s) correspondant(s) à un $y$ égal à $\dfrac{\strut 1}{\strut 2}$ ;
    ![](images/trigo7.png){width=20% ; .center}
    * $\dfrac{\strut 1}{\strut 2}$ est le sinus de $\dfrac{\pi}{6}$ ;
    * l'observation du graphique nous donne alors comme solution(s) : $\dfrac{\pi}{6}$ et $\pi-\dfrac{\pi}{6} = \dfrac{5\pi}{6}$ ;
    * nous pouvons ajouter un nombre quelconque de tours ; les solutions s'écrivent donc :
    $\dfrac{\pi}{6}+2k\pi$ et $\dfrac{5\pi}{6}+2k\pi$, $k\in ℤ$.


!!!danger "Attention"
    * taper \(\arcsin(1/2)\) sur la calculatrice ne suffit pas (cela ne donne qu'une solution) !
    * dans l'équation $\sin x = \dfrac{1}{2}$, le nombre $x$ représente un angle (une position sur le cercle trigonométrique), pas une abscisse !


!!! question "Exercice à faire ensemble"

    <iframe scrolling="no" title="Résoudre une équation trigonométrique" src="https://www.geogebra.org/material/iframe/id/J5z4XSpq/width/1904/height/809/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="1904px" height="400px" style="border:0px;"> </iframe>

!!! question "Exercices"
    Fiche d'exercices : Exercice 8  
    [Transmath](https://biblio.manuel-numerique.com/) :  86 page 183 et 67 page 181 (+ solutions sur [0 ; 2 $\pi$])


