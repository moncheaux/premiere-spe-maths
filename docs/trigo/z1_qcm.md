# ⁉️ QCM
!!! tip "Attention"
    Il peut y avoir plusieurs réponses correctes à chaque question !

#### Q1 - Sans calculatrice, convertir en degré $\frac{\pi}{10}$ :
{{ qcm(["18", "36", "0,314", "$-\dfrac{1}{10}$"], [1], shuffle = True) }}

#### Q2 - Parmi les réels (angles) suivants, lesquels ont le même point image que $\dfrac{\pi}{4}$ :
{{ qcm(["$-\dfrac{\pi}{4}$", "$\dfrac{9\pi}{4}$", "$\dfrac{11\pi}{4}$", "$\dfrac{13\pi}{4}$", "$\dfrac{3\pi}{12}$"], [2,5], shuffle = True) }}

#### Q3 - Sans calculatrice, $\dfrac{1}{2}$ est :
{{ qcm(["le cosinus de $\dfrac{\pi}{3}$", "le cosinus de $-\dfrac{\pi}{3}$", "le cosinus de $\dfrac{\pi}{6}$", "le cosinus de $-\dfrac{\pi}{6}$", "le sinus de $\dfrac{\pi}{3}$", "le sinus de $-\dfrac{\pi}{3}$", "le sinus de $\dfrac{\pi}{6}$", "le sinus de $-\dfrac{\pi}{6}$"], [1,2,7], shuffle = True) }}

#### Q4 - Sans calculatrice, $-\dfrac{\sqrt{2}}{2}$ est :
{{ qcm(["le cosinus de $\dfrac{\pi}{4}$", "le cosinus de $-\dfrac{\pi}{4}$", "le cosinus de $\dfrac{3\pi}{4}$", "le cosinus de $-\dfrac{3\pi}{4}$", "le sinus de $\dfrac{\pi}{4}$", "le sinus de $-\dfrac{\pi}{4}$", "le sinus de $\dfrac{3\pi}{4}$", "le sinus de $-\dfrac{3\pi}{4}$"], [3,4,6,8], shuffle = True) }}

#### Q5 - L'équation $\cos x = -1/2$ :
{{ qcm(["a deux solutions", "a une infinité de solutions", "n'a pas de solution", "a une solution unique"], [2], shuffle = True) }}

#### Q6 - L'équation $\sin x + 2 = 0$ :
{{ qcm(["a deux solutions", "a une infinité de solutions", "n'a pas de solution", "a une solution unique"], [3], shuffle = True) }}

#### Q7 - L'équation $\cos x = -1$ à résoudre sur $[0;2\pi]$ :
{{ qcm(["a deux solutions", "a une infinité de solutions", "n'a pas de solution", "a une solution unique"], [4], shuffle = True) }}


Vous trouverez [un autre quiz ici](https://www.lumni.fr/quiz/trigonometrie-cosinus-et-sinus-d-un-nombre-reel).