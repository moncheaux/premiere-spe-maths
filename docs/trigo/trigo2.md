# I - Longueur d'un arc d'un cercle. Radians
## I.1 Unité de longueur
!!! tip "Qu'est ce qu'une longueur de 1 ?"
    Pour représenter des longueurs ou des angles, il faut choisir une unité de longueur.  
    Par exemple, nous pouvons décider que 4 cm représentent une unité de longueur.

## I.2 Longueur d'un arc d'un cercle de rayon 1

!!! abstract "Propriété"

    !!! tip inline end ""

        ![](images/cercle_rayon_1.png){ width=50%; : .center }

    La circonférence d'un cercle de rayon 1 est \(2\pi\).

    En effet, la circonférence d'un cercle de rayon $R$ est \(2\pi R\). 




!!! abstract "Propriété"
    La longueur d'un arc d'un cercle est proportionnelle à l'angle au centre correspondant.

!!! example "Exemple I.1"
    === "Questions"
        Les cercles suivants sont de rayon 1.
        Déterminez la longueur de l'arc en gras (fig.1) ainsi que l'angle au centre (fig.2).

        ![](images/trigo1.png){width=50%}

    === "Réponses"

        D'après la propriété précédente, il suffit de faire un tableau de proportionnalité :

        &nbsp; | Cercle entier | Fig.1 | Fig.2  
        --- | --- | --- | ---  
        Angle au centre | 360° | 60° | \(114,59°\) environ  
        Longueur d'arc |\(2\pi\)| \(\dfrac{\pi}{3}\) | 2


        ??? "Détails des calculs"
            \(\dfrac{\strut 2\pi \times 60}{360}=\dfrac{\pi}{3}\)
            ou 
            \(\dfrac{\strut 2\pi}{6}=\dfrac{\pi}{3}\)
            et
            \(\dfrac{\strut 2 \times 360}{2\pi}=\dfrac{360}{\pi} \simeq 114,59°\).


## I.3 Mesure en radians d'un angle

!!! abstract "Définition"
    La mesure en <span style="color:red">radians</span> d'un angle est égale à la longueur de l'arc de cercle de rayon 1 correspondant : $\widehat{AOB}\text{ (en radians)} =\overset{⁔}{AB}$.

!!! example "Exemple I.1"
    D'après l'exemple précédent, un angle au centre de 60° intercepte un arc de longueur \(\dfrac{\pi}{3}\) (sur un cercle de rayon 1) 
    donc cet angle a pour mesure \(\dfrac{\pi}{3}\) radians.


!!! abstract "Conversion des angles les plus utilisés"

    $\begin{array}{c|c|c|c|c|c|c|c}
    \alpha\mbox{ (en degrés)}&0&30&45&60&90&180&360\\\hline
    \alpha\mbox{ (en radians)}&0&\dfrac{\strut\pi}{6}&\dfrac{\pi}{4}&\dfrac{\pi}{3}&\dfrac{\pi}{2}&\pi&2\pi
    \end{array}$

    Retenez que les mesures en radians et en degrés sont proportionnelles et que <span style="color:red">$\pi$ rad représentent 180°</span> et <span style="color:red">$2\pi$ rad représentent 1 tour complet</span>.
    
!!! faq "Exercice"
    [Transmath](https://biblio.manuel-numerique.com/) : 8 page 177