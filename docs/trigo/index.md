# Histoire et objectifs du chapitre "Trigonométrie"

!!! faq "Exercice"
    [Fiche d’introduction (satellite)](intro_trigo.pdf)


## Histoire
Les origines de la trigonométrie remontent aux civilisations d’Égypte antique, de Mésopotamie et de la vallée de l’Indus, il y a plus de 4 000 ans.

![Une table trigonométrique ?](images/Plimpton_322.jpg){width=30%; : .center}  

Elle servait principalement pour l'astronomie.

## Trigonométrie de collège et objectif principal de ce chapitre
Au collège, vous avez vues les définitions du cosinus, sinus et tangente d'un angle __d'un triangle rectangle__ ([piqûre de rappel si vous avez oublié](https://www.youtube.com/watch?v=Lbl6WmSA014)). Le problème est que ces angles sont forcément aigus (entre 0 et 90°), ainsi ces définitions ne permettent pas de parler du cosinus de 120° par exemple.
Ceci est pourtant nécessaire pour étudier la trajectoire d'un satellite autour de la Terre, pour représenter un encéphalogramme etc.

Nous verrons donc dans ce chapitre qu'il est possible de définir le cosinus et le sinus de n'importe quel angle ! Vous découvrirez par ailleurs deux nouvelles fonctions...

## À quoi ça sert en fait ?
La trigonométrie permet de calculer des longueurs ou des angles.

Ses applications sont nombreuses : construction,  astronomie, navigation, sciences physiques (électricité, électronique, mécanique, acoustique, optique), statistiques, économie, biologie, chimie, médecine, météorologie, géodésie, géographie, cartographie, cryptographie, informatique etc ([d'après Wikipédia](https://fr.wikipedia.org/wiki/Trigonom%C3%A9trie))

