# II - Enroulement de la droite numérique sur le cercle trigonométrique
## II.1 Cercle trigonométrique
!!! abstract "Définitions"
    * le <span style="color:red">sens trigonométrique</span> (ou sens positif) est le sens contraire des aiguilles d'une montre ;
    * le <span style="color:red">cercle trigonométrique</span>  est un cercle de rayon 1 (c'est-à-dire l'unité de mesure choisie), orienté dans le sens trigonométrique.

    ![](images/trigo2.png){width=20%; .center}


## II.2 Enroulement de la droite numérique sur le cercle trigonométrique
Soit $I$ un point du cercle trigonométrique (le plus à droite) ; traçons un axe gradué tangent au cercle en $I$. Cet axe représente l'ensemble des réels IR.

À tout réel $\alpha$ correspond alors un point $N$ d’abscisse $\alpha$ sur cet axe. En enroulant l’axe sur le cercle, nous associons au point $N$ un point $M$ du cercle tel que \(\overset{⁔}{IM}=IN\).

Le point $M$ est appelé __point image__ de $\alpha$.

<iframe scrolling="no" title="Enroulement de la droite autour du cercle trigonométrique" src="https://www.geogebra.org/material/iframe/id/Ry4j9ZT2/width/1024/height/695/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="1024px" height="695px" style="border:0px;"></iframe>

??? tip "En cas de problème avec l'animation"
    En cas de problème avec l'animation ci-dessus dans Firefox, vous pouvez essayer de modifier le zoom (Ctrl + molette de la souris) ou utiliser Chromium...


!!! tip "Remarque"
    $\widehat{IOM} \text{ (en radians)} = \overset{⁔}{IM} = \alpha$ donc $\alpha$ désigne ici à la fois une longueur et un angle !


!!! faq "Exercice en classe"
    === "Questions"

        ![](images/trigo3.png){width=20% ; .center}

        1) À quel(s) réel(s) correspondent :  
     
        * le point du cercle le plus à gauche ?
        * le point du cercle le plus bas ?  

        2) Quels sont les points images des réels :

        * $\alpha_1=0$ ;
        * $\alpha_2=2\pi$
        * $\alpha_3=\pi$
        * $\alpha_4=\dfrac{\pi}{2}$
        * $\alpha_5=-\dfrac{\pi}{2}$
        * $\alpha_6=7\pi$
        * $\alpha_7=-\dfrac{3\pi}{4}$
        * $\alpha_8=\dfrac{45\pi}{3}$

        

    === "Réponses"

        1) 
     
        * le point du cercle le plus à gauche correspond à $\pi$ (ou $-\pi$)
        * le point du cercle le plus bas correspond à $-\dfrac{\pi}{2}$ (ou $\dfrac{3\pi}{2}$) 

        2)
        $\alpha_1=0$ ; $\alpha_2=2\pi$ ; $\alpha_3=\pi$ ; $\alpha_4=\dfrac{\pi}{2}$ ; $\alpha_5=-\dfrac{\pi}{2}$ ; $\alpha_6=7\pi$ ;  $\alpha_7=-\dfrac{3\pi}{4}$ ; $\alpha_8=\dfrac{45\pi}{3}$

        ![](images/trigo3b.png){width=20% ; .center}

        $M_1=I$ ;  $M_2=I$ ; $M_3=I'$ ; $M_4=J$ ;
        $M_5=J'$ ; $M_6=I'$ ; $M_7$ : voir le cercle ; $M_8=I'$.


!!! faq "Exercice"
    Fiche d'exercices distribuée en classe : Exercice 2 (+ Exercice 1 pour les plus rapides)

    ??? success "Réponses pour l'exercice 1"
        a) $SL=LO=SO$ car ce sont des rayons de deux cercles de même rayon. Le triangle $SOL$ est donc équilatéral.  
        b) L'angle $\widehat{SOL}$ est donc de 60° soit $\dfrac{\pi}{3}$ radians.  
        c) Il suffit de multiplier la mesure de l'angle $2\times\widehat{SOL}$ en radians par le rayon : $\dfrac{2\,\pi}{3} \times 696000 \simeq 1457699$ km.

!!! tip "Remarque"
    À chaque réel correspond un seul point mais à un point donné correspond une infinité de réels. Deux réels $\alpha$ et $\alpha'$ correspondent au même point si $\alpha-\alpha' = k\times (2\pi)=2\,k\,\pi$ avec $k$ entier relatif ($k \in ℤ$).

!!! example "Exemple II.1"
    $-\dfrac{2\pi}{3}$ et $\dfrac{16\pi}{3}$ sont associés au même point car la différence entre ces deux angles est :  
    $\dfrac{16\pi}{3}-\left(-\dfrac{2\pi}{3}\right)=\dfrac{18\pi}{3}
    =6\pi$      
    donc trois tours complets d'écart.

!!! faq "Exercices"
    [Transmath](https://biblio.manuel-numerique.com/) : 11 et 12 page 177
