# V - Courbes représentatives
## V.1 Courbe représentative de la fonction cosinus
Quand \(\alpha\) varie de 0 à \(\dfrac{\pi}{2}\), son cosinus diminue et nous connaissons quelques valeurs du cosinus :

\(\alpha\) | 0 | \(\dfrac{\strut\pi}{\strut6} \)| \( \dfrac{\pi }{4} \)| \( \dfrac{\pi }{3} \)| \( \dfrac{\pi }{2} \)  
--- | --- | --- | --- | --- | ---  
\( \cos \alpha \)| 1| \( \dfrac{\strut \sqrt{3}}{\strut 2} \)| \( \dfrac{\sqrt{2}} 2 \)| \( \dfrac 1 2 \)| 0  

![](images/trigo8a.png){width=20% ; .center}


En utilisant \(\cos\left(\pi-\alpha\right)=-\cos\left(\alpha\right)\), nous complétons avec des valeurs de \(\alpha\) entre 0 et \(\pi\) :

\(\alpha\) | 0 | \(\dfrac{2\strut\pi}{\strut3} \)| \( \dfrac{3\pi }{4} \)| \( \dfrac{5\pi }{6} \) | \(\pi\)  
--- | --- | --- | --- | --- | ---  
\( \cos \alpha \)| 1| \( -\dfrac{\strut 1}{\strut 2} \)| \( -\dfrac{\sqrt{2}}{2} \)| \( -\dfrac{\strut \sqrt{3}}{\strut 2} \)| -1

![](images/trigo8b.png){width=30% ; .center}


La fonction cosinus étant paire, nous retrouvons les mêmes valeurs pour des \(\alpha\) négatifs : la courbe est symétrique par rapport à l'axe des ordonnées.

![](images/trigo8c.png){width=30% ; .center}


Enfin, la fonction  cosinus est périodique de période \(2\pi\), ce qui permet de compléter la courbe.

![](images/trigo8d.png){width=40% ; .center}


!!! tip "Remarque"
     Une telle courbe est appelée une <span style="color:red">sinusoïde</span>.

<iframe scrolling="no" title="Courbe de la fonction cosinus" src="https://www.geogebra.org/material/iframe/id/AHmd92wz/width/806/height/225/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="806px" height="225px" style="border:0px;"> </iframe>




## V.2 Courbe représentative de la fonction sinus
Il suffit d'appliquer le même raisonnement pour obtenir la courbe de la fonction sinus :

![](images/trigo9.png){width=40% ; .center}

<iframe scrolling="no" title="Courbe de la fonction sinus" src="https://www.geogebra.org/material/iframe/id/C4ADwDrH/width/771/height/360/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="771px" height="360px" style="border:0px;"> </iframe>


!!! tip "Remarques"
     * quand \(\alpha\) varie de 0 à \(\dfrac{\pi}{\strut2}\), son sinus augmente ;
     * la fonction sinus étant impaire, sa courbe est symétrique par rapport à l'origine du repère ;
     * les deux courbes sont identiques, à une translation horizontale près.




<iframe scrolling="no" title="cosinus, sinus et cercle trigonométrique" src="https://www.geogebra.org/material/iframe/id/wywrwgmh/width/1366/height/671/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/true/asb/false/sri/true/rc/true/ld/false/sdz/true/ctl/false" width="1366px" height="671px" style="border:0px;"> </iframe>


## V.3 Exemple d'utilisation d'une courbe

!!! example "Exemple V.1"
     Revenons sur l'équation $\sin x = \dfrac{1}{\strut2}$.

     En observant la courbe de la fonction sinus, nous constatons qu'il y a une infinité de solutions, les deux premières solutions positives sont $\dfrac{\strut\pi}{\strut6}$ et $\pi-\dfrac{\pi}{6}=\dfrac{5\pi}{6}$.
     
     ![](images/trigo10.png){width=60% ; .center}
     
     Les autres solutions se déduisent de celles-ci en ajoutant des périodes :
     $\dfrac{\strut\pi}{\strut6}+2k\pi$ et $\dfrac{5\pi}{6}+2k\pi$, $k\in ℤ$.

!!! question "Exercices"
     [Transmath](https://biblio.manuel-numerique.com/) :  
     16 page 177, 18 page 178,  
     72 page 181, 76 page 182  
     21, 23 page 48 ; 95 page 51  

!!! question "Exercices de logique"
     [Transmath](https://biblio.manuel-numerique.com/) : 100, 101, 102, 108 page 184


!!! question "Exercices bilans"
     Exercices 10, 11, 12 de la fiche  
     [Transmath](https://biblio.manuel-numerique.com/) : 114 page 188
