# Copier-coller !

ℕ
ℤ
ℝ

«  »


<span style="color:red"></span>



formules centrées :

	<div style="color:red">

	\[
	\]

	</div>

![](images/secante.png){width=50% ; : .center}

!!! question "Exercices"
    Exercices 5 et 6 [de la fiche d'exercices](../exo_derivation2.pdf)  
    [Transmath](https://biblio.manuel-numerique.com/) :  
    75 page 128  
    96 page 134



!!! note "burpo"
    coucou
    test !

!!! abstract
   test !

!!! info
   test !

!!! tip "Remarque"
   test !

!!! success
   test !

!!! question
   test !

!!! warning
   test !

!!! failure
   test !

!!! danger
   test !

!!! bug
   test !

!!! example
   test !

!!! quote
   test !


!!! faq "Question 1"
	=== "Énoncé"

		Cette fonction doit multiplier deux nombres.
		
		```python
		def fonc(a, b):
			a = a * b
			return b
		```

		{{ qcm(["OK", "Syntaxe", "Inutile", "Inadéquat"], [4], shuffle = False) }}

	=== "Indication"
		Que renvoie la fonction ?

	=== "Correction"
		L'instruction `#!python a = a * b` calcule bien le produit de a et b mais c'est b qui est renvoyé : la fonction ne fait pas ce qu'on attend.




{{ terminal()}}


```python
import keyword
print(keyword.kwlist)
```

Vous devriez reconnaître quelques mots déjà utilisés en seconde : `#!python print`, `#!python if`, `#!python for` `#!python while`, `#!python def` entre autres.




!!! summary "Les quatre types de choix"

	* OK : pas d'erreur !
	* Syntaxe : lors de son exécution du script, une ou des erreur(s) apparaîtra(ont), par exemple car le code ne respecte pas la grammaire de Python ou une variable n'a pas été définie ;
	* Inutile : pas d'erreur à l'éxecution mais **une partie du code** est inutile.
	* Inadéquat : pas d'erreur à l'éxecution mais le programme ne fait pas ce qu'on attendait de lui.


!!! faq "Question 2"

	===! "Énoncé"

		```python
		texte = 'Bonjour !'
		print a	
		```

		{{ qcm(["OK", "Syntaxe", "Inutile", "Inadéquat"], [2], shuffle = False) }}

	=== "Indication"

		`#!python print` est une fonction et l'appel d'une fonction comporte toujours...

	=== "Correction"

		```python hl_lines="2"
		texte = 'Bonjour !'
		print(a)
		```

!!! faq "Question 3"

	===! "Énoncé"

		```python
		def f(a, b):
			c = a + b
			c = a * b
			return c
		```

		{{ qcm(["OK", "Syntaxe", "Inutile", "Inadéquat"], [3], shuffle = False) }}


	=== "Indication"

		On peut raccourcir la fonction.

	=== "Correction"

		La valeur de `c` calculée par `c = a + b` est écrasée par celle calculée par `c = a * b`, sans que la première valeur de `c` ait été utilisée. 
		
		La ligne `c = a + b` est donc inutile.


!!! faq "Question 4"

	===! "Énoncé"

		```python
		for c in range("Bonjour"):
			print(c)
		```

		{{ qcm(["OK", "Syntaxe", "Inutile", "Inadéquat"], [2], shuffle = False) }}

	=== "Indication"

		Les paramètres de `range` doivent être...

	=== "Correction"

		Les paramètres de `range` doivent être des entiers.

		```python hl_lines="2"
		for c in range("Bonjour"):
			print(c)
		```

